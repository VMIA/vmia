<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-UserProfile</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-File</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Application__c</tab>
    <tab>Property__c</tab>
    <tab>Certificate__c</tab>
    <tab>Property_Owners__c</tab>
    <tab>Eligibility_History__c</tab>
    <tab>Invoice__c</tab>
    <tab>PremiumRatesTable__c</tab>
    <tab>BAT_Letter__c</tab>
</CustomApplication>
