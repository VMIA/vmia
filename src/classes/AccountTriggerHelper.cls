/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Helper Class for Certificate trigger
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public with sharing class AccountTriggerHelper{
    
    // variables
    private Boolean beforeEventFirstRun = true;
    private Boolean afterEventFirstRun = true;
    
    private Map<String,Id> accRecTypeMap ;
    
    // constructor : initialize necessary variables
    public AccountTriggerHelper(){
        accRecTypeMap = GlobalUtility.getsObjectRecordTypes(String.valueOf(Account.sObjectType));
    }
    
    public Boolean isBeforeEventFirstRun(){
        Boolean retVal = beforeEventFirstRun;
        beforeEventFirstRun = false;
        return retVal;
    }
    
    public Boolean isAfterEventFirstRun(){
        Boolean retVal = afterEventFirstRun;
        afterEventFirstRun = false;
        return retVal;
    }

    /**
      * @description       This method is invoked from handler for before update event to assign owner to user modifying the account
      *                     to overcome the limitation of "You cannot assign a Portal Account or Contact to a Portal user."                     
      * @param             NA 
      * @return            Void
      * @throws            NA
      */
    public void setOwnerToInternalUser(){
        Set<String> communityUserTypes = new Set<String>{GlobalConstants.USER_TYPE_CUSTOMER ,
                                                            GlobalConstants.USER_TYPE_POWER_CUSTOMER,
                                                            GlobalConstants.USER_TYPE_HIGH_VOLUME};
        System.debug('** communityUserTypes  ==>'+ communityUserTypes );
        System.debug('** User Type ==>'+ UserInfo.getUserType());
        if(!communityUserTypes.contains(UserInfo.getUserType())){
            for(Account acc : (List<Account>)Trigger.new){
                acc.OwnerId = UserInfo.getUserId();
            }
        }   
    }
    

    /**
      * @description       This method is invoked from handler for after insert event to share accounts to distributors                      
      * @param             accList 
      * @return            Void
      * @throws            Exception is handled in shareBuildersWithDistributor method 
      */
    public void shareAccountsToDistributor(List<Account> accList,Map<Id,Account> oldMap){
        if(oldMap == null){
            if(!GlobalUtility.isBuilder){
                DistributorAccessController.shareBuildersWithDistributor(accList);
            }
        }
        else{
            Set<Account> accsToShare = new Set<Account>();
            List<Account> accsToRemoveSharing = new List<Account>();
            for(Account acc : accList){
                Account oldAcc = oldMap.get(acc.Id) ;// get old account
                if(oldAcc != null){
                    if(acc.OwnerId != oldAcc.OwnerId){
                        accsToShare.add(acc);
                    }
                    if(acc.Agent__c != oldAcc.Agent__c){
                        accsToShare.add(acc);
                        accsToRemoveSharing.add(oldAcc);
                    }
                }
            }
            if(!accsToRemoveSharing.isEmpty()){
                DistributorAccessController.removeSharingBuildersWithDistributor(accsToRemoveSharing);
            }

            if(!accsToShare.isEmpty()){
                DistributorAccessController.shareBuildersWithDistributor(new List<Account>(accsToShare));
            }
        }
    }

    public void createHistoryEligibility(){
        List<Eligibility_History__c> newHistoryList = new list<Eligibility_History__c>();
        try{
            for (Account acc: (List<account>) Trigger.new) {
                Account oldAccount = ((Account)Trigger.oldMap.get(acc.id));
                if(((acc.Construction_Limit__c != oldAccount.Construction_Limit__c) ||  
                    (acc.SD_Flat_Rate_Premium__c != oldAccount.SD_Flat_Rate_Premium__c) || 
                    (acc.SP_Flat_Rate_Premium__c != oldAccount.SP_Flat_Rate_Premium__c)|| 
                    (acc.C03_Multi_Dwelling_Flat_Rate_Premium__c != oldAccount.C03_Multi_Dwelling_Flat_Rate_Premium__c) ||
                    (acc.C04_Structural_Flat_Rate_Premium__c != oldAccount.C04_Structural_Flat_Rate_Premium__c ) ||
                    (acc.Non_Structural_Flat_Rate__c != oldAccount.Non_Structural_Flat_Rate__c) ||
                    (acc.C07_Other_Flat_Rate_Premium__c != oldAccount.C07_Other_Flat_Rate_Premium__c) ||
                    (acc.CO1_New_Single_Dwelling__c != oldAccount.CO1_New_Single_Dwelling__c) || 
                    (acc.CO3_New_Multi_Dwelling__c != oldAccount.CO3_New_Multi_Dwelling__c)|| 
                    (acc.C04_Alterations_Additions__c != oldAccount.C04_Alterations_Additions__c) || 
                    (acc.C05_Swimming_Pools__c != oldAccount.C05_Swimming_Pools__c)|| 
                    (acc.C06_Renovations_Tradespeople__c != oldAccount.C06_Renovations_Tradespeople__c) || 
                    (acc.C07_Other__c != oldAccount.C07_Other__c)|| 
                    //(acc.C08_All_Cover__c != oldAccount.C08_All_Cover__c) ||  
                    (acc.Construction_Category_Limit_1__c != oldAccount.Construction_Category_Limit_1__c) ||
                    (acc.Eligibility_Status__c !=  oldAccount.Eligibility_Status__c) ||
                    (acc.Construction_Category_Limit_3__c != oldAccount.Construction_Category_Limit_3__c)|| 
                    (acc.Construction_Category_Limit_4__c != oldAccount.Construction_Category_Limit_4__c) || 
                    (acc.Construction_Category_Limit_5__c != oldAccount.Construction_Category_Limit_5__c)|| 
                    (acc.Construction_Category_Limit_6__c != oldAccount.Construction_Category_Limit_6__c) ||
                    (acc.Construction_Category_Limit_7__c != oldAccount.Construction_Category_Limit_7__c) ||
                    (acc.Eligibility_Conditions__c != oldAccount.Eligibility_Conditions__c) ||
                    //(acc.Other_Terms__c != oldAccount.Other_Terms__c) ||
                    (acc.Last_Eligibility_Reviewed_Date__c != oldAccount.Last_Eligibility_Reviewed_Date__c) ||
                    (acc.Eligibility_Review_Date__c != oldAccount.Eligibility_Review_Date__c) ||
                    (acc.Eligibility_Review_Frequency__c != oldAccount.Eligibility_Review_Frequency__c) ||
                    (acc.Name != oldAccount.Name) ||
                    (acc.FirstName != oldAccount.FirstName) ||
                    (acc.LastName != oldAccount.LastName)) 
                    && oldAccount.Eligibility_Status__c != globalConstants.ACCOUNT_STATUS_UNDERAPP
                    )
                    {
                    
                    Eligibility_History__c newHistory = new Eligibility_History__c();

                    newHistory.Builder__c = oldAccount.id; 
                    newHistory.Construction_Limit__c = oldAccount.Construction_Limit__c;
                    newHistory.Eligibility_Commencement_Date__c = oldAccount.Current_Eligibility_Approval_Date__c;
                    newHistory.Eligibility_Expiry_Date__c = system.today();
                    newHistory.Single_Dwelling_Category_Limit__c = oldAccount.Construction_Category_Limit_1__c;
                    newHistory.Multi_Dwelling_Category_Limit__c = oldAccount.Construction_Category_Limit_3__c;
                    newHistory.Structural_Alterations_Category_Limit__c = oldAccount.Construction_Category_Limit_4__c;
                    newHistory.Swimming_Pool_Category_Limit__c = oldAccount.Construction_Category_Limit_5__c;
                    newHistory.Non_Structural_Renovations__c = oldAccount.Construction_Category_Limit_6__c;
                    newHistory.Other_Limits__c = oldAccount.Construction_Category_Limit_7__c;
                    newHistory.other_terms__c = oldAccount.other_terms__c;
                    newHistory.Builder_Risk_Score__c = oldAccount.Builder_Risk_Rating__c;
                    newHistory.Eligibility_Conditions__c = oldAccount.Eligibility_Conditions__c;
                    newHistory.Eligibility_Review_Frequency__c = oldAccount.Eligibility_Review_Frequency__c;
                    newHistory.Eligibility_Status__c = oldAccount.Eligibility_Status__c;
                    newHistory.Flat_Rate_Premium_C01__c = oldAccount.SD_Flat_Rate_Premium__c;
                    newHistory.Flat_Rate_Premium_C03__c = oldAccount.C03_Multi_Dwelling_Flat_Rate_Premium__c;
                    newHistory.Flat_Rate_Premium_C04__c = oldAccount.C04_Structural_Flat_Rate_Premium__c;
                    newHistory.Flat_Rate_Premium_C05__c = oldAccount.SP_Flat_Rate_Premium__c;
                    newHistory.Flat_Rate_Premium_C06__c = oldAccount.Non_Structural_Flat_Rate__c;
                    newHistory.Flat_Rate_Premium_C07__c = oldAccount.C07_Other_Flat_Rate_Premium__c;
                    newHistory.Other_Terms__c = oldAccount.Other_Terms__c;
                    newHistory.Percentage_of_GST__c = oldAccount.Percentage_of_GST__c;
                    newHistory.Registered_for_GST__c = oldAccount.Registered_for_GST__c;
                    newHistory.Security_Held__c = oldAccount.Security_Held__c;
                    newHistory.Security_Type__c = oldAccount.Security_Type__c;
                    if(!String.isBlank(oldAccount.Name))
                        newHistory.Legal_Entity_Name__c = oldAccount.Name;
                    else{
                        if(String.isNotBlank(oldAccount.FirstName)){
                           newHistory.Legal_Entity_Name__c = oldAccount.FirstName + ' ' + oldAccount.LastName; 
                        }
                        else{
                            newHistory.Legal_Entity_Name__c = oldAccount.LastName;
                        }
                        
                    }

                    newHistoryList.add(newHistory);

                    
                    acc.Current_Eligibility_Approval_Date__c = system.today();

                }
            }

            if(!newHistoryList.isEmpty()){
                Database.insert(newHistoryList);
            }
        }
        catch(Exception exp){
            // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(CertificateTriggerHelper.class),
                                GlobalConstants.FN_CREATE_ELIGIBILITY_HISTORY,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }

    public void disableBuilderUser(List<Account> accList, Map<Id,Account> oldMap){
        List<Id> cancelledAccounts = new List<Id>();
        Map<Id, Id> contactAccountIds = new Map<Id,Id>();
        Set<Id> contactWithNoActiveAccounts = new Set<Id>();
        
        Set<String> incorrectStatuses = new Set<String>{GlobalConstants.STAT_CANCELLED, GlobalConstants.STAT_DECLINED, GlobalConstants.STAT_WITHDRAWN, GlobalConstants.ACCOUNT_STATUS_UNDERAPP};
        for (Account acc : accList){
        Account oldAcc = oldMap.get(acc.Id);
            if (oldAcc.Eligibility_Status__c != acc.Eligibility_Status__c && acc.Eligibility_Status__c == GlobalConstants.STAT_CANCELLED){
                cancelledAccounts.add(acc.Id);
            }
        }

        for(Contact_Role__c crole : [SELECT Contact__c,Account__c FROM Contact_Role__c WHERE Account__c IN: cancelledAccounts]){
            contactAccountIds.put(crole.Contact__c, crole.Account__c);
            contactWithNoActiveAccounts.add(crole.Contact__c);
        }

        for(Contact_Role__c crole : [SELECT Contact__c, Account__c FROM Contact_Role__c WHERE Account__r.Eligibility_Status__c NOT IN: incorrectStatuses AND Contact__c IN: contactAccountIds.keyset()]){

            if(crole.Account__c != contactAccountIds.get(crole.Contact__C)){
                contactWithNoActiveAccounts.remove(crole.Contact__c);
            }
        }

        disableUser(contactWithNoActiveAccounts);

    }

    @future
    public static void disableUser(Set<Id> usrList){
        List<User> usersToDisable = new List<User>();
        for(User u : [SELECT IsActive FROM User WHERE ContactId IN: usrList AND IsActive = TRUE]){
            u.IsActive = False;
            usersToDisable.add(u);
        }
        try{
            update(usersToDisable);
        }
        catch(Exception exp){
            // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(CertificateTriggerHelper.class),
                                GlobalConstants.FN_DISABLE_BUILDER_USER,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        } 
    }


    public void setEligibilityReviewDate(List<Account> accList, Map<Id,Account> oldMap){
        for (Account acc : accList){
            Account oldAcc = oldMap.get(acc.Id);
            if (oldAcc.Builder_Eligibility_Status_Reason__c == GlobalConstants.ACCOUNT_BUILDER_ELIGIBILITY_STATUS_UNDERREV && String.isBlank(acc.Builder_Eligibility_Status_Reason__c)){
                if(acc.Eligibility_Review_Frequency__c == globalConstants.ACCOUNT_ELIGIBILITY_REVIEW_FEQ_6){
                    acc.Eligibility_Review_Date__c = system.today() + 182;
                }else if(acc.Eligibility_Review_Frequency__c == globalConstants.ACCOUNT_ELIGIBILITY_REVIEW_FEQ_12){
                    acc.Eligibility_Review_Date__c = system.today() + 365;
                }else{
                    acc.Eligibility_Review_Date__c = system.today() + 730;
                }
                acc.Last_Eligibility_Reviewed_Date__c = system.today();
            }
        }
    }

    public void categoryLimitEligibility(){
        for (Account acc: (List<account>) Trigger.new) {
            acc.Flat_Rate_Premium__c = False;
            if((acc.Construction_Category_Limit_1__c == null || acc.Construction_Category_Limit_1__c == 0) &&
                (acc.SD_Flat_Rate_Premium__c == null || acc.SD_Flat_Rate_Premium__c == 0)){
                    acc.CO1_New_Single_Dwelling__c = False;
            }else{
                acc.CO1_New_Single_Dwelling__c = True;
                if(acc.SD_Flat_Rate_Premium__c != null && acc.SD_Flat_Rate_Premium__c != 0){
                    acc.Flat_Rate_Premium__c = True;
                }
            }
            if(( acc.Construction_Category_Limit_3__c == null || acc.Construction_Category_Limit_3__c == 0) &&
                (acc.C03_Multi_Dwelling_Flat_Rate_Premium__c == null || acc.C03_Multi_Dwelling_Flat_Rate_Premium__c == 0)){
                    acc.CO3_New_Multi_Dwelling__c = False;
            }else{
                acc.CO3_New_Multi_Dwelling__c = True;
                if(acc.C03_Multi_Dwelling_Flat_Rate_Premium__c != null && acc.C03_Multi_Dwelling_Flat_Rate_Premium__c != 0){
                    acc.Flat_Rate_Premium__c = True;
                }
            }
            if((acc.Construction_Category_Limit_4__c == null || acc.Construction_Category_Limit_4__c == 0) &&
                (acc.C04_Structural_Flat_Rate_Premium__c == null || acc.C04_Structural_Flat_Rate_Premium__c == 0)){
                    acc.C04_Alterations_Additions__c = False;
            }else{
                acc.C04_Alterations_Additions__c = True;
                if(acc.C04_Structural_Flat_Rate_Premium__c != null && acc.C04_Structural_Flat_Rate_Premium__c != 0){
                    acc.Flat_Rate_Premium__c = True;
                }
            }
            if((acc.Construction_Category_Limit_5__c == null || acc.Construction_Category_Limit_5__c == 0) &&
                (acc.SP_Flat_Rate_Premium__c == null || acc.SP_Flat_Rate_Premium__c == 0)){
                    acc.C05_Swimming_Pools__c = False;
            }else{
                acc.C05_Swimming_Pools__c = True;
                if(acc.SP_Flat_Rate_Premium__c != null && acc.SP_Flat_Rate_Premium__c != 0){
                    acc.Flat_Rate_Premium__c = True;
                }
            }
            if((acc.Construction_Category_Limit_6__c == null || acc.Construction_Category_Limit_6__c == 0) &&
                (acc.Non_Structural_Flat_Rate__c == null || acc.Non_Structural_Flat_Rate__c == 0)){
                    acc.C06_Renovations_Tradespeople__c = False;
            }else{
                acc.C06_Renovations_Tradespeople__c = True;
                if(acc.Non_Structural_Flat_Rate__c != null && acc.Non_Structural_Flat_Rate__c != 0){
                    acc.Flat_Rate_Premium__c = True;
                }
            }
            if((acc.Construction_Category_Limit_7__c == null || acc.Construction_Category_Limit_7__c == 0) &&
                (acc.C07_Other_Flat_Rate_Premium__c == null || acc.C07_Other_Flat_Rate_Premium__c == 0)){
                    acc.C07_Other__c = False;
            }else{
                acc.C07_Other__c = True;
                if(acc.C07_Other_Flat_Rate_Premium__c != null && acc.C07_Other_Flat_Rate_Premium__c != 0){
                    acc.Flat_Rate_Premium__c = True;
                }
            }
        }
    }  
    
}