/**
  * Date         :  16-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for Account trigger
  */ 
  
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
private with sharing class AccountTrigger_Test{
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> certRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> invRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup(){

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        List<Account> accList = new List<Account>();
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());

        Account lexDistributor = TestUtility.createBusinessAccount('Lex Distributors',accRecTypeMap.get('Distributor').getRecordTypeId());

        accList.add(rexDistributor);
        accList.add(lexDistributor);
        Database.insert(accList);

        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,
                                                        conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
    }

    @future
    private static void setupUser(){
        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        System.debug('** rexUser ==>'+rexUser);
        System.debug('** vmiaInternalUser ==>'+vmiaInternalUser);
        Database.insert(new List<User>{vmiaInternalUser,rexUser});
    }
    
    /**
      * @description       This method tests scenario of policy number generation as a distributor
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testBuilderShareToDistirbutorInsert(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder;
        System.runAs(rexUser){
            bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
            bornBuilder.Agent__c = rexSmith.AccountId;
            bornBuilder.Agent_Contact__c = rexSmith.Id;
            
            Test.startTest();
                Database.insert(bornBuilder);
            Test.stopTest();
        }
        Integer accShareCount = [SELECT count() FROM AccountShare WHERE RowCause = 'MANUAL' AND AccountId =: bornBuilder.Id];
        System.assertEquals(1, accShareCount);
    }

    /**
      * @description       This method tests scenario of account update by internal user
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testAccountUpdateInternalUser(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            bornBuilder.Industry = 'Construction';
            Test.startTest();
                Database.update(bornBuilder);
            Test.stopTest();
        }
        Integer accShareCount = [SELECT count() FROM AccountShare WHERE RowCause = 'MANUAL' AND AccountId =: bornBuilder.Id];
        Account accAfterUpdate = [SELECT OwnerId,Name FROM Account WHERE Id =: bornBuilder.Id LIMIT 1];
        System.assertEquals(1, accShareCount);
        System.assertEquals(runningUser.Id, accAfterUpdate.OwnerId);
    }

    /**
      * @description       This method tests scenario of account update by internal user
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testDistributorChange(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];

        Account lexDistributor = [SELECT Id FROM Account WHERE Name = 'Lex Distributors' LIMIT 1];

        System.runAs(runningUser){
            bornBuilder.Agent__c = lexDistributor.Id;
            Test.startTest();
                Database.update(bornBuilder);
            Test.stopTest();
        }
        Integer accShareCount = [SELECT count() FROM AccountShare WHERE RowCause = 'MANUAL' AND AccountId =: bornBuilder.Id];
        Account accAfterUpdate = [SELECT OwnerId,Name FROM Account WHERE Id =: bornBuilder.Id LIMIT 1];
        //System.assertEquals(1, accShareCount);
        System.assertEquals(runningUser.Id, accAfterUpdate.OwnerId);
    }

    /**
      * @description       This method tests scenario of account update to create eligibility history
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testCreateEligibilityHistory(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];

        System.runAs(runningUser){
            bornBuilder.Construction_Limit__c = 500000;
            bornBuilder.Current_Eligibility_Approval_Date__c = System.today();
            bornBuilder.Construction_Category_Limit_1__c = 50000;
            bornBuilder.Construction_Category_Limit_3__c = 50000;
            bornBuilder.Construction_Category_Limit_4__c = 50000;
            bornBuilder.Construction_Category_Limit_5__c = 50000;
            bornBuilder.Construction_Category_Limit_6__c = 50000;
            bornBuilder.Construction_Category_Limit_7__c = 50000;
            bornBuilder.Builder_Risk_Rating__c = 'A';
            bornBuilder.Eligibility_Conditions__c = 'Other';
            bornBuilder.Eligibility_Review_Frequency__c = '6 Months';
            bornBuilder.Eligibility_Status__c = 'Active';
            bornBuilder.SD_Flat_Rate_Premium__c = 113;
            bornBuilder.C03_Multi_Dwelling_Flat_Rate_Premium__c = 113;
            bornBuilder.C04_Structural_Flat_Rate_Premium__c = 113;
            bornBuilder.SP_Flat_Rate_Premium__c = 113;
            bornBuilder.Non_Structural_Flat_Rate__c = 113;
            bornBuilder.C07_Other_Flat_Rate_Premium__c = 113;
            bornBuilder.Other_Terms__c  = 'Other';
            bornBuilder.Percentage_of_GST__c = 11.3;
            bornBuilder.Registered_for_GST__c = true;
            bornBuilder.Security_Held__c = true;
            //bornBuilder.Security_Type__c = 'S04 None';

            Test.startTest();
                Database.update(bornBuilder);
            Test.stopTest();
        }
        Integer historyCount = [SELECT count() FROM Eligibility_History__c WHERE Builder__c =: bornBuilder.Id];
        System.assertEquals(1, historyCount);
    }

    /**
      * @description       This method tests scenario of account update by internal user
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testEligibilityReviewDate_12Months(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        bornBuilder.Eligibility_Status__c = 'Active';
        bornBuilder.Builder_Eligibility_Status_Reason__c = GlobalConstants.ACCOUNT_BUILDER_ELIGIBILITY_STATUS_UNDERREV;
        bornBuilder.Eligibility_Review_Frequency__c = '12 Months';
        Database.insert(bornBuilder);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];

        System.runAs(runningUser){
            bornBuilder.Builder_Eligibility_Status_Reason__c = null;
            Test.startTest();
                Database.update(bornBuilder);
            Test.stopTest();
        }
        Account accAfterUpdate = [SELECT Eligibility_Review_Date__c,Last_Eligibility_Reviewed_Date__c FROM Account WHERE Id =: bornBuilder.Id LIMIT 1];
        System.assertEquals(System.today(), accAfterUpdate.Last_Eligibility_Reviewed_Date__c);
        System.assertEquals(System.today().addDays(365), accAfterUpdate.Eligibility_Review_Date__c);
    }

    /**
      * @description       This method tests scenario of account update by internal user
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testEligibilityReviewDate_6Months(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        bornBuilder.Eligibility_Status__c = 'Active';
        bornBuilder.Builder_Eligibility_Status_Reason__c = GlobalConstants.ACCOUNT_BUILDER_ELIGIBILITY_STATUS_UNDERREV;
        bornBuilder.Eligibility_Review_Frequency__c = '6 Months';
        Database.insert(bornBuilder);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];

        System.runAs(runningUser){
            bornBuilder.Builder_Eligibility_Status_Reason__c = null;
            Test.startTest();
                Database.update(bornBuilder);
            Test.stopTest();
        }
        Account accAfterUpdate = [SELECT Eligibility_Review_Date__c,Last_Eligibility_Reviewed_Date__c FROM Account WHERE Id =: bornBuilder.Id LIMIT 1];
        System.assertEquals(System.today(), accAfterUpdate.Last_Eligibility_Reviewed_Date__c);
        System.assertEquals(System.today().addDays(182), accAfterUpdate.Eligibility_Review_Date__c);
    }

    /**
      * @description       This method tests scenario of account update by internal user
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testEligibilityReviewDate_24Months(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        bornBuilder.Eligibility_Status__c = 'Active';
        bornBuilder.Builder_Eligibility_Status_Reason__c = GlobalConstants.ACCOUNT_BUILDER_ELIGIBILITY_STATUS_UNDERREV;
        bornBuilder.Eligibility_Review_Frequency__c = '24 Months';
        Database.insert(bornBuilder);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];

        System.runAs(runningUser){
            bornBuilder.Builder_Eligibility_Status_Reason__c = null;
            Test.startTest();
                Database.update(bornBuilder);
            Test.stopTest();
        }
        Account accAfterUpdate = [SELECT Eligibility_Review_Date__c,Last_Eligibility_Reviewed_Date__c FROM Account WHERE Id =: bornBuilder.Id LIMIT 1];
        System.assertEquals(System.today(), accAfterUpdate.Last_Eligibility_Reviewed_Date__c);
        System.assertEquals(System.today().addDays(730), accAfterUpdate.Eligibility_Review_Date__c);
    }

    /**
      * @description       Test method to validate deletion and undelete of Account
      *                     Scenario is not part of requirement, created for code coverage purpose only
      * @param             NA
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testAccountDeleteUndelete(){
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
            bornBuilder.Agent__c = rexSmith.AccountId;
            bornBuilder.Agent_Contact__c = rexSmith.Id;
            Database.insert(bornBuilder);

            System.debug('** bornBuilder ==>'+ bornBuilder);
            Database.delete(bornBuilder);
            Test.startTest();
                Database.undelete(bornBuilder);
            Test.stopTest();
        }
    }

    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testHistoryExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            AccountTriggerHelper helper = new AccountTriggerHelper();
            Test.startTest();
                helper.createHistoryEligibility();
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }
}