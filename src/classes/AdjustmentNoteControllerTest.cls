/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AdjustmentNoteControllerTest {
    
    //variables
    private static Map<String, Id> profileMap = new Map<String, Id>();
    private static Map<String, Schema.RecordTypeInfo> accRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    private static Map<String, Schema.RecordTypeInfo> conRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    private static Map<String, Schema.RecordTypeInfo> certRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    private static Map<String, Schema.RecordTypeInfo> appRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> invRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static {
        for (Profile prof : [SELECT Name FROM Profile]) {
            profileMap.put(prof.Name, prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup() {
        for (Profile prof : [SELECT Name FROM Profile]) {
            profileMap.put(prof.Name, prof.Id);
        }

        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal', profileMap.get('VMIA BAT Internal'), batMember.Id);
        Database.insert(vmiaInternalUser);

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML
        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML
    }
    
    @future
    private static void setupData() {
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor', accRecTypeMap.get('Distributor').getRecordTypeId());
        rexDistributor.BillingStreet = '140 William St';
        rexDistributor.BillingCity = 'Melbourne';
        rexDistributor.BillingState = 'VIC';
        rexDistributor.BillingPostalCode = '3000';
        rexDistributor.BillingCountry = 'Australia';
        rexDistributor.Phone = '0399999999';
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);

        Contact rexSmith = TestUtility.createContact('Rex', 'Smith', 'rex.smith@rexHomeDistributorTest.com', rexDistributor.Id,
                           conRecTypeMap.get('Distributor').getRecordTypeId());  
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders', accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Name = 'Henry Niu';
        bornBuilder.BillingStreet = '140 William St';
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        
        Application__c app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility',
                             appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
        Database.insert(app);
        
        Property__c property = TestUtility.createProperty('140 William St', 'Melbourne', '3000', 'VIC');
        Database.insert(property);   
        
        Invoice__c inv = TestUtility.createInvoice(bornBuilder.Id, 'New', invRecTypeMap.get('New Invoice').getRecordTypeId());        
        Database.insert(inv);
        
        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id, bornBuilder.Agent__c,
                              certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        cert.InvoiceNo__c = inv.Id;
        cert.Certificate_Type__c = 'Certificate';
        cert.Contract_Price__c = 2000000;
        Database.insert(cert);
        
        Property_Owners__c propOwner = TestUtility.createPropertyOwner(rexSmith.Id, cert.Id, property.Id);
        Database.insert(propOwner); 
    }

    @future
    private static void setupUser() {
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith', profileMap.get('Distributor'), rexSmith.Id);
        Database.insert(rexUser);
    }

    @isTest
    static void testConstructor() {   
        Invoice__c invoice = [SELECT Id FROM Invoice__c LIMIT 1];
            
        PageReference pageRef = Page.GenerateInvoice;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id',invoice.Id);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(new Invoice__c());
        AdjustmentNoteController controller = new AdjustmentNoteController(stdController);
        
        System.assertEquals('', controller.ownerText);
        System.assertEquals('Henry Niu', controller.owners);
        System.assertEquals(2000000, controller.contractPriceTotal);
        System.assertEquals(false, controller.displayACN);
        System.assertEquals(false, controller.displayABN);
        System.assertNotEquals(null, controller.inv);
        System.assertEquals(null, controller.invDate);
        System.assertEquals('Rex Home Distributor', controller.distributorName);
        System.assertEquals('140 William St', controller.distributorStreet);
        System.assertEquals('Melbourne', controller.distributorCity);
        System.assertEquals('VIC', controller.distributorState);
        System.assertEquals('3000', controller.distributorPostalcode); 
        System.assertEquals('Australia', controller.distributorCountry);
        System.assertEquals('0399999999', controller.distributorPhone);                
    }
    
    @isTest
    static void testForwardToCustomAuthPage() {
        Invoice__c invoice = [SELECT Id FROM Invoice__c LIMIT 1];
            
        PageReference pageRef = Page.GenerateInvoice;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id',invoice.Id);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(new Invoice__c());
        AdjustmentNoteController controller = new AdjustmentNoteController(stdController);
        
        System.assertEquals(null, controller.forwardToCustomAuthPage());
    }
    
    @isTest
    static void testGetCertificates() {     
        Certificate__c cert = [SELECT Id, InvoiceNo__c, Builder__c FROM Certificate__c LIMIT 1];
        cert.Builder__c = null;
        update cert;
            
        PageReference pageRef = Page.GenerateInvoice; 
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', cert.InvoiceNo__c);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(new Invoice__c());
        AdjustmentNoteController controller = new AdjustmentNoteController(stdController);
        
        List<AdjustmentNoteController.cCertificate> certs = controller.getCertificates();
        
        System.assertEquals(1, controller.getCertificates().size());  
        System.assertEquals('Owner ', controller.ownerText);
        System.assertEquals(null, controller.builderACN);
        System.assertEquals(null, controller.builderABN);
        System.assertEquals('Rex Home Distributor', controller.owners);
        //System.assertEquals(null, controller.city); 
        //System.assertEquals(null, controller.state);
        //System.assertEquals(null, controller.postalCode);
        //System.assertEquals(null, controller.country);   
    }
    
    @isTest
    static void testGetCertificatesForMailingAddressProvided() {        
        Certificate__c cert = [SELECT Id, InvoiceNo__c, Builder__c FROM Certificate__c LIMIT 1];
        cert.Builder__c = null;
        update cert;
        
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        contact.Mailingstreet = '141 William St';
        contact.Mailingcity = 'Melbourne';
        contact.Mailingstate = 'VIC';
        contact.Mailingpostalcode = '3333';
        contact.Mailingcountry = 'Australia';
        update contact;
            
        PageReference pageRef = Page.GenerateInvoice; 
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', cert.InvoiceNo__c);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(new Invoice__c());
        AdjustmentNoteController controller = new AdjustmentNoteController(stdController);
        
        List<AdjustmentNoteController.cCertificate> certs = controller.getCertificates();
        
        System.assertEquals(1, controller.getCertificates().size());  
        System.assertEquals('Owner ', controller.ownerText);
        System.assertEquals(null, controller.builderACN);
        System.assertEquals(null, controller.builderABN);
        System.assertEquals('Rex Home Distributor', controller.owners);
        System.assertEquals('141 William St', controller.street);
        System.assertEquals('Melbourne', controller.city); 
        System.assertEquals('VIC', controller.state);
        System.assertEquals('3333', controller.postalCode);
        System.assertEquals('Australia', controller.country);   
    }
    
    @isTest
    static void testGetCertificatesForShippingAddressProvided() {       
        Certificate__c cert = [SELECT Id, InvoiceNo__c, Builder__c FROM Certificate__c LIMIT 1];
        cert.Builder__c = null;
        update cert;
        
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        contact.Mailingstreet = null;
        contact.Mailingcity = null;
        contact.Mailingstate = null;
        contact.Mailingpostalcode = null;
        contact.Mailingcountry = null;
        update contact;
        
        Account propOwner = [SELECT Id FROM Account WHERE name='Rex Home Distributor' LIMIT 1];   
        propOwner.Shippingstreet = '142 William St';
        propOwner.Shippingcity = 'Melbourne';
        propOwner.Shippingstate = 'VIC';
        propOwner.Shippingpostalcode = '3444';
        propOwner.Shippingcountry = 'Australia';
        update propOwner;
        
        Property_Owners__c propOwners = [SELECT Id, Property_Owner__c FROM Property_Owners__c LIMIT 1];
        propOwners.Property_Owner__c = propOwner.Id;
        update propOwners;
            
        PageReference pageRef = Page.GenerateInvoice; 
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', cert.InvoiceNo__c);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(new Invoice__c());
        AdjustmentNoteController controller = new AdjustmentNoteController(stdController);
        
        List<AdjustmentNoteController.cCertificate> certs = controller.getCertificates();
        
        System.assertEquals(1, controller.getCertificates().size());  
        System.assertEquals('Owner ', controller.ownerText);
        System.assertEquals(null, controller.builderACN);
        System.assertEquals(null, controller.builderABN);
        System.assertEquals('Rex Home DistributorRex Home Distributor', controller.owners);
        System.assertEquals('142 William St', controller.street);
        System.assertEquals('Melbourne', controller.city); 
        System.assertEquals('VIC', controller.state);
        System.assertEquals('3444', controller.postalCode);
        System.assertEquals('Australia', controller.country);   
    }
    
        @isTest
    static void testGetCertificatesForBillingAddressProvided() {        
        Certificate__c cert = [SELECT Id, InvoiceNo__c, Builder__c FROM Certificate__c LIMIT 1];
        cert.Builder__c = null;
        update cert;
        
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        contact.Mailingstreet = null;
        contact.Mailingcity = null;
        contact.Mailingstate = null;
        contact.Mailingpostalcode = null;
        contact.Mailingcountry = null;
        update contact;
        
        Account propOwner = [SELECT Id FROM Account WHERE name='Rex Home Distributor' LIMIT 1];   
        propOwner.Shippingstreet = null;
        propOwner.Shippingcity = null;
        propOwner.Shippingstate = null;
        propOwner.Shippingpostalcode = null;
        propOwner.Shippingcountry = null;
        propOwner.Billingstreet = '143 William St';
        propOwner.Billingcity = 'Melbourne';
        propOwner.Billingstate = 'VIC';
        propOwner.Billingpostalcode = '3555';
        propOwner.Billingcountry = 'Australia';
        update propOwner;
        
        Property_Owners__c propOwners = [SELECT Id, Property_Owner__c FROM Property_Owners__c LIMIT 1];
        propOwners.Property_Owner__c = propOwner.Id;
        update propOwners;
            
        PageReference pageRef = Page.GenerateInvoice; 
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', cert.InvoiceNo__c);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(new Invoice__c());
        AdjustmentNoteController controller = new AdjustmentNoteController(stdController);
        
        List<AdjustmentNoteController.cCertificate> certs = controller.getCertificates();
        
        System.assertEquals(1, controller.getCertificates().size());  
        System.assertEquals('Owner ', controller.ownerText);
        System.assertEquals(null, controller.builderACN);
        System.assertEquals(null, controller.builderABN);
        System.assertEquals('Rex Home DistributorRex Home Distributor', controller.owners);
        System.assertEquals('143 William St', controller.street);
        System.assertEquals('Melbourne', controller.city); 
        System.assertEquals('VIC', controller.state);
        System.assertEquals('3555', controller.postalCode);
        System.assertEquals('Australia', controller.country);   
    }
    
    @isTest
    static void testValidateAndRedirect() {
        Invoice__c invoice = [SELECT Id FROM Invoice__c LIMIT 1];
            
        PageReference pageRef = Page.GenerateInvoice;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id',invoice.Id);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(new Invoice__c());
        AdjustmentNoteController controller = new AdjustmentNoteController(stdController);
        
        PageReference pr = controller.validateAndRedirect();
        
        System.assertEquals('/DistributorLanding', pr.getUrl());
               
    }
    
    @isTest
    static void testCCertificate() {
        Certificate__c certificate = [SELECT Id FROM Certificate__c LIMIT 1];
        
        AdjustmentNoteController.cCertificate wrapper 
            = new AdjustmentNoteController.cCertificate(certificate);
            
        System.assertEquals(certificate, wrapper.con);
        System.assertEquals(false, wrapper.selected);
        System.assertEquals(null, wrapper.numberOfRow);  
    }
    
}