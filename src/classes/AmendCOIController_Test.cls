/**
* Date         :  23-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for AmendCOIController controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class AmendCOIController_Test {
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        UserRole batMember = TestUtility.getUserRole('BAT_Member');
        
        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        Database.insert(vmiaInternalUser);
        setupData();
        setupUser();
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);
        
        Certificate__c  cert = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
        cert.Building_Contract_Date__c = Date.today()+1;
        cert.Building_Permit_Issued_Date__c = Date.today()-2;
        cert.Actual_Start_Date__c= Date.today()-1;
        cert.Actual_Completion_Date__c = Date.today();
        cert.Estimated_Start_Date__c= Date.today()-1;
        cert.Estimated_Completion_Date__c = Date.today();
        cert.status__c = constants.certificate_status_Issued_Works_In_Progress;
        Database.insert(cert);
        System.debug('** Certificate ==>'+ cert.id);
        
        Blob b = Blob.valueOf('Test Data');
        Attachment attachment = new Attachment(ParentId = cert.Id, Name='Test Attachment for certificate', Body=b);
        Database.insert(attachment);        
        
        Property__c prop = new Property__c(Builder__c = bornBuilder.Id, Suburb__c='Dockland');
        Database.insert(prop);
        System.debug('** Property ==>'+ prop.id);
        
        Property_Owners__c propOwn = new Property_Owners__c(Property__c = prop.id, Certificate__c = cert.id, property_Owner__c=rexDistributor.Id);
        Database.insert(propOwn);
        System.debug('** Property Owner ==>'+ propOwn.id);
        
        Application__c  app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(),cert.Id);
        Database.insert(app);
        System.debug('** Application ==>'+ app);
        
             
        
        Account personAc = TestUtility.createPersonAccount('Person',accRecTypeMap.get('Person Account').getRecordTypeId());
        personAc.PersonMailingStreet = '9 Beach Street';
        Database.insert(personAc);
        System.debug('** personAc ==>'+ personAc);     
        
        Account personBuilder = TestUtility.createPersonAccount('Builder',accRecTypeMap.get('Person Account').getRecordTypeId());
        personBuilder.firstName = 'Person';
        Database.insert(personBuilder);
        System.debug('** personBuilder ==>'+ personBuilder);
        
        Certificate__c  certPerson = TestUtility.createCertificate(personBuilder.Id, null, certRecTypeMap.get('Certificate').getRecordTypeId());
        Database.insert(certPerson);
        System.debug('** Certificate ==>'+ certPerson.id); 

               
    } 

    @future
    private static void setupUser(){
        Contact rexSmith = [SELECT AccountId FROM Contact where FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        System.debug('** rexUser ==>'+ rexUser + ' @@acc ' + rexUser.AccountId);  
        Database.insert(rexUser);
    }  
    


    
    public static testMethod void forwardToCustomAuthPageGuestUserTest(){
        List<User> userList = [select Id from User where userType = 'Guest'];
        
        System.runAs(userList[0]) {
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            PageReference pg = controller.forwardToCustomAuthPage();
            System.assertEquals('/CustomLoginPage', pg.getUrl());
            Test.stopTest();
        }
    }
    
    public static testMethod void forwardToCustomAuthPageInternalUserTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account personBuilder = [SELECT Id, FirstName, LastName FROM Account WHERE LastName = 'Builder' LIMIT 1];
        Certificate__c cert = [Select Speculative__c from Certificate__c where Builder__c =: personBuilder.Id limit 1];
        cert.Speculative__c = true;
        Database.update(cert);
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters			
            ApexPages.currentPage().getParameters().put('id', cert.Id);            
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            PageReference pg = controller.forwardToCustomAuthPage();
            System.assertEquals(null, pg);
            Test.stopTest();                
        }     
    }   
    
    public static testMethod void withdrawCertificateTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id from Certificate__c where Builder__c =: bornBuilder.Id limit 1];
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters			
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            PageReference pg = controller.withdrawCertificate();
            System.assertEquals('/WithdrawConfirmation?certId=' + cert.Id, pg.getUrl());
            Test.stopTest();                
        }     
    }   
    
    public static testMethod void generateCertNonOBCertificateTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            System.assertNotEquals('OBCertificate', cert.Certificate_Type__c);
            
            //Set url parameters	
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();
            AmendCOIController controller = new AmendCOIController();
            PageReference pg = controller.generateCert();
            System.assertEquals('/GenerateCertificate?id=' + cert.Id, pg.getUrl());
            Test.stopTest();                
        }     
    } 
    
    public static testMethod void generateCertOBCertificateTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];		
        cert.Certificate_Type__c = 'OBCertificate';
        Database.update(cert);
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            System.assertEquals('OBCertificate', cert.Certificate_Type__c);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();
            AmendCOIController controller = new AmendCOIController();
            PageReference pg = controller.generateCert();
            System.assertEquals('/GenerateOwnerBuilderCert?id=' + cert.Id, pg.getUrl());
            Test.stopTest();                
        }     
    }     
    
    //otherRequests = null; Choice = null
    public static testMethod void saveExceptionCase1Test(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];			
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[0].getSummary().contains('Please provide a reason for the request '), 'Please provide a reason for the request ');
            Test.stopTest();                
        }     
    } 
    
    //otherRequests != null; Choice = null
    public static testMethod void saveExceptionCase2Test(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];		
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            controller.otherRequests = 'Cancel it.';
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[0].getSummary().contains('Please select the reason for the request '), 'Please select the reason for the request ');
            Test.stopTest();                
        }     
    }  
    
    //otherRequests != null; Choice = 'Amend'
    public static testMethod void saveExceptionCase3Test(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];			
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            controller.otherRequests = 'Cancel it.';
            controller.Choice = 'Amend';
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[0].getSummary().contains('Please select a reason to AMEND this certificate'), 'There should be a pageMessage advising to select a reason to AMEND this certificate.');
            Test.stopTest();                
        }     
    } 
    
    //otherRequests != null; Choice = 'Cancel'
    public static testMethod void saveExceptionCase4Test(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];		
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            controller.otherRequests = 'Cancel it.';
            controller.Choice = 'Cancel';
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[0].getSummary().contains('Please select a reason to CANCEL this certificate.'), 'There should be a pageMessage advising to select a reason to CANCEL this certificate.');
            Test.stopTest();                
        }     
    }   
    
    //CancelCOI Flow: contractTerminated
    public static testMethod void saveCancelCOIcontractTerminatedTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];		
        Application__c app = [Select id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        RecordType recTypeCancel = [Select Id,SobjectType,Name From RecordType WHERE Name =:constants.cancelCOI and SobjectType ='Application__c'  limit 1];		
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters			
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            ApexPages.currentPage().getParameters().put('cancel', 'True');
            
            Test.startTest();
            AmendCOIController controller = new AmendCOIController();
            controller.newApplication = app;
            controller.otherRequests = 'Cancel it.';
            controller.Choice = 'Cancel';
            controller.contractTerminated = true;
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assertEquals(recTypeCancel.id, controller.newApplication.recordtypeid);
            System.assertEquals(constants.cancelCOI, controller.newApplication.type__c);
            System.assert(controller.newApplication.Contract_Terminated__c);
            System.assert(!controller.newApplication.Construction_Did_Not_Proceed__c);
            System.assert(!controller.newApplication.Other_Reason__c);
            
            pg = controller.confirmation();
            System.assertEquals('/apex/EligibilityApplicationConfirmation?id=' + app.Id, pg.getUrl());               
            Test.stopTest();                
        }     
    }   
    
    //CancelCOI Flow: Complete : acceptCompletion, with External Comments in Certificate
    public static testMethod void saveCompleteacceptCompletionTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];		
        cert.External_Comments__c = 'Test Cert';
        Database.update(cert);
        Application__c app = [Select id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            controller.newApplication = app;
            controller.otherRequests = 'Complete it.';
            controller.Choice = 'Complete';
            controller.acceptCompletion = true;
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assertEquals(System.today(), controller.currentCert[0].Actual_Completion_Date__c);
            System.assertEquals(True, controller.currentCert[0].Has_work_Completed__c);
            System.assertEquals(constants.certificate_status_Issued_Works_Complete, controller.currentCert[0].Status__c);
            System.assertEquals(cert.Id, controller.attachmentParentID);
            pg = controller.confirmation();
            System.assertEquals('/apex/CertificateSubmit?id=' + cert.Id, pg.getUrl());     
            System.assert(controller.currentCert[0].External_Comments__c.contains('Test Cert'));
            Test.stopTest();                
        }     
    }   
    
    //CancelCOI Flow: Complete : acceptCompletion, No External Comments in Certificate, With Propert Owners Shippingstreet populated
    public static testMethod void saveCompleteacceptCompletionNoExtCommentTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];		
        Application__c app = [Select id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        
        Account proOwner = [Select Shippingstreet from Account where Name='Rex Home Distributor'];
        proOwner.Shippingstreet = '9 Beach Street';
        Database.update(proOwner);
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            controller.newApplication = app;
            controller.otherRequests = 'Complete it.';
            controller.Choice = 'Complete';
            controller.acceptCompletion = true;
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assertEquals(System.today(), controller.currentCert[0].Actual_Completion_Date__c);
            System.assertEquals(True, controller.currentCert[0].Has_work_Completed__c);
            System.assertEquals(constants.certificate_status_Issued_Works_Complete, controller.currentCert[0].Status__c);
            System.assertEquals(cert.Id, controller.attachmentParentID);
            pg = controller.confirmation();
            System.assertEquals('/apex/CertificateSubmit?id=' + cert.Id, pg.getUrl());  
            System.assert(controller.currentCert[0].External_Comments__c!=null && !String.isEmpty(controller.currentCert[0].External_Comments__c));
            Test.stopTest();                
        }     
    }
    
    //CancelCOI Flow: Complete : acceptCompletion, with Person Account as Property Owner (PersonMailingStreet populated)
    public static testMethod void saveCompleteacceptCompletionPersonalOwner(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];		
        Application__c app = [Select id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        
        Database.delete([Select Id from Property_Owners__c]);
        Account personAc = [Select Id from Account where LastName = 'Person' LIMIT 1];
        Property__c prop = [Select Id from Property__c where Builder__c =: bornBuilder.Id];
        Property_Owners__c propOwn = new Property_Owners__c(Property__c = prop.id, Certificate__c = cert.id, property_Owner__c=personAc.Id);
        Database.insert(propOwn);	        
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            controller.newApplication = app;
            controller.otherRequests = 'Complete it.';
            controller.Choice = 'Complete';
            controller.acceptCompletion = true;
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assertEquals(System.today(), controller.currentCert[0].Actual_Completion_Date__c);
            System.assertEquals(True, controller.currentCert[0].Has_work_Completed__c);
            System.assertEquals(constants.certificate_status_Issued_Works_Complete, controller.currentCert[0].Status__c);
            System.assertEquals(cert.Id, controller.attachmentParentID);
            pg = controller.confirmation();
            System.assertEquals('/apex/CertificateSubmit?id=' + cert.Id, pg.getUrl());  
            System.assert(controller.currentCert[0].External_Comments__c!=null && !String.isEmpty(controller.currentCert[0].External_Comments__c));
            Test.stopTest();                
        }     
    } 
    
    //CancelCOI Flow: Complete : acceptCompletion, with no Property Owner & no address defined in the account related to Property Contact
    public static testMethod void saveCompleteacceptCompletionNoOwnerNoAddressTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];		
        Application__c app = [Select id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        
        Database.delete([Select Id from Property_Owners__c]);
        Property__c prop = [Select Id from Property__c where Builder__c =: bornBuilder.Id];
        Property_Owners__c propOwn = new Property_Owners__c(Property__c = prop.id, Certificate__c = cert.id);
        Database.insert(propOwn);	        
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            controller.newApplication = app;
            controller.otherRequests = 'Complete it.';
            controller.Choice = 'Complete';
            controller.acceptCompletion = true;
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assertEquals(System.today(), controller.currentCert[0].Actual_Completion_Date__c);
            System.assertEquals(True, controller.currentCert[0].Has_work_Completed__c);
            System.assertEquals(constants.certificate_status_Issued_Works_Complete, controller.currentCert[0].Status__c);
            System.assertEquals(cert.Id, controller.attachmentParentID);
            pg = controller.confirmation();
            System.assertEquals('/apex/CertificateSubmit?id=' + cert.Id, pg.getUrl());  
            System.assert(controller.currentCert[0].External_Comments__c!=null && !String.isEmpty(controller.currentCert[0].External_Comments__c));
            Test.stopTest();                
        }     
    }     
    
    //CancelCOI Flow: Complete : acceptCompletion, with no Property Owner & address defined in the account related to Property Contact
    public static testMethod void saveCompleteacceptCompletionNoOwnerTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];		
        Application__c app = [Select id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        
        Database.delete([Select Id from Property_Owners__c]);
        Account proOwner = [Select Shippingstreet from Account where Name='Rex Home Distributor'];
        proOwner.Shippingstreet = '9 Beach Street';
        Database.update(proOwner);        
        Property__c prop = [Select Id from Property__c where Builder__c =: bornBuilder.Id];
        Contact contact = [Select Id from Contact where FirstName = 'Rex'];
        Property_Owners__c propOwn = new Property_Owners__c(Property__c = prop.id, Certificate__c = cert.id, contact__c=contact.Id);
        Database.insert(propOwn);	        
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            controller.newApplication = app;
            controller.otherRequests = 'Complete it.';
            controller.Choice = 'Complete';
            controller.acceptCompletion = true;
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assertEquals(System.today(), controller.currentCert[0].Actual_Completion_Date__c);
            System.assertEquals(True, controller.currentCert[0].Has_work_Completed__c);
            System.assertEquals(constants.certificate_status_Issued_Works_Complete, controller.currentCert[0].Status__c);
            System.assertEquals(cert.Id, controller.attachmentParentID);
            pg = controller.confirmation();
            System.assertEquals('/apex/CertificateSubmit?id=' + cert.Id, pg.getUrl());  
            System.assert(controller.currentCert[0].External_Comments__c!=null && !String.isEmpty(controller.currentCert[0].External_Comments__c));
            Test.stopTest();                
        }     
    }    
    
    //CancelCOI Flow: Complete : Don't acceptCompletion
    public static testMethod void saveCompleteDontAcceptCompletionTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];		
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];
        Application__c app = [Select id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        
        System.runAs(runningUser){
            Test.startTest();
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            AmendCOIController controller = new AmendCOIController();
            controller.newApplication = app;
            controller.otherRequests = 'Complete it.';
            controller.Choice = 'Complete';
            controller.acceptCompletion = false;
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[0].getSummary().contains('Please accept the terms before proceeding'), 'Please accept the terms before proceeding');
            Test.stopTest();                
        }     
    }     
    
    //CancelCOI Flow: Amend
    public static testMethod void saveAmendTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];		
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];
        Application__c app = [Select id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        RecordType amendCOIRecType = [Select Id,SobjectType,Name From RecordType WHERE Name =:constants.amendCOI and SobjectType ='Application__c'  limit 1];
        
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            controller.newApplication = app;
            controller.otherRequests = 'Amend it.';
            controller.Choice = 'Amend';
            controller.limitChanges = true;
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assertEquals(amendCOIRecType.id, controller.newApplication.recordtypeid);
            System.assertEquals(constants.amendCOI, controller.newApplication.type__c);
            System.assert(controller.newApplication.Project_Contract_Change__c);
            System.assert(!controller.newApplication.Property_Address_Change__C);
            System.assert(!controller.newApplication.Owner_Details_Change__c);
            System.assertEquals(app.Id, controller.attachmentParentID);
            System.assertEquals(GlobalUtility.getQueues(new list<String>{GlobalConstants.BAT_QUEUE}).get(GlobalConstants.BAT_QUEUE), controller.newApplication.ownerId);
            System.assertEquals(constants.application_Status_DD_Review, controller.newApplication.Application_Status__c);
            Test.stopTest();                
        }     
    }     
    
    //CancelCOI Flow: Run as Distributor
    public static testMethod void saveAmendAsDistributorTest(){
        User runningUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];		
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];
        Application__c app = [Select id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        RecordType amendCOIRecType = [Select Id,SobjectType,Name From RecordType WHERE Name =:constants.amendCOI and SobjectType ='Application__c'  limit 1];
        cert.OwnerId = runningUser.Id;
        Database.update(cert);
        System.runAs(runningUser){
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters				
            ApexPages.currentPage().getParameters().put('id', cert.Id);
            Test.startTest();			
            AmendCOIController controller = new AmendCOIController();
            controller.newApplication = app;
            controller.otherRequests = 'Amend it.';
            controller.Choice = 'Amend';
            controller.limitChanges = true;
            PageReference pg = controller.save();
            System.assertEquals(null, pg);
            System.assertEquals(amendCOIRecType.id, controller.newApplication.recordtypeid);
            System.assertEquals(constants.amendCOI, controller.newApplication.type__c);
            System.assert(controller.newApplication.Project_Contract_Change__c);
            System.assert(!controller.newApplication.Property_Address_Change__C);
            System.assert(!controller.newApplication.Owner_Details_Change__c);
            System.assertEquals(app.Id, controller.attachmentParentID);
            System.assertEquals(GlobalUtility.getQueues(new list<String>{GlobalConstants.BAT_QUEUE}).get(GlobalConstants.BAT_QUEUE), controller.newApplication.ownerId);
            System.assertEquals(constants.application_Status_new, controller.newApplication.Application_Status__c);
            Test.stopTest();                
        }     
    }     
    
    
    public static testMethod void cancelTest(){
        List<User> userList = [select Id from User where userType = 'Guest'];
        System.runAs(userList[0]) {
            Test.startTest();
            PageReference pageRef = Page.RequestAmendCOI;
            Test.setCurrentPage(pageRef);
            AmendCOIController controller = new AmendCOIController();
            PageReference pg = controller.cancel();
            System.assertEquals('/apex/DistributorLanding', pg.getUrl());
            Test.stopTest();
        }
    }    
}