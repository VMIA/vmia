/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Handler Class for CaseComment object trigger which includes context-specific methods 
                    that are automatically called when a trigger is executed.
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public with sharing class ApplicationTriggerHandler{
    
    // Variables
    private static ApplicationTriggerHelper helper;    // Helper class variable to call methods based on scenario
    
    /**
      * @description       This method is invoked from trigger which in-turn calls the handlers
                           for before and after event.
      * @param             NA 
      * @return            Void
      * @throws            NA
      */        
    public static void execute(){
        helper = new ApplicationTriggerHelper();   // instance of helper class for Case trigger

        // Check for event type of trigger
       
        if(Trigger.isBefore && ApplicationTriggerHelper.isBeforeEventFirstRun()){
            beforeHandler();        // invoke of before handler
        }
        else if(Trigger.isAfter && ApplicationTriggerHelper.isAfterEventFirstRun()){
            afterHandler();         // invoke of after handler
        }
    }
    
    /**
      * @description       This method is handler for before events invoked by execute method.
                           Method invokes the helper class methods based on the scenario.                       
      * @param             NA 
      * @return            Void
      * @throws            NA
      */  
    private static void beforeHandler(){
        // Check for type of operation
        if(Trigger.isInsert){
            // Methods to be invoked for before insert event
            System.debug('Inside Insert');
            helper.migratedBuilderApplication(Trigger.new);
            helper.initBuilderInsuranceApplication((Application__c[]) Trigger.new);
            helper.categoryLimitEligibility((Application__c[]) Trigger.new);
            helper.assignRBPtToHiddenField((Application__c[]) Trigger.new);
            helper.updateBuilderNameDetails((Application__c[]) Trigger.new);
            helper.setTypeForManualCreation((Application__c[]) Trigger.new);

            //helper.populateBuilderDetailsInAdverseAlert((Application__c[]) Trigger.new);
        }
        else if(Trigger.isUpdate){
            System.debug('Inside before Update');
            helper.changeRecordType((Application__c[]) Trigger.new, (Map<ID,Application__c>) Trigger.oldMap);
            helper.copyDetailsToBuilder((Application__c[]) Trigger.new, (Map<ID,Application__c>) Trigger.oldMap);
            helper.processBuilderInsuranceApplication((Application__c[]) Trigger.new, (Map<ID,Application__c>) Trigger.oldMap);
            helper.categoryLimitEligibility((Application__c[]) Trigger.new);
            helper.cloneOriginalCertificate(Trigger.new,(Map<Id,Application__c>)Trigger.oldMap);
            helper.changeDDUserAfterDDChange(Trigger.new,(Map<Id,Application__c>)Trigger.oldMap);
        }
        else if(Trigger.isDelete){
            // Methods to be invoked for before update event
        }
    }
    
    /**
      * @description       This method is handler for after events invoked by execute method.
                           Method invokes the helper class methods based on the scenario.                          
      * @param             NA 
      * @return            Void
      * @throws            NA
      */  
    private static void afterHandler(){
        // Check for type of operation
        if(Trigger.isInsert){
          helper.adverseAlertTask(Trigger.new);
          helper.shareApplicationsToDistributor(Trigger.new,null);
          helper.sendForDDApproval(Trigger.new);
    helper.shareApplicationsToDirectors(Trigger.new);
        }
        else if(Trigger.isUpdate){
            // Methods to be invoked for after update event
            helper.shareApplicationsToDistributor(Trigger.new,(Map<Id,Application__c>)Trigger.oldMap);
            helper.withdrawBuilderOrCertificate(Trigger.new,(Map<Id,Application__c>)Trigger.oldMap);
            helper.createTaskFromCertificateDecision(Trigger.new,(Map<Id,Application__c>)Trigger.oldMap);
      helper.shareApplicationsToDirectors(Trigger.new);

        }
        else if(Trigger.isDelete){
            // Methods to be invoked for after delete event
        }
        else if(Trigger.isUndelete){
            // Methods to be invoked for after undelete event
        }
    }
}