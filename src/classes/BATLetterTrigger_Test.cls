/**
  * Date         :  17-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for BAT Letter trigger
  */ 
  
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
private with sharing class BATLetterTrigger_Test{
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> certRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> appRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup(){

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);

        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,
                                                        conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        
        Application__c app = TestUtility.createApplication(bornBuilder.Id,rexDistributor.Id,'New Eligibility',
                                                            appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
        Database.insert(app);
        
        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        Database.insert(cert);
    }

    @future
    private static void setupUser(){
        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(new List<User>{vmiaInternalUser,rexUser});
    }
    
    /**
      * @description       This method tests scenario of updating Builder details for batLetter from application
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSetBuilderApplication(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        
        Application__c app = [SELECT Builder_Number__c FROM Application__c LIMIT 1];
        System.runAs(runningUser){
            
            BAT_Letter__c batLetter = TestUtility.createBATLetter(null,app.Id,null);
            Test.startTest();
                Database.insert(batLetter);
            Test.stopTest();
            BAT_Letter__c batLetterAfterInsert = [SELECT Builder__c FROM BAT_Letter__c LIMIT 1];
            System.assertEquals(app.Builder_Number__c, batLetterAfterInsert.Builder__c);
        }
    }
    
    /**
      * @description       This method tests scenario of updating Builder details for batLetter from certificate
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSetBuilderCertificate(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        
        Certificate__c cert = [SELECT Builder__c FROM Certificate__c LIMIT 1];
        System.runAs(runningUser){
            
            BAT_Letter__c batLetter = TestUtility.createBATLetter(null,null,cert.Id);
            Test.startTest();
                Database.insert(batLetter);
            Test.stopTest();
            BAT_Letter__c batLetterAfterInsert = [SELECT Builder__c FROM BAT_Letter__c LIMIT 1];
            System.assertEquals(cert.Builder__c, batLetterAfterInsert.Builder__c);
        }
    }

    /**
      * @description       This method is for coverage purpose only, not a valid business scenario at this point
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testLetterUpdateDelete(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        
        Application__c app = [SELECT Builder_Number__c FROM Application__c LIMIT 1];
        System.runAs(runningUser){
            BAT_Letter__c batLetter = TestUtility.createBATLetter(null,app.Id,null);
            Database.insert(batLetter);
            Test.startTest();
                batLetter.Letter_Subject__c = 'Test Subject';
                Database.update(batLetter);
            Test.stopTest();
        }
    }

    /**
      * @description       Test method to validate deletion and undelete of bat letter
      *                     Scenario is not part of requirement, created for code coverage purpose only
      * @param             NA
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testLetterDeleteUndelete(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        
        Application__c app = [SELECT Builder_Number__c FROM Application__c LIMIT 1];

        BAT_Letter__c batLetter = TestUtility.createBATLetter(null,app.Id,null);
        Database.insert(batLetter);
        System.runAs(runningUser){
            Database.delete(batLetter);
            Test.startTest();
                Database.undelete(batLetter);
            Test.stopTest();
        }
    }
}