@isTest
private class BuilderDetailPageController_Test {
	
	private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        UserRole batMember = TestUtility.getUserRole('BAT_Member');
        
        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        Database.insert(vmiaInternalUser);
        setupData();
        setupUsers();
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        

        Account texDistributor = TestUtility.createBusinessAccount('Tex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        texDistributor.website = 'www.textDis.com';
        Database.insert(texDistributor);
        System.debug('** texDistributor ==>'+ texDistributor);

        Contact texSmoth = TestUtility.createContact('Tex','Smoth','tex.smoth@texHomeDistributorTest.com',texDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(texSmoth);
        System.debug('** texSmoth ==>'+ texSmoth);

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);        
    }

    @future
    private static void setupUsers(){
    	Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        Contact tonyStark = [SELECT id FROM Contact WHERE FirstName = 'Tony' and LastName = 'Stark' LIMIT 1];
        Contact rexSmith = [SELECT id FROM Contact WHERE FirstName = 'Rex' and LastName = 'Smith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];

        User builderUser = TestUtility.createPortalUser('ExternB',profileMap.get('Builder'),tonyStark.Id);
        Database.insert(builderUser);

        User ddUser = TestUtility.createPortalUser('ExternD',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(ddUser);

        System.RunAs(builderUser) {
        	Application__c  app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
    		Database.insert(app);
    		System.debug('** Application ==>'+ app);
        }
    }

    public static testMethod void saveUpdatesTest(){
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];	
		Application__c app = [Select Id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];

        User distributor = [SELECT AccountId, LastName, UserRoleId, UserRole.DeveloperName FROM User WHERE LastName = 'ExternD' LIMIT 1];		
        Group grp = [SELECT Name, DeveloperName,RelatedId,Type 
                                        FROM Group WHERE Type =: GlobalConstants.ROLE_AND_SUBORDINATES AND
                                        RelatedId =:distributor.UserRoleId LIMIT 1];

        Test.setCurrentPageReference(new PageReference('Page.EditBuilderDetails')); 
		System.currentPageReference().getParameters().put('id', bornBuilder.Id);

        builderDetailPageController controller = new builderDetailPageController();
        controller.editPhone = '97001211';
        controller.editEmail = 'test@test.com';
        controller.editTradingStreet = '12 Waddle Street';
        controller.editTradingCity = 'TestCity';
        controller.editTradingState = 'VIC';
        controller.editTradingPostalCode = '3000';
        controller.editTradingCountry = 'Australia';
        controller.editPostalStreet = '11 Waddle Street';
        controller.editPostalCity = 'TestCity2';
        controller.editPostalState = 'VIC';
        controller.editPostalPostalCode = '3001';
        controller.editPostalCountry = 'Australia';

        PageReference confirmPageExpected = new PageReference('/builderLandingPage?id=' + bornBuilder.Id);
        PageReference confirmPage;
        System.RunAs(new User(Id=UserInfo.getUserId())) {
			Test.startTest();
	        confirmPage = controller.save();
	        Test.stopTest(); 
        }   
        Account bornBuilderAfter = [SELECT Id, Phone, Primary_Email__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, 
        								BillingCountry, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry 
        								FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        system.debug('** bornBuilderAfter ' + bornBuilderAfter); 

        System.assertEquals('97001211', bornBuilderAfter.Phone);			
        System.assertEquals('test@test.com', bornBuilderAfter.Primary_Email__c);            
        System.assertEquals('12 Waddle Street', bornBuilderAfter.BillingStreet);			
        System.assertEquals('TestCity', bornBuilderAfter.BillingCity); 
        System.assertEquals('VIC', bornBuilderAfter.BillingState);			
        System.assertEquals('3000', bornBuilderAfter.BillingPostalCode); 
        System.assertEquals('Australia', bornBuilderAfter.BillingCountry);			
        System.assertEquals('11 Waddle Street', bornBuilderAfter.ShippingStreet); 
        System.assertEquals('TestCity2', bornBuilderAfter.ShippingCity);			
        System.assertEquals('VIC', bornBuilderAfter.ShippingState);
        System.assertEquals('3001', bornBuilderAfter.ShippingPostalCode);			
        System.assertEquals('Australia', bornBuilderAfter.ShippingCountry);  
        System.assertEquals(confirmPageExpected.getURL(), confirmPage.getURL());
    }

    public static testmethod void cancel(){
    	Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];

        Test.setCurrentPageReference(new PageReference('Page.EditBuilderDetails')); 
		System.currentPageReference().getParameters().put('id', bornBuilder.Id);
		PageReference cancelPageExpected = new PageReference('/builderLandingPage?id=' + bornBuilder.Id);
        builderDetailPageController controller = new builderDetailPageController();
        System.RunAs(new User(Id=UserInfo.getUserId())) {
			Test.startTest();
	        PageReference cancelPage = controller.cancel();
			System.assertEquals(cancelPageExpected.getURL(), cancelPage.getURL());
	        Test.stopTest(); 
        }  
    }

    public static testmethod void confirm(){
    	Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];

        Test.setCurrentPageReference(new PageReference('Page.EditBuilderDetails')); 
		System.currentPageReference().getParameters().put('id', bornBuilder.Id);
		PageReference confirmPageExpected = new PageReference('/builderLandingPage?id=' + bornBuilder.Id);
        builderDetailPageController controller = new builderDetailPageController();
        System.RunAs(new User(Id=UserInfo.getUserId())) {
			Test.startTest();
	        PageReference confirmPage = controller.confirm();
			System.assertEquals(confirmPageExpected.getURL(), confirmPage.getURL());
	        Test.stopTest(); 
        }  
    }

    public static testMethod void failValidateWithBlankTest(){
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];	
		Application__c app = [Select Id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];

        User distributor = [SELECT AccountId, LastName, UserRoleId, UserRole.DeveloperName FROM User WHERE LastName = 'ExternD' LIMIT 1];		
        Group grp = [SELECT Name, DeveloperName,RelatedId,Type 
                                        FROM Group WHERE Type =: GlobalConstants.ROLE_AND_SUBORDINATES AND
                                        RelatedId =:distributor.UserRoleId LIMIT 1];

        Test.setCurrentPageReference(new PageReference('Page.EditBuilderDetails')); 
		System.currentPageReference().getParameters().put('id', bornBuilder.Id);

        builderDetailPageController controller = new builderDetailPageController();
        controller.editPhone = '';
        controller.editEmail = 'test@test.com';
        controller.editTradingStreet = '12 Waddle Street';
        controller.editTradingCity = 'TestCity';
        controller.editTradingState = 'VIC';
        controller.editTradingPostalCode = '3000';
        controller.editTradingCountry = 'Australia';
        controller.editPostalStreet = '11 Waddle Street';
        controller.editPostalCity = 'TestCity2';
        controller.editPostalState = 'VIC';
        controller.editPostalPostalCode = '3001';
        controller.editPostalCountry = 'Australia';

        PageReference saveLocation;
        System.RunAs(new User(Id=UserInfo.getUserId())) {
			Test.startTest();
	        saveLocation = controller.save();
	        Test.stopTest(); 
        }   
        Account bornBuilderAfter = [SELECT Id, Phone, Primary_Email__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, 
        								BillingCountry, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry 
        								FROM Account WHERE Name = 'Born Builders' LIMIT 1];
	
        System.assertNotEquals('test@test.com', bornBuilderAfter.Primary_Email__c);            
        System.assertNotEquals('12 Waddle Street', bornBuilderAfter.BillingStreet);			 
        System.assertEquals(null, saveLocation);			
        
    }

    public static testMethod void failValidateWithInvalidEmailTest(){
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];	

        Test.setCurrentPageReference(new PageReference('Page.EditBuilderDetails')); 
		System.currentPageReference().getParameters().put('id', bornBuilder.Id);

        builderDetailPageController controller = new builderDetailPageController();
        controller.editPhone = '9700000';
        controller.editEmail = 'invalidemail';
        controller.editTradingStreet = '12 Waddle Street';
        controller.editTradingCity = 'TestCity';
        controller.editTradingState = 'VIC';
        controller.editTradingPostalCode = '3000';
        controller.editTradingCountry = 'Australia';
        controller.editPostalStreet = '11 Waddle Street';
        controller.editPostalCity = 'TestCity2';
        controller.editPostalState = 'VIC';
        controller.editPostalPostalCode = '3001';
        controller.editPostalCountry = 'Australia';

        PageReference saveLocation;
       	Boolean emailIsValid;
        System.RunAs(new User(Id=UserInfo.getUserId())) {
			Test.startTest();
	        emailIsValid = controller.isValidEmail('invalidemail');

	        saveLocation = controller.save();
	        Test.stopTest(); 

        }   
        System.assertEquals(false, emailIsValid);
        System.assertEquals(null, saveLocation);			
        
    }

    public static testMethod void failValidateWithInvalidDDSelectionCriteriaTest(){
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];	
		Account texDistributor = [SELECT Id, Name FROM Account WHERE Name = 'Tex Home Distributor' LIMIT 1];


        Test.setCurrentPageReference(new PageReference('Page.EditBuilderDetails')); 
		System.currentPageReference().getParameters().put('id', bornBuilder.Id);

        builderDetailPageController controller = new builderDetailPageController();
        controller.editPhone = '9700000';
        controller.editEmail = 'test@test.com';
        controller.editTradingStreet = '12 Waddle Street';
        controller.editTradingCity = 'TestCity';
        controller.editTradingState = 'VIC';
        controller.editTradingPostalCode = '3000';
        controller.editTradingCountry = 'Australia';
        controller.editPostalStreet = '11 Waddle Street';
        controller.editPostalCity = 'TestCity2';
        controller.editPostalState = 'VIC';
        controller.editPostalPostalCode = '3001';
        controller.editPostalCountry = 'Australia';
        controller.ddSelection = texDistributor.Name;
        controller.declarationAccept = false;

        PageReference saveLocation;
        Boolean validationFailed;
        System.RunAs(new User(Id=UserInfo.getUserId())) {
			Test.startTest();
			validationFailed = controller.validate();
	        saveLocation = controller.save();
	        Test.stopTest(); 
        }  
        system.assertEquals(true, validationFailed); 
        System.assertEquals(null, saveLocation);			
        
    }

    public static testMethod void passValidateWithValidDDSelectionCriteriaTest(){
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];	
		Account texDistributor = [SELECT Id, Name, website FROM Account WHERE Name = 'Tex Home Distributor' LIMIT 1];
		Contact texSmoth = [SELECT id FROM Contact WHERE FirstName = 'Tex' and LastName = 'Smoth' LIMIT 1];
		Account rexDistributor = [SELECT Id, Name, website FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];

        Test.setCurrentPageReference(new PageReference('Page.EditBuilderDetails')); 
		System.currentPageReference().getParameters().put('id', bornBuilder.Id);

        builderDetailPageController controller = new builderDetailPageController();
        controller.editPhone = '9700000';
        controller.editEmail = 'test@test.com';
        controller.editTradingStreet = '12 Waddle Street';
        controller.editTradingCity = 'TestCity';
        controller.editTradingState = 'VIC';
        controller.editTradingPostalCode = '3000';
        controller.editTradingCountry = 'Australia';
        controller.editPostalStreet = '11 Waddle Street';
        controller.editPostalCity = 'TestCity2';
        controller.editPostalState = 'VIC';
        controller.editPostalPostalCode = '3001';
        controller.editPostalCountry = 'Australia';
        controller.setddSelection(texDistributor.Name);
        String testGetter = controller.getDDSelection();
        controller.declarationAccept = true;
		
		Controller.ddMap = new Map<String, Account>{rexDistributor.Name => rexDistributor, texDistributor.Name => texDistributor};
		List<SelectOption> newList = controller.getDDListSelection();

		PageReference confirmPageExpected = new PageReference('/builderLandingPage?id=' + bornBuilder.Id);
        PageReference saveLocation;
        System.RunAs(new User(Id=UserInfo.getUserId())) {
			Test.startTest();
	        saveLocation = controller.save();
	        Test.stopTest(); 
        } 
        Account bornBuilderAfter = [SELECT Id, Agent__c, Agent_Contact__c, Date_DD_Nominated_by_Builder__c FROM Account WHERE Name = 'Born Builders' LIMIT 1];  
        
        System.assertEquals(texDistributor.Id, bornBuilderAfter.Agent__c);
        System.assertEquals(texSmoth.Id, bornBuilderAfter.Agent_Contact__c);
        System.assertEquals(System.today(), bornBuilderAfter.Date_DD_Nominated_by_Builder__c);
        System.assertEquals(confirmPageExpected.getURL(), saveLocation.getURL());			
        
    }

	private static testMethod void testForwardToCustomAuthPage(){
		Account builder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
		PageReference pageRef = Page.EditBuilderDetails;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',builder.id);
		Test.startTest();		
		builderDetailPageController controller = new builderDetailPageController();
		PageReference directPage = controller.forwardToCustomAuthPage();
		System.assertEquals(null, directPage);
		
		Test.stopTest();

	}
    

}