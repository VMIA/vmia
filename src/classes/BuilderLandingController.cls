/**
  * Date         :  10-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Controller for BuilderLanding visualforce page to display builder details.
  */
public with sharing class BuilderLandingController{ 
    
    public boolean displayTC {get; set;}
    public boolean displayTCError {get; set;}
    public User chkUser {get; set;}
    public String rateChartURL {get;set;}
    
    public PageReference dagreeTerms() {
    
        displayTC = true;
        displayTCError = true;
        return null;
    }


    public PageReference agreeTerms() {
    
        chkUser.Terms_and_Condition__c = true;
        chkUser.Terms_and_Condition_Date__c = System.Today();
        if (!Test.isRunningTest()) update chkUser; 
        
        displayTC = false;
        displayTCError = false;
        
        return null;
    }



    // constants
    private static final String ID = 'id';
    private static final String MY_PARAM = 'myParam';
    private static final String GUEST = 'Guest';
    private static final String LOGOUT_URL = '/secur/logout.jsp';
    private static final String AWAITING_PAYMENT = 'Awaiting Payment';
    private static final String CERT_RECTYPE_CONTAINER = 'Container';
    
    public string status_works_in_progress{get;set;}
    public string status_works_complete{get;set;}
    public string status_takenover{get;set;}
    public string CATEGORY_1 {get;set;}
    public string CATEGORY_3 {get;set;}
    public string CATEGORY_4 {get;set;}
    public string CATEGORY_5 {get;set;}
    public string CATEGORY_6 {get;set;}
    public string CATEGORY_7 {get;set;}
    public string CATEGORY_8 {get;set;}
    
    // page variables
    public Account builderRecord {get;set;}
    public list <categoryWrapper> categories {get;set;}
    public list <String> eligibilityCondtions {get;set;}
    public list <Contact_Role__c> relatedContacts {get;set;}
    public id newEligibilityApplicationId {get;set;}
    public Integer applicationCount {get;set;}
    public Integer certificateCount {get;set;}
    public Integer letterCount {get;set;}
    
    // variables
    private Id builderId ;
    private User loggedInUser;

    public boolean disableRequestEligibilityButton {get;set;}
    public boolean disableApplyForCOIButton {get;set;}
    public boolean showHistoryButton {get;set;}
    
    /*
        constructor : fetch the builder record & related applications and certificates
     */
     
    public string userProfile {get { 
        if(userProfile == NULL) {
            User u = [SELECT AccountId, Profile.Name FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
            userProfile = u.Profile.Name;
        }
        return userProfile;
    } set;}
    
    public PageReference validateAndRedirect(){
    
        chkUser = [SELECT  Username, Terms_and_Condition__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        
        
        if (chkUser.Terms_and_Condition__c == false){     
             displayTC = true; 
             displayTCError = false;               
        }
        return null;
    
     }

    public BuilderLandingController(){
        status_works_in_progress = constants.certificate_status_Issued_Works_In_Progress;
        status_works_complete = constants.certificate_status_Issued_Works_Complete;
        status_takenover = constants.certificate_status_Takeover;
        CATEGORY_1 = constants.New_Single_Dwelling;
        CATEGORY_3 = constants.New_Multi_Dwelling;
        CATEGORY_4 = constants.Alterations_Additions;
        CATEGORY_5 = constants.Swimming_Pools;
        CATEGORY_6 = constants.Renovations_Tradespeople;
        CATEGORY_7 = constants.Other;
        CATEGORY_8 = constants.All_Cover;
        try{
            builderId = ApexPages.currentPage().getParameters().get(ID); // fetch builder id from URL
            
            if(builderId == null){
                // fetch builder id from logged in user if id not available in URL
                loggedInUser = [SELECT AccountId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
                builderId = loggedInUser.AccountId;
            }
            
            if(builderId != null){
                builderId = String.escapeSingleQuotes(builderId);
                // query builder & related applications, certificates and contacts
                builderRecord = [SELECT Name,Agent_contact__c, Builder_Eligibility_Status_Reason__c, Agent_contact__r.Name, ABN__c, ACN__c, AvailableConstructionLimit__c, 
                                    Eligibility_Status__c, Construction_Limit__c, SD_Flat_Rate_Premium__c, SP_Flat_Rate_Premium__c,
                                    UsedConstructionLimit__c, Original_Eligibility_Approval_Date__c,Construction_Category_Limit_1__c, 
                                    Construction_Category_Limit_3__c, Construction_Category_Limit_4__c,Construction_Category_Limit_5__c, 
                                    Construction_Category_Limit_6__c,Construction_Category_Limit_7__c,After_Hours_Phone__c,
                                    DBDRU_Ref__c, DBDRU_Date__c, BillingCity,BillingState, BillingStreet, BillingCountry, BillingPostalCode, 
                                    ShippingCity,ShippingState, ShippingStreet, ShippingCountry, ShippingPostalCode, Account_Number__c,
                                    Legal_Business_Type__c, phone, Primary_Email__c, Agent__c,Agent__r.Primary_Email__c,Agent__r.Name, Parent.Name,Date_Started_Trading__c,
                                    ASIC_Entity__c, ASIC_Status__c, CO1_New_Single_Dwelling__c,C03_Multi_Dwelling_Flat_Rate_Premium__c,CO3_New_Multi_Dwelling__c,
                                    C04_Alterations_Additions__c,C04_Structural_Flat_Rate_Premium__c,C05_Swimming_Pools__c,C06_Renovations_Tradespeople__c,
                                    C07_Other__c,C07_Other_Flat_Rate_Premium__c,C08_All_Cover__c,C08_All_Cover_Flat_Rate_Premium__c,Flat_Rate_Premium__c, 
                                    Builder_Risk_Rating__c,Eligibility_Review_Date__c,Non_Structural_Flat_Rate__c,Eligibility_Conditions__c,Other_Terms__c, Pending_Eligibility_Issued_Date__c,
                                    RBP_VBA_Number__pc, Builder_Role__pc, Drivers_License_Number__pc, RBP_License_First_Issued__pc, Date_Commenced_with_Company__pc,
                                    VBA_Anniversary_Date__pc, VBA_Expiry_Date__pc, VBA_Issued_Date__pc, VBA_Registration_Type__pc, PersonMobilePhone, IsPersonAccount, PersonBirthdate,
                                    
                                    (SELECT Name,Policy_Number__c,date_issued__c, builder__c, Category_of_Building_Works__c, Property_Address__c,  
                                        Total__c, Status__c, Property_Owner__r.Name, Contract_Price__c FROM Certificates__r 
                                        WHERE Status__c !=: AWAITING_PAYMENT AND RecordType.Name !=: CERT_RECTYPE_CONTAINER AND RecordType.Name != :GlobalConstants.REC_TYPE_HISTORICAL_CERT AND Date_Issued__c >= LAST_N_DAYS:7 AND Status__c IN (:constants.Certificate_status_Issued_Works_In_Progress, :Constants.Certificate_status_Issued_Works_Complete) ORDER BY CreatedDate DESC LIMIT 3),
                                        
                                    (SELECT Name,Builder_Legal_Entity_Name__c,Builder_ABN__c,Hidden_Builder_Name__c,RecordType.Name,Application_Status__c, Hidden_Builder_ABN__c, Hidden_Builder_current_Legal_Name__c
                                        FROM Applications__r ORDER BY CreatedDate DESC LIMIT 3),

                                    (SELECT Name, Application__r.Name, Certificate__r.Name, Letter_Subject__c, CreatedDate 
                                        FROM BAT_Letters__r ORDER BY CreatedDate DESC LIMIT 3),

                                    
                                    /*(SELECT Contact.Name,Contact.MailingStreet,Contact.MailingState,Contact.MailingCity,Contact.MailingPostalCode, Contact.MailingCountry,Contact.MobilePhone,Contact.Phone,Contact.Email,Contact.RBP_VBA_Number__c,
                                        Contact.BirthDate,Contact.RBP_License_First_Issued__c,Contact.Drivers_License_Number__c,Contact.Date_Commenced_with_Company__c,Contact.Department
                                        FROM AccountContactRelations),*/
                                        
                                    (SELECT Name,Eligibility_Status__c,Builder_Risk_Score__c,Construction_Limit__c,Flat_Rate_Premium_C01__c,Flat_Rate_Premium_C03__c,
                                        Flat_Rate_Premium_C04__c,Flat_Rate_Premium_C05__c,Flat_Rate_Premium_C06__c,Flat_Rate_Premium_C07__c,Single_Dwelling_Category_Limit__c,
                                        Multi_Dwelling_Category_Limit__c,Structural_Alterations_Category_Limit__c,Swimming_Pool_Category_Limit__c,Non_Structural_Renovations__c,
                                        Other_Limits__c,Eligibility_Conditions__c,Other_Terms__c,Eligibility_Commencement_Date__c,Eligibility_Expiry_Date__c, LastModifiedBy.Name
                                        FROM Eligibility_Histories__r ORDER BY CreatedDate)
                                    FROM Account WHERE Id =: builderId LIMIT 1];
                                    
                                    System.debug('++++++ Applications : ' + builderRecord.Applications__r);
                                    System.debug('++++++ Certificates : ' + builderRecord.Certificates__r);
                                    //System.debug('++++++ AccountContactRelations : ' + builderRecord.AccountContactRelations);
                                    System.debug('++++++ History : ' + builderRecord.Eligibility_Histories__r);
                                    
                                    
                relatedContacts = GlobalUtility.relatedContactRoles;

                applicationCount = [SELECT count() FROM APPLICATION__c WHERE Builder_Number__c =: builderId AND Application_Status__c IN (:constants.Application_Status_New, :constants.Application_Status_DD_Review, :constants.Application_Status_Assigned, :constants.Application_Status_IN_PROGRESS, :constants.Application_Status_Awaiting_Info, :constants.Application_Status_New_Information_Received)];
                certificateCount = [SELECT count() FROM CERTIFICATE__C WHERE Builder__c =: builderId AND Status__c IN (:constants.Certificate_status_Issued_Works_In_Progress, :Constants.Certificate_status_Issued_Works_Complete) AND Date_Issued__c >= LAST_N_DAYS:7 ];
                letterCount = [SELECT count() FROM BAT_Letter__c WHERE Builder__c =: builderId];

                listCategories(builderRecord);
                eligibilityCondtions = String.isNotBlank(builderRecord.Eligibility_Conditions__c) ? 
                                        builderRecord.Eligibility_Conditions__c.split(';') : eligibilityCondtions;

                if(builderRecord.Builder_Eligibility_Status_Reason__c == constants.account_Builder_Eligibility_Status_Reason_Under_Review ||
                   builderRecord.Eligibility_Status__c == constants.account_Eligibility_Status_Under_App || 
                   builderRecord.Eligibility_Status__c == constants.account_Eligibility_Status_Cancelled ||
                   builderRecord.Eligibility_Status__c == constants.account_Eligibility_Status_Declined ){
                    disableRequestEligibilityButton = True;
                }

                if(builderRecord.Eligibility_Histories__r.isEmpty()){
                    showHistoryButton = False;
                }else{
                    showHistoryButton = True;
                }

                if(builderRecord.Eligibility_Status__c == constants.account_Eligibility_Status_Active_Restricted || 
                   builderRecord.Eligibility_Status__c == constants.account_Eligibility_Status_Active ){
                    disableApplyForCOIButton = False;
                }else{
                    disableApplyForCOIButton = True;
                }

                for(Application__c app : builderRecord.Applications__r){
                    if(app.RecordType.Name == globalConstants.REC_TYPE_APPLICATION_NEW_ELIGIBILITY){
                        newEligibilityApplicationId = app.Id;
                    }
                }
            }

            Document rateChart = [SELECT Id FROM Document WHERE DeveloperName =: Label.RateChart LIMIT 1];
            if(rateChart != null){
                rateChartURL = Label.DocumentRefURL + rateChart.Id; 
            }
        }
        catch(Exception exp){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Generic_ERROR));
        }
    }
    
    /**
      * @description       This method is invoked on page load to redirect to login page if guest user
      * @param             NA 
      * @return            void
      * @throws            NA
      */ 
    public PageReference forwardToCustomAuthPage() {
        PageReference pageRef;
        if(GUEST.equalsIgnoreCase(UserInfo.getUserType())){
            pageRef = Page.Login;
        }
        return pageRef;
    }
    
    /**
      * @description       This method is invoked from BuilderLanding page to logout user from the application
      * @param             NA 
      * @return            void
      * @throws            NA
      */ 
    public PageReference userLogout(){
        return new PageReference(LOGOUT_URL);
    }
    
    /**
      * @description       This method is invoked from BuilderLanding page to amend the certificate of insurance,
                           certificate id is passed as URL paramater
      * @param             NA 
      * @return            void
      * @throws            NA
      */
    public PageReference requestAmend(){
        PageReference pageRef = Page.RequestAmendCOI;
        string certID = Apexpages.currentPage().getParameters().get(MY_PARAM);
        certID = String.isNotBlank(certID) ? String.escapeSingleQuotes(certID) : certID;
        pageRef.getParameters().put(ID,certID);
        return pageRef;
    }
    
    public void listCategories(Account objAcc) {
        categories = new list <categoryWrapper>();
        
        if(objAcc.CO1_New_Single_Dwelling__c) {
            categories.add(new categoryWrapper(CATEGORY_1,objAcc.Construction_Category_Limit_1__c, objAcc.SD_Flat_Rate_Premium__c));
        }
            
        if(objAcc.CO3_New_Multi_Dwelling__c) {
            categories.add(new categoryWrapper(CATEGORY_3,objAcc.Construction_Category_Limit_3__c, objAcc.C03_Multi_Dwelling_Flat_Rate_Premium__c));
        }
            
        if(objAcc.C04_Alterations_Additions__c) {
            categories.add(new categoryWrapper(CATEGORY_4,objAcc.Construction_Category_Limit_4__c, objAcc.C04_Structural_Flat_Rate_Premium__c));
        }
            
        if(objAcc.C05_Swimming_Pools__c) {
            categories.add(new categoryWrapper(CATEGORY_5,objAcc.Construction_Category_Limit_5__c, objAcc.SP_Flat_Rate_Premium__c));
        }
            
        if(objAcc.C06_Renovations_Tradespeople__c) {
            categories.add(new categoryWrapper(CATEGORY_6,objAcc.Construction_Category_Limit_6__c, objAcc.Non_Structural_Flat_Rate__c));
        }
            
        if(objAcc.C07_Other__c) {
            categories.add(new categoryWrapper(CATEGORY_7,objAcc.Construction_Category_Limit_7__c, objAcc.C07_Other_Flat_Rate_Premium__c));
        }
    }
    
    public class categoryWrapper{
        public String catName{get;set;}
        public Decimal catLimit{get;set;}
        public Decimal catPremium{get;set;}
        public categoryWrapper(String newCatName, Decimal newCatLimit, Decimal newCatPremium){
            catName = newCatName;
            catLimit = newCatLimit;
            catPremium = newCatPremium;
        }
    }
}