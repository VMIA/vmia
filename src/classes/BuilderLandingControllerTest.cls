@IsTest(SeeAllData=false) 
public class BuilderLandingControllerTest {
    List <Account> distAccounts;
    List <Account> builderAccounts;
    List <Contact> distContacts;
    List <Contact> buildContacts;
    Contact newContact;
    User newUser;
    //TestDataFactory testData = new TextDataFactory();

    static testMethod void testForwardToCustomAuth() {
        //Instantiate a new controller with all parameters in the page


        BuilderLandingController controller = new BuilderLandingController();
        PageReference pageRef = controller.forwardToCustomAuthPage();
        //PageReference newPageRef;
        //PageRef is either null or an empty object in test context
        if(pageRef != null){
            System.assertEquals(pageRef.getUrl(), '/CustomLoginPage');
        }
    }

    public BuilderLandingControllerTest(){
        List<RecordType> recTypeBuild = [Select Id,SobjectType,Name From RecordType WHERE Name ='Company' and SobjectType ='Account'  limit 1];
        List<RecordType> recTypeDist = [Select Id,SobjectType,Name From RecordType WHERE Name ='Company' and SobjectType ='Account'  limit 1];
        
        distAccounts = TestDataFactory.createDistributorAccounts(1);
        insert distAccounts;

        distContacts = TestDataFactory.createDistributorContacts(1);
        distContacts[0].AccountId = distAccounts[0].id;
        insert distContacts;

        builderAccounts = TestDataFactory.createBuilderAccounts(1);
        builderAccounts[0].Agent__c = distAccounts[0].id;
        builderAccounts[0].Agent_Contact__c = distContacts[0].id;
        insert builderAccounts;

        buildContacts =  TestDataFactory.createBuilderContacts(1);
        buildContacts[0].AccountId = builderAccounts[0].id;
        insert buildContacts;


    }
}