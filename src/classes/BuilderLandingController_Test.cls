/**
  * Date         :  24-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for BuilderLandingController
  */ 
  
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
private with sharing class BuilderLandingController_Test{
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> certRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> appRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> invRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup(){
        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);

        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,
                                                        conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        bornBuilder.Construction_Category_Limit_1__c = 100000;
        bornBuilder.Construction_Category_Limit_3__c = 100000;
        bornBuilder.Construction_Category_Limit_4__c = 100000;
        bornBuilder.Construction_Category_Limit_5__c = 100000;
        bornBuilder.Construction_Category_Limit_6__c = 100000;
        bornBuilder.Construction_Category_Limit_7__c = 100000;
        bornBuilder.Eligibility_Status__c = constants.account_Eligibility_Status_Active;

        Account declinedBuilder = TestUtility.createBusinessAccount('Declined Builders',accRecTypeMap.get('Company').getRecordTypeId());
        declinedBuilder.Eligibility_Status__c = constants.account_Eligibility_Status_Declined;
        Database.insert(new List<Account>{bornBuilder,declinedBuilder});

        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,
                                                        conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);
        
        Application__c app = TestUtility.createApplication(bornBuilder.Id,rexDistributor.Id,'New Eligibility',
                                                            appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
        app.External_Comments__c = 'DD/Builder comments';
        Database.insert(app);

        Task appTsk = TestUtility.createTask(app.Id,rexSmith.Id,bornBuilder.Id);
        appTsk.Description = 'App Description';

        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        cert.External_Comments__c = 'DD/Builder comments';
        cert.Certificate_Type__c = 'Certificate';
        Database.insert(cert);

        Task certTsk = TestUtility.createTask(cert.Id,rexSmith.Id,bornBuilder.Id);
        certTsk.Description = 'Certificate Description';
        certTsk.Subject = Label.Task_Subject_DD_Review;
        Database.insert(new List<Task>{appTsk,certTsk});
    }

    @future
    private static void setupUser(){
        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(new List<User>{vmiaInternalUser,rexUser});
    }
    
    /**
      * @description       This method tests scenario of page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testPageLoad(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        bornBuilder.Construction_Category_Limit_1__c = 200000;
        Database.update(bornBuilder);

        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        app.OwnerId = rexUser.Id;

        Certificate__c cert = [SELECT Id FROM Certificate__c LIMIT 1];
        cert.OwnerId = rexUser.Id;

        Database.update(new List<sObject>{app,cert});
        System.runAs(rexUser){
            PageReference pageRef = Page.BuilderLandingPage;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',bornBuilder.Id);
            Test.startTest();
                BuilderLandingController landingController  = new BuilderLandingController();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testPageLoadDeclinedBuilder(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account declinedBuilder = [SELECT Id FROM Account WHERE Name = 'Declined Builders' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.BuilderLandingPage;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',declinedBuilder.Id);
            Test.startTest();
                BuilderLandingController landingController  = new BuilderLandingController();
                System.assert(landingController.userProfile != null);
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of user logout
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testUserLogout(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account declinedBuilder = [SELECT Id FROM Account WHERE Name = 'Declined Builders' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.BuilderLandingPage;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',declinedBuilder.Id);
            BuilderLandingController landingController  = new BuilderLandingController();
            Test.startTest();
                landingController.userLogout();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of validate & Redirect
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testValidateAndRedirect(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account declinedBuilder = [SELECT Id FROM Account WHERE Name = 'Declined Builders' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.BuilderLandingPage;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',declinedBuilder.Id);
            BuilderLandingController landingController  = new BuilderLandingController();
            Test.startTest();
                landingController.validateAndRedirect();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of terms agreement
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testAgreeTerms(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account declinedBuilder = [SELECT Id FROM Account WHERE Name = 'Declined Builders' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.BuilderLandingPage;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',declinedBuilder.Id);
            BuilderLandingController landingController  = new BuilderLandingController();
            landingController.validateAndRedirect();
            Test.startTest();
                landingController.agreeTerms();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of terms disagreement
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testDisAgreeTerms(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account declinedBuilder = [SELECT Id FROM Account WHERE Name = 'Declined Builders' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.BuilderLandingPage;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',declinedBuilder.Id);
            BuilderLandingController landingController  = new BuilderLandingController();
            landingController.validateAndRedirect();
            Test.startTest();
                landingController.dagreeTerms();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of auth forward
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testForwardAuthPage(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account declinedBuilder = [SELECT Id FROM Account WHERE Name = 'Declined Builders' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.BuilderLandingPage;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',declinedBuilder.Id);
            BuilderLandingController landingController  = new BuilderLandingController();
            Test.startTest();
                landingController.forwardToCustomAuthPage();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of auth forward
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testCertAmend(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account declinedBuilder = [SELECT Id FROM Account WHERE Name = 'Declined Builders' LIMIT 1];
        Certificate__c cert = [SELECT Id FROM Certificate__c LIMIT 1];
        System.runAs(rexUser){
            PageReference pageRef = Page.BuilderLandingPage;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',declinedBuilder.Id);
            ApexPages.CurrentPage().getParameters().put('myParam',cert.Id);
            BuilderLandingController landingController  = new BuilderLandingController();
            Test.startTest();
                landingController.requestAmend();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of page load as admin to cover exception scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testPageLoadException(){
        System.runAs(new User(Id = UserInfo.getUserId())){
            PageReference pageRef = Page.BuilderLandingPage;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',UserInfo.getUserId());
            Test.startTest();
                BuilderLandingController landingController  = new BuilderLandingController();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of page load as guest
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testPageLoadGuest(){
        User guestUser = [SELECT Id,Name FROM User WHERE UserType = 'Guest' LIMIT 1];
        System.runAs(guestUser){
            PageReference pageRef = Page.BuilderLandingPage;
            Test.setCurrentPage(pageRef);
            BuilderLandingController landingController  = new BuilderLandingController();
            Test.startTest();
                landingController.forwardToCustomAuthPage();
            Test.stopTest();
        }
    }
    
}