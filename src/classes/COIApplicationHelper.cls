/**
  * Date         :  28-March-2017
  * Author       :  SMS Management & Technology
  * Description  :  Classs contains the methods used while COI Application Journey
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/ 
public class COIApplicationHelper{
    
    public  static Boolean DistributorUser = false;
    public  static id distributorId ;
    public  static id distributorUserId ;
    public  static id builderId;

    /**
      * @description       This method is used to find the id for Logged In User
      * @param             userRecord 
      * @return            void
      * @throws            NA
      */ 
    public  static void LoggedInUserInformation(id userId){
        List<User> userRecord = [Select id,Contactid,AccountId,UserType from User where Id =:userId];

        if(null != userRecord &&  userRecord[0].UserType == 'PowerCustomerSuccess'){
            List<Contact> distributorRecord = [Select id, accountId from Contact where id=:userRecord[0].Contactid];
            System.debug('DistributorRecord<>'+DistributorRecord);
            if(distributorRecord.size()>0){
                distributorId = distributorRecord[0].accountId;
                distributorUserId = userRecord[0].id;
                System.debug('distributorId<>'+distributorId);
                DistributorUser = true;
            }
        }else if (null != userRecord &&  userRecord[0].UserType == 'CspLitePortal'){
            DistributorUser = false;
            List<User> distributorUserrec  = [SELECT Id,ContactId,AccountId FROM User WHERE AccountId IN (SELECT Agent__c FROM Account WHERE Id =: userRecord[0].AccountId)];
            System.debug('** distributorUserrec  ==>'+distributorUserrec  );
            if(!distributorUserrec.isEmpty()){
                distributorUserId = distributorUserrec[0].Id;
                distributorId = distributorUserrec[0].AccountId;
        builderId = userRecord[0].AccountId;
            }
            System.debug('** distributorUserId  ==>'+distributorUserId );
            System.debug('** distributorId ==>'+distributorId );
            //list <Contact> builderRec = [Select id, accountId,Acount. from Contact where id=:userRecord[0].contactID];
            /* if(builderRec.size()>0){
                builderId = builderRec[0].accountId;
                list<account> act = [select id ,Agent_Contact__c from account where id =: builderRec[0].id] ;
                if(!act.isEmpty()){
                    list<user> userRec = [select id from user where contactId =: act[0].Agent_Contact__c];

                    if(!userRec.isEmpty()){
                        distributorUserId = userRec[0].id;
                    }
                }


            } */
        }

      }

}