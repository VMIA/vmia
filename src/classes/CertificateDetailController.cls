public without sharing class CertificateDetailController {

    public String certificateId{get;set;}
    public string builderId{get;set;}
    public list<Certificate__c> cerlist{get;set;}
    public list<propertyIndividualOwnerWrapper> propertyIndividualOwnerWrapperList{get;set;}
    public list<propertyOrgOwnerWrapper> propertyOrgOwnerWrapperList{get;set;}
    public CertificateDetailController(ApexPages.StandardController controller) {

        certificateId= ApexPages.CurrentPage().getParameters().get('id');
        builderId= ApexPages.CurrentPage().getParameters().get('builderId');
       propertyIndividualOwnerWrapperList = new list<propertyIndividualOwnerWrapper>();
       propertyOrgOwnerWrapperList = new list<propertyOrgOwnerWrapper>();
       set<id> individualOwnerSet = new Set<id>();
       set<id> orgOwnerSet = new set<id>();
       
        cerlist = new list<Certificate__c>();
        System.debug('certificateId>>'+certificateId);

        
         if(certificateId != null && certificateId != ''){
            for(Certificate__c c : [Select id,name,Builder__r.name,Builder__r.Agent__r.name,Building_Contract_Date__c,Category_of_Building_Works__c,Contract_Price__c,Date_Issued__c,Estimated_Completion_Date__c,Estimated_Start_Date__c,Has_work_Completed__c,Have_Works_Commenced__c,Number_of_Properties__c,status__c,Sub_Categories__c,Premium__c,GST__c,StampDuty__c,Total__c  from Certificate__c where id =: certificateId]){
                cerlist.add(c);
                 //invoiceNumber = cerlist[0].InvoiceNo__c ;
            }


            for(Property_Owners__c pOwner : [select id,Contact__c,Property_Owner__c,Property_Owner__r.RecordType.name from Property_Owners__c where Certificate__c =: certificateId]){
                if(pOwner.Contact__c != null)
                    individualOwnerSet.add(pOwner.Contact__c);
                if(pOwner.Property_Owner__c != null && pOwner.Property_Owner__r.RecordType.name=='Person Account'){
                    individualOwnerSet.add(pOwner.Property_Owner__c);
                } else {   
                    orgOwnerSet.add(pOwner.Property_Owner__c);
                }
            }

            System.debug('individualOwnerSet>'+individualOwnerSet);
            system.debug('orgOwnerSet>'+orgOwnerSet);
            for(contact con:[select id , firstName,lastName,email from contact where ID IN : individualOwnerSet]){
                propertyIndividualOwnerWrapperList.add(new propertyIndividualOwnerWrapper(con.firstName,con.lastName,con.email));
            }
            for(account acc:[select id , Name,FirstName,LastName,Primary_Email__c from account where ID IN : individualOwnerSet]){
                propertyIndividualOwnerWrapperList.add(new propertyIndividualOwnerWrapper(acc.FirstName,acc.LastName,acc.Primary_Email__c));
            }

            for(account acc:[select id , name,abn__c,acn__c from account where ID IN : orgOwnerSet]){
                propertyOrgOwnerWrapperList.add(new propertyOrgOwnerWrapper(acc.name,acc.abn__c,acc.acn__c));
            }
            
        }
    }

    public class propertyIndividualOwnerWrapper{
        public string firstName{get;set;}
        public string lastName{get;set;}
        public string email{get;set;}
        public propertyIndividualOwnerWrapper(string firstNameStr,string lastNameStr,string emailStr){

            firstName = firstNameStr;
            lastName = lastNameStr ;
            email = emailStr ;
        }
    }

    public class propertyOrgOwnerWrapper{
        public string legalEntityName{get;set;}
        public string abn{get;set;}
        public string acn{get;set;}
        public propertyOrgOwnerWrapper(string legalEntityNameStr,string abnStr,string acnStr){

            legalEntityName = legalEntityNameStr;
            abn = abnStr ;
            acn = acnStr ;
        }
    }

    public pagereference generateCertificate(){
        string urlStrr = '/apex/GenerateCertificate?id='+cerlist[0].id;
        PageReference pageRefback= new PageReference(urlStrr);
           pageRefback.setRedirect(false);        
            return pageRefback;  

    }
    public pagereference returnHome(){
        list<Certificate__c> cListVar = new list<Certificate__c>();
        for(Certificate__c c:cerlist){
            c.payStatus__c = '' ;
            cListVar.add(c);
        }

        if(!cListVar.isEmpty())
            update cListVar ;

            string urlStrr = '/apex/DistributorLanding';
             PageReference pageRefback= new PageReference(urlStrr);
           pageRefback.setRedirect(false);        
            return pageRefback;  
        }

        public pagereference backToPrevious(){

            string urlStrr ;
            if(builderId != null && builderId != ''){
                    urlStrr = '/apex/multiCertificateDetailPage?id='+builderId; 
                }else{
                    urlStrr = '/apex/paymentSuccessPage?id='+cerlist[0].id ; 
                }
                       
          
             PageReference pageRefback= new PageReference(urlStrr);
           pageRefback.setRedirect(false);        
            return pageRefback;  
        }
        

}