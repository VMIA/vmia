/**
* Date         :  23-June-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for AmendCOIController controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class CertificateDetailController_Test
{
	private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        UserRole batMember = TestUtility.getUserRole('BAT_Member');
        
        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        Database.insert(vmiaInternalUser);
        setupData();
        setupUser();
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);
        
        Certificate__c  cert = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
        cert.Building_Contract_Date__c = Date.today()+1;
        cert.Building_Permit_Issued_Date__c = Date.today()-2;
        cert.Actual_Start_Date__c= Date.today()-1;
        cert.Actual_Completion_Date__c = Date.today();
        cert.Estimated_Start_Date__c= Date.today()-1;
        cert.Estimated_Completion_Date__c = Date.today();
        cert.status__c = constants.certificate_status_Issued_Works_In_Progress;
        Database.insert(cert);
        System.debug('** Certificate ==>'+ cert.id);
        
        Blob b = Blob.valueOf('Test Data');
        Attachment attachment = new Attachment(ParentId = cert.Id, Name='Test Attachment for certificate', Body=b);
        Database.insert(attachment);        
        
        Property__c prop = new Property__c(Builder__c = bornBuilder.Id, Suburb__c='Dockland');
        Database.insert(prop);
        System.debug('** Property ==>'+ prop.id);
        
        Property_Owners__c propOwn = new Property_Owners__c(Property__c = prop.id, Certificate__c = cert.id, property_Owner__c=rexDistributor.Id);
        Database.insert(propOwn);
        System.debug('** Property Owner ==>'+ propOwn.id);

        Property_Owners__c propOwn2 = new Property_Owners__c(Property__c = prop.id, Certificate__c = cert.id, Contact__c=rexSmith.Id);
        Database.insert(propOwn2);
        System.debug('** Property Owner2 ==>'+ propOwn2.id);
        
        Application__c  app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(),cert.Id);
        Database.insert(app);
        System.debug('** Application ==>'+ app);
        
       	      
        
        Account personAc = TestUtility.createPersonAccount('Person',accRecTypeMap.get('Person Account').getRecordTypeId());
        personAc.PersonMailingStreet = '9 Beach Street';
        Database.insert(personAc);
        System.debug('** personAc ==>'+ personAc); 

        Property_Owners__c propOwn3 = new Property_Owners__c(Property__c = prop.id, Certificate__c = cert.id, property_Owner__c=personAc.Id);
        Database.insert(propOwn3);
        System.debug('** Property Owner2 ==>'+ propOwn2.id);    
        
        Account personBuilder = TestUtility.createPersonAccount('Builder',accRecTypeMap.get('Person Account').getRecordTypeId());
        personBuilder.firstName = 'Person';
        Database.insert(personBuilder);
        System.debug('** personBuilder ==>'+ personBuilder);
        
        Certificate__c  certPerson = TestUtility.createCertificate(personBuilder.Id, null, certRecTypeMap.get('Certificate').getRecordTypeId());
        Database.insert(certPerson);
        System.debug('** Certificate ==>'+ certPerson.id); 

               
    } 

    @future
    private static void setupUser(){
        Contact rexSmith = [SELECT AccountId FROM Contact where FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        System.debug('** rexUser ==>'+ rexUser + ' @@acc ' + rexUser.AccountId);  
        Database.insert(rexUser);
    }  

    public static testMethod void generateCertificateTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];
        cert.Speculative__c = true;
        Database.update(cert);
        
        System.runAs(runningUser){
        	ApexPages.StandardController sc = new ApexPages.StandardController(cert);
        	
        	string urlStrr = '/apex/GenerateCertificate?id='+cert.id;
            PageReference pageRef = new PageReference(urlStrr);
            
            //Set url parameters			
            ApexPages.currentPage().getParameters().put('id', cert.Id);            
            Test.startTest();			
            CertificateDetailController controller = new CertificateDetailController(sc);
            PageReference pg = controller.generateCertificate();
            System.assertEquals(pageRef.getUrl(), pg.getURL());
            Test.stopTest();                
        }     
    }

    public static testMethod void returnHomeTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];
        cert.Speculative__c = true;
        Database.update(cert);
        
        System.runAs(runningUser){
        	ApexPages.StandardController sc = new ApexPages.StandardController(cert);
        	
        	string urlStrr = '/apex/DistributorLanding';
            PageReference pageRef = new PageReference(urlStrr);
            
            //Set url parameters			
            ApexPages.currentPage().getParameters().put('id', cert.Id);            
            Test.startTest();			
            CertificateDetailController controller = new CertificateDetailController(sc);
            PageReference pg = controller.returnHome();
            System.assertEquals(pageRef.getUrl(), pg.getURL());
            Test.stopTest();                
        }     
    }

    public static testMethod void backToPreviousTest(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1];
        cert.Speculative__c = true;
        Database.update(cert);
        
        System.runAs(runningUser){
        	ApexPages.StandardController sc = new ApexPages.StandardController(cert);
        	
        	string urlStrr = '/apex/paymentSuccessPage?id='+cert.id ;
            PageReference pageRef = new PageReference(urlStrr);
            
            //Set url parameters			
            ApexPages.currentPage().getParameters().put('id', cert.Id);            
            Test.startTest();			
            CertificateDetailController controller = new CertificateDetailController(sc);
            PageReference pg = controller.backToPrevious();
            System.assertEquals(pageRef.getUrl(), pg.getURL());
            Test.stopTest();                
        }     
    }
}