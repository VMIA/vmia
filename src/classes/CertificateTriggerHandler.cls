/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Handler Class for Certificate object trigger which includes context-specific methods 
  *                  that are automatically called when a trigger is executed.
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public with sharing class CertificateTriggerHandler{
    
    // Variables
    private static CertificateTriggerHelper helper;    // Helper class variable to call methods based on scenario
    private static CertificateTriggerHelperExtension helperExt;
    /**
      * @description       This method is invoked from trigger which in-turn calls the handlers
      *                     for before and after event.
      * @param             NA 
      * @return            Void
      * @throws            NA
      */        
    public static void execute(){
        helper = new CertificateTriggerHelper();   // instance of helper class for Case trigger
        helperExt = new CertificateTriggerHelperExtension(); 

        // Check for event type of trigger
        if(Trigger.isBefore){
            beforeHandler();        // invoke of before handler
        }
        else if(Trigger.isAfter){
            afterHandler();         // invoke of after handler
        }
    }
    
    /**
      * @description       This method is handler for before events invoked by execute method.
      *                     Method invokes the helper class methods based on the scenario.                       
      * @param             NA 
      * @return            Void
      * @throws            NA
      */  
    private static void beforeHandler(){
        // Check for type of operation
        if(Trigger.isInsert){
            // Methods to be invoked for before insert event
            helper.generatePolicyNumber(Trigger.new);   // generate policy number for certificates
            helper.assignToBATQueue(Trigger.new);  // assign to bat queue if created by distributor
        }
        else if(Trigger.isUpdate && CertificateTriggerHelper.isBeforeUpdateEventFirstRun()){
            // Methods to be invoked for before update event
            helper.createAmendCancelTaskForCertificate(Trigger.new,(Map<Id,Certificate__c>)Trigger.oldMap);
            helperExt.captureSnapshotFields((Map<Id,Certificate__c>) Trigger.newMap,(Map<Id,Certificate__c>) Trigger.oldMap);
           // helper.replenishConstructionLimit((Map<Id,Certificate__c>) Trigger.newMap,(Map<Id,Certificate__c>) Trigger.oldMap);
            helper.updateCertificateHistory((Map<Id,Certificate__c>) Trigger.newMap,(Map<Id,Certificate__c>) Trigger.oldMap);
            helper.createInvoiceForCertificate((Map<Id,Certificate__c>)Trigger.newMap,(Map<Id,Certificate__c>)Trigger.oldMap);
            helper.updateCertificateStatus((Map<Id,Certificate__c>) Trigger.newMap,(Map<Id,Certificate__c>) Trigger.oldMap);
        }
        else if(Trigger.isDelete){
            // Methods to be invoked for before update event
        }
    }
    
    /**
      * @description       This method is handler for after events invoked by execute method.
      *                     Method invokes the helper class methods based on the scenario.                          
      * @param             NA 
      * @return            Void
      * @throws            NA
      */  
    private static void afterHandler(){
        // Check for type of operation
        if(Trigger.isInsert){
            // Methods to be invoked for after insert event
            helper.shareCertificatesToDistributor(Trigger.new,null);
            helper.shareCertificatesToDirectors(Trigger.new);
            helper.createDistributorTask((Map<Id,Certificate__c>)Trigger.newMap,null);
            helper.calculateConstructionLimit((Map<Id,Certificate__c>) Trigger.newMap);
        }
        else if(Trigger.isUpdate && CertificateTriggerHelper.isAfterUpdateEventFirstRun()){
            // Methods to be invoked for after update event
            helper.updateChildCertificates((Map<Id,Certificate__c>) Trigger.newMap,(Map<Id,Certificate__c>) Trigger.oldMap);
            helper.shareCertificatesToDistributor(Trigger.new,(Map<Id,Certificate__c>)Trigger.oldMap);
            helper.shareCertificatesToDirectors(Trigger.new);
            helper.createDistributorTask((Map<Id,Certificate__c>)Trigger.newMap,(Map<Id,Certificate__c>)Trigger.oldMap);
        }
        else if(Trigger.isDelete){
            // Methods to be invoked for after delete event
        }
        else if(Trigger.isUndelete){
            // Methods to be invoked for after undelete event
        }
    }
}