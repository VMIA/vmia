/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Helper Class for Certificate trigger
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public without sharing class CertificateTriggerHelperExtension{

    /**
      * @description       This method is invoked from handler for before update event to capture snapshot fields for certificates when issued                      
      * @param             newCertMap , oldMap
      * @return            Void
      * @throws            Exception is handled by try-catch block
      */
    public void captureSnapshotFields(Map<Id,Certificate__c> newCertMap,Map<Id,Certificate__c> oldMap){
        try{
            Set<Id> certIds = new Set<Id>();
            for(Certificate__c cert : newCertMap.values()){
                System.debug('** cert status ==>'+cert.Status__c);
                Certificate__c oldCert = (oldMap != null) ? oldMap.get(cert.Id) : null; 
                if(oldCert != null && oldCert.Status__c != cert.Status__c 
                    && GlobalConstants.STAT_ISSUED_IN_PROGRESS.equalsIgnorecase(cert.Status__c)){

                    certIds.add(cert.Id);
                }
            }
            System.debug('** certIds ==>'+certIds);
            if(!certIds.isEmpty()){
                for(Certificate__c cert : [SELECT Distributor__r.Name,Distributor__r.BillingStreet,Distributor__r.BillingCity,
                                                Distributor__r.BillingState,Distributor__r.BillingPostalCode,Distributor__r.Website,
                                                Distributor__r.Phone,Policy_Number__c,Date_Issued__c,Contract_Price__c,Conditions__c,Premium__c,
                                                GST__c,StampDuty__c,Total__c,Category_of_Building_Works__c,Property_Address__c,Builder__r.Name,
                                                Builder__r.ACN__c,Builder__r.ABN__c,Builder__r.ShippingStreet,Builder__r.ShippingCity,Builder__r.ShippingState,
                                                Builder__r.ShippingPostalCode,Builder__r.ShippingCountry,Actual_Completion_Date__c,Defects_Inspector_Name__c,
                                                Defects_Inspection_Report_Date__c,Original_COI__c,Original_COI__r.Date_Issued__c,Unit__c,
                                                
                                                (SELECT Contact__c,Property_Owner__c,Contact__r.IsPersonAccount,Contact__r.MailingStreet,
                                                    Contact__r.MailingCity,Contact__r.MailingState,Contact__r.MailingPostalCode,Contact__r.MailingCountry,
                                                    Contact__r.Account.Shippingstreet,Contact__r.Account.ShippingCity,Contact__r.Account.ShippingState,
                                                    Contact__r.Account.ShippingPostalCode,Contact__r.Account.ShippingCountry,
                                                    Contact__r.Account.BillingStreet,Contact__r.Account.BillingCity,Contact__r.Account.BillingState,
                                                    Contact__r.Account.BillingPostalCode,Contact__r.Account.BillingCountry,Contact__r.Account.ACN__c,
                                                    Contact__r.Account.Name,Contact__r.Name,
                                                    Property_Owner__r.IsPersonAccount,Property_Owner__r.Name,Property_Owner__r.ACN__c,
                                                    Property_Owner__r.PersonMailingStreet,Property_Owner__r.PersonMailingState,Property_Owner__r.PersonMailingCity,
                                                    Property_Owner__r.PersonMailingCountry,Property_Owner__r.PersonMailingPostalCode,Property_Owner__r.ShippingStreet,
                                                    Property_Owner__r.ShippingCity,Property_Owner__r.ShippingState,Property_Owner__r.ShippingPostalCode,
                                                    Property_Owner__r.ShippingCountry, Property_Owner__r.BillingStreet,Property_Owner__r.BillingCity,
                                                    Property_Owner__r.BillingState,Property_Owner__r.BillingCountry,Property_Owner__r.BillingPostalCode
                                                    FROM PropertyOwners__r)
                                            FROM Certificate__c WHERE Id IN: certIds ]){
                    System.debug('** cert ==>'+cert);
                    Certificate__c newCert = newCertMap.get(cert.Id);
                    // map snapshot fields when COI is issued for COI pdf
                    newCert.PIT_Distributor_Name__c =  cert.Distributor__r.Name;
                    if(String.isNotBlank(cert.Distributor__r.BillingStreet)){
                        newCert.PIT_Distributor_Address__c = cert.Distributor__r.BillingStreet + GlobalConstants.EMPTY_SPACE ;
                    }
                    newCert.PIT_Distributor_Address__c = String.isNotBlank(cert.Distributor__r.BillingCity) ? 
                                                            (newCert.PIT_Distributor_Address__c + GlobalConstants.LINE_BREAK + cert.Distributor__r.BillingCity) : newCert.PIT_Distributor_Address__c;

                    newCert.PIT_Distributor_Address__c = String.isNotBlank(cert.Distributor__r.BillingState) ? 
                                                            (newCert.PIT_Distributor_Address__c + GlobalConstants.EMPTY_SPACE + cert.Distributor__r.BillingState) : newCert.PIT_Distributor_Address__c;

                    newCert.PIT_Distributor_Address__c = String.isNotBlank(cert.Distributor__r.BillingPostalCode) ?   
                                                            (newCert.PIT_Distributor_Address__c + GlobalConstants.EMPTY_SPACE + cert.Distributor__r.BillingPostalCode)  : newCert.PIT_Distributor_Address__c;                                    
                                                            
                    newCert.PIT_Distributor_Website__c =  cert.Distributor__r.Website;
                    newCert.PIT_Distributor_Phone__c =  cert.Distributor__r.Phone;
                    newCert.PIT_Policy_Number__c =  cert.Policy_Number__c;
                    //newCert.PIT_Policy_Inception_Date__c =  cert.Date_Issued__c;
                    newCert.PIT_Domestic_Building_Work__c =  cert.Category_of_Building_Works__c;
                    newCert.PIT_At_the_Property__c =  cert.Property_Address__c;
                    newCert.PIT_Builder_Name__c =  cert.Builder__r.Name;
                    newCert.PIT_Builder_ACN__c =  cert.Builder__r.ACN__c;
                    newCert.PIT_Builder_ABN__c =  cert.Builder__r.ABN__c;
                    newCert.PIT_Contract_Price__c =  cert.Contract_Price__c;
                    newCert.PIT_Special_Conditions__c =  cert.Conditions__c;
                    newCert.PIT_Base_DBI_Premium__c =  cert.Premium__c;
                    newCert.PIT_GST__c =  cert.GST__c;
                    newCert.PIT_Stamp_Duty__c =  cert.StampDuty__c;
                    newCert.PIT_Total__c =  cert.Total__c;
                    newCert.PIT_Actual_Completion_Date__c =  cert.Actual_Completion_Date__c;
                    newCert.PIT_Defects_Inspector_Name__c =  cert.Defects_Inspector_Name__c;
                    newCert.PIT_Defects_Inspection_Report_Date__c =  cert.Defects_Inspection_Report_Date__c;
                    newCert.PIT_Unit__c = cert.Unit__c ;
                    if(newCert.Original_COI__c != null){
                        newCert.Date_Issued__c = cert.Original_COI__r.Date_Issued__c;
                        newCert.PIT_Policy_Inception_Date__c = cert.Original_COI__r.Date_Issued__c;
                    }

                    capturePropertyOwnerSnapshot(newCert,cert,cert.PropertyOwners__r);
                    System.debug('** newCert ==>'+newCert);
                }
                SuccessinvoiceDetailController.SendWelcomeLetter(JSON.serialize(newCertMap.values()));
            }
        } 
        catch(Exception exp){
            // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(CertificateTriggerHelper.class),
                                GlobalConstants.FN_CREATE_SNAPSHOT,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }

    /**
      * @description       This method is invoked from captureSnapshotFields method to capture snapshot of first 3 property owners
      * @param             newCert , propertyOwners
      * @return            Void
      * @throws            Exception is handled by invoked method
      */
    private void capturePropertyOwnerSnapshot(Certificate__c newCert,Certificate__c certFromDB,List<Property_Owners__c> propertyOwners){
        String ownerAddress;
        String ownerName;

        if(!newCert.Speculative__c){
            newCert.PIT_Property_Owner_Count__c = propertyOwners.size();
            for(Integer i=0;i<propertyOwners.size();i++){
                if(i < 3){
                    Property_Owners__c pOwner = propertyOwners[i];
                    if(pOwner.Contact__c != null){
                        if((String.isNotBlank(pOwner.Contact__r.MailingStreet) 
                            || String.isNotBlank(pOwner.Contact__r.MailingCity) || String.isNotBlank(pOwner.Contact__r.MailingState) 
                            || String.isNotBlank(pOwner.Contact__r.MailingCountry) || String.isNotBlank(pOwner.Contact__r.MailingPostalCode))){
                            
                            ownerAddress = pOwner.Contact__r.MailingStreet + GlobalConstants.LINE_BREAK + pOwner.Contact__r.MailingCity + GlobalConstants.LINE_BREAK + pOwner.Contact__r.MailingState +
                                            GlobalConstants.EMPTY_SPACE + pOwner.Contact__r.MailingPostalCode;
                            
                            if(String.isNotBlank(pOwner.Contact__r.MailingCountry) && !pOwner.Contact__r.MailingCountry.equalsIgnoreCase(GlobalConstants.AUSTRALIA)){
                                ownerAddress += GlobalConstants.LINE_BREAK +  pOwner.Contact__r.MailingCountry;
                            } 
                        }

                        if(String.isBlank(ownerAddress) && pOwner.Contact__r.IsPersonAccount && (String.isNotBlank(pOwner.Contact__r.Account.ShippingStreet) 
                            || String.isNotBlank(pOwner.Contact__r.Account.ShippingCity) || String.isNotBlank(pOwner.Contact__r.Account.ShippingState) 
                            || String.isNotBlank(pOwner.Contact__r.Account.ShippingCountry) || String.isNotBlank(pOwner.Contact__r.Account.ShippingPostalCode))){
                            
                            ownerAddress = pOwner.Contact__r.Account.ShippingStreet + GlobalConstants.LINE_BREAK + pOwner.Contact__r.Account.ShippingCity + GlobalConstants.LINE_BREAK 
                                            + pOwner.Contact__r.Account.ShippingState + GlobalConstants.EMPTY_SPACE + pOwner.Contact__r.Account.ShippingPostalCode;
                            
                            if(String.isNotBlank(pOwner.Contact__r.Account.ShippingCountry) && !pOwner.Contact__r.Account.ShippingCountry.equalsIgnoreCase(GlobalConstants.AUSTRALIA)){
                                ownerAddress += GlobalConstants.LINE_BREAK +  pOwner.Contact__r.Account.ShippingCountry;
                            }   
                        }

                        if(String.isBlank(ownerAddress) && pOwner.Contact__r.IsPersonAccount && (String.isNotBlank(pOwner.Contact__r.Account.BillingStreet) 
                            || String.isNotBlank(pOwner.Contact__r.Account.BillingCity) || String.isNotBlank(pOwner.Contact__r.Account.BillingState) 
                            || String.isNotBlank(pOwner.Contact__r.Account.BillingCountry) || String.isNotBlank(pOwner.Contact__r.Account.BillingPostalCode))){
                            
                            ownerAddress = pOwner.Contact__r.Account.BillingStreet + GlobalConstants.LINE_BREAK + pOwner.Contact__r.Account.BillingCity + GlobalConstants.LINE_BREAK 
                                            + pOwner.Contact__r.Account.BillingState + GlobalConstants.EMPTY_SPACE + pOwner.Contact__r.Account.BillingPostalCode;
                            
                            if(String.isNotBlank(pOwner.Contact__r.Account.BillingCountry) && !pOwner.Contact__r.Account.BillingCountry.equalsIgnoreCase(GlobalConstants.AUSTRALIA)){
                                ownerAddress += GlobalConstants.LINE_BREAK +  pOwner.Contact__r.Account.BillingCountry;
                            }  
                        }

                        ownerName = pOwner.Contact__r.Name;
                    }

                    if(pOwner.Property_Owner__c != null){
                        if(pOwner.Property_Owner__r.IsPersonAccount && (String.isNotBlank(pOwner.Property_Owner__r.PersonMailingStreet) 
                            || String.isNotBlank(pOwner.Property_Owner__r.PersonMailingCity) || String.isNotBlank(pOwner.Property_Owner__r.PersonMailingState) 
                            || String.isNotBlank(pOwner.Property_Owner__r.PersonMailingCountry) || String.isNotBlank(pOwner.Property_Owner__r.PersonMailingPostalCode))){
                            
                            ownerAddress = pOwner.Property_Owner__r.PersonMailingStreet + GlobalConstants.LINE_BREAK + pOwner.Property_Owner__r.PersonMailingCity + GlobalConstants.LINE_BREAK 
                                            + pOwner.Property_Owner__r.PersonMailingState + GlobalConstants.EMPTY_SPACE + pOwner.Property_Owner__r.PersonMailingPostalCode;
                            
                            if(String.isNotBlank(pOwner.Property_Owner__r.PersonMailingCountry) && !pOwner.Property_Owner__r.PersonMailingCountry.equalsIgnoreCase(GlobalConstants.AUSTRALIA)){
                                ownerAddress += GlobalConstants.LINE_BREAK +  pOwner.Property_Owner__r.PersonMailingCountry;
                            }  
                        }
                        
                        if(String.isBlank(ownerAddress) && (String.isNotBlank(pOwner.Property_Owner__r.ShippingStreet) 
                            || String.isNotBlank(pOwner.Property_Owner__r.ShippingCity) || String.isNotBlank(pOwner.Property_Owner__r.ShippingState) 
                            || String.isNotBlank(pOwner.Property_Owner__r.ShippingCountry) || String.isNotBlank(pOwner.Property_Owner__r.ShippingPostalCode))){
                            
                            ownerAddress = pOwner.Property_Owner__r.ShippingStreet + GlobalConstants.LINE_BREAK + pOwner.Property_Owner__r.ShippingCity + GlobalConstants.LINE_BREAK 
                                            + pOwner.Property_Owner__r.ShippingState + GlobalConstants.EMPTY_SPACE + pOwner.Property_Owner__r.ShippingPostalCode; 
                            
                            if(String.isNotBlank(pOwner.Property_Owner__r.ShippingCountry) && !pOwner.Property_Owner__r.ShippingCountry.equalsIgnoreCase(GlobalConstants.AUSTRALIA)){
                                ownerAddress += GlobalConstants.LINE_BREAK +  pOwner.Property_Owner__r.ShippingCountry;
                            } 
                        }
                        
                        if(String.isBlank(ownerAddress) && (String.isNotBlank(pOwner.Property_Owner__r.BillingStreet) 
                            || String.isNotBlank(pOwner.Property_Owner__r.BillingCity) || String.isNotBlank(pOwner.Property_Owner__r.BillingState) 
                            || String.isNotBlank(pOwner.Property_Owner__r.BillingCountry) || String.isNotBlank(pOwner.Property_Owner__r.BillingPostalCode))){
                            
                            ownerAddress = pOwner.Property_Owner__r.BillingStreet + GlobalConstants.LINE_BREAK + pOwner.Property_Owner__r.BillingCity + GlobalConstants.LINE_BREAK 
                                            + pOwner.Property_Owner__r.BillingState + GlobalConstants.EMPTY_SPACE + pOwner.Property_Owner__r.BillingPostalCode;
                                            
                            if(String.isNotBlank(pOwner.Property_Owner__r.BillingCountry) && !pOwner.Property_Owner__r.BillingCountry.equalsIgnoreCase(GlobalConstants.AUSTRALIA)){
                                ownerAddress += GlobalConstants.LINE_BREAK +  pOwner.Property_Owner__r.BillingCountry;
                            } 
                                            
                        }
                            
						System.debug('+++++++++++++ Check 1001 : ' + pOwner.Property_Owner__r.Name);
						System.debug('+++++++++++++ Check 1002 : ' + ownerAddress);
                        ownerName = pOwner.Property_Owner__r.Name;  
                    }

                    if(i == 0){
                        newCert.PIT_First_Owner_Name__c =  ownerName;
                        newCert.PIT_First_Owner_Address__c =  ownerAddress;

                        if(GlobalConstants.CERT_TYPE_OBCERT.equalsIgnorecase(newCert.Certificate_Type__c)){
                            newCert.PIT_Builder_ACN__c = !String.isBlank(pOwner.Contact__r.Account.ACN__c) ? pOwner.Contact__r.Account.ACN__c :
                                                            !String.isBlank(pOwner.Property_Owner__r.ACN__c) ? 
                                                                pOwner.Property_Owner__r.ACN__c : newCert.PIT_Builder_ACN__c;
                        }

                    }
                    else if(i == 1){
                        newCert.PIT_Second_Owner_Name__c =  ownerName;
                        newCert.PIT_Second_Owner_Address__c =  ownerAddress;
                    }
                    else if(i == 2){
                        newCert.PIT_Third_Owner_Name__c =  ownerName;
                        newCert.PIT_Third_Owner_Address__c =  ownerAddress;
                    }
                }
                else{
                    break;
                }
            }
        }
        else{
        
            System.debug('** certFromDB ==>'+ certFromDB);
            ownerAddress = certFromDB.Builder__r.ShippingStreet + GlobalConstants.LINE_BREAK + certFromDB.Builder__r.ShippingCity + GlobalConstants.LINE_BREAK + certFromDB.Builder__r.ShippingState 
                                + GlobalConstants.EMPTY_SPACE + certFromDB.Builder__r.ShippingPostalCode;
            
            if(String.isNotBlank(certFromDB.Builder__r.ShippingCountry) && !certFromDB.Builder__r.ShippingCountry.equalsIgnoreCase(GlobalConstants.AUSTRALIA)){
                ownerAddress += GlobalConstants.LINE_BREAK + certFromDB.Builder__r.ShippingCountry;
            }
            System.debug('** ownerAddress ==>'+ ownerAddress);
            ownerAddress = ownerAddress.replace(GlobalConstants.NULL_VAL, GlobalConstants.WHITE_SPACE);
            ownerAddress = ownerAddress.replace(GlobalConstants.LINE_BREAK+GlobalConstants.LINE_BREAK, GlobalConstants.WHITE_SPACE);
            System.debug('** ownerAddress ==>'+ ownerAddress);
            System.debug('** certFromDB.Builder__r.Name ==>'+ certFromDB.Builder__r.Name);
            newCert.PIT_First_Owner_Name__c =  certFromDB.Builder__r.Name;
            newCert.PIT_First_Owner_Address__c =  ownerAddress;
            
        }
    }


}