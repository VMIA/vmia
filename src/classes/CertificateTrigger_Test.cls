/**
  * Date         :  16-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for Certificate trigger
  */ 
  
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
private with sharing class CertificateTrigger_Test{
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> certRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> invRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup(){

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);

        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,
                                                        conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);

    }

    @future
    private static void setupUser(){
        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(new List<User>{vmiaInternalUser,rexUser});
    }
    
    /**
      * @description       This method tests scenario of policy number generation as a distributor
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testGeneratePolicyNumberDistirbutor(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
            bornBuilder.Agent__c = rexSmith.AccountId;
            bornBuilder.Agent_Contact__c = rexSmith.Id;
            Database.insert(bornBuilder);

            System.debug('** bornBuilder ==>'+ bornBuilder);

            Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                    certRecTypeMap.get('Certificate').getRecordTypeId());
            
            Test.startTest();
                Database.insert(cert);
            Test.stopTest();
            Certificate__c certAfterInsert = [SELECT Policy_Number__c FROM Certificate__c LIMIT 1];
            System.assert(certAfterInsert.Policy_Number__c != null);
        }
    }

    /**
      * @description       This method tests scenario of amending COI for invoice creation
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testInvoiceCreation(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);

        System.debug('** bornBuilder ==>'+ bornBuilder);

        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        cert.Status__c = GlobalConstants.STAT_REFERRED;
        Database.insert(cert);

        Invoice__c inv = TestUtility.createInvoice(bornBuilder.Id,'New',invRecTypeMap.get('New Invoice').getRecordTypeId());
        Database.insert(inv);

        cert.InvoiceNo__c = inv.Id;
        cert.Type__c = 'Amend COI';
        Database.update(cert);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];

        System.runAs(runningUser){
            Test.startTest();
                cert.Status__c = GlobalConstants.SUB_STAT_REFUND;
                Database.update(cert);
            Test.stopTest();
        }
        Certificate__c certAfterUpdate = [SELECT InvoiceNo__r.Parent_Invoice__c FROM Certificate__c WHERE Id =: cert.Id LIMIT 1];
        if(certAfterUpdate.InvoiceNo__r.Parent_Invoice__c != null){
            System.assertEquals(inv.Id, certAfterUpdate.InvoiceNo__r.Parent_Invoice__c);
        }
    }

    /**
      * @description       This method tests scenario of assigning referred certificate to BAT Queue as a distributor
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testAssignToBATQueueDistirbutor(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
            bornBuilder.Agent__c = rexSmith.AccountId;
            bornBuilder.Agent_Contact__c = rexSmith.Id;
            Database.insert(bornBuilder);

            System.debug('** bornBuilder ==>'+ bornBuilder);

            Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                    certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
            cert.Status__c = GlobalConstants.STAT_REFERRED;
            Test.startTest();
                Database.insert(cert);
            Test.stopTest();
            Certificate__c certAfterInsert = [SELECT OwnerId FROM Certificate__c LIMIT 1];
            System.assert(certAfterInsert.OwnerId != rexUser.Id);
            Id batQueue = [SELECT Id FROM Group WHERE Type = 'Queue' AND Name =: GlobalConstants.BAT_QUEUE LIMIT 1].Id;
            System.assertEquals(batQueue,certAfterInsert.OwnerId);
        }
    }

    /**
      * @description       This method tests scenario of updating certificate owner
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testUpdateCertOwner(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);

        System.debug('** bornBuilder ==>'+ bornBuilder);

        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        cert.Sub_Status__c = GlobalConstants.SUB_STAT_COI_APPROVED;
        Database.insert(cert);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        Id batQueue = [SELECT Id FROM Group WHERE Type = 'Queue' AND Name =: GlobalConstants.BAT_QUEUE LIMIT 1].Id;

        System.runAs(runningUser){
            Test.startTest();
                cert.OwnerId = batQueue;
                Database.update(cert);
            Test.stopTest();
            Integer certShareCount = [SELECT count() FROM Certificate__share 
                                        WHERE RowCause =: Schema.Certificate__share.RowCause.DistributorAccess__c AND ParentId =: cert.Id];
            System.assertEquals(1, certShareCount);
        }
    }

    /**
      * @description       This method tests scenario of updating parent certificate to approved
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testUpdateParentCertStatusApproved(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);

        System.debug('** bornBuilder ==>'+ bornBuilder);

        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        Database.insert(cert);

        Certificate__c childCert = cert.clone();
        childCert.Parent_COI__c = cert.Id;
        Database.insert(childCert);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];

        System.runAs(runningUser){
            Test.startTest();
                cert.Status__c = GlobalConstants.STAT_MULTI_APPROVED;
                Database.update(cert);
            Test.stopTest();
            Certificate__c childCertAfterUpdate = [SELECT Status__c FROM Certificate__c WHERE Id =: childCert.Id LIMIT 1];
            System.assertEquals(GlobalConstants.STAT_AWAITING_PAYMENT, childCertAfterUpdate.Status__c);
        }
    }

    /**
      * @description       This method tests scenario of updating parent certificate to rejected
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testUpdateParentCertStatusRejected(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);

        System.debug('** bornBuilder ==>'+ bornBuilder);

        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        Database.insert(cert);

        Certificate__c childCert = cert.clone();
        childCert.Parent_COI__c = cert.Id;
        Database.insert(childCert);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];

        System.runAs(runningUser){
            Test.startTest();
                cert.Status__c = GlobalConstants.STAT_REJECTED;
                Database.update(cert);
            Test.stopTest();
            Certificate__c childCertAfterUpdate = [SELECT Status__c FROM Certificate__c WHERE Id =: childCert.Id LIMIT 1];
            System.assertEquals(cert.Status__c, childCertAfterUpdate.Status__c);
        }
    }

    /**
      * @description       This method tests scenario of updating certificate status to approved as a distributor
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testUpdateCertStatusNoContractPrice(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);

        System.debug('** bornBuilder ==>'+ bornBuilder);

        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        cert.Status__c = GlobalConstants.STAT_REFERRED;
        Database.insert(cert);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            Test.startTest();
                cert.Premium__c = 0;
                cert.Sub_Status__c = GlobalConstants.SUB_STAT_COI_APPROVED;
                try{
                    Database.update(cert);
                }
                catch(Exception exp){
                    System.assert(exp.getMessage().contains('Please ensure Contract Value and Premium Value are not $0'));
                }
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of updating certificate status to rejected as a distributor
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testUpdateCertStatusRejectedDistirbutor(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);

        System.debug('** bornBuilder ==>'+ bornBuilder);

        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        cert.Status__c = GlobalConstants.STAT_REFERRED;
        Database.insert(cert);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            Test.startTest();
                cert.Premium__c = 0;
                cert.Sub_Status__c = GlobalConstants.SUB_STAT_COI_REJECTED;
                Database.update(cert);
            Test.stopTest();
            Certificate__c certAfterUpdate = [SELECT Status__c FROM Certificate__c LIMIT 1];
            System.assertEquals(GlobalConstants.STAT_REJECTED,certAfterUpdate.Status__c);
        }
    }

    /**
      * @description       This method tests scenario of updating certificate status to rejected as a distributor
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testUpdateApprovedCertStatusDistirbutor(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);

        System.debug('** bornBuilder ==>'+ bornBuilder);

        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        cert.Sub_Status__c = GlobalConstants.SUB_STAT_COI_APPROVED;
        Database.insert(cert);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            Test.startTest();
                cert.Premium__c = 0;
                cert.Sub_Status__c = 'New Information Received';
                Database.update(cert);
            Test.stopTest();
            Certificate__c certAfterUpdate = [SELECT Status__c FROM Certificate__c LIMIT 1];
            System.assertEquals(GlobalConstants.STAT_REFERRED,certAfterUpdate.Status__c);
        }
    }

    /**
      * @description       This method tests scenario of updating certificate status to rejected as a distributor
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testUpdateCertStatusAwaitingPaymentDistirbutor(){
        System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);

        System.debug('** bornBuilder ==>'+ bornBuilder);

        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        cert.Sub_Status__c = GlobalConstants.SUB_STAT_COI_APPROVED;
        Database.insert(cert);

        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            Test.startTest();
                cert.Premium__c = 0;
                cert.Status__c = GlobalConstants.STAT_AWAITING_PAYMENT;
                try{
                    Database.update(cert);
                }
                catch(Exception exp){
                    System.assert(exp.getMessage().contains('Please ensure Contract Value and Premium Value are not $0'));
                }
            Test.stopTest();
        }
    }

    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testGeneratePolicyNumberExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            CertificateTriggerHelper helper = new CertificateTriggerHelper();
            Test.startTest();
                helper.generatePolicyNumber(null);
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }

    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testAssignToBATQueueExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            CertificateTriggerHelper helper = new CertificateTriggerHelper();
            Test.startTest();
                helper.assignToBATQueue(null);
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }

    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testUpdateCertStatusExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            CertificateTriggerHelper helper = new CertificateTriggerHelper();
            Test.startTest();
                helper.updateCertificateStatus(null,new Map<Id,Certificate__c>{null => null});
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }

    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testUpdateChildCertExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            CertificateTriggerHelper helper = new CertificateTriggerHelper();
            Test.startTest();
                helper.updateChildCertificates(new Map<Id,Certificate__c>{null => null},new Map<Id,Certificate__c>{null => null});
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }

    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testCalculateConstructionLimitExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            CertificateTriggerHelper helper = new CertificateTriggerHelper();
            Test.startTest();
                helper.calculateConstructionLimit(new Map<Id,Certificate__c>{null => null});
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }

    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testCreateInvoiceForCertificateExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            CertificateTriggerHelper helper = new CertificateTriggerHelper();
            Test.startTest();
                helper.createInvoiceForCertificate(new Map<Id,Certificate__c>{null => null},new Map<Id,Certificate__c>{null => null});
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }

    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testCreateDistributorTaskExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            CertificateTriggerHelper helper = new CertificateTriggerHelper();
            Test.startTest();
                helper.createDistributorTask(new Map<Id,Certificate__c>{null => null},new Map<Id,Certificate__c>{null => null});
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }

    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testCertificateHistoryExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            CertificateTriggerHelper helper = new CertificateTriggerHelper();
            Test.startTest();
                helper.updateCertificateHistory(new Map<Id,Certificate__c>{null => null},new Map<Id,Certificate__c>{null => null});
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }

    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testReplenishConstructionLimitExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            CertificateTriggerHelper helper = new CertificateTriggerHelper();
            Test.startTest();
                helper.replenishConstructionLimit(new Map<Id,Certificate__c>{null => null},new Map<Id,Certificate__c>{null => null});
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }

    /**
      * @description       Test method to validate deletion and undelete of Certificate
      *                     Scenario is not part of requirement, created for code coverage purpose only
      * @param             NA
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testCertificateDeleteUndelete(){
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
            bornBuilder.Agent__c = rexSmith.AccountId;
            bornBuilder.Agent_Contact__c = rexSmith.Id;
            Database.insert(bornBuilder);

            System.debug('** bornBuilder ==>'+ bornBuilder);

            Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                    certRecTypeMap.get('Certificate').getRecordTypeId());
            Database.insert(cert);
            Database.delete(cert);
            Test.startTest();
                Database.undelete(cert);
            Test.stopTest();
        }
    }
}