/**
  * Date         :  14-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Apex class to share access on certificates & applications to distributor
  *                 Sharing of applications or certificates with distributor fails if the volume 
                    is more than 10K in a transaction due to governor limit as of Spring 17 release.
  *                 Class is without sharing as the execution of this is by community users & AccessLevel is not
  *                 writeable for the community profile.
  */
public without sharing class CommonDirectorAccessController{
    
    //constants
    private static final String EDIT_ACCESS = 'Edit';
    private static final String READ_ACCESS = 'Read';
    private static final String ACCESS_LEVEL = 'AccessLevel';
    private static final String MANUAL_ROW_CAUSE = 'Manual';
    private static final String COMMUNITY_PLUS_USER_TYPE = 'PowerCustomerSuccess';
    private static final String REC_TYPE_DISTRIBUTOR = 'Distributor';
    private static final String SHARE_ERROR_MSG = 'Unable to grant sharing access due to following exception: ';
    private static final list<profile> directorProfile = [Select id from profile where name =: Constants.Common_RBP];
    
    /**
      * @description       This method is invoked from Contact Role trigger 
      * @param             Map<id,List<Certificate__c>> directorWithCertificatesMap
      * @return            void
      * @throws            Method might throw exception which is handled by try-catch block
      */ 
    public static void shareCertificatesWithCommonDirector(Map<id,List<Certificate__c>> directorWithCertificatesMap ){
    
        try{
            Set<Id> distributorIds  = new Set<Id>();
                      
            
            for(id accId : directorWithCertificatesMap.keySet()){
                distributorIds.add(accId);                
            }

            if(!distributorIds.isEmpty()){
                Set<Id> userRoleIds = new Set<Id>();
                Map<Id,String> distributorRoleMap = new Map<Id,String>();
                for(User distributor : [SELECT contactId,UserRoleId,UserRole.DeveloperName FROM User WHERE contactId IN: distributorIds AND profileId =: directorProfile[0].Id]){
                    userRoleIds.add(distributor.UserRoleId);
                    distributorRoleMap.put(distributor.contactId,distributor.UserRole.DeveloperName);
                }

                system.debug('distributorRoleMap<>'+distributorRoleMap);
                
                 if(!userRoleIds.isEmpty()){
                    Map<String,Id> roleGroupMap = new Map<String,Id>();
                    for(Group grp : [SELECT Name,DeveloperName,RelatedId,Type 
                                        FROM Group WHERE Type =: GlobalConstants.ROLE_AND_SUBORDINATES AND
                                        RelatedId IN: userRoleIds]){
                        roleGroupMap.put(grp.DeveloperName,grp.Id);
                    }

                 system.debug('roleGroupMap<>'+roleGroupMap);    
                    List<Certificate__Share> certShareList = new List<Certificate__Share>();
                    for(id accId : directorWithCertificatesMap.keySet()){

                        for(Certificate__c cert : directorWithCertificatesMap.get(accId)){
                            
                            Certificate__Share certShare = new Certificate__Share(ParentId = cert.Id,AccessLevel = EDIT_ACCESS,
                                                                                    RowCause = Schema.Certificate__share.RowCause.CommonDirectorAccess__c);
                            Id userOrGroupIdVal = roleGroupMap.get(distributorRoleMap.get(accId));
                            if(userOrGroupIdVal != null ){
                                certShare.UserOrGroupId = userOrGroupIdVal;
                                certShareList.add(certShare);
                            } 
                        }
                    }
                system.debug('certShareList<>'+certShareList); 
                    if(!certShareList.isEmpty()){
                        Database.insert(certShareList);
                    }
                }
            }
            
        }
        catch(Exception exp){
             Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(CommonDirectorAccessController.class),
                                'shareCertificatesWithCommonDirector',null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }

    /**
      * @description       This method is invoked from ContactRole trigger 
      * @param             Map<id,List<Application__c>> directorWithApplicaationsMap
      * @return            void
      * @throws            Method might throw exception which is handled by try-catch block
      */ 
    public static void shareApplicationsWithCommonDirector(Map<id,List<Application__c>> directorWithApplicaationsMap){
        
        try{
            Set<Id> distributorIds  = new Set<Id>();                 
            
            for(id accId : directorWithApplicaationsMap.keySet()){
                distributorIds.add(accId);              
            }

            if(!distributorIds.isEmpty()){
                Set<Id> userRoleIds = new Set<Id>();
                Map<Id,String> distributorRoleMap = new Map<Id,String>();
                for(User distributor : [SELECT contactId,UserRoleId,UserRole.DeveloperName FROM User WHERE contactId IN: distributorIds AND profileId =: directorProfile[0].Id] ){
                    userRoleIds.add(distributor.UserRoleId);
                    distributorRoleMap.put(distributor.contactId,distributor.UserRole.DeveloperName);
                }
                
                 if(!userRoleIds.isEmpty()){
                    Map<String,Id> roleGroupMap = new Map<String,Id>();
                    for(Group grp : [SELECT Name,DeveloperName,RelatedId,Type 
                                        FROM Group WHERE Type =: GlobalConstants.ROLE_AND_SUBORDINATES AND
                                        RelatedId IN: userRoleIds]){
                        roleGroupMap.put(grp.DeveloperName,grp.Id);
                    }
                    List<Application__Share> appShareList = new List<Application__Share>();
                    for(id accId : directorWithApplicaationsMap.keySet()){
                        for(Application__c app : directorWithApplicaationsMap.get(accId)){
                            Application__Share appShare = new Application__Share(ParentId = app.Id,AccessLevel = EDIT_ACCESS,
                                                                                    RowCause = Schema.Application__share.RowCause.CommonDirectorAccess__c);
                            Id userOrGroupIdVal = roleGroupMap.get(distributorRoleMap.get(accId));
                            if(userOrGroupIdVal != null ){
                                appShare.UserOrGroupId = userOrGroupIdVal;
                                appShareList.add(appShare);
                            } 
                        }
                    }    
                    System.debug('appShareList<>'+appShareList);
                    if(!appShareList.isEmpty()){
                        Database.insert(appShareList);
                    }
                }
            }
            
        }
        catch(Exception exp){
             Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(CommonDirectorAccessController.class),
                                'shareApplicationsWithCommonDirector',null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }

    /**
      * @description       This method is invoked from Contact Role trigger to 
      *                    share invoice with Distributors 
      * @param             Map<id,List<Invoice__c>> directorWithinvoicesMap 
      * @return            void
      * @throws            Method might throw exception which is handled by try-catch block
      */ 
    public static void shareInvoiceWithCommonDirector(Map<id,List<id>> directorWithinvoicesMap){
        
        Set<Id> distributorIds = new Set<Id>();      
        
        try{
            
            for(id accId : directorWithinvoicesMap.keySet()){
                distributorIds.add(accId);                
            }

            if(!distributorIds.isEmpty()){
                Set<Id> userRoleIds = new Set<Id>();
                Map<Id,String> distributorRoleMap = new Map<Id,String>();
                for(User distributor : [SELECT contactId,UserRoleId,UserRole.DeveloperName FROM User WHERE contactId IN: distributorIds AND profileId =: directorProfile[0].Id]){
                    userRoleIds.add(distributor.UserRoleId);
                    distributorRoleMap.put(distributor.contactId,distributor.UserRole.DeveloperName);
                }
                
                 if(!userRoleIds.isEmpty()){
                    Map<String,Id> roleGroupMap = new Map<String,Id>();
                    for(Group grp : [SELECT Name,DeveloperName,RelatedId,Type 
                                        FROM Group WHERE Type =: GlobalConstants.ROLE_AND_SUBORDINATES AND
                                        RelatedId IN: userRoleIds]){
                        roleGroupMap.put(grp.DeveloperName,grp.Id);
                    }
                    List<Invoice__share> invShareList = new List<Invoice__share>();
                    for(id accId : directorWithinvoicesMap.keySet()){
                        for(id app : directorWithinvoicesMap.get(accId)){
                            Invoice__share appShare = new Invoice__share(ParentId = app,AccessLevel = EDIT_ACCESS,
                                                                                    RowCause = Schema.Invoice__share.RowCause.CommonDirectorAccess__c);
                            Id userOrGroupIdVal = roleGroupMap.get(distributorRoleMap.get(accId));
                            if(userOrGroupIdVal != null ){
                                appShare.UserOrGroupId = userOrGroupIdVal;
                                invShareList.add(appShare);
                            } 
                        }
                    }    
                    
                    System.debug('invShareList<>'+invShareList);
                    if(!invShareList.isEmpty()){
                        Database.insert(invShareList);
                    }
                }
            }   
            
        }
        catch(Exception exp){
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(CommonDirectorAccessController.class),
                                'shareInvoiceWithCommonDirector',null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }


    /**
      * @description       This method is invoked from Contact Role trigger to 
      *                    share invoice with Distributors staff
      * @param             invoiceList - List of invoices to be shared 
      * @return            void
      * @throws            Method might throw exception which is handled by try-catch block
      */ 
        public static void shareBuilderWithCommonDirector(Map<id,List<id>> builderWithDirectorsMap){

            set<Id> directorIdSet = new Set<Id>();
            for(id builderId : builderWithDirectorsMap.keySet()){
                for(id directorId : builderWithDirectorsMap.get(builderId)){
                    directorIdSet.add(directorId);
                }
            }

            System.debug('directorIdSet<>>'+directorIdSet);
            Map<id,id> direcotorWithUserMap = new  Map<id,id>();

            for(user u : [Select id,contactId from User where contactId IN : directorIdSet AND profileId =: directorProfile[0].Id]){
                direcotorWithUserMap.put(u.contactId,u.Id);
            }
            System.debug('direcotorWithUserMap<>>'+direcotorWithUserMap);
            List<AccountShare> accShareList = new List<AccountShare>();
            for(id builderId : builderWithDirectorsMap.keySet()){
                for(id directorId : builderWithDirectorsMap.get(builderId)){
                    
                    if(direcotorWithUserMap.containsKey(directorId)){
                        id directorUserId = direcotorWithUserMap.get(directorId);

                        AccountShare accShare = new AccountShare(AccountId = builderId,AccountAccessLevel = EDIT_ACCESS,
                                                                    OpportunityAccessLevel = READ_ACCESS, CaseAccessLevel = READ_ACCESS, 
                                                                    RowCause = MANUAL_ROW_CAUSE,UserOrGroupId = directorUserId);

                        accShareList.add(accShare);

                    }

                }
            }
             System.debug('accShareList<>>'+accShareList);
            if(!accShareList.isEmpty()){
                        Database.insert(accShareList);
            }   


        }

        /**
      * @description       This method is invoked from User trigger to 
      *                    share invoice with Distributors staff
      * @param             invoiceList - List of invoices to be shared 
      * @return            void
      * @throws            Method might throw exception which is handled by try-catch block
      */ 
        public static void shareBuilderWithCommonDirectorOnUserTrigger(Map<id,List<id>> directorWithBuildersMap){
            
            Map<id,id> direcotorWithUserMap = new  Map<id,id>();

            for(user u : [Select id,contactId from User where contactId IN : directorWithBuildersMap.keySet()]){
                direcotorWithUserMap.put(u.contactId,u.Id);
            }
            System.debug('direcotorWithUserMap<>>'+direcotorWithUserMap);
            List<AccountShare> accShareList = new List<AccountShare>();

            

            for(id directorContactId : directorWithBuildersMap.keySet()){
                for(id builderAccountId : directorWithBuildersMap.get(directorContactId)){

                    if(direcotorWithUserMap.containsKey(directorContactId)){
                        id directorUserId = direcotorWithUserMap.get(directorContactId);

                        AccountShare accShare = new AccountShare(AccountId = builderAccountId,AccountAccessLevel = EDIT_ACCESS,
                                                                    OpportunityAccessLevel = READ_ACCESS, CaseAccessLevel = READ_ACCESS, 
                                                                    RowCause = MANUAL_ROW_CAUSE,UserOrGroupId = directorUserId);

                        accShareList.add(accShare);
                    }
                }

            }


             System.debug('accShareList<>>'+accShareList);
            if(!accShareList.isEmpty()){
                        Database.insert(accShareList);
            }   


        }
    
    }