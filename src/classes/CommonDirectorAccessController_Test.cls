/**
* Date         :  23-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for DirectorAccessController controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class CommonDirectorAccessController_Test {    
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        UserRole batMember = TestUtility.getUserRole('BAT_Member');
        
        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        Database.insert(vmiaInternalUser);
        setupData();
        setupUsers();
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> rateTableRecTypeMap = Schema.Sobjecttype.PremiumRatesTable__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
        
        PremiumRatesTable__c flatRate = TestUtility.createFlatRateTable(1500, rateTableRecTypeMap.get('FlatRate').getRecordTypeId());
        Database.insert(flatRate);

        Account texDistributor = TestUtility.createBusinessAccount('Tex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        texDistributor.website = 'www.textDis.com';
        Database.insert(texDistributor);
        System.debug('** texDistributor ==>'+ texDistributor);

        Contact texSmoth = TestUtility.createContact('Tex','Smoth','tex.smoth@texHomeDistributorTest.com',texDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(texSmoth);
        System.debug('** texSmoth ==>'+ texSmoth);

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);

        Account smallBuilder = TestUtility.createBusinessAccount('small Builders',accRecTypeMap.get('Company').getRecordTypeId());
        smallBuilder.Agent__c = rexDistributor.Id;
        smallBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(smallBuilder);
        System.debug('** smallBuilder ==>'+ smallBuilder);

        Contact commonDirector = TestUtility.createContact('Common','Director','c.director@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(commonDirector);
        System.debug('** commonDirector ==>'+ commonDirector);

        Invoice__c inv1 = TestUtility.createInvoice(texDistributor.Id, 'New', invRecTypeMap.get('New Invoice').getRecordTypeId());
        Database.insert(inv1);

        Certificate__c cert1 = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
        cert1.InvoiceNo__c = inv1.Id;
        Certificate__c cert2 = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
        cert2.InvoiceNo__c = inv1.Id;
        Database.insert(new List<Certificate__c>{cert1,cert2});   


        Property__c property = TestUtility.createProperty('12 Test Street', 'TestVille', '3000', 'VIC');
        Database.insert(property);

        Property_Owners__c propOwner = TestUtility.createPropertyOwner(tonyStark.Id, cert1.Id, property.Id);
        propOwner.property_owner__c = texDistributor.Id;
        Database.insert(propOwner);

        Property_Owners__c propOwner2 = TestUtility.createPropertyOwner(tonyStark.Id, cert1.Id, property.Id);
        Database.insert(propOwner2);
    }

    @future
    private static void setupUsers(){
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();

        Contact tonyStark = [SELECT id FROM Contact WHERE FirstName = 'Tony' and LastName = 'Stark' LIMIT 1];
        Contact texSmoth = [SELECT id FROM Contact WHERE FirstName = 'Tex' and LastName = 'Smoth' LIMIT 1];
        Contact rexSmith = [SELECT id FROM Contact WHERE FirstName = 'Rex' and LastName = 'Smith' LIMIT 1];
        Contact commonDirector = [SELECT id FROM Contact WHERE FirstName = 'Common' and LastName = 'Director' LIMIT 1];

        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];
        Account texDistributor = [SELECT Id FROM Account WHERE Name = 'Tex Home Distributor' LIMIT 1];

        User builderUser = TestUtility.createPortalUser('ExternB',profileMap.get('Builder'),tonyStark.Id);
        Database.insert(builderUser);

        User ddUser = TestUtility.createPortalUser('ExternD',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(ddUser);

        User ddUser2 = TestUtility.createPortalUser('ExternDD',profileMap.get('Distributor'),texSmoth.Id);
        Database.insert(ddUser2);

        User directorUser = TestUtility.createPortalUser('ExternDD',profileMap.get('Common RBP'),commonDirector.Id);
        Database.insert(directorUser);

         System.RunAs(ddUser) {
            Application__c app1 = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(), null);   
            Application__c app2 = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(), null);    
            Database.insert(new List<Application__c>{app1,app2});
        }
         System.RunAs(ddUser2){            
            Invoice__c inv = TestUtility.createInvoice(texDistributor.Id, 'New', invRecTypeMap.get('New Invoice').getRecordTypeId());
            Database.insert(inv);
        }    
        
    }
       
    private static testMethod void testshareCertificatesWithCommonDirector(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        List<Certificate__c> certList = [SELECT Id FROM Certificate__c WHERE Builder__c =: bornBuilder.Id];
        Contact commonDirector = [Select Id from Contact where email = 'c.director@bornBuildersTest.com'];

        Contact_Role__c conRole = TestUtility.createContactRole(bornBuilder.Id, commonDirector.Id,'Director');

        Test.startTest(); 
        Database.insert(conRole);   
        Test.stopTest();
    }
    
     private static testMethod void testprovideAccessToDirector(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();Profile directorProfile = [Select Id from Profile where name = 'Common RBP'];
        Profile cdirectorProfile = [Select Id from Profile where name = 'Common RBP'];
        Contact commonDirectorOne = TestUtility.createContact('CommonOne','Director','c.director@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(commonDirectorOne);

        User directorUser = TestUtility.createPortalUser('ExternDD',cdirectorProfile.Id,commonDirectorOne.Id);

        Contact_Role__c conRole = TestUtility.createContactRole(bornBuilder.Id, commonDirectorOne.Id,'Director');
        insert conRole;

        Test.startTest(); 
         Database.insert(directorUser); 
         set<id> userId = new Set<id>();
         userId.add(directorUser.Id);
         UserTriggerHelper.provideAccessToDirector(userId);
        Test.stopTest();
     }   
}