/**
  * Date         :  31-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Controller for Distributor Landing
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/ 

public with sharing class CommonDirectorLandingController {
    
    // Page variables
    public list<Contact_Role__c> builderList {get; set;} 
    public Map<Id, Integer> numberOfCertsInCart {get; set;}
    public list<Certificate__c> cList {get; set;}
    public Integer certificateCount {get; set;}
    private static final String AWAITING_PAYMENT = 'Awaiting Payment';
    private static final String CERT_RECTYPE_CONTAINER = 'Container';

    public CommonDirectorLandingController() {
        
        Set<Id> accountIds = new Set<Id>();

        builderList = new list<Contact_Role__c>();
        cList = new list<Certificate__c>();
        numberOfCertsInCart = new Map<Id, Integer>();
        
        list<user> uList = [SELECT Id, ContactId FROM User WHERE Id =: UserInfo.getUserId()];

        if (null != uList && !uList.isEmpty()) {  
            for (Contact_Role__c acr : 
              [SELECT Account__c, Account__r.Name, Account__r.Eligibility_Status__c, Account__r.Builder_Eligibility_Status_Reason__c, 
                  Account__r.AvailableConstructionLimit__c, Account__r.Construction_Limit__c, Account__r.Primary_Email__c, Contact__c, Role__c,
                  Account__r.Agent__r.Name, Account__r.Agent__r.Id 
               FROM Contact_Role__c 
               WHERE Contact__c =: uList[0].contactId AND (Role__c=: Constants.Primary_RBP OR Role__c=: Constants.EmployeeRole) LIMIT 5]) {
                if(acr.Account__c != null && !accountIds.contains(acr.Account__c)){
                    builderList.add(acr);
                    accountIds.add(acr.Account__c);     
                    numberOfCertsInCart.put(acr.Account__c, 0);  // some builders may have no certs    
                }     
            }
            System.debug('numberOfCertsInCart<>'+numberOfCertsInCart);
            System.debug('accountIds<>'+accountIds);
            System.debug('builderList<>'+builderList);

            List<AggregateResult> numberOfCertsInCartResults =
                [SELECT Builder__c, count(Name)
                 FROM Certificate__c 
                 WHERE Builder__r.Id IN :accountIds
                 AND Status__c = :constants.certificate_status_Awaiting_Payment
                 GROUP BY Builder__c]; 
                 
            for (AggregateResult result : numberOfCertsInCartResults) {
                numberOfCertsInCart.put((Id)result.get('Builder__c'), (Integer)result.get('expr0'));
            }

            for (Certificate__c c : 
              [Select id,Name,Policy_Number__c,date_issued__c, builder__r.name,builder__c, Category_of_Building_Works__c, 
                  Property_Address__c, Total__c, Status__c, Property_Owner__r.Name, Contract_Price__c 
               FROM Certificate__c 
               WHERE (Builder__c IN :accountIds AND Status__c !=: AWAITING_PAYMENT AND RecordType.Name !=: CERT_RECTYPE_CONTAINER AND RecordType.Name != :GlobalConstants.REC_TYPE_HISTORICAL_CERT AND Date_Issued__c >= LAST_N_DAYS:7 AND Status__c IN (:constants.Certificate_status_Issued_Works_In_Progress, :Constants.Certificate_status_Issued_Works_Complete)) ORDER BY CreatedDate DESC LIMIT 3]) {
                cList.add(c);
            }

            certificateCount = [
                SELECT count() 
                FROM CERTIFICATE__C 
                WHERE Status__c IN (:constants.Certificate_status_Issued_Works_In_Progress, :Constants.Certificate_status_Issued_Works_Complete) 
                AND Date_Issued__c >= LAST_N_DAYS:7 ];

        }

    }

}