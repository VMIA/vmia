/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class CommunitiesLoginController {

    global CommunitiesLoginController () {}
    
    // Code we will invoke on page load.

    /* Code Commented to Check the Login Issue
    global PageReference forwardToAuthPage() {
    	
    	
    	String startUrl = System.currentPageReference().getParameters().get('startURL');
    	String displayType = System.currentPageReference().getParameters().get('display');
        return Network.forwardToAuthPage(startUrl, displayType);		
    }
    */

    global PageReference forwardToCustomAuthPage() {
		
		String startUrl = System.currentPageReference().getParameters().get('startURL');
		System.debug('startUrl>>>'+startUrl);
		return new PageReference(Site.getPathPrefix() + '/CustomLoginPage?startURL=' + EncodingUtil.urlEncode(startURL, 'UTF-8'));
		
		//return new PageReference( '/CustomLoginPage');
	}
}