/**
 * An apex page controller that exposes the site login functionality
 */
@IsTest global with sharing class CommunitiesLoginControllerTest {
    @IsTest(SeeAllData=true) 
    global static void testCommunitiesLoginController () {
     	CommunitiesLoginController controller = new CommunitiesLoginController();
     	PageReference pageRef = Page.CommunitiesLanding;
        Test.setCurrentPage(pageRef);
            
        //Set url parameters            
        ApexPages.currentPage().getParameters().put('startURL', '');
     	System.assertEquals('/CustomLoginPage?startURL=', controller.forwardToCustomAuthPage().getURL());       
    }    
}