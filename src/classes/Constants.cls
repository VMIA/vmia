/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Apex class is for storing the constants which is being used in various places
  */
public class Constants{
    
    public static final String SALESFORCE_USER = 'Salesforce';
    public static final String LOGOUT_URL = '/secur/logout.jsp';
    public static final String UTF_8 = 'UTF-8';
    public static final String CSP_LITE_PORTAL = 'CSPLitePortal';
    public static final String GUEST = 'Guest';
    public static final String ID = 'id';
    public static final String MY_PARAM = 'myParam';
    public static final String REWORK_NEEDED = 'Rework Needed';
    public static final String IN_PROGRESS = 'In Progress';
    public static final String EXPR_0 = 'expr0';
    public static final string Distributor = 'Distributor';
    public static final string Builder = 'Builder';
    public static final string Common_Director = 'Common Director';
    public static final string Common_RBP = 'Common RBP';
    public static final string Primary_RBP = 'Primary RBP';
    public static final string Director = 'Director';
    public static final string EmployeeRole = 'Employee';
    public static final String role ='SVP Customer Experience';
    
    public static final string OBJ_Account = 'Account';
    public static final string OBJ_Application = 'Application__c';
    public static final string OBJ_Certificate = 'Certificate__c';
    public static final string OBJ_Invoice = 'Invoice__c';

    public static final string cancelCOI = 'Cancel COI';
    public static final string amendCOI = 'Amend COI';
    public static final string reviewEligibility = 'Review Eligibility';
    public static final string newCOI = 'New COI' ;
    public static final string invoiceStatus  = 'Draft' ;
    
    //public static final string batQueue = [select id from group where name = 'Bat Queue' limit 1].id;
    //public static final string batManagerQueue = [select id from group where name = 'Bat Manager Queue' limit 1].id;
    public static final string VMIA_Internal_Manager_Profile = 'VMIA Internal Manager';

    
    public static final string application_Eligibility_Status_UnderApp = 'Under Application';
    public static final string application_Builder_Eligibility_Status_UnderRev = 'Under Review';
    public static final string application_Status_New = 'New';
    public static final string application_Status_DD_Review = 'DD Review';
    public static final string application_Status_Assigned = 'Assigned';
    public static final string application_Status_IN_PROGRESS = 'In Progress';
    public static final string application_Status_Awaiting_Info = 'Awaiting Information';
    public static final string application_Status_Pending_Declinature = 'Pending Declinature';
    public static final string application_Status_Suspended = 'Suspended';
    public static final string application_Status_New_Information_Received = 'New Information Received';  
    public static final string application_Status_COI_Request_Approved = 'COI Request Approved';
    public static final string application_Status_COI_Request_Rejected = 'COI Request Rejected';
    public static final string application_Status_Builder_Changed = 'Builder Change'; 
    public static final string application_Status_Builder_Unchanged = 'Builder Unchanged'; 
    public static final string application_Status_Declined = 'Declined';
    public static final string application_Status_Builder_Withdrawn = 'Withdrawn';
    public static final string application_Current_Activity_Final_Declinature = 'Final Declinature';
    public static final string application_Eligibility_Letter_Avaliable_Successful_LOE = 'Successful LOE';
    public static final string application_Eligibility_Letter_Avaliable_Declined_LOE = 'Declined LOE';
    public static final string application_Eligibility_Letter_Avaliable_Pending_Declinature = 'Pending Declinature';
    public static final string application_Task_Summary_Selection_Pending_Declinature = 'Pending Declinature - More Information Required';
    public static final string application_Task_Summary_Selection_Builder_Suspended = 'Builder Account Suspended - More Information Required';
    public static final string application_source_DD_Initiated = 'Builder/DD Initiated';
    public static final string application_source_Adverse_Alert = 'Adverse Alert';
    public static final string application_Source_Status_New = 'New';
    public static final string application_Source_Status_Scheduled_Review = 'Scheduled Review';
    public static final string application_Builder_Result_Status_Declined = 'Declined';
    public static final string application_Builder_Result_Status_Cancelled = 'Cancelled';
    public static final string application_RecordTypeId_New_Eligibility = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('New Eligibility').getRecordTypeId();//'012N000000057qiIAA';
    public static final string application_RecordTypeId_Review_Eligibility = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Review Eligibility').getRecordTypeId();
    public static final string application_RecordTypeId_Amend_COI = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Amend COI').getRecordTypeId();
    public static final string application_RecordTypeId_Cancel_COI = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Cancel COI').getRecordTypeId();
    public static final string application_error_Message_Duplicate_ABN = 'Duplicate ABN';
    public static final string application_error_Message_Duplicate_Legal_Entity_Name = 'Duplicate Legal Entity Name';
    public static final string application_error_Message_Duplicate_ACN = 'Duplicate ACN';
    public static final string application_task_summary_other = 'Other';


    public static final string accountContactRole_Primary_Director = 'Primary Director';
    public static final string account_record_type_Person_Account = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();


    public static final string account_Eligibility_Status_Active_Restricted = 'Active - Restricted';
    public static final string account_Eligibility_Status_Active = 'Active';
    public static final string account_Eligibility_Status_Cancelled = 'Cancelled';
    public static final string account_Eligibility_Status_Declined = 'Declined';
    public static final string account_Builder_Eligibility_Status_Reason_Under_Review = 'Under Review';
    public static final string account_Eligibility_Status_Under_App = 'Under Application';

    public static final string certificate_status_Issued_Works_In_Progress = 'Issued Works in Progress';
    public static final string certificate_status_Issued_Works_Complete = 'Issued Works Complete';
    public static final string certificate_status_Certificate_Issued = 'Certificate Issued';
    public static final string certificate_status_Takeover = 'Taken Over';
    public static final string certificate_status_Issued_Historical = 'Issued - Historical';
    public static final string certificate_status_Awaiting_Payment = 'Awaiting Payment'; 
    public static final string certificate_sub_status_New_Info_Recived = 'New Information Received';       

    public static final string contact_record_Type_Builder = 'Builder';


    //Premium Calculation 
    public static final string categoryA = 'CAT A';
    public static final string categoryB = 'CAT B';
    public static final string categoryC = 'CAT C';
    public static final string code_NSD_S = 'NSD-S';
    public static final string code_NSD_NS = 'NSD-NS';
    public static final string code_SP = 'SP';
    public static final string code_MBW_S = 'MBW-S';
    public static final string code_MBW_NS = 'MBW-NS';

    public static final decimal code_NSD_S_MaxLimit = 1000001;
    public static final decimal code_NSD_NS_MaxLimit = 250001;
    public static final decimal code_SP_MaxLimit = 100001;
    public static final decimal code_MBW_S_MaxLimit = 1000001;
    public static final decimal code_MBW_NS_MaxLimit = 250001;
    
    //public static boolean referralFlg =false;
    
    public static final string New_Single_Dwelling = 'C01: New Single Dwelling Construction';
    public static final string New_Multi_Dwelling = 'C03: New Multi-Dwelling Construction';
    public static final string Alterations_Additions = 'C04: Alterations/Additions/Renovations - Structural';
    public static final string Swimming_Pools = 'C05: Swimming Pools';
    public static final string Renovations_Tradespeople = 'C06: Refurbishment - Non Structural';
    public static final string Other = 'C07: Other';
    public static final string All_Cover = 'C08: All Cover';
    
    // -- Status constants --
    public static final string COI_Approved = 'COI Approved';
    public static final string COI_Rejected = 'COI Rejected';
    public static final string DD_Review = 'DD Review';
    public static final string New_COI = 'New';
    public static final string Referred = 'Referred';
    public static final string Multi_Approved = 'Multi Approved';
    public static final string Awaiting_Payment = 'Awaiting Payment';
    public static final string Rejected = 'Rejected';
    public static final string Not_Started = 'Not Started';
    public static final string Container = 'Container';
    public static final string obContainer = 'OBContainer';
    public static final string obCertificate = 'obCertificate' ;
    public static final string NormalCertificate = 'Certificate' ;

    //System Generated Referral Reason

    // public static final string actualStartDateReason = 'Building Works Start Date is more than two years in the past';
     public static final string estimatedStartDateFutureReason = 'Building Works Estimated Start Date is more than two years in the future';
     public static final string estimatedStartDatePastReason = 'Building Works Start Date is more than two years in the past';
     
     public static final string actualCompletionDateFutureReason = 'Building Works Estimated Completion Date is more than two years in the future';
     public static final string actualCompletionDatePastReason = 'Building Works Completion Date is more than two years in the past' ;

     
     public static final string contractDateReason = 'Building Contract Date is more than two years in the past';
     public static final string contractDateAnotherReason = 'Building Contract Date is more than seven days in the future';
     public static final string propertyC01MatchingReason = 'Another Certificate of Insurance (C01) already exists for this property';
     public static final string propertyC03MatchingReason = 'Another Certificate of Insurance (C03) already exists for this property';
     public static final string builderStatusReason = 'Builder account status is Active-Restricted. All COI applications will be referred.';
     public static final string numberOfPropertyReason = 'Applications is for more than 3 dwellings.' ;
     public static final string premiumReason = 'Application would be referred since premium must be calculated by Assessor';
     public static final string contractValueMsg = 'Contract value should be entered in the correct format (E.g. 20,000)' ;
     public static final string contractValueLimitMsg = 'Contract value is less than 16k' ;
     public static final string obStartDateReason = 'Building Works End Date is more than six years in the past ';
     public static final string obNumberOfPropertyReason = 'Applications involving four or more properties require manual assessment.';
     public static final string propertyNotValidated = 'This Property has not been successfully validated';
     public static final string categoryLimitValidation = ' Contract value is above the selected category Limit ';
     public static final string builderLimitValidation = ' Contract Value is above the available construction Limit';
     public static final string endDateLessThanStartDate = ' Work estimation completion date should be before start date';
     public static final string contractValueNullCheck = 'Contract Value Can not be blank' ;
     public static final string buildingEndDateValidation = 'Building Works End Date should be in past';
     public static final string permitIssueDateMsg = 'Building Permit Date is not provided';
     public static final string occupancyPermitDateMsg = 'Occupany Permit Date is not provided';

     public static final string contractStandardFixed = 'Standard fixed price building contract';
     public static final string contractCostPlus = 'Cost plus building contract';
     public static final string individualOwner = 'Individual' ;
     public static final string organisationOwner = 'Organisation' ;
     public static final string SpeculativeOwner = 'Speculative' ;
     public static final string personAccount = 'Person Account' ;
     public static final string C06_MAXLIMITMSG = 'This contract value falls outside the current rate table. Your application will be assessed and you will be advised of your premium.' ;
     public static final string C06_MAXLIMIT = '100000' ;
    
    // -- Record Type constants --
    public static final string Referred_Certificate = 'Referred Certificate';
    public static final string ObFirstCover = '0-1 Yrs';
    public static final string ObSecondCover = '1-3 Yrs';
    public static final string ObThirdCover = '3-5 Yrs';
    public static final string ObFourthCover = '>5 Yrs';

}