/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Handler Class for Contact Role object trigger which includes context-specific methods 
                    that are automatically called when a trigger is executed.
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public with sharing class ContactRoleTriggerHandler{
    
    // Variables
    private static ContactRoleTriggerHelper helper;    // Helper class variable to call methods based on scenario
    
    /**
      * @description       This method is invoked from trigger which in-turn calls the handlers
                           for before and after event.
      * @param             NA 
      * @return            Void
      * @throws            NA
      */        
    public static void execute(){
        helper = new ContactRoleTriggerHelper();   // instance of helper class for Case trigger

        // Check for event type of trigger
       
        if(Trigger.isBefore && ContactRoleTriggerHelper.isBeforeEventFirstRun()){
            beforeHandler();        // invoke of before handler
        }
        else if(Trigger.isAfter && ContactRoleTriggerHelper.isAfterEventFirstRun()){
            afterHandler();         // invoke of after handler
        }
    }
    
    /**
      * @description       This method is handler for before events invoked by execute method.
                           Method invokes the helper class methods based on the scenario.                       
      * @param             NA 
      * @return            Void
      * @throws            NA
      */  
    private static void beforeHandler(){
        // Check for type of operation
        if(Trigger.isInsert){
            // Methods to be invoked for before insert event
           
        }
        else if(Trigger.isUpdate){
           
        }else if(Trigger.isDelete){
            // Methods to be invoked for before update event
        }
    }
    
    /**
      * @description       This method is handler for after events invoked by execute method.
                           Method invokes the helper class methods based on the scenario.                          
      * @param             NA 
      * @return            Void
      * @throws            NA
      */  
    private static void afterHandler(){
        // Check for type of operation
        if(Trigger.isInsert){
          ContactRoleTriggerHelper.AccessWithDirector(Trigger.new);
          helper.assignPrimaryRBPDetailsToApplication(Trigger.new, null);
        }
        else if(Trigger.isUpdate){
            // Methods to be invoked for after update event
             
             list<Contact_Role__c> conRoleList = new List<Contact_Role__c>();
             List<Contact_Role__C> revokeAccessList = new List<Contact_Role__c>();

              for(Integer i=0; i<trigger.new.size(); i++){
                /*
                if((Contact_Role__c) trigger.new[i].get('Role__c') != (Contact_Role__c) trigger.old[i].get('Role__c') && string.valueOf((Contact_Role__c) trigger.new[i].get('Role__c')).equalsIgnoreCase(Constants.Director)){
                  conRoleList.add((Contact_Role__c) trigger.new[i]);
                }
                */
                Contact_Role__c newRole = (Contact_Role__c) trigger.new[i];
                Contact_Role__c oldRole = (Contact_Role__c) trigger.old[i];

                if(newRole.Role__c != oldRole.Role__c && newRole.Role__c == Constants.Primary_RBP){
                  conRoleList.add(newRole);
                }

                if(newRole.Role__c != oldRole.Role__c && oldRole.Role__c == Constants.Primary_RBP){
                  revokeAccessList.add(newRole);
                }

             }

             if(!conRoleList.isEmpty())
              ContactRoleTriggerHelper.AccessWithDirector(conRoleList);

            /*  
            if(!conRoleList.isEmpty())
              ContactRoleTriggerHelper.recokeAccessOfRBP(revokeAccessList);*/  

              helper.assignPrimaryRBPDetailsToApplication(Trigger.new,(Map<Id,Contact_Role__c>) Trigger.oldMap);
        }
        else if(Trigger.isDelete){
            // Methods to be invoked for after delete event
        }
        else if(Trigger.isUndelete){
            // Methods to be invoked for after undelete event
        }
    }
}