public with sharing class ContactRoleTriggerHelper{
    
    // variables

   @testvisible private static Boolean beforeEventFirstRun = true;
   @testvisible private static Boolean afterEventFirstRun = true;

    private static Map<String,Id> appRecTypeMap ;

    public ContactRoleTriggerHelper(){
        
    }
    
    public static Boolean isBeforeEventFirstRun(){
        Boolean retVal = beforeEventFirstRun;
        beforeEventFirstRun = false;
        return retVal;
    }
    
    public static Boolean isAfterEventFirstRun(){
        Boolean retVal = afterEventFirstRun;
        afterEventFirstRun = false;
        return retVal;
    }

    /*
    public static void recokeAccessOfRBP(list<Contact_Role__c> contactRoleList){

        Set<id> intialContactSet = new Set<Id>();
        for(Contact_Role__c crc : contactRoleList){
            if(crc.contact__c != null)
                intialContactSet.add(crc.contact__c);
        }

         

    }*/


    public static void AccessWithDirector(list<Contact_Role__c> contactRoleList){

        Map<id,list<certificate__c>> directorWithCertificatesMap = new Map<id,list<certificate__c>>();
        Map<id,list<id>> builderWithDirectorsMap = new Map<id,list<id>>();
        Set<id> builderSet = new Set<id>();
        Map<id,list<certificate__c>> builderWithCertificates = new Map<id,list<certificate__c>>();
        Map<id,list<id>> builderWithInvoices = new Map<id,list<id>>();
        Map<id,list<id>> directorWithInvoicesMap = new Map<id,list<id>>();
        Set<id> contactSetId = new Set<id>();
        List<Contact_Role__c> filteredcontactRoleList = new list<Contact_Role__c>();
        Set<id> intialContactSet = new Set<Id>();
        for(Contact_Role__c crc : contactRoleList){
            if(crc.contact__c != null)
                intialContactSet.add(crc.contact__c);
        }

        System.debug('contactRoleList<>><'+contactRoleList);

     /*AggregateResult[] groupedResults = [SELECT Contact__c, count(Id)
            FROM Contact_Role__c where ((Role__c =: Constants.Primary_RBP ) AND Account__r.agent__c != null AND (Account__r.Eligibility_Status__c =: Constants.account_Eligibility_Status_Active OR Account__r.Eligibility_Status__c =: Constants.account_Eligibility_Status_Active_Restricted) AND contact__c IN : intialContactSet) GROUP BY Contact__c];


     for (AggregateResult ar : groupedResults)  {      
        integer contactCount = integer.valueOf(ar.get('expr0'));
       //contactStr.add(string.valueOf(ar.get('Contact__c')));
        if(contactCount > 1){
            string conId = string.valueOf(ar.get('Contact__c'));
            contactSetId.add(conId);
        }
    }  
    */

    for(Contact_Role__c cr : [Select id ,Role__c,contact__c from Contact_Role__c where ((Role__c =: Constants.EmployeeRole OR Role__c =:Constants.Primary_RBP) AND (Account__r.Eligibility_Status__c =: Constants.account_Eligibility_Status_Active OR Account__r.Eligibility_Status__c =: Constants.account_Eligibility_Status_Active_Restricted) AND contact__c IN : intialContactSet)]){
        contactSetId.add(cr.contact__c);
    }

    System.debug('contactSetId<><><<'+contactSetId);
    for(Contact_Role__c cr : [Select Id, Account__c,Contact__c,Role__c from Contact_Role__c where ID IN : contactRoleList and Contact__c IN:contactSetId ]){
        filteredcontactRoleList.add(cr);
    }

    system.debug('filteredcontactRoleList<>'+filteredcontactRoleList);
        for(Contact_Role__c conrole :filteredcontactRoleList){
            if(conRole.Account__c != null && conRole.contact__c != null){
                builderSet.add(conRole.Account__c);
                if(builderWithDirectorsMap.containsKey(conRole.Account__c)){
                    list<id> directors = new List<Id>();
                    directors = builderWithDirectorsMap.get(conRole.Account__c);
                    directors.add(conrole.contact__c);
                    builderWithDirectorsMap.put(conRole.Account__c,directors);
                }else{
                    list<id> directors = new List<Id>();
                    directors.add(conrole.contact__c);
                    builderWithDirectorsMap.put(conRole.Account__c,directors);
                }
            }
        }

        System.debug('builderWithDirectorsMap<>><'+builderWithDirectorsMap);
        // Code for Sharing Builders With Distributors

        CommonDirectorAccessController.shareBuilderWithCommonDirector(builderWithDirectorsMap);

        //Code for Sharing the Certificates with Distributors

        for(certificate__c cer : [Select id,builder__c, InvoiceNo__c from certificate__c where builder__c IN : builderSet]){
            if(builderWithCertificates.containsKey(cer.builder__c)){
                    list<certificate__c> certificates = new List<certificate__c>();
                    certificates = builderWithCertificates.get(cer.builder__c);
                    certificates.add(cer);
                    builderWithCertificates.put(cer.builder__c,certificates);
                }else{
                    list<certificate__c> certificates = new List<certificate__c>();                    
                    certificates.add(cer);
                    builderWithCertificates.put(cer.builder__c,certificates);
                }


                if(builderWithInvoices.containsKey(cer.InvoiceNo__c)){
                    list<id> invList = new List<Id>();
                    invList = builderWithInvoices.get(cer.InvoiceNo__c);
                    invList.add(cer.InvoiceNo__c);
                    builderWithInvoices.put(cer.builder__c,invList);

                }else{
                    list<id> invList = new List<Id>();
                    invList.add(cer.InvoiceNo__c);
                    builderWithInvoices.put(cer.builder__c,invList);
                }
        }       

        System.debug('builderWithInvoices<>><'+builderWithInvoices);

        for(id builderId : builderWithCertificates.keySet()){
            list<certificate__c> certificateList = builderWithCertificates.get(builderId);
            if(builderWithDirectorsMap.containsKey(builderId)){
                for(id directorIds : builderWithDirectorsMap.get(builderId)){
                    directorWithCertificatesMap.put(directorIds,certificateList);
                }
            }
        }

        System.debug('directorWithCertificatesMap<>><'+directorWithCertificatesMap);

        CommonDirectorAccessController.shareCertificatesWithCommonDirector(directorWithCertificatesMap);

        for(id builderId : builderWithInvoices.keySet()){
            list<id> invoiceList = builderWithInvoices.get(builderId);
            if(builderWithDirectorsMap.containsKey(builderId)){
                for(id directorIds : builderWithDirectorsMap.get(builderId)){
                    directorWithInvoicesMap.put(directorIds,invoiceList);
                }
            }
        }
        System.debug('directorWithInvoicesMap<>><'+directorWithInvoicesMap);
       CommonDirectorAccessController.shareInvoiceWithCommonDirector(directorWithInvoicesMap);

        //Code for Sharing the Applications with Distributors

        Map<id,list<Application__c>>buildersWithApplications =new Map<id,list<Application__c>>();
        Map<id,list<Application__c>> directorWithApplicationsMap = new Map<id,list<Application__c>>();

        for(Application__c app : [Select id,Builder_Number__c from Application__c where Builder_Number__c IN : builderSet]){
            if(buildersWithApplications.containsKey(app.Builder_Number__c)){
                    list<Application__c> Applications = new List<Application__c>();
                    Applications = buildersWithApplications.get(app.Builder_Number__c);
                    Applications.add(app);
                    buildersWithApplications.put(app.Builder_Number__c,Applications);
                }else{
                   list<Application__c> Applications = new List<Application__c>();
                    Applications.add(app);
                    buildersWithApplications.put(app.Builder_Number__c,Applications);
                }
        }

        System.debug('buildersWithApplications<>><'+buildersWithApplications);

        for(id builderId : buildersWithApplications.keySet()){
            list<Application__c> appList = buildersWithApplications.get(builderId);
            if(builderWithDirectorsMap.containsKey(builderId)){
                for(id directorIds : builderWithDirectorsMap.get(builderId)){
                    directorWithApplicationsMap.put(directorIds,appList);
                }
            }
        }
        System.debug('directorWithApplicationsMap<>><'+directorWithApplicationsMap);
        CommonDirectorAccessController.shareApplicationsWithCommonDirector(directorWithApplicationsMap);

        //Code for Sharing Invoices with Directors.


    }

    public void assignPrimaryRBPDetailsToApplication(List<Contact_Role__c> roleList,Map<Id,Contact_Role__c> oldMap){
        try{
            Map<Id, Contact_Role__c> accMap = new Map<Id, Contact_Role__c>();
            List<Application__c> appList = new List<Application__c>();
            for(Contact_Role__c cRole : roleList){
                Contact_Role__c oldCRole = (oldMap != null && oldMap.containsKey(cRole.Id)) ? oldMap.get(cRole.Id) : null;
                if(oldCRole != null){
                    accMap.put(cRole.Account__c, null);
                    if(cRole.Account__c != oldCRole.Account__c){
                        accMap.put(oldCRole.Account__c, null);
                    }
                }else if(oldCRole == null && (cRole.Role__c ==GlobalConstants.ACCOUNTCONTACTROLE_PRIMARY_DIRECTOR || cRole.Role__c == GlobalConstants.ACCOUNTCONTACTROLE_RBP_PENDING)){
                    accMap.put(cRole.Account__c, null);
                }
                
            }
            
            //for(Application__c app : [SELECT Primary_RBP_Contact__c])
            for(Contact_Role__c cRole : [SELECT Contact__c, Role__c, Account__c, Id FROM Contact_Role__c WHERE Account__c IN: accMap.keyset() AND (Role__c =: GlobalConstants.ACCOUNTCONTACTROLE_PRIMARY_DIRECTOR OR Role__c =:GlobalConstants.ACCOUNTCONTACTROLE_RBP_PENDING) AND (End_Date__c = null OR End_Date__c > Today) ]){
                if(accMap.get(cRole.Account__c) == null || accMap.get(cRole.Account__c).Role__c != GlobalConstants.ACCOUNTCONTACTROLE_PRIMARY_DIRECTOR){
                    if(cRole.Role__c == GlobalConstants.ACCOUNTCONTACTROLE_PRIMARY_DIRECTOR){
                        accMap.put(cRole.Account__c, cRole);
                    }else if(cRole.Role__c == GlobalConstants.ACCOUNTCONTACTROLE_RBP_PENDING){
                        accMap.put(cRole.Account__c, cRole);
                    }
                }
            }

            for(Application__c app : [SELECT Primary_RBP_Contact__c, Builder_Number__c FROM Application__c WHERE Builder_Number__c IN: accMap.keyset()]){
                if(accMap.get(app.Builder_Number__c)!=null){
                    if(app.Primary_RBP_Contact__c == null || app.Primary_RBP_Contact__c != accMap.get(app.Builder_Number__c).Contact__c){
                        app.Primary_RBP_Contact__c = accMap.get(app.Builder_Number__c).Contact__c;
                    }
                    appList.add(app); 
                }
                
            }

            if(!appList.isEmpty()){
                update appList;
            }
        }
        catch(Exception exp){
            // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(ContactRoleTriggerHelper.class),
                                GlobalConstants.FN_SET_ROLE,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }
}