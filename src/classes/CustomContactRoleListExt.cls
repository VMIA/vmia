public with sharing class CustomContactRoleListExt {
    
    public List<Contact_Role__c> contactRoles {get; set;}   
    public Account currentAccount {get;set;} 
    public Id accountId {get;set;}
    public String contactRolePrefix {get;set;}
    public String accountName {get;set;}
    
    public CustomContactRoleListExt(ApexPages.StandardController controller) {
        accountId = controller.getId();
        //Account account = (Account) controller.getRecord();            
        //Account accountFromDatabase = [Select id ,PersonContactId from Account where Id =: account.Id];
        contactRolePrefix = Schema.getGlobalDescribe().get('Contact_Role__c').getDescribe().getKeyPrefix();

        
        contactRoles = [SELECT Id, Name, Account__c,Contact__r.AccountId, Contact__r.Account.Name, Contact__c, Role__c, Start_Date__c, End_Date__c, Account__r.Name, Contact__r.Name 
                        FROM Contact_Role__c 
                        WHERE Contact__r.AccountId =: accountId];

        currentAccount = [SELECT Name FROM Account WHERE id =: accountId limit 1];

        accountName = currentAccount.Name.replace('\'','');


	}
    
}