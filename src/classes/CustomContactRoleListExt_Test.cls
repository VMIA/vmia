/**
  * Date         :  16-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for Application trigger
  */ 
  
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
private with sharing class CustomContactRoleListExt_Test{
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> certRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> invRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> appRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> premiumRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        premiumRecTypeMap = Schema.Sobjecttype.PremiumRatesTable__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }

        UserRole batMember = TestUtility.getUserRole('BAT_Member');
        UserRole batManager = TestUtility.getUserRole('BAT_Manager');
        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        Database.insert(vmiaInternalUser);

        User vmiaInternalManagerUser = TestUtility.createUser('IntMan',profileMap.get('VMIA Internal Manager'),batManager.Id);
        Database.insert(vmiaInternalManagerUser);

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        //setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);

        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(rexUser);  

        Account texDistributor = TestUtility.createBusinessAccount('Tex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(texDistributor);
        System.debug('** texDistributor ==>'+ texDistributor);
        
        Contact texSmoth = TestUtility.createContact('Tex','Smoth','tex.smoth@rexHomeDistributorTest.com',texDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(texSmoth);
        System.debug('** texSmoth ==>'+ texSmoth);

        User texUser = TestUtility.createPortalUser('texSmoth',profileMap.get('Distributor'),texSmoth.Id);
        Database.insert(texUser); 

        System.runAs(rexUser){
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Partnership').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);

        Contact tabySparks = TestUtility.createContact('Taby','Sparks','taby.sparks@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        tabySparks.RBP_VBA_Number__c = 'DB-U 12345';
        tabySparks.VBA_Issued_Date__c = system.Today();
        tabySparks.VBA_Expiry_Date__c = system.today() + 60;
        Database.insert(tabySparks);
        System.debug('** tonyStark ==>'+ tabySparks);

        Account personAccount = TestUtility.createPersonAccount('PersonAccountLast',accRecTypeMap.get('Sole Trader').getRecordTypeId());
        personAccount.Agent__c = rexDistributor.Id;
        personAccount.Agent_Contact__c = rexSmith.Id;
        personAccount.RBP_VBA_Number__pc = 'DB-U 123456';
        personAccount.VBA_Issued_Date__pc = system.today();
        personAccount.VBA_Expiry_Date__pc = system.today() + 60;
        Database.insert(personAccount);


        Contact_Role__c conRole =  TestUtility.createContactRole(bornBuilder.Id, tonyStark.Id ,'Director');
        Database.insert(conRole);
        Contact_Role__c conRole2 =  TestUtility.createContactRole(bornBuilder.Id, tabySparks.Id ,'Director');
        Database.insert(conRole2);

        Contact_Role__c conRole3 =  TestUtility.createContactRole(bornBuilder.Id, tabySparks.Id ,'Primary RBP');
        Database.insert(conRole3);
    }
        
        
    }

    /**
      * @description       This method tests scenario of CustomContactRoleListExt class creation
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testGeneratePolicyNumberDistirbutor(){
		System.debug('** Rex User ==>'+ [SELECT Id,Name FROM User WHERE Name = 'RexSmith']);
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id, Name, Agent__c FROM Account WHERE Name='Born Builders'];
        //Contact perContact = [Select Id, FirstName, LastName FROM Contact WHERE AccountId =: bornBuilder.Id];
        System.runAs(rexUser){
        	PageReference pageRef = Page.Custom_Contact_Role_List;
        	Test.setCurrentPage(pageRef);
        	ApexPages.CurrentPage().getParameters().put('id', bornBuilder.Id);
            Test.startTest();
            CustomContactRoleListExt controller = new CustomContactRoleListExt(new ApexPages.StandardController(bornBuilder));
            Test.stopTest();
            system.assertEquals(controller.accountName,'Born Builders');
        }    
	}
	
	
}