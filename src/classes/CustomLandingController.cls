/**
  * Date         :  31-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Controller for Distributor Landing
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/ 

public with sharing class CustomLandingController {
public boolean displayTC {get; set;}
    public boolean displayTCError {get; set;}
    public User chkUser {get; set;}
    
    public PageReference dagreeTerms() {
    
        displayTC = true;
        displayTCError = true;
        return null;
    }


    public PageReference agreeTerms() {
    
        chkUser.Terms_and_Condition__c = true;
        chkUser.Terms_and_Condition_Date__c = System.Today();
        if (!Test.isRunningTest()) update chkUser; 
        
        displayTC = false;
        displayTCError = false;
        
        return forwardToCustomAuthPage();
    }
    
    /*
     public PageReference validateAndRedirect(){
    
        chkUser = [SELECT  Username, Terms_and_Condition__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        
        
        if (chkUser.Terms_and_Condition__c == false){     
             displayTC = true; 
             displayTCError = false;   
             return null;            
        }else{
             PageReference pageRef;
                if(constants.GUEST.equalsIgnoreCase(UserInfo.getUserType())){ 
                    pageRef = Page.login;
                } 
                else if(constants.CSP_LITE_PORTAL.equalsIgnoreCase(UserInfo.getUserType())) {
                    pageRef = Page.BuilderLandingPage;
                }
                return pageRef;
        }
        
    
     }
*/
    
    // Page variables
    public Account distributorRecord {get;set;}
    public List<Task> distributorTasks {get;set;}
    public Integer taskCount {get;set;}
    public Integer applicationCount {get;set;}
    public Integer certificateCount {get;set;}
    public Integer builderCount {get;set;}
    public Id distributorId {get;set;}
    public Map<id, CertificateWithPropertyOwner> certWithOwnersMap {get;set;}
    public List<CertificateWithPropertyOwner> certWithOwnersList {get;set;}
    public Map<id, id> certsWithBlankBuilder {get;set;}
    public Map<Id,String> applicationBuilderABNMap {get;set;}
    public Map<Id,String> applicationBuilderMap {get;set;}
    public Map<Id,Id> applicationCertificateMap {get;set;}
    public Map<Id, Task> taskMap {get;set;}
    public Map<id, TasksWithBuilder> taskWithBuildersMap {get;set;}
    public List<TasksWithBuilder> taskWithBuildersList {get;set;}
    public Map<id, Certificate__c> taskCertWhatIdMap {get;set;}
    public Map<id, Application__c> taskAppWhatIdMap {get;set;}
    public Map<id, List<id>> taskCertIdWhatIdMap {get;set;}
    public Map<id, List<id>> taskAppIdWhatIdMap {get;set;}
    public id recordId{get;set;}
    public string objectName{get;set;}
    public Schema.SObjectType objType {get;set;}

    
    // Variables
    private Integer listSize = 3; 
    
    // Constructor
    public CustomLandingController() {
        try {
            applicationBuilderMap = new Map<Id,String>();
            applicationBuilderABNMap = new Map<Id,String>();
            applicationCertificateMap = new Map<Id,Id>();
            if(GlobalUtility.isDistributor){
                distributorId = GlobalUtility.loggedInUser.AccountId;
            }

            if(distributorId != null){
                distributorRecord = [SELECT Id,Name,
                                        (SELECT Id, Name, Builder_Number__c,Certificate__c,Hidden_Builder_Name__c, Builder_Legal_Entity_Name__c, Hidden_Builder_ABN__c, Hidden_Builder_ACN__c, Builder_ABN__c, Application_Status__c, Hidden_Builder_current_Legal_Name__c
                                            FROM DistributorApplications__r WHERE Application_Status__c IN (:constants.Application_Status_New, :constants.Application_Status_DD_Review, :constants.Application_Status_Assigned, :constants.Application_Status_IN_PROGRESS, :constants.Application_Status_Awaiting_Info, :constants.Application_Status_New_Information_Received) ORDER BY Name DESC LIMIT :listSize),
                                            
                                        (SELECT Name, Builder__r.Name, Category_of_Building_Works__c, Status__c, Contract_Price__c,Date_Issued__c,  Property_Address__c,Policy_Number__c, Certificate_Type__c
                                            FROM DistributorCertificates__r WHERE (RecordType.Name != :GlobalConstants.REC_TYPE_CONTAINER AND RecordType.Name != :GlobalConstants.REC_TYPE_HISTORICAL_CERT AND Status__c IN (:constants.Certificate_status_Issued_Works_In_Progress, :Constants.Certificate_status_Issued_Works_Complete) AND Date_Issued__c >= LAST_N_DAYS:7) ORDER BY Name DESC LIMIT :listSize),
                                            
                                        (SELECT Name, Legal_Business_Type__c,Eligibility_Status__c,Primary_Email__c
                                            FROM Accounts__r ORDER BY Name DESC LIMIT :listSize)
                                        FROM Account WHERE Id =: distributorId LIMIT 1]; 
                
                distributorTasks = [SELECT Subject, Activity_Number__c, ActivityDate, WhoID, WhatId, Builder__r.Name, Status 
                                    FROM Task WHERE ( Owner_Account_Id__c =: distributorId OR Builder__r.Agent__c =: distributorId OR  Builder__c =: distributorId) AND Status IN (:constants.NOT_STARTED, :constants.REWORK_NEEDED, :constants.IN_PROGRESS) ORDER BY Activity_Number__c DESC LIMIT :listSize];
                taskcount = [SELECT count() FROM TASK WHERE (Builder__r.Agent__c =: distributorId OR  Builder__c =: distributorId) AND Status IN (:constants.NOT_STARTED, :constants.REWORK_NEEDED, :constants.IN_PROGRESS)];
                applicationCount = [SELECT count() FROM APPLICATION__C WHERE DD_Account__c =: distributorId AND Application_Status__c IN (:constants.Application_Status_New, :constants.Application_Status_DD_Review, :constants.Application_Status_Assigned, :constants.Application_Status_IN_PROGRESS, :constants.Application_Status_Awaiting_Info, :constants.Application_Status_New_Information_Received)];
                certificateCount = [SELECT count() FROM CERTIFICATE__C WHERE Distributor__c =: distributorId AND RecordType.Name != :GlobalConstants.REC_TYPE_CONTAINER AND Status__c IN (:constants.Certificate_status_Issued_Works_In_Progress, :Constants.Certificate_status_Issued_Works_Complete) AND Date_Issued__c >= LAST_N_DAYS:7 ];
                builderCount = [SELECT count() FROM ACCOUNT WHERE Agent__c =: distributorId];    

                certWithOwnersMap = new Map<id, CertificateWithPropertyOwner>();
                certsWithBlankBuilder = new Map<Id, id>();
                certWithOwnersList = new List<CertificateWithPropertyOwner>();
                taskWithBuildersMap = new Map<id, TasksWithBuilder>();
                taskWithBuildersList = new List<TasksWithBuilder>();
                taskCertWhatIdMap = new Map<id, Certificate__c>();
                taskAppWhatIdMap = new Map<id, Application__c>();
                taskCertIdWhatIdMap = new Map<id, List<id>>();
                taskAppIdWhatIdMap = new Map<id, List<id>>();
                taskMap = new Map<id, Task>();

                System.debug('** distributorRecord ==>'+ distributorRecord);
                System.debug('** Tasks ==>'+ distributorTasks);
                System.debug('** Applications ==>'+ distributorRecord.DistributorApplications__r);
                System.debug('** Certificates ==>'+ distributorRecord.DistributorCertificates__r);
                System.debug('** Accounts ==>'+ distributorRecord.Accounts__r);

                for(Application__c app : distributorRecord.DistributorApplications__r){
                    if(app.Builder_Number__c == null){
                        if(app.Certificate__c != null){
                            applicationCertificateMap.put(app.Certificate__c,app.Id);
                        }
                    }
                }
                
                for(Task tk : distributorTasks){
                    
                    recordId =  tk.whatId ;
                    objType = recordId.getSobjectType();
                    objectName = string.valueOf(objType);
                    if(objectName.equalsIgnoreCase('Certificate__c')){
                        List<id> taskIdCertList;
                        if(taskCertIdWhatIdMap.get(tk.WhatId) == null){
                            taskIdCertList = new List<Id>{tk.Id};
                        }else{
                            taskIdCertList = taskCertIdWhatIdMap.get(tk.WhatId);
                            taskIdCertList.add(tk.Id);
                        }
                        taskCertIdWhatIdMap.put(tk.WhatId, taskIdCertList);
                    }else{
                        List<id> taskIdAppList;
                        if(taskAppIdWhatIdMap.get(tk.WhatId) ==null){
                            taskIdAppList = new List<id>{tk.Id};
                        }
                        else{
                            taskIdAppList = taskAppIdWhatIdMap.get(tk.WhatId);
                            taskIdAppList.add(tk.Id);
                        }
                        taskAppIdWhatIdMap.put(tk.WhatId, taskIdAppList);
                    }
                    taskMap.put(tk.Id, tk);
                    taskWithBuildersMap.put(tk.Id, null);
                }
                System.debug('** taskWithBuildersMap ==>'+ taskWithBuildersMap);
                System.debug('** taskAppIdWhatIdMap ==>'+ taskAppIdWhatIdMap);
                System.debug('** applicationCertificateMap ==>'+ applicationCertificateMap);
                for( Certificate__c c : distributorRecord.DistributorCertificates__r){
                    certWithOwnersMap.put(c.id, new CertificateWithPropertyOwner(c, c.Builder__r.Name));
                    if(String.isBlank(c.Builder__c)){
                        certsWithBlankBuilder.put(c.Id, null);
                    }
                }

                for(Property_Owners__c p : [SELECT Certificate__c, Certificate__r.Id, Certificate__r.Builder__r.Name, Property_Owner__r.Name,Property_Owner__r.ABN__c, Contact__r.Name 
                                                FROM Property_Owners__c WHERE Certificate__r.Id IN : taskCertIdWhatIdMap.keyset()] ){
                    for(Id tskId : taskCertIdWhatIdMap.get(p.Certificate__r.Id)){
                        if(String.isBlank(p.Certificate__r.Builder__r.Name)){
                            if(!String.isBlank(p.Property_Owner__r.Name)){
                                taskWithBuildersMap.put(tskId, new TasksWithBuilder(taskMap.get(tskId), p.Property_Owner__r.Name));
                            }else if(!String.isBlank(p.Contact__r.Name)) {
                                taskWithBuildersMap.put(tskId, new TasksWithBuilder(taskMap.get(tskId), p.Contact__r.Name));
                            }else{
                                taskWithBuildersMap.put(tskId, new TasksWithBuilder(taskMap.get(tskId), GlobalConstants.OWNER_BUILDER));
                            }
                        }else{
                            taskWithBuildersMap.put(tskId, new TasksWithBuilder(taskMap.get(tskId), p.Certificate__r.Builder__r.Name));
                        }
                    }
                }
                
                Map<Id,List<Id>> appBuilderNameMap = new Map<Id,List<Id>>();
                for(Application__c a : [SELECT Builder_Number__r.Name, Certificate__c FROM Application__c WHERE Id IN : taskAppIdWhatIdMap.keyset()]){
                    for(Id tskId : taskAppIdWhatIdMap.get(a.Id)){
                        if(a.Builder_Number__r.Name == null){
                            List<id> taskIdAppList = taskAppIdWhatIdMap.get(a.Id);
                            appBuilderNameMap.put(a.Certificate__c, taskIdAppList);
                        }else
                            taskWithBuildersMap.put(tskId, new TasksWithBuilder(taskMap.get(tskId), a.Builder_Number__r.Name));
                    }
                    
                }

                //for(Id tskId : taskAppIdWhatIdMap.keyset()){
                //    taskWithBuildersMap.put(tskId, new TasksWithBuilder(taskMap.get(tskId), appBuilderNameMap.get(taskAppIdWhatIdMap.get(tskId))));
                //}
                System.debug('** taskWithBuildersMap ==>'+ taskWithBuildersMap);

                


                for (Property_Owners__c p : [SELECT Certificate__c, Certificate__r.Builder__r.Name, Property_Owner__r.Name,Property_Owner__r.ABN__c, Contact__r.Name FROM Property_Owners__c 
                                                WHERE (Certificate__c IN : certsWithBlankBuilder.keyset() OR Certificate__c IN: applicationCertificateMap.keyset() OR Certificate__c IN: appBuilderNameMap.keyset() )
                                                AND OwnerBuilder__c = TRUE] ){
                    String builderName = 'Owner Builder';
                    if(!String.isBlank(p.Property_Owner__r.Name)){
                        if(applicationCertificateMap.containsKey(p.Certificate__c)){
                            applicationBuilderMap.put(applicationCertificateMap.get(p.Certificate__c),p.Property_Owner__r.Name);
                            if(!String.isBlank(p.Property_Owner__r.ABN__c))
                                applicationBuilderABNMap.put(applicationCertificateMap.get(p.Certificate__c),p.Property_Owner__r.ABN__c);
                            else
                                applicationBuilderABNMap.put(applicationCertificateMap.get(p.Certificate__c),GlobalConstants.EMPTY_SPACE);
                        }
                        else if(certsWithBlankBuilder.containsKey(p.Certificate__c)){
                            certWithOwnersMap.get(p.Certificate__c).propertyOwnerName = p.Property_Owner__r.Name;
                        }
                        builderName = p.Property_Owner__r.Name;
                    }
                    else if(!String.isBlank(p.Contact__r.Name)) {
                        if(applicationCertificateMap.containsKey(p.Certificate__c)){
                            applicationBuilderMap.put(applicationCertificateMap.get(p.Certificate__c),p.Contact__r.Name);
                            applicationBuilderABNMap.put(applicationCertificateMap.get(p.Certificate__c),GlobalConstants.EMPTY_SPACE);
                        }
                        else if(certsWithBlankBuilder.containsKey(p.Certificate__c)){
                            certWithOwnersMap.get(p.Certificate__c).propertyOwnerName = p.Contact__r.Name;
                        }
                        builderName = p.Contact__r.Name;
                    }
                    else{
                        if(applicationCertificateMap.containsKey(p.Certificate__c)){
                            applicationBuilderMap.put(applicationCertificateMap.get(p.Certificate__c),GlobalConstants.OWNER_BUILDER);
                            applicationBuilderABNMap.put(applicationCertificateMap.get(p.Certificate__c),GlobalConstants.EMPTY_SPACE);
                        }
                        else if(certsWithBlankBuilder.containsKey(p.Certificate__c)){
                            certWithOwnersMap.get(p.Certificate__c).propertyOwnerName = GlobalConstants.OWNER_BUILDER;
                        }
                    }
                    if(appBuilderNameMap.get(p.Certificate__c)!= null){
                        for(Id tskId : appBuilderNameMap.get(p.Certificate__c)){
                            taskWithBuildersMap.put(tskId, new TasksWithBuilder(taskMap.get(tskId), builderName));
                        }
                    }
                    
                    
                }
                taskWithBuildersList = taskWithBuildersMap.values();
                System.debug('** taskWithBuildersList ==>'+ taskWithBuildersList);
                
                System.debug('** applicationBuilderMap ==>'+ applicationBuilderMap);
                System.debug('** applicationBuilderABNMap ==>'+ applicationBuilderABNMap);
                certWithOwnersList = certWithOwnersMap.values();
                
            }
        } 
        catch(exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Generic_ERROR));
        }    
    }
    
    // Redirecting to corresponding landing page
    public PageReference forwardToCustomAuthPage() {
        chkUser = [SELECT  Username, Terms_and_Condition__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        
        
        if (chkUser.Terms_and_Condition__c == false){     
             displayTC = true; 
             displayTCError = false;   
             return null;            
        }else {
                PageReference pageRef;
                    list<user> userList = [select id,profile.name from user where id =: UserInfo.getUserId()];
                if(constants.GUEST.equalsIgnoreCase(UserInfo.getUserType())){ 
                    pageRef = Page.login;
                } 
                else if(constants.CSP_LITE_PORTAL.equalsIgnoreCase(UserInfo.getUserType())) {
                    pageRef = Page.BuilderLandingPage;                
                }else if(null != userList && !userList.isEmpty() && userList[0].profile.name == Constants.Common_RBP ) {
                    pageRef = Page.CommonDirectorLanding;
                }
                return pageRef;
        }        
        
    }
    
    // Request to amend or cancel
    public PageReference requestAmend(){
        PageReference pageRef = Page.RequestAmendCOI;
        string certID = Apexpages.currentPage().getParameters().get(constants.MY_PARAM);
        certID = String.isNotBlank(certID) ? String.escapeSingleQuotes(certID) : certID;
        pageRef.getParameters().put(constants.ID,certID);
        return pageRef;
    }
    
    // User log out
    public PageReference UserLogout(){
        return new PageReference(constants.LOGOUT_URL);
    }

    public class CertificateWithPropertyOwner {
        public Certificate__c cert {get;set;}
        public String propertyOwnerName {get;set;}

        public CertificateWithPropertyOwner(Certificate__c newCert, String newOwnerName){
            cert = newCert;
            propertyOwnerName = newOwnerName;
        }
    }

     public class TasksWithBuilder {
        public Task tsk {get;set;}
        public String builderName {get;set;}

        public TasksWithBuilder(Task newTsk, String newBuilderName){
            tsk = newTsk;
            builderName = newBuilderName;
        }
    }
}