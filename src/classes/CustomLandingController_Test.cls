/**
  * Date         :  23-June-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for Distributor Landing class
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
private with sharing class CustomLandingController_Test {
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> certRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> appRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> invRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
    }

    @testSetup
    private static void testDataSetup(){
        
        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML
    }

    @future
    private static void setupUser(){
        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(new List<User>{vmiaInternalUser,rexUser});
    }
  
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();

        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];

        /*
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        */
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        System.runAs(rexUser){
            Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
            bornBuilder.Agent__c = rexDistributor.Id;
            bornBuilder.Agent_Contact__c = rexSmith.Id;
            Database.insert(bornBuilder);
            System.debug('** bornBuilder ==>'+ bornBuilder);
            
            Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
            Database.insert(tonyStark);
            System.debug('** tonyStark ==>'+ tonyStark);
            
            Certificate__c  cert = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
            cert.Building_Contract_Date__c = Date.today()+1;
            cert.Building_Permit_Issued_Date__c = Date.today()-2;
            cert.Actual_Start_Date__c= Date.today()-1;
            cert.Actual_Completion_Date__c = Date.today();
            cert.Estimated_Start_Date__c= Date.today()-1;
            cert.Estimated_Completion_Date__c = Date.today();
            cert.status__c = constants.certificate_status_Issued_Works_In_Progress;
            Database.insert(cert);
            System.debug('** Certificate ==>'+ cert.id);
            
            Blob b = Blob.valueOf('Test Data');
            Attachment attachment = new Attachment(ParentId = cert.Id, Name='Test Attachment for certificate', Body=b);
            Database.insert(attachment);        
            
            Property__c prop = new Property__c(Builder__c = bornBuilder.Id, Suburb__c='Dockland');
            Database.insert(prop);
            System.debug('** Property ==>'+ prop.id);
            
            Property_Owners__c propOwn = new Property_Owners__c(Property__c = prop.id, Certificate__c = cert.id, property_Owner__c=rexDistributor.Id);
            Database.insert(propOwn);
            System.debug('** Property Owner ==>'+ propOwn.id);
            
            Application__c  app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(),cert.Id);
            Database.insert(app);
            System.debug('** Application ==>'+ app);
            
            Account personAc = TestUtility.createPersonAccount('Person',accRecTypeMap.get('Person Account').getRecordTypeId());
            personAc.PersonMailingStreet = '9 Beach Street';
            Database.insert(personAc);
            System.debug('** personAc ==>'+ personAc);     
            
            Account personBuilder = TestUtility.createPersonAccount('Builder',accRecTypeMap.get('Person Account').getRecordTypeId());
            personBuilder.firstName = 'Person';
            Database.insert(personBuilder);
            System.debug('** personBuilder ==>'+ personBuilder);
            
            Certificate__c  certPerson = TestUtility.createCertificate(personBuilder.Id, null, certRecTypeMap.get('Certificate').getRecordTypeId());
            Database.insert(certPerson);
            System.debug('** Certificate ==>'+ certPerson.id); 

            // create BAT_Letters
            BAT_Letter__c letter= TestUtility.createBATLetter(bornBuilder.Id,app.id, cert.id);
            Database.insert(letter);
            System.debug('** BAT letter ==>'+ letter);
            
            Task certTsk = TestUtility.createTask(cert.Id,rexSmith.Id,bornBuilder.Id);
            certTsk.Description = 'Cert Description';
            
            Task appTsk = TestUtility.createTask(app.Id,rexSmith.Id,bornBuilder.Id);
            appTsk.Description = 'Cert Description';
            Database.insert(new List<Task>{appTsk});
        }
    } 
  
    /**
      * @description       This method tests scenario of DistributorLanding page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testPageLoad(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        /*
        List<sObject> recsToUpdate = new List<sObject>();

        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        app.OwnerId = rexUser.Id;
        recsToUpdate.add(app);

        Certificate__c cert = [SELECT Id FROM Certificate__c LIMIT 1];
        cert.OwnerId = rexUser.Id;
        recsToUpdate.add(cert);
        BAT_Letter__c letter = [SELECT Id FROM BAT_Letter__c LIMIT 1];
        letter.OwnerId = rexUser.Id;
        recsToUpdate.add(letter);

        for(Task tsk : [SELECT Id FROM Task]){
            tsk.OwnerId = rexUser.Id;
            recsToUpdate.add(tsk);
        }
        
        Property_Owners__c propOwner = [SELECT Id FROM Property_Owners__c WHERE LIMIT 1];
        propOwner.OwnerId = rexUser.Id;
        recsToUpdate.add(propOwner);
        
   
        System.debug('** recsToUpdate ==>'+ recsToUpdate);

        Database.update(recsToUpdate);
        */
        System.runAs(rexUser){
            PageReference pageRef = Page.DistributorLanding;
            Test.setCurrentPage(pageRef);
            Test.startTest();
                CustomLandingController landingController = new CustomLandingController();
            Test.stopTest();
        }
    }
    
    
}