/*
    Date            :   23-Feb-2017
    Modified        :   17-Jun-2017
    Author          :   SMS Management & Technology
    Description     :   Login controller
*/
public without sharing class CustomLoginController {
  

    public String username {get;set;}
    public String password {get;set;}
    public Boolean displayMsg {get;set;}
    public Boolean displayMsg2 {get;set;}
    public Boolean TermsDisplayed {get; set;}
    public User pageUser {get; set;}
  
 

    
    public CustomLoginController () {
        TermsDisplayed = false;
        pageUser = new User();
  
    }
    
    

    public PageReference logintoCommunity() {
    
        PageReference ref = Site.login(username, password, null);
        
       //Just checking if the user exist, if not it will display error invalid username/password
        if(ref == NULL) {
            displayMsg = true;
            return NULL;
        } else {
        
            displayMsg = false;         
        
        //if it exist, need to check/get its information    
            pageUser = [Select Terms_and_Condition__c From User
                    Where Username = :username Limit 1];
                    
            System.debug('pageUser: '+pageUser );        
                    
                 
        //if he already agreed the terms and condition, it should go to the home page            
            if (pageUser.Terms_and_Condition__c){
                ref.setRedirect(true);
                return ref;
            }
            else{
         //Display the terms and condition page
                TermsDisplayed = true;                
                return null;
            }
        }
        
    }//end method
    
    //The button "I agree" has been clicked
    public PageReference agreeTerms(){
        
        pageUser.Terms_and_Condition__c = true;
        pageUser.Terms_and_Condition_Date__c = System.Today();
        
        if (pageUser != null){
            if (!Test.isRunningTest()) update pageUser;         
            
            //The user tick the checkbox and clicked the button "I agree"
            if (pageUser.Id != null){
                PageReference ref = null;
              
                    ref = Site.login(username, password, null);
                             
                if (Test.isRunningTest()) ref = new PageReference('/apex/login');
                
                if (ref != null){
                    ref.setRedirect(true);
                    return ref;
                }
            }else{
                TermsDisplayed = true;  
                displayMsg2 = true;              
            }                      
        }
        
         return null; 
        
    }
    
     public PageReference dagreeTerms() {
         pageUser.Terms_and_Condition__c = false;
        
         TermsDisplayed = false;  
     
         return null; 
    }
   

    
    
    
}