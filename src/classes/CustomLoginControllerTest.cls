@isTest
public class CustomLoginControllerTest{
     static String username;
     static String password;
     static User pageUser;

    //Method to create the "Asset" object record 
    private static void init(){
          
        Account a = New Account(Name = 'TestDistributorAccount');
        insert a;

        Contact c = New Contact(FirstName = 'Distributor', LastName = 'Test', AccountID = a.id);
        insert c;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Builder' LIMIT 1];
  
        pageUser = new User(LastName = 'LIVESTON',
                     FirstName='JASON',
                     Alias = 'jliv',
                     Email = 'jason.liveston@asdf.com',
                     Username = 'jason.liveston@asdf.com',
                     ProfileId = profileId.id,
                     TimeZoneSidKey = 'GMT',
                     LanguageLocaleKey = 'en_US',
                     EmailEncodingKey = 'UTF-8',
                     LocaleSidKey = 'en_US',
                     ContactId = c.Id
                     );

        database.insert(pageUser);             
        system.setPassword(pageUser.Id, 'Pwerddfg1');             
        username  = pageUser.Username;
        password  = 'Pwerddfg1';
              
    }

    //method to test all use cases.
    private static Testmethod void logintoCommunityTest(){
        init();
        Test.startTest();
        
        System.debug(pageUser);
        
        CustomLoginController ctrl = new CustomLoginController();
        ctrl.username = username;
        
        PageReference retRef = ctrl.logintoCommunity();
            
        //User still not agreed the terms and condition
        system.assertEquals(retRef,null);
        
    
        Test.stopTest();
    }
    
    private static Testmethod void agreeTermsTest(){
        init();
        Test.startTest();
        
        System.debug(pageUser);
        User pgUsr = [SELECT Id FROM User WHERE FirstName ='JASON' AND LastName = 'LIVESTON' LIMIT 1];
        
        CustomLoginController ctrl = new CustomLoginController();
        ctrl.username = username;
        ctrl.password = 'Pwerddfg1';
        ctrl.pageUser = pgUsr;
        //ctrl.isTermsCheck = false;
        PageReference retRef = ctrl.logintoCommunity();
        system.runAs(pgUsr){
            retRef = ctrl.agreeTerms();  
        }    
        
        //User still not agreed the terms and condition
        system.assertNotEquals(retRef,null);
        
    
        Test.stopTest();
    }

    private static Testmethod void agreeTermsAlreadyAgreedTest(){
        init();
        Test.startTest();
        
        System.debug(pageUser);
        User pgUsr = [SELECT Id, Terms_and_Condition__c FROM User WHERE FirstName ='JASON' AND LastName = 'LIVESTON' LIMIT 1];
        pgUsr.Terms_and_Condition__c = true;
        //Database.Update(pgUsr);
        CustomLoginController ctrl = new CustomLoginController();
        ctrl.username = username;
        ctrl.password = 'PwerdHfg1';
        ctrl.pageUser = pgUsr;
        system.setPassword(pgUsr.Id, 'PwerdHfg1');
        //ctrl.isTermsCheck = false;
        PageReference retRef = ctrl.logintoCommunity();
        system.runAs(pgUsr){
            retRef = ctrl.agreeTerms();  
        }    
        
        //User still not agreed the terms and condition
        system.assertNotEquals(retRef,null);
        
    
        Test.stopTest();
    }
    
     private static Testmethod void dagreeTermsTest(){
        init();
        Test.startTest();
        
        System.debug(pageUser);
        
        CustomLoginController ctrl = new CustomLoginController();
        ctrl.username = username;
        
        PageReference retRef = ctrl.logintoCommunity();
            
        retRef = ctrl.dagreeTerms();    
        //User still not agreed the terms and condition
        system.assertEquals(retRef,null);
        
    
        Test.stopTest();
    }

}