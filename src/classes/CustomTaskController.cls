public without sharing class CustomTaskController {
    public String taskID {get;set;}
    public Task tsk {get;set;}
    public List<Application__c> app {get;set;}
    public list<certificate__c> cerList{get;set;}
    public String comments{get;set;}
    public Schema.SObjectType objType {get;set;}
    public id recordId{get;set;}
    public string detailurl{get;set;}
    public string objectName{get;set;}
    public boolean taskResponded{get;set;}
    public string descriptionText{get;set;}

    public PageReference forwardToCustomAuthPage() {
        string taskID = ApexPages.currentPage().getParameters().get('id');
        task tk = [select id,status,Activity_Number__c  from task where id =:taskID ]; 
        string taskStatus = tk.status ;

        if(taskStatus.equalsIgnoreCase('Responded')){
            taskResponded = true;
            PageReference pg  = new PageReference('/apex/CustomTaskDetailView');
            pg.setRedirect(false);
            return pg;
        }
        else{
            return null;
        }
    }

    public PageReference close() {
        return new PageReference('/apex/DistributorLanding');
    }

    public PageReference goToApplication() {
        return new PageReference('/apex/reviewEligibility?id=' + app[0].Builder_Number__c + '&AppID=' + app[0].id);
    } 

    public PageReference markAsDone(){
        if(!tsk.mark_as_done__c){
            tsk.status = 'Responded';
            tsk.mark_as_done__c = True;
            if(objectName.equalsIgnoreCase('Application__c')){
                if(String.isBlank(app[0].external_comments__c)){
                   app[0].external_comments__c = system.now().format('dd/MM/yyyy HH:mm:ss ') + 'SYSTEM: Task - ' + tsk.Description ; 
                }else{
                    app[0].external_comments__c += '\r\n' + '\r\n' + system.now().format('dd/MM/yyyy HH:mm:ss ') + 'SYSTEM: Task - ' + tsk.Description ; 
                }
                app[0].New_Information_Entered__c = System.now();
                
                try{
                    update app;   
                } catch(DmlException e) {
                    System.debug('An unexpected error has occurred: ' + e.getMessage());
                }
            }else if (objectName.equalsIgnoreCase('Certificate__c')){

                if(String.isBlank(cerList[0].external_comments__c)){
                   cerList[0].external_comments__c = system.now().format('dd/MM/yyyy HH:mm:ss ') + 'SYSTEM: Task - ' + tsk.Description ; 
                }else{
                    cerList[0].external_comments__c += '\r\n' + '\r\n'  + system.now().format('dd/MM/yyyy HH:mm:ss ') + 'SYSTEM: Task - ' + tsk.Description ; 
                }

                System.debug('cerList[0].sub_status__c<><>'+cerList[0].sub_status__c);

                if(tsk.Subject == label.Task_Subject_DD_Review){
                    cerList[0].sub_status__c = constants.New_COI;
                
                    Group batQueue =  [select Id from Group where DeveloperName = 'BAT_Queue' and Type = 'Queue' ];

                    cerList[0].ownerId = batQueue.id;
                }else if(cerList[0].sub_status__c == GlobalConstants.SUB_STAT_AWAITING_INFORMATION) {
                    cerList[0].sub_status__c = GlobalConstants.APPLICATION_STATUS_NEW_INFORMATION_RECEIVED;
                }
               // app[0].New_Information_Entered__c = System.now();
               System.debug('cerList<><>'+cerList);
                try{
                    update cerList;   
                } catch(DmlException e) {
                    System.debug('An unexpected error has occurred: ' + e.getMessage());
                }

                
            }
            system.debug('hey2: ' + tsk.status);
            try{
                    update tsk;   
                } catch(DmlException e) {
                    System.debug('An unexpected error has occurred: ' + e.getMessage());
                }

        }
        return new PageReference('/apex/DistributorLanding');
    }

    public PageReference SaveTask(){

       
        if(comments != null && comments != ''){
            if(String.isBlank(tsk.Description)){
                tsk.Description = system.now().format('dd/MM/yyyy HH:mm:ss ') + '[Comments By ' + userinfo.getName() + '] : ' + comments ;
            }else
                tsk.Description = tsk.Description + '\n' + '\n'+ system.now().format('dd/MM/yyyy HH:mm:ss ') + '[Comments By ' + userinfo.getName() + '] : ' + comments ;
        }
       if(tsk.status.equalsIgnoreCase('Responded')){
            taskResponded = true;
        }else{
            taskResponded = false;
        }
        update tsk ;
            System.debug('Update Task'+tsk);
            comments = '';
            if(!String.isBlank(tsk.description)){
                descriptionText = tsk.description;
                descriptionText = descriptionText.replace('\n', '<br/>');
            }
        return null ;
        
        
    }

    public PageReference confirmation(){

        PageReference pg = new PageReference('/apex/CustomTaskDetailView');
        pg.getParameters().put('id',taskID);
        pg.setRedirect(true);

        system.debug('pg>>'+pg);
        return pg;
    }

    public PageReference editPage(){
        PageReference pg = new PageReference('/apex/CustomTaskView');
        pg.setRedirect(false);

        system.debug('pg>>'+pg);
        return pg;
    }

    public CustomTaskController(){
        tsk = new Task();
        app = new List<Application__c>();
        cerList = new list<Certificate__c>();
        taskResponded = false;
        taskID = ApexPages.currentPage().getParameters().get('id');
        comments = '';
        descriptionText = '';
        List<Certificate__c> tempCert = new List<Certificate__c>();
        Map<String, Id> certRecordTypes = GlobalUtility.getsObjectRecordTypes('Certificate__c');

        try{
            tsk = [Select id, Activity_Number__c ,subject,createdBy.name, mark_as_done__c, whatId,what.name, status, ownerID, description, whoID, activitydate, createddate,        Builder__c,Builder__r.Name from Task where id=:taskId];    
        } catch(DmlException e) {
                System.debug('An unexpected error has occurred: ' + e.getMessage());
                
        }

        if(tsk.status.equalsIgnoreCase('Responded')){ 
            taskResponded = true;
        }

         recordId =  tsk.whatId ;
         objType = recordId.getSobjectType();
         if(!String.isBlank(tsk.description)){
             descriptionText = tsk.description;
             descriptionText = descriptionText.replace('\n', '<br/>');
         }


         objectName = string.valueOf(objType);
         if(objectName.equalsIgnoreCase('Certificate__c')){

            tempCert = [select id,name ,external_comments__c,sub_status__c,Builder__c,Builder__r.name, RecordTypeId, status__c, Policy_Number__c from Certificate__c where id=: tsk.whatId] ; 
            if(tempCert[0].RecordTypeId == certRecordTypes.get(GlobalConstants.REC_TYPE_CONTAINER)){
                cerList = [SELECT Id, name, external_comments__c,sub_status__c,Builder__c,Builder__r.name, status__c, Policy_Number__c from Certificate__c where Parent_COI__c=: tsk.whatId];
            }else{
                cerList = tempCert;
            }

            detailurl = '/apex/certificateDetailPage?id='+tsk.whatId;
         }else if (objectName.equalsIgnoreCase('Application__c' )){
            
                //detailurl = '/apex/certificateDetailPage?id'+tsk.whatId;
                
                try{
                 app = [Select id, name, external_comments__c, New_Information_Entered__c, review_reason__c, application_status__c, Builder_Number__c from Application__c where id=:tsk.whatId Limit 1];
            } catch(DmlException e) {
                    System.debug('An unexpected error has occurred: ' + e.getMessage());
                    
            }

            detailurl = '' ; 

         }
         /*
         System.debug('CreatedById:::'+tsk.createdBy.name);
         string tskDescription = tsk.Description ;

        if(!tskDescription.contains('Comment By')){ 
            comments = 'Comment By' + ' [ '+ tsk.createdBy.name+ ' ]: ' +  tsk.Description;
        }else{
            comments = tsk.Description ;
        }
        
        */
         System.debug('detailurl>'+detailurl);

    }   
    
    public List<Attachment> getTaskAttachment() {
        system.debug('@@@@@@ -> gettingAttachments');
        List<Attachment> lstAtt = new List<Attachment>();
        lstAtt = [SELECT Name,ContentType, CreatedDate, CreatedById, CreatedBy.Name FROM Attachment WHERE ParentId = :recordId ORDER BY CreatedDate DESC];
        return lstAtt;
    }
    
}