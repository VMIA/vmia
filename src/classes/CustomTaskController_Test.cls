/**
  * Date         :  25-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for CustomTaskController
  */ 
  
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
private with sharing class CustomTaskController_Test{
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> certRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> appRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup(){

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);

        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,
                                                        conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        
        Application__c app = TestUtility.createApplication(bornBuilder.Id,rexDistributor.Id,'New Eligibility',
                                                            appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
        app.External_Comments__c = 'DD/Builder comments';
        Database.insert(app);

        Task appTsk = TestUtility.createTask(app.Id,rexSmith.Id,bornBuilder.Id);
        appTsk.Description = 'App Description';

        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        cert.External_Comments__c = 'DD/Builder comments';
        Database.insert(cert);

        Task certTsk = TestUtility.createTask(cert.Id,rexSmith.Id,bornBuilder.Id);
        certTsk.Description = 'Certificate Description';
        certTsk.Subject = Label.Task_Subject_DD_Review;
        Database.insert(new List<Task>{appTsk,certTsk});
    }

    @future
    private static void setupUser(){
        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(new List<User>{vmiaInternalUser,rexUser});
    }
    
    /**
      * @description       This method tests scenario of task view from the portal
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testPageLoad(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: app.Id LIMIT 1];
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            Test.startTest();
                CustomTaskController controller  = new CustomTaskController();
            Test.stopTest();
            System.assert(controller.tsk != null);
            System.assert(controller.app != null);
            System.assertEquals(controller.recordId,app.Id);
        }
    }
    
    /**
      * @description       This method tests scenario of task close from the portal
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testTaskClose(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: app.Id LIMIT 1];
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            Test.startTest();
                controller.close();
            Test.stopTest();
        }
    }
    
    /**
      * @description       This method tests scenario of mark done for task from the portal
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testMarkDoneAppTask(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: app.Id LIMIT 1];
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskDetailView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            Test.startTest();
                controller.markAsDone();
            Test.stopTest();
        }
        Application__c appAfterTest = [SELECT New_Information_Entered__c FROM Application__c LIMIT 1];
        if(appAfterTest.New_Information_Entered__c != null){
            System.assertEquals(System.today(),appAfterTest.New_Information_Entered__c.date());
        }
    }

    /**
      * @description       This method tests scenario of mark done for task from the portal without comments on application
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testMarkDoneAppTaskWithoutExternalComments(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        app.External_Comments__c = null;
        Database.update(app);

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: app.Id LIMIT 1];
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskDetailView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            Test.startTest();
                controller.markAsDone();
            Test.stopTest();
        }
        Application__c appAfterTest = [SELECT External_Comments__c FROM Application__c LIMIT 1];
        System.assert(appAfterTest.External_Comments__c.contains('SYSTEM: Task'));
    }

    /**
      * @description       This method tests scenario of mark done for task from the portal
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testMarkDoneCertTask(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Certificate__c cert = [SELECT Id FROM Certificate__c LIMIT 1];

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: cert.Id LIMIT 1];
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskDetailView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            Test.startTest();
                controller.markAsDone();
            Test.stopTest();
        }
        Group batQueue =  [select Id from Group where DeveloperName = 'BAT_Queue' and Type = 'Queue' ];
        Certificate__c certAfterTest = [SELECT OwnerId FROM Certificate__c LIMIT 1];
        System.assertEquals(batQueue.Id,certAfterTest.OwnerId);
    }

    /**
      * @description       This method tests scenario of mark done for certificate task from the portal without comments on certificate
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testMarkDoneCertTaskWithoutComments(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Certificate__c cert = [SELECT Id FROM Certificate__c LIMIT 1];
        cert.External_Comments__c = null;
        Database.update(cert);

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: cert.Id LIMIT 1];
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskDetailView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            Test.startTest();
                controller.markAsDone();
            Test.stopTest();
        }
        Certificate__c certAfterTest = [SELECT External_Comments__c FROM Certificate__c LIMIT 1];
        System.assert(certAfterTest.External_Comments__c.contains('SYSTEM: Task'));
    }

    /**
      * @description       This method tests scenario of mark done for certificate task from the portal with status awaiting information
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testMarkDoneCertTaskAwaitingInfo(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Certificate__c cert = [SELECT Id FROM Certificate__c LIMIT 1];
        cert.Sub_Status__c = GlobalConstants.SUB_STAT_AWAITING_INFORMATION;
        cert.Further_Information_Summary__c = 'Further Information Required';
        cert.Further_Information_Description__c = 'Further Information Required';
        Database.update(cert);

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: cert.Id LIMIT 1];
        tsk.Subject = 'Awaiting Info';
        Database.update(tsk);
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskDetailView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            Test.startTest();
                controller.markAsDone();
            Test.stopTest();
        }
        Certificate__c certAfterTest = [SELECT Sub_Status__c FROM Certificate__c LIMIT 1];
        System.assertEquals(GlobalConstants.APPLICATION_STATUS_NEW_INFORMATION_RECEIVED,certAfterTest.Sub_Status__c);
    }
    
    /**
      * @description       This method tests scenario of goToApplication method from the portal
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testGoToApplication(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: app.Id LIMIT 1];
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            Test.startTest();
                controller.goToApplication();
            Test.stopTest();
        }
    }
    
    /**
      * @description       This method tests scenario of confirmation method from the portal
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testConfirmation(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: app.Id LIMIT 1];
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            Test.startTest();
                controller.confirmation();
            Test.stopTest();
        }
    }
    
    /**
      * @description       This method tests scenario of confirmation method from the portal
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testEditPage(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: app.Id LIMIT 1];
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskDetailView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            Test.startTest();
                controller.editPage();
            Test.stopTest();
        }
    }
    
    /**
      * @description       This method tests scenario of saveTask method from the portal
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSaveTask(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: app.Id LIMIT 1];
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            controller.comments = 'New comments';
            Test.startTest();
                controller.SaveTask();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of saveTask method from the portal without description & responded
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSaveTaskNoDescription(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: app.Id LIMIT 1];
        tsk.Description = null;
        tsk.Status = 'Responded';
        Database.update(tsk);

        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            controller.comments = 'New comments';
            Test.startTest();
                controller.SaveTask();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of getTaskAttachment method from the portal
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testGetAttachment(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: app.Id LIMIT 1];
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskDetailView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            Test.startTest();
                controller.getTaskAttachment();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of forwardToCustomAuthPage method from the portal with responded task
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testForwardToCustomAuthPageResponded(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: app.Id LIMIT 1];
        tsk.Status = 'Responded';
        Database.update(tsk);
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            Test.startTest();
                controller.forwardToCustomAuthPage();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of forwardToCustomAuthPage method from the portal
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testForwardToCustomAuthPage(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];

        Task tsk = [SELECT WhatId,WhoId,Builder__c FROM Task WHERE WhatId =: app.Id LIMIT 1];
        System.runAs(rexUser){
            
            PageReference pageRef = Page.CustomTaskView;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',tsk.Id);
            CustomTaskController controller  = new CustomTaskController();
            Test.startTest();
                controller.forwardToCustomAuthPage();
            Test.stopTest();
        }
    }
}