/**
  * Date         :  15-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Apex batch class to share access on applications to distributor
  *                 Sharing of applications  with distributor fails if the volume 
                    is more than 10K in a transaction due to governor limit as of Spring 17 release.
  *                 Class is without sharing as the execution of this is by community users & AccessLevel is not
  *                 writeable for the community profile. This batch is used for when builders create applications.
  */
  global class DistributorAccessBatch implements Database.Batchable<sObject> {
	
	String query;
	
	global DistributorAccessBatch() {
		query = 'SELECT DD_Account__c, OwnerId from Application__c WHERE CreatedDate = TODAY AND CreatedBy.Profile.Name = \'Builder\'' ;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
	    List<Application__c> appList = new List<Application__c>();
	    for (sObject objScope: scope) { 
	        Application__c newObjScope = (Application__c)objScope ;//type casting from generic sOject to Application__c
	        appList.add(newObjScope);//Add records to the List
	        System.debug('Value of appList: '+appList);
	    } 
        if (appList != null && appList.size()>0) {//Check if List is empty or not
            DistributorAccessController.shareApplicationsWithDistributor(appList);
            System.debug('List Size '+appList.size());//Update the Records
        }
	}
	
	global void finish(Database.BatchableContext BC) {
	}
	
}