/**
* Date         :  23-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for DistributorAccessBatch controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class DistributorAccessBatch_Test {
	private static Map<String,Id> profileMap = new Map<String,Id>();

	@testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        
        

        setupData();
        setupUsers();
    }
	
	@future
    private static void setupData(){
    	TestUtility.createCustomSettings();

        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        
        
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);

    }

    @future
    private static void setupUsers(){
    	Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        Contact tonyStark = [SELECT id FROM Contact WHERE FirstName = 'Tony' and LastName = 'Stark' LIMIT 1];
        Contact rexSmith = [SELECT id FROM Contact WHERE FirstName = 'Rex' and LastName = 'Smith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];

        User builderUser = TestUtility.createPortalUser('ExternB',profileMap.get('Builder'),tonyStark.Id);
        Database.insert(builderUser);

        User ddUser = TestUtility.createPortalUser('ExternD',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(ddUser);

        System.RunAs(ddUser) {
        	Application__c  app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
    		Database.insert(app);
    		System.debug('** Application ==>'+ app);
        }
    }


    public static testMethod void executeBatchAndShareWithDistributorTest(){
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];	
		Application__c app = [Select Id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];

        User distributor = [SELECT AccountId, LastName, UserRoleId, UserRole.DeveloperName FROM User WHERE LastName = 'ExternD' LIMIT 1];		
        Group grp = [SELECT Name, DeveloperName,RelatedId,Type 
                                        FROM Group WHERE Type =: GlobalConstants.ROLE_AND_SUBORDINATES AND
                                        RelatedId =:distributor.UserRoleId LIMIT 1];

        DistributorAccessBatch controller = new DistributorAccessBatch();
        Database.BatchableContext BC;
        CronTrigger ct;
        String query = 'Select Id, Name, DD_Account__c, OwnerId, Builder_Number__c from Application__c where Id = \'' + app.Id + '\'';
        List<Application__c> apps = Database.query(query);
        
        system.debug('** App list: ' + apps);
        system.debug('** role ' + grp.Id);
        System.RunAs(new User(Id=UserInfo.getUserId())) {
			Test.startTest();
         //   controller.start(BC);
	        controller.execute(BC, apps);
         //   controller.finish(BC);
            String CRON_EXP = '0 0 * * * ?'; 
            DistributorSharingSchedulable obj = new DistributorSharingSchedulable();
            String jobId = System.schedule('Distributor Access Job', CRON_EXP, obj);
            ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                                FROM CronTrigger WHERE id = :jobId];

	        Test.stopTest(); 
        }   
        List<Application__Share> appShr = [SELECT AccessLevel, UserOrGroupId FROM Application__Share WHERE ParentId=:app.Id AND UserOrGroupId=:grp.Id];
        system.debug('** appShrList ' + appShr);
        System.assertEquals(0, ct.TimesTriggered);
        //System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));

        System.assertEquals(1, appShr.size());			
        System.assertEquals('Edit', appShr[0].AccessLevel);            
    }
	
}