/**
  * Date         :  14-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Apex class to share access on certificates & applications to distributor
  *                 Sharing of applications or certificates with distributor fails if the volume 
                    is more than 10K in a transaction due to governor limit as of Spring 17 release.
  *                 Class is without sharing as the execution of this is by community users & AccessLevel is not
  *                 writeable for the community profile.
  */
public without sharing class DistributorAccessController{
    
    //constants
    private static final String EDIT_ACCESS = 'Edit';
    private static final String READ_ACCESS = 'Read';
    private static final String ACCESS_LEVEL = 'AccessLevel';
    private static final String MANUAL_ROW_CAUSE = 'Manual';
    private static final String COMMUNITY_PLUS_USER_TYPE = 'PowerCustomerSuccess';
    private static final String REC_TYPE_DISTRIBUTOR = 'Distributor';
    private static final String SHARE_ERROR_MSG = 'Unable to grant sharing access due to following exception: ';
    
    /**
      * @description       This method is invoked from application trigger & Visualforce page to 
      * @param             certificateList - List of certificates to be shared 
      * @return            void
      * @throws            Method might throw exception which is handled by try-catch block
      */ 
    public static void shareCertificatesWithDistributor(List<Certificate__c> certificateList){
        
        try{
            Set<Id> distributorIds  = new Set<Id>();
            List<Certificate__c> certificatesToShare  = new List<Certificate__c>();
            
            for(Certificate__c cert : [SELECT Builder__c,Builder__r.Agent__c,Distributor__c,OwnerId FROM Certificate__c WHERE Id IN: certificateList]){
                Id distributorId = (cert.Builder__c != null && cert.Builder__r.Agent__c != null) ?  
                                    cert.Builder__r.Agent__c : (cert.Distributor__c != null) ? cert.Distributor__c : null;
                if(distributorId != null){
                    distributorIds.add(distributorId);
                    certificatesToShare.add(cert);
                }
            }

            if(!distributorIds.isEmpty()){
                Set<Id> userRoleIds = new Set<Id>();
                Map<Id,String> distributorRoleMap = new Map<Id,String>();
                for(User distributor : [SELECT AccountId,UserRoleId,UserRole.DeveloperName FROM User WHERE AccountId IN: distributorIds]){
                    userRoleIds.add(distributor.UserRoleId);
                    distributorRoleMap.put(distributor.AccountId,distributor.UserRole.DeveloperName);
                }
                
                 if(!userRoleIds.isEmpty()){
                    Map<String,Id> roleGroupMap = new Map<String,Id>();
                    for(Group grp : [SELECT Name,DeveloperName,RelatedId,Type 
                                        FROM Group WHERE Type =: GlobalConstants.ROLE_AND_SUBORDINATES AND
                                        RelatedId IN: userRoleIds]){
                        roleGroupMap.put(grp.DeveloperName,grp.Id);
                    }
                    List<Certificate__Share> certShareList = new List<Certificate__Share>();
                    for(Certificate__c cert : certificatesToShare){
                        Id distributorId = (cert.Builder__c != null && cert.Builder__r.Agent__c != null) ?  
                                            cert.Builder__r.Agent__c : (cert.Distributor__c != null) ? cert.Distributor__c : null;
                        Certificate__Share certShare = new Certificate__Share(ParentId = cert.Id,AccessLevel = EDIT_ACCESS,
                                                                                RowCause = Schema.Invoice__share.RowCause.DistributorAccess__c);
                        Id userOrGroupIdVal = roleGroupMap.get(distributorRoleMap.get(distributorId));
                        if(userOrGroupIdVal != null && userOrGroupIdVal != cert.OwnerId){
                            certShare.UserOrGroupId = userOrGroupIdVal;
                            certShareList.add(certShare);
                        } 
                    }
                
                    if(!certShareList.isEmpty()){
                        Database.insert(certShareList);
                    }
                }
            }
            
        }
        catch(Exception exp){
             // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(DistributorAccessController.class),
                                GlobalConstants.FN_SHARE_CERTIFICATE,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }

    /**
      * @description       This method is invoked from application trigger & Visualforce page to 
      * @param             certificateList - List of certificates to be shared 
      * @return            void
      * @throws            Method might throw exception which is handled by try-catch block
      */ 
    public static void shareApplicationsWithDistributor(List<Application__c> applicationList){
        system.debug('@@@@@here');
        try{
            Set<Id> distributorIds  = new Set<Id>();
            List<Application__c> appsToShare  = new List<Application__c>();
            
            for(Application__c app : applicationList){
                if(app.DD_Account__c != null){
                    distributorIds.add(app.DD_Account__c);
                    appsToShare.add(app);
                    system.debug('@@@@@here2');
                }
            }

            if(!distributorIds.isEmpty()){
                Set<Id> userRoleIds = new Set<Id>();
                Map<Id,String> distributorRoleMap = new Map<Id,String>();
                for(User distributor : [SELECT AccountId,UserRoleId,UserRole.DeveloperName FROM User WHERE AccountId IN: distributorIds]){
                    userRoleIds.add(distributor.UserRoleId);
                    distributorRoleMap.put(distributor.AccountId,distributor.UserRole.DeveloperName);
                }
                
                 if(!userRoleIds.isEmpty()){
                    Map<String,Id> roleGroupMap = new Map<String,Id>();
                    for(Group grp : [SELECT Name,DeveloperName,RelatedId,Type 
                                        FROM Group WHERE Type =: GlobalConstants.ROLE_AND_SUBORDINATES AND
                                        RelatedId IN: userRoleIds]){
                        roleGroupMap.put(grp.DeveloperName,grp.Id);
                    }
                    system.debug('** Group ' + roleGroupMap);
                    List<Application__Share> appShareList = new List<Application__Share>();
                    for(Application__c app : appsToShare){
                        Application__Share appShare = new Application__Share(ParentId = app.Id,AccessLevel = EDIT_ACCESS,
                                                                                RowCause = Schema.Invoice__share.RowCause.DistributorAccess__c);
                        Id userOrGroupIdVal = roleGroupMap.get(distributorRoleMap.get(app.DD_Account__c));
                        if(userOrGroupIdVal != null && userOrGroupIdVal != app.OwnerId){
                            appShare.UserOrGroupId = userOrGroupIdVal;
                            appShareList.add(appShare);
                            system.debug('@@@@@here3 ' + userOrGroupIdVal);
                        } 
                    }
                
                    if(!appShareList.isEmpty()){
                        system.debug('@@@@@here4');
                        Database.insert(appShareList);
                    }
                }
            }
            
        }
        catch(Exception exp){
             // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(DistributorAccessController.class),
                                GlobalConstants.FN_SHARE_APPLICATION,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }

    /**
      * @description       This method is invoked from inovice trigger to 
      *                    share invoice with distributor staff
      * @param             invoiceList - List of invoices to be shared 
      * @return            void
      * @throws            Method might throw exception which is handled by try-catch block
      */ 
    public static void shareInvoiceWithDistributor(List<Invoice__c> invoiceList){
        Set<Id> purchaserIds = new Set<Id>();
        List<Invoice__c> invoicesToShare = new List<Invoice__c>();
        Set<Id> distributorIds = new Set<Id>();
        Map<Id,Id> builderDistributorMap = new Map<Id,Id>();
        Map<Id,Set<Id>> distributorStaffMap = new Map<Id,Set<Id>>();
        List<Invoice__Share> invShareList = new List<Invoice__Share>();
        try{
            for(Invoice__c inv : invoiceList){
                if(inv.Purchaser__c != null){
                    purchaserIds.add(inv.Purchaser__c);
                    invoicesToShare.add(inv);
                }
            }
            
            if(!purchaserIds.isEmpty()){
               
                for(Account acc : [SELECT RecordType.Name,Agent__c FROM Account WHERE Id IN: purchaserIds]){
                    if(REC_TYPE_DISTRIBUTOR.equalsIgnoreCase(acc.RecordType.Name)){
                        distributorIds.add(acc.Id);
                    }
                    else{
                        distributorIds.add(acc.Agent__c);
                        builderDistributorMap.put(acc.Id,acc.Agent__c);
                    }
                }
                
                
                distributorStaffMap = fetchDistributorMap(new Set<Id>(distributorIds));

                if(!distributorStaffMap.isEmpty()){
                    
                    for(Invoice__c invoice : invoicesToShare){
                        Id distributorId = distributorIds.contains(invoice.Purchaser__c) ? invoice.Purchaser__c : 
                                                builderDistributorMap.get(invoice.Purchaser__c);
                        for(Id userId : distributorStaffMap.get(distributorId)){
                            if(userId != invoice.OwnerId){  
                                Invoice__share invShare = new Invoice__share();
                                invShare.AccessLevel = EDIT_ACCESS;
                                invShare.ParentID = invoice.id; 
                                invShare.RowCause = Schema.Invoice__share.RowCause.DistributorAccess__c;
                                invShare.UserOrGroupId = userId;
                                invShareList.add(invShare); 
                            }
                        }
                    }
                    
                    if(!invShareList.isEmpty()){    // check if list is empty before DML
                        Database.insert(invShareList);
                    }
                }
            }
        }
        catch(Exception exp){
             // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(DistributorAccessController.class),
                                GlobalConstants.FN_SHARE_INVOICE,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }

    /**
      * @description       This method is invoked from account trigger to 
      *                    share builders with distributor staff
      * @param             builderList - List of builders to be shared 
      * @return            void
      * @throws            Method might throw exception which is handled by try-catch block
      */ 
    public static void shareBuildersWithDistributor(List<Account> builderList){
        try{
            Set<Id> distributorIds = new Set<Id>();
            List<Id> validBuilderIds = new List<Id>();
            List<Account> noAgentAccounts = new List<Account>();
            Map<Id, Id> builderDistributorIDsMap = new Map<Id, Id>();

            for(Account acc : builderList){
                if(acc.Agent__c != null){
                    distributorIds.add(acc.Agent__c);
                    validBuilderIds.add(acc.Id);
                    builderDistributorIDsMap.put(acc.Id, acc.Agent__c);
                }
                else if(acc.recordTypeId != GlobalUtility.getsObjectRecordTypes('Account').get(GlobalConstants.REC_TYPE_DISTRIBUTOR)){
                    noAgentAccounts.add(acc);
                }
            }

            if(!noAgentAccounts.isEmpty()){
                for(Property_Owners__c prop : [SELECT Property_Owner__c, Contact__c, Contact__r.AccountId, Certificate__c, Certificate__r.Distributor__c,
                                                Certificate__r.Builder__c, Certificate__r.Builder__r.Agent__c 
                                                FROM Property_Owners__c 
                                                WHERE Contact__r.AccountId IN: noAgentAccounts OR Property_Owner__c IN: noAgentAccounts]){
                    Id distributorId = (prop.Certificate__r.Builder__c != null && prop.Certificate__r.Builder__r.Agent__c != null) ?  
                                        prop.Certificate__r.Builder__r.Agent__c : (prop.Certificate__r.Distributor__c != null) ? 
                                        prop.Certificate__r.Distributor__c : null;
                    if(distributorId != null){
                        distributorIds.add(distributorId);
                        if(prop.Property_Owner__c != null){
                            validBuilderIds.add(prop.Property_Owner__c);
                            builderDistributorIDsMap.put(prop.Property_Owner__c, distributorId);
                        }
                        else{
                            validBuilderIds.add(prop.Contact__r.AccountId);
                            builderDistributorIDsMap.put(prop.Contact__r.AccountId, distributorId);
                        }
                    }
                }
            }
            
        
            if(!validBuilderIds.isEmpty()){
                Set<Id> userRoleIds = new Set<Id>();
                Map<Id,String> distributorRoleMap = new Map<Id,String>();
                for(User distributor : [SELECT AccountId,UserRoleId,UserRole.DeveloperName FROM User WHERE AccountId IN: distributorIds]){
                    userRoleIds.add(distributor.UserRoleId);
                    distributorRoleMap.put(distributor.AccountId,distributor.UserRole.DeveloperName);
                }
                
                if(!userRoleIds.isEmpty()){
                    Map<String,Id> roleGroupMap = new Map<String,Id>();
                    for(Group grp : [SELECT Name,DeveloperName,RelatedId,Type 
                                        FROM Group WHERE Type =: GlobalConstants.ROLE_AND_SUBORDINATES AND
                                        RelatedId IN: userRoleIds]){
                        roleGroupMap.put(grp.DeveloperName,grp.Id);
                    }
                    List<AccountShare> accShareList = new List<AccountShare>();
                    for(Id builderId : validBuilderIds){
                        AccountShare accShare = new AccountShare(AccountId = builderId,AccountAccessLevel = EDIT_ACCESS,
                                                                    OpportunityAccessLevel = READ_ACCESS, CaseAccessLevel = READ_ACCESS, 
                                                                    RowCause = MANUAL_ROW_CAUSE);
                        Id distributorIdForUserGroupVal = builderDistributorIDsMap.get(builderId);
                        Id userOrGroupIdVal = roleGroupMap.get(distributorRoleMap.get(distributorIdForUserGroupVal));
                        if(userOrGroupIdVal != null){
                            accShare.UserOrGroupId = userOrGroupIdVal;
                            accShareList.add(accShare);
                        }
                    }
                    if(!accShareList.isEmpty()){
                        Database.insert(accShareList);
                    }
                }
            }
        }
        catch(Exception exp){
             // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(DistributorAccessController.class),
                                GlobalConstants.FN_SHARE_BUILDER,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }
    
    /**
      * @description       This method is invoked from shareApplicationsWithDistributor & shareCertificatesWithDistributor
      *                    methods to fetch distributor staff
      * @param             distributorSet - Set of distributor ids 
      * @return            Map<Id,Set<Id>>
      * @throws            NA
      */ 
    private static Map<Id,Set<Id>> fetchDistributorMap(Set<Id> distributorSet){
        Map<Id,Set<Id>> distributorStaffMap = new Map<Id,Set<Id>>();
        for(User distributor : [SELECT AccountId FROM User WHERE AccountId IN: distributorSet 
                                    AND UserType =: COMMUNITY_PLUS_USER_TYPE AND IsActive = true]){
            if(distributorStaffMap.containsKey(distributor.AccountId)){
                distributorStaffMap.get(distributor.AccountId).add(distributor.Id);
            }
            else{
                distributorStaffMap.put(distributor.AccountId,new Set<Id>{distributor.Id});
            }
        }
        return distributorStaffMap;
    }

    public static void removeSharingBuildersWithDistributor(List<Account> builderList){
        try{
            Set<Id> distributorIds = new Set<Id>();
            List<Account> validBuilders = new List<Account>();
            for(Account acc : builderList){
                if(acc.Agent__c != null){
                    distributorIds.add(acc.Agent__c);
                    validBuilders.add(acc);
                }
            }
        
            if(!validBuilders.isEmpty()){
                Set<Id> userRoleIds = new Set<Id>();
                Map<Id,String> distributorRoleMap = new Map<Id,String>();
                for(User distributor : [SELECT AccountId,UserRoleId,UserRole.DeveloperName FROM User WHERE AccountId IN: distributorIds]){
                    userRoleIds.add(distributor.UserRoleId);
                    distributorRoleMap.put(distributor.AccountId,distributor.UserRole.DeveloperName);
                }
                
                if(!userRoleIds.isEmpty()){
                    Map<String,Id> roleGroupMap = new Map<String,Id>();
                    for(Group grp : [SELECT Name,DeveloperName,RelatedId,Type 
                                        FROM Group WHERE Type =: GlobalConstants.ROLE_AND_SUBORDINATES AND
                                        RelatedId IN: userRoleIds]){
                        roleGroupMap.put(grp.DeveloperName,grp.Id);
                    }
                    List<AccountShare> accShareList = new List<AccountShare>();
                    list<Id> builderIds = new list<Id>();
                    list<Id> userOrGroupIdVals = new list<Id>();
                    for(Account builder : validBuilders){
                        builderIds.add(builder.Id);
                        userOrGroupIdVals.add(roleGroupMap.get(distributorRoleMap.get(builder.Agent__c)));
                    }

                    if(!userOrGroupIdVals.isEmpty()){
                        accShareList = [SELECT AccountId 
                                            FROM AccountShare WHERE AccountId IN : builderIds AND UserOrGroupId IN : userOrGroupIdVals];
                    }

                    if(!accShareList.isEmpty()){
                        Database.delete(accShareList);
                    }
                }
            }
        }
        catch(Exception exp){
             // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(DistributorAccessController.class),
                                GlobalConstants.FN_REMOVE_BUILDER_SHARING,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }
}