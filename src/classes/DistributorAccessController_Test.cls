/**
* Date         :  23-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for DistributorAccessBatch controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class DistributorAccessController_Test {    
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        UserRole batMember = TestUtility.getUserRole('BAT_Member');
        
        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        Database.insert(vmiaInternalUser);
        setupData();
        setupUsers();
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> rateTableRecTypeMap = Schema.Sobjecttype.PremiumRatesTable__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        
        PremiumRatesTable__c flatRate = TestUtility.createFlatRateTable(1500, rateTableRecTypeMap.get('FlatRate').getRecordTypeId());
        Database.insert(flatRate);

        Account texDistributor = TestUtility.createBusinessAccount('Tex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        texDistributor.website = 'www.textDis.com';
        Database.insert(texDistributor);
        System.debug('** texDistributor ==>'+ texDistributor);

        Contact texSmoth = TestUtility.createContact('Tex','Smoth','tex.smoth@texHomeDistributorTest.com',texDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(texSmoth);
        System.debug('** texSmoth ==>'+ texSmoth);

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);

        Certificate__c cert1 = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());

        Certificate__c cert2 = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
        Database.insert(new List<Certificate__c>{cert1,cert2});   


        Property__c property = TestUtility.createProperty('12 Test Street', 'TestVille', '3000', 'VIC');
        Database.insert(property);

        Property_Owners__c propOwner = TestUtility.createPropertyOwner(tonyStark.Id, cert1.Id, property.Id);
        propOwner.property_owner__c = texDistributor.Id;
        Database.insert(propOwner);

        Property_Owners__c propOwner2 = TestUtility.createPropertyOwner(tonyStark.Id, cert1.Id, property.Id);
        Database.insert(propOwner2);

        
    }

    @future
    private static void setupUsers(){
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();

        Contact tonyStark = [SELECT id FROM Contact WHERE FirstName = 'Tony' and LastName = 'Stark' LIMIT 1];
        Contact texSmoth = [SELECT id FROM Contact WHERE FirstName = 'Tex' and LastName = 'Smoth' LIMIT 1];
        Contact rexSmith = [SELECT id FROM Contact WHERE FirstName = 'Rex' and LastName = 'Smith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];
        Account texDistributor = [SELECT Id FROM Account WHERE Name = 'Tex Home Distributor' LIMIT 1];

        User builderUser = TestUtility.createPortalUser('ExternB',profileMap.get('Builder'),tonyStark.Id);
        Database.insert(builderUser);

        User ddUser = TestUtility.createPortalUser('ExternD',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(ddUser);

        User ddUser2 = TestUtility.createPortalUser('ExternDD',profileMap.get('Distributor'),texSmoth.Id);
        Database.insert(ddUser2);

        System.RunAs(ddUser) {
            Application__c app1 = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(), null);   
            Application__c app2 = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(), null);    
            Database.insert(new List<Application__c>{app1,app2});
        }
        System.RunAs(ddUser2){
            Invoice__c inv = TestUtility.createInvoice(texDistributor.Id, 'New', invRecTypeMap.get('New Invoice').getRecordTypeId());
            Database.insert(inv);
        }

    }
        
    private static testMethod void testShareCertificatesWithDistributor(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        List<Certificate__c> certList = [SELECT Id FROM Certificate__c WHERE Builder__c =: bornBuilder.Id];

        Test.startTest();       
        //DistributorAccessController controller = new DistributorAccessController();
        DistributorAccessController.shareCertificatesWithDistributor(certList);

        List<Certificate__Share> certShr = [SELECT AccessLevel, UserOrGroupId FROM Certificate__Share 
												WHERE ParentId IN: certList AND RowCause = 'DistributorAccess__c'];

        System.assertEquals(2, certShr.size());
        System.assertEquals('Edit', certShr[0].AccessLevel);    
        System.assertEquals('Edit', certShr[1].AccessLevel);        
        Test.stopTest();
    }

    private static testMethod void testShareApplicationsWithDistributor(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        List<Application__c> appList = [SELECT DD_Account__c, OwnerId FROM Application__c WHERE Builder_Number__c =: bornBuilder.Id];

        Test.startTest();       
        //DistributorAccessController controller = new DistributorAccessController();
        DistributorAccessController.shareApplicationsWithDistributor(appList);

        Integer appShr = [SELECT count() FROM Application__Share WHERE ParentId IN: appList AND RowCause = 'DistributorAccess__c'];

        System.assertEquals(2, appShr);
        Test.stopTest();
    }

    private static testMethod void testShareInvoicesPurchasedByDDWithDistributor(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];
        Account texDistributor = [SELECT Id FROM Account WHERE Name = 'Tex Home Distributor' LIMIT 1];
        User ddUser = [SELECT Id FROM User WHERE LastName = 'ExternD' LIMIT 1];
        Invoice__c invoice = [SELECT Id, Purchaser__c, OwnerId FROM Invoice__c WHERE Purchaser__c =: texDistributor.Id LIMIT 1];
        
        invoice.Purchaser__c = rexDistributor.Id;
        //invoice.OwnerId = texDistributor.Id;
        Database.Update(invoice);

        List<Invoice__c> invList = [SELECT Id, Purchaser__c, OwnerId FROM Invoice__c WHERE Purchaser__c =: rexDistributor.Id LIMIT 1];

        Test.startTest();       
        //DistributorAccessController controller = new DistributorAccessController();
        DistributorAccessController.shareInvoiceWithDistributor(invList);

        Integer invShr = [SELECT count() FROM Invoice__Share WHERE ParentId IN: invList AND RowCause = 'DistributorAccess__c'];

        System.assertEquals(1, invShr);
        Test.stopTest();
    }
    private static testMethod void testShareInvoicesPurchasedByBuilderWithDistributor(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];
        Account texDistributor = [SELECT Id FROM Account WHERE Name = 'Tex Home Distributor' LIMIT 1];
        User builderUser = [SELECT Id FROM User WHERE LastName = 'ExternD' LIMIT 1];
        Invoice__c invoice = [SELECT Id, Purchaser__c, OwnerId FROM Invoice__c WHERE Purchaser__c =: texDistributor.Id LIMIT 1];
        
        invoice.Purchaser__c = bornBuilder.Id;
        //invoice.OwnerId = texDistributor.Id;
        Database.Update(invoice);

        List<Invoice__c> invList = [SELECT Id, Purchaser__c, OwnerId FROM Invoice__c WHERE Purchaser__c =: bornBuilder.Id LIMIT 1];

        Test.startTest();       
        //DistributorAccessController controller = new DistributorAccessController();
        DistributorAccessController.shareInvoiceWithDistributor(invList);

        Integer invShr = [SELECT count() FROM Invoice__Share WHERE ParentId IN: invList AND RowCause =: Schema.Invoice__share.RowCause.DistributorAccess__c];

        System.assertEquals(1, invShr);
        Test.stopTest();
    }

    private static testMethod void testShareBuildersWithDistributor(){
        Account bornBuilder = [SELECT id, Agent__c FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];
        Account texDistributor = [SELECT Id FROM Account WHERE Name = 'Tex Home Distributor' LIMIT 1];
        User builderUser = [SELECT Id FROM User WHERE LastName = 'ExternD' LIMIT 1];
        
        bornBuilder.Agent__c = texDistributor.Id;
        //invoice.OwnerId = texDistributor.Id;
        //Database.Update(bornBuilder);

        List<Account> builderList = new List<Account>{bornBuilder};

        Test.startTest();       
        //DistributorAccessController controller = new DistributorAccessController();
        DistributorAccessController.shareBuildersWithDistributor(builderList);

        Integer accShr = [SELECT count() FROM AccountShare WHERE AccountId IN: builderList AND RowCause =: 'Manual'];

        System.assertEquals(1, accShr);
        Test.stopTest();
    }

    private static testMethod void testShareNoAgentBuildersWithDistributor(){
        Account bornBuilder = [SELECT id, Agent__c FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];
        Account texDistributor = [SELECT Id FROM Account WHERE Name = 'Tex Home Distributor' LIMIT 1];
        User builderUser = [SELECT Id FROM User WHERE LastName = 'ExternD' LIMIT 1];
        
        bornBuilder.Agent__c = null;
        //invoice.OwnerId = texDistributor.Id;
        //Database.Update(bornBuilder);

        List<Account> builderList = new List<Account>{bornBuilder};

        Test.startTest();       
        //DistributorAccessController controller = new DistributorAccessController();
        DistributorAccessController.shareBuildersWithDistributor(builderList);

        Integer accShr = [SELECT count() FROM AccountShare WHERE AccountId IN: builderList AND RowCause =: 'Manual'];

        System.assertEquals(1, accShr);
        Test.stopTest();
    }

    private static testMethod void testremoveSharingBuildersWithDistributor(){
        Account bornBuilder = [SELECT id, Agent__c FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];
        Account texDistributor = [SELECT Id FROM Account WHERE Name = 'Tex Home Distributor' LIMIT 1];
        User builderUser = [SELECT Id FROM User WHERE LastName = 'ExternD' LIMIT 1];
        User distributor = [SELECT AccountId,UserRoleId,UserRole.DeveloperName FROM User WHERE AccountId =: bornBuilder.Agent__c LIMIT 1];
        Group grp = [SELECT Name,DeveloperName,RelatedId,Type FROM Group WHERE Type =: GlobalConstants.ROLE_AND_SUBORDINATES AND
                                        RelatedId =: distributor.UserRoleId LIMIT 1];
        
        bornBuilder.Agent__c = null;
        //invoice.OwnerId = texDistributor.Id;
        //Database.Update(bornBuilder);

        List<Account> builderList = new List<Account>{bornBuilder};

        Test.startTest();       
        //DistributorAccessController controller = new DistributorAccessController();
        DistributorAccessController.shareBuildersWithDistributor(builderList);
        Integer accShrBefore = [SELECT count() FROM AccountShare WHERE AccountId IN: builderList AND UserOrGroupId =: grp.Id];
        DistributorAccessController.removeSharingBuildersWithDistributor(builderList);

        Integer accShrAfter = [SELECT count() FROM AccountShare WHERE AccountId IN: builderList AND UserOrGroupId =: grp.Id];

        System.assertEquals(1, accShrBefore);
        System.assertEquals(1, accShrAfter);
        Test.stopTest();
    }

    
}