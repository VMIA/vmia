global class DistributorSharingSchedulable implements Schedulable {
	global void execute(SchedulableContext sc) {
		DistributorAccessBatch objClass = new DistributorAccessBatch();
		Database.executeBatch (objClass);
	}
}