public with sharing class EditEligibilityApplication {


public Account accountRecord {get;set;}
public List<Attachment> attachments{get;set;}
public Application__c application {get;set;}
public Application__c builderAccount {get;set;}
public String appID {get;set;}
public boolean newEligibility {get;set;}
public boolean reviewEligibility{get;set;}
public boolean hasFlatRates {get;set;}
public list<Account> builder{get;set;}
public list<Contact> rbpContact{get;set;}
//public list<rbpContactOnAccount> rbpContactWithRoles{get;set;}
public list <Contact_Role__c> relatedContacts {get;set;}
public String comments{get;set;}
public String currentCommentsOutput{get;set;}
public String application_Status_Awaiting_Info{get;set;}
public String application_Current_Activity_Final_Declinature {get;set;}
public String application_Status_Builder_Changed {get;set;}
public String application_Status_Builder_Unchanged {get;set;}
public String application_Status_Withdrawn {get;set;}
public String application_source_DD_Initiated {get;set;}
public String application_source_new{get;set;}
public String application_source_Adverse_Alert {get;set;}
public list<categories> categoriesList {get;set;}
public list<categories> categoriesListNonSnapshot {get;set;}
private id recTypeNewEligibility;
private id recTypeReviewEligibility;

public String billingAddress {get;set;}
public String mailingAddress {get;set;}
public String builderName {get;set;}


private String billStreet = '';
private String billCity = '';
private String billState = '';
private String billPostal = '';
private String billCountry ='';
private String mailStreet = '';
private String mailCity = '';
private String mailState = '';
private String mailPostal = '';
private String mailCountry = '';

public boolean hasNoDocuments{get;set;}
public boolean errorsFound{get;set;}
public boolean showWithdrawBtn {get;set;}
public boolean amendCOI {get;set;}
public boolean cancelCOI {get;set;}
public Boolean isOwnerBuilder {get;set;}


// Code we will invoke on page load.
    public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return Page.Login;
        }
        else{
            return null;
        }
    }
    
    public PageReference GoToApplyforEligibilityFlow(){
        return new PageReference('/apex/ApplyForEligibility');
    }
    
    public PageReference save(){
        if(hasNoDocuments && comments == '' ){
            errorsFound = True;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No Additional Information has been entered. Please add comments or attach files');
            ApexPages.addMessage(myMsg);
            return null;
        }
        else{
            application.New_Information_Entered__c = system.now();
            application.ownerId = GlobalUtility.getQueues(new list<String>{GlobalConstants.BAT_QUEUE}).get(GlobalConstants.BAT_QUEUE); 
            if(comments!='' && comments != null){
                if(String.isBlank(application.external_comments__c)){
                    application.external_comments__c = system.now().format('dd/MM/yyyy HH:mm:ss ') + ' ' + userInfo.getName() + ': ' + comments;
                    application.External_Comments__c = application.External_Comments__c.replace('null', '');
                }else{
                    application.external_comments__c += '\n' + '\n' + system.now().format('dd/MM/yyyy HH:mm:ss ') + ' ' + userInfo.getName() + ': ' + comments; 
                }
            }
            try{
                update application;  
            } catch(DmlException e) {
                System.debug('An unexpected error has occurred: ' + e.getMessage());
            }
            //return new PageReference('/apex/EligibilityApplicationConfirmation?id='+appID);
            return null;
        }
    }
    
        
        
    public PageReference confirmation(){
        System.debug('Inside the Confirmation');
        System.debug('appID::'+appID);
        return new PageReference('/apex/EligibilityApplicationConfirmation?id='+appID);
    }

    public PageReference cancel(){
        return new PageReference('/apex/DistributorLanding');
    }

    //public void withdrawApplication(){
    //    try{
    //        application.Application_Status__c = GlobalConstants.APP_STATUS_WITHDRAWN;
    //        showWithdrawBtn = false;
    //        Database.update(application);
    //    }
    //    catch(Exception exp){
    //        // log all the exceptions to application log
    //        Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(EditEligibilityApplication.class),
    //                            GlobalConstants.FN_WITHDRAWN_APPLICATION,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
    //    }
    //}
    public PageReference withdrawApplication(){
        return new PageReference('/WithdrawConfirmation?appId=' + application.id);
    }
   
    public EditEligibilityApplication () {
        
        newEligibility = False;
        hasNoDocuments = False;
        reviewEligibility = False;
        comments = '';
        categoriesList = new list<categories>();
        categoriesListNonSnapshot = new list<categories>();
        //rbpContactWithRoles = new list<rbpContactOnAccount>();
        attachments = new list<Attachment>();
        appId = ApexPages.currentPage().getParameters().get('id');
        recTypeNewEligibility = constants.application_RecordTypeId_New_Eligibility;
        recTypeReviewEligibility = constants.application_RecordTypeId_Review_Eligibility;
        isOwnerBuilder = false;
        try {
            if(appId != null){  
                system.debug('here2: ' + appId);
                List<String> fields = new List<String>(Application__c.SObjectType.getDescribe().fields.getMap().keySet());
                String soql = ''
                        + ' select ' + String.join(fields, ',')
                        + ',  Builder_Number__r.Name,Builder_Number__r.Account_Number__c, Builder_Number__r.isPersonAccount, RecordType.Name, Certificate__r.Name, Certificate__r.Policy_Number__c, Certificate__r.Property_Address__c, Certificate__r.Id, Certificate__r.Status__c'
                        + ' from Application__c'
                        + ' where id=:' + 'appId';
                system.debug('here query44: ' + soql);        
                application = Database.query(soql); 
                //builderAccount = [SELECT builder_number__r.Name FROM Application__c WHERE id=: appId]; 
                String builderNo = application.Builder_Number__c;
                //builderName = builderAccount.builder_number__r.name;
                system.debug('here55: ' + builderName);
                List<Attachment> lstAtt = new List<Attachment>();
        		lstAtt = [SELECT Name,ContentType, BodyLength, CreatedDate, CreatedById, CreatedBy.Name FROM Attachment WHERE ParentId = :appID ORDER BY CreatedDate DESC];
        		
        		if(!lstAtt.isEmpty()) {
                	attachments = lstAtt;
        		}
                system.debug('here44: ' + attachments);

                showWithdrawBtn = (String.isNotBlank(application.Application_Status__c) 
                                    && !GlobalConstants.APP_STATUS_WITHDRAWN.equalsIgnoreCase(application.Application_Status__c) 
                                    && !GlobalConstants.APPLICATION_STATUS_BUILDER_CHANGED.equalsIgnoreCase(application.Application_Status__c)
                                    && !GlobalConstants.APPLICATION_STATUS_BUILDER_UNCHANGED.equalsIgnoreCase(application.Application_Status__c)
                                    && !GlobalConstants.ADVERSE_ALERT.equalsIgnoreCase(application.Application_Source__c)
                                    && !GlobalConstants.APPLICATION_STATUS_COI_REQUEST_REJECTED.equalsIgnoreCase(application.Application_Status__c)
                                    && !(GlobalConstants.APPLICATION_STATUS_COI_REQUEST_APPROVED.equalsIgnoreCase(application.Application_Status__c)
                                        && (GlobalConstants.STAT_ISSUED_IN_PROGRESS.equalsIgnoreCase(application.Certificate__r.Status__c)
                                        || GlobalConstants.STAT_ISSUED_COMPLETED.equalsIgnoreCase(application.Certificate__r.Status__c)
                                        || GlobalConstants.STAT_CANCELLED.equalsIgnoreCase(application.Certificate__r.Status__c)
                                        || GlobalConstants.STAT_EXPIRED.equalsIgnoreCase(application.Certificate__r.Status__c)
                                        || GlobalConstants.STAT_TAKEN_OVER.equalsIgnoreCase(application.Certificate__r.Status__c)))) ?
                                    true : false;


                if(application.Builder_Number__c == null){
                    if(application.Certificate__c != null){
                        isOwnerBuilder = true;
                        for(Property_Owners__c owner : [SELECT Property_Owner__r.Name, Contact__r.Name FROM Property_Owners__c 
                                                        WHERE Certificate__c =: application.Certificate__c AND OwnerBuilder__c = TRUE LIMIT 1]){
                            if(!String.isBlank(owner.Property_Owner__r.Name)){
                                builderName = owner.Property_Owner__r.Name;
                            }
                            else if(!String.isBlank(owner.Contact__r.Name)) {
                                builderName = owner.Contact__r.Name;
                            }
                            else{
                                builderName = GlobalConstants.OWNER_BUILDER;
                            }
                        }
                    }
                }
                else{
                    builderName = application.Builder_Number__r.name;
                }
                System.debug('** builderName ==>'+ builderName);
                /*if(!application.Builder_Number__r.isPersonAccount){
                    List<String> contactFields = new List<String>(contact.SObjectType.getDescribe().fields.getMap().keySet());
                    String contactSoql = ''
                            + ' select ' + String.join(contactFields, ',')
                            + ', (SELECT roles from AccountContactRelations where accountID=: ' 
                            + 'builderNo' 
                            + ')'
                            + ' from Contact'
                            + ' where accountId=:' + 'builderNo';
                    rbpContact = Database.query(contactSoql);

                    for(Contact c: rbpContact){
                        rbpContactWithRoles.add(new rbpContactOnAccount(c, c.AccountContactRelations));
                    }                   
                }else{
                    List<String> contactFields = new List<String>(Contact.SObjectType.getDescribe().fields.getMap().keySet());
                    String contactSoql = ''
                            + ' select ' + String.join(contactFields, ',')
                            + ' from Contact'
                            + ' where accountId=:' + 'builderNo';
                    rbpContact = Database.query(contactSoql);

                    for(Contact c: rbpContact){
                        AccountContactRelation newTempRelation = new AccountContactRelation(accountId =application.Builder_Number__c, contactId = c.id, roles = GlobalConstants.ACCOUNTCONTACTROLE_PRIMARY_DIRECTOR);
                        rbpContactWithRoles.add(new rbpContactOnAccount(c, newTempRelation));
                    }
                }*/
                
                relatedContacts = GlobalUtility.relatedContactRoles;
                
                if(application.type__c == GlobalConstants.CANCELCOI) {
                    cancelCOI = true;
                } else {
                    cancelCOI = false;
                }
                
                if(application.type__c == GlobalConstants.AMENDCOI) {
                    amendCOI = true;
                } else {
                    amendCOI = false;
                }
                
                currentCommentsOutput = '';
                if(!String.isBlank(application.external_comments__c)){
                    currentCommentsOutput = application.external_comments__c;
                    currentCommentsOutput = currentCommentsOutput.replace('\n', '<br/>');
                }
                
                
            }    
        } catch(DmlException e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }

        if(application.recordTypeId == recTypeNewEligibility){
            newEligibility = True;
        }else if(application.recordTypeId == recTypeReviewEligibility){
            reviewEligibility  = True;
        }
        if(application.Snapshot_C01__c){
           categoriesList.add(new categories(constants.New_Single_Dwelling, application.Snapshot_C01_Limit__c, application.Snapshot_C01_Flat_Rate__c ));
        }
        if(application.Snapshot_C03__c){
            categoriesList.add(new categories(constants.New_Multi_Dwelling, application.Snapshot_C03_Limit__c, application.Snapshot_C03_Flat_Rate__c ));
        }
        if(application.Snapshot_C04__c){
            categoriesList.add(new categories(constants.Alterations_Additions, application.Snapshot_C04_Limit__c, application.Snapshot_C04_Flat_Rate__c ));
        }
        if(application.Snapshot_C05__c){
            categoriesList.add(new categories(constants.Swimming_Pools, application.Snapshot_C05_Limit__c, application.Snapshot_C05_Flat_Rate__c ));
        }
        if(application.Snapshot_C06__c){
           categoriesList.add(new categories(constants.Renovations_Tradespeople, application.Snapshot_C06_Limit__c, application.Snapshot_C06_Flat_Rate__c ));
        }
        if(application.Snapshot_C07__c){
            categoriesList.add(new categories(constants.Other, application.Snapshot_C07_Limit__c, application.Snapshot_C07_Flat_Rate__c ));
        }

        if(application.Single_Dwelling__c){
           categoriesListNonSnapshot.add(new categories(constants.New_Single_Dwelling, application.Single_Dwelling_Limit__c, application.Flat_Rate_Premium_C01__c ));
        }
        if(application.Multi_Dwelling__c){
            categoriesListNonSnapshot.add(new categories(constants.New_Multi_Dwelling, application.Multi_Dwelling_Limit__c, application.Flat_rate_Premium_C03__c ));
        }
        if(application.Alterations__c){
            categoriesListNonSnapshot.add(new categories(constants.Alterations_Additions, application.Alteration_Limit__c, application.Flat_Rate_Premium_C04__c ));
        }
        if(application.Swimming_Pool__c){
            categoriesListNonSnapshot.add(new categories(constants.Swimming_Pools, application.Swimming_Pool_Limit__c, application.Flat_Rate_Premium_C05__c ));
        }
        if(application.Renovation_Non_Structural__c){
           categoriesListNonSnapshot.add(new categories(constants.Renovations_Tradespeople, application.Renovation_Non_Structural_Limit__c, application.Flat_Rate_Premium_C06__c ));
        }
        if(application.C07_Other__c){
            categoriesListNonSnapshot.add(new categories(constants.Other, application.Category_Limit_Other__c, application.Flat_Rate_Premium_C07__c ));
        }

        for(Categories c : categoriesList){
            if(c.categoryFlatRate !=0 ){
                hasFlatRates = True;
            }
        }

        for(Categories c : categoriesListNonSnapshot){
            if(c.categoryFlatRate !=0 ){
                hasFlatRates = True;
            }
        }

        

        application_Status_Awaiting_Info = constants.application_Status_Awaiting_Info;
        application_Current_Activity_Final_Declinature = constants.application_Current_Activity_Final_Declinature;
        application_Status_Builder_Changed = constants.application_Status_Builder_Changed;
        application_Status_Builder_Unchanged = constants.application_Status_Builder_Unchanged;
        application_Status_Withdrawn = constants.application_Status_Builder_Withdrawn;
        application_source_DD_Initiated = constants.application_source_DD_Initiated;
        application_source_Adverse_Alert = constants.application_source_Adverse_Alert;
        application_source_new = constants.application_Source_Status_New;

        if(application.Snapshot_Billing_Street__c != null){
            billStreet = application.Snapshot_Billing_Street__c;
        }
        if(application.Snapshot_Billing_City__c != null){
            billCity = application.Snapshot_Billing_City__c;
        }
        if(application.Snapshot_Billing_State__c != null){
            billState = application.Snapshot_Billing_State__c;
        }
        if(application.Snapshot_Billing_Postal_Code__c != null){
            billPostal = application.Snapshot_Billing_Postal_Code__c;
        }
        if(application.Snapshot_Billing_Country__c != null){
            billCountry = application.Snapshot_Billing_Country__c;
        }

        if(application.Snapshot_Mailing_Street__c != null){
            mailStreet = application.Snapshot_Mailing_Street__c;
        }
        if(application.Snapshot_Mailing_City__c != null){
            mailCity = application.Snapshot_Mailing_City__c;
        }
        if(application.Snapshot_Mailing_State__c != null){
            mailState = application.Snapshot_Mailing_State__c;
        }
        if(application.Snapshot_Mailing_Postal_Code__c != null){
            mailPostal = application.Snapshot_Mailing_Postal_Code__c;
        }
        if(application.Snapshot_Mailing_Country__c != null){
            mailCountry = application.Snapshot_Mailing_Country__c;
        }

        billingAddress = billStreet + '\n' + billCity + ' ' + billState + '\n' +  billPostal + '\n' + billCountry;
        mailingAddress = mailStreet + '\n' + mailCity + ' ' + mailState + '\n' +  mailPostal + '\n' + mailCountry;

    }


    public class categories{
        public string categoryName{get;set;}
        public decimal categoryLimit{get;set;}
        public decimal categoryFlatRate{get;set;}

        public categories(String newName, decimal newLimit, decimal newFlatRate){
            categoryName = newName;
            if(newLimit!=null){
                categoryLimit = newLimit; 
            }else{
                categoryLimit = 0;
            }
            if(newFlatRate!=null){
                categoryFlatRate = newFlatRate; 
            }else{
                categoryFlatRate = 0;
            }
        }

    }

    
    
    //public class rbpContactOnAccount{
    //    public Contact con{get;set;}
    //    public String conRole{get;set;}

    //    public rbpContactOnAccount(Contact newCon, AccountContactRelation newConRole){
    //        con = newCon;
    //        conRole = newConRole.roles;
    //    }

    //}
    
}