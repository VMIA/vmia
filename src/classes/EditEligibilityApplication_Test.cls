/**
* Date         :  23-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for DistributorAccessBatch controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class EditEligibilityApplication_Test {
	
	private static Map<String,Id> profileMap = new Map<String,Id>();
	
	@testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        UserRole batMember = TestUtility.getUserRole('BAT_Member');
        
        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        Database.insert(vmiaInternalUser);
        setupData();
        setupUsers();
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> rateTableRecTypeMap = Schema.Sobjecttype.PremiumRatesTable__c.getRecordTypeInfosByName();
        
        PremiumRatesTable__c flatRate = TestUtility.createFlatRateTable(1500, rateTableRecTypeMap.get('FlatRate').getRecordTypeId());
        Database.insert(flatRate);

        Account texDistributor = TestUtility.createBusinessAccount('Tex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        texDistributor.website = 'www.textDis.com';
        Database.insert(texDistributor);
        System.debug('** texDistributor ==>'+ texDistributor);

        Contact texSmoth = TestUtility.createContact('Tex','Smoth','tex.smoth@texHomeDistributorTest.com',texDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(texSmoth);
        System.debug('** texSmoth ==>'+ texSmoth);

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);        
    }

    @future
    private static void setupUsers(){
    	Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        Contact tonyStark = [SELECT id FROM Contact WHERE FirstName = 'Tony' and LastName = 'Stark' LIMIT 1];
        Contact rexSmith = [SELECT id FROM Contact WHERE FirstName = 'Rex' and LastName = 'Smith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];

        User builderUser = TestUtility.createPortalUser('ExternB',profileMap.get('Builder'),tonyStark.Id);
        Database.insert(builderUser);

        User ddUser = TestUtility.createPortalUser('ExternD',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(ddUser);

        System.RunAs(builderUser) {
        	Application__c  app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
    		Database.insert(app);
    		System.debug('** Application ==>'+ app);
        }
    }

    public static testmethod void cancel(){
    	Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
    	Application__c app = [Select Id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];

		System.currentPageReference().getParameters().put('id', app.Id);
		PageReference cancelPageExpected = new PageReference('/apex/DistributorLanding');
        EditEligibilityApplication controller = new EditEligibilityApplication();
        System.RunAs(new User(Id=UserInfo.getUserId())) {
			Test.startTest();
	        PageReference cancelPage = controller.cancel();
			System.assertEquals(cancelPageExpected.getURL(), cancelPage.getURL());
	        Test.stopTest(); 
        }  
    }

    public static testmethod void confirm(){
    	Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
    	Application__c app = [Select Id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];


        
		System.currentPageReference().getParameters().put('id', app.Id);
		PageReference confirmPageExpected = new PageReference('/apex/EligibilityApplicationConfirmation?id=' + app.Id);
        EditEligibilityApplication controller = new EditEligibilityApplication();
        System.RunAs(new User(Id=UserInfo.getUserId())) {
			Test.startTest();
	        PageReference confirmPage = controller.confirmation();
			System.assertEquals(confirmPageExpected.getURL(), confirmPage.getURL());
	        Test.stopTest(); 
        }  
    }

    public static testmethod void withdrawApplication(){
    	Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
    	Application__c app = [Select Id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];

		System.currentPageReference().getParameters().put('id', app.Id);
		PageReference pageExpected = new PageReference('/WithdrawConfirmation?appId=' + app.Id);
        EditEligibilityApplication controller = new EditEligibilityApplication();
        System.RunAs(new User(Id=UserInfo.getUserId())) {
			Test.startTest();
	        PageReference confirmPage = controller.withdrawApplication();
			System.assertEquals(pageExpected.getURL(), confirmPage.getURL());
	        Test.stopTest(); 
        }  
    }

    private static testMethod void testForwardToCustomAuthPage(){
		Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
		Application__c app = [Select Id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];

		PageReference pageRef = Page.EditEligibilityApplication;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',app.id);
		Test.startTest();		
		EditEligibilityApplication controller = new EditEligibilityApplication();
		PageReference directPage = controller.forwardToCustomAuthPage();
		System.assertEquals(null, directPage);
		
		Test.stopTest();

	}

	private static testMethod void testGoToApplyforEligibilityFlow(){
		Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
		Application__c app = [Select Id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];

		PageReference pageRef = Page.EditEligibilityApplication;
        Test.setCurrentPage(pageRef);
        PageReference confirmPageExpected = new PageReference('/apex/ApplyForEligibility');
        ApexPages.currentPage().getParameters().put('Id',app.id);
		Test.startTest();		
		EditEligibilityApplication controller = new EditEligibilityApplication();
		PageReference directPage = controller.GoToApplyforEligibilityFlow();
		System.assertEquals(confirmPageExpected.getUrl(), directPage.getUrl());
		
		Test.stopTest();

	}

	private static testMethod void testAmendCOIWithoutCommentsAndReviewRecordType(){
		Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
		Application__c app = [Select Id, External_Comments__c, RecordTypeId, Type__c, Snapshot_C01__c from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
		app.External_Comments__c = 'Test external';
		app.Type__c = GlobalConstants.AMENDCOI;
		app.RecordTypeId = constants.application_RecordTypeId_Review_Eligibility;
		app.Snapshot_C01__c = true;
		app.Snapshot_C03__c = true;
		app.Snapshot_C04__c = true;
		app.Snapshot_C05__c = true;
		app.Snapshot_C06__c = true;
		app.Snapshot_C07__c = true;
		app.Snapshot_C01_Limit__c = 100000;
		app.Snapshot_C03_Limit__c = 20000;
		app.Snapshot_C04_Limit__c = 20000;
		app.Snapshot_C05_Limit__c = 20000;
		app.Snapshot_C06_Limit__c = 20000;
		app.Snapshot_C07_Limit__c = 20000;
		app.Snapshot_C04_Flat_Rate__c = 2000;
		app.Single_Dwelling__c = true;
		app.Multi_Dwelling__c = true;
		app.Alterations__c = true;
		app.Swimming_Pool__c = true;
		app.Renovation_Non_Structural__c = true;
		app.C07_Other__c = true;
		app.Single_Dwelling_Limit__c = 20000;
		app.Multi_Dwelling_Limit__c = 20000;
		app.Alteration_Limit__c = 20000;
		app.Swimming_Pool_Limit__c = 20000;
		app.Renovation_Non_Structural_Limit__c = 20000;
		app.Category_Limit_Other__c = 20000;

		app.Snapshot_Billing_Street__c = '32 test street';
		app.Snapshot_Billing_City__c = 'Testville';
		app.Snapshot_Billing_State__c = 'VIC';
		app.Snapshot_Billing_Postal_Code__c = '3000';
		app.Snapshot_Billing_Country__c = 'Australia';
		app.Snapshot_Mailing_Street__c = '31 test street';
		app.Snapshot_Mailing_City__c = 'Testvillee';
		app.Snapshot_Mailing_State__c = 'VIC';
		app.Snapshot_Mailing_Postal_Code__c = '3001';
		app.Snapshot_Mailing_Country__c = 'Australia';
		update app;

		PageReference pageRef = Page.EditEligibilityApplication;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',app.id);
		Test.startTest();		
		EditEligibilityApplication controller = new EditEligibilityApplication();
		controller.hasNoDocuments = true;
		controller.comments = '';
		PageReference directPage = controller.Save();
		
		System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[0].getSummary().contains('No Additional Information has been entered. Please add comments or attach files'), 'There should be a pageMessage advising no Additional Information entered.');            

		System.assertEquals(null, directPage);
		
		Test.stopTest();

	}

	private static testMethod void testAmendCOIWithCommentsAndReviewRecordType(){
		Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
		Application__c app = [Select Id, External_Comments__c, RecordTypeId, Type__c, Snapshot_C01__c from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
		app.External_Comments__c = '';
		app.Type__c = GlobalConstants.AMENDCOI;
		app.RecordTypeId = constants.application_RecordTypeId_Review_Eligibility;
		app.Snapshot_C01__c = true;
		app.Snapshot_C03__c = true;
		app.Snapshot_C04__c = true;
		app.Snapshot_C05__c = true;
		app.Snapshot_C06__c = true;
		app.Snapshot_C07__c = true;
		app.Snapshot_C01_Limit__c = 100000;
		app.Snapshot_C03_Limit__c = 20000;
		app.Snapshot_C04_Limit__c = 20000;
		app.Snapshot_C05_Limit__c = 20000;
		app.Snapshot_C06_Limit__c = 20000;
		app.Snapshot_C07_Limit__c = 20000;
		app.Snapshot_C04_Flat_Rate__c = 2000;
		app.Single_Dwelling__c = true;
		app.Multi_Dwelling__c = true;
		app.Alterations__c = true;
		app.Swimming_Pool__c = true;
		app.Renovation_Non_Structural__c = true;
		app.C07_Other__c = true;
		app.Single_Dwelling_Limit__c = 20000;
		app.Multi_Dwelling_Limit__c = 20000;
		app.Alteration_Limit__c = 20000;
		app.Swimming_Pool_Limit__c = 20000;
		app.Renovation_Non_Structural_Limit__c = 20000;
		app.Category_Limit_Other__c = 20000;

		app.Snapshot_Billing_Street__c = '32 test street';
		app.Snapshot_Billing_City__c = 'Testville';
		app.Snapshot_Billing_State__c = 'VIC';
		app.Snapshot_Billing_Postal_Code__c = '3000';
		app.Snapshot_Billing_Country__c = 'Australia';
		app.Snapshot_Mailing_Street__c = '31 test street';
		app.Snapshot_Mailing_City__c = 'Testvillee';
		app.Snapshot_Mailing_State__c = 'VIC';
		app.Snapshot_Mailing_Postal_Code__c = '3001';
		app.Snapshot_Mailing_Country__c = 'Australia';
		update app;

		PageReference pageRef = Page.EditEligibilityApplication;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',app.id);
		Test.startTest();		
		EditEligibilityApplication controller = new EditEligibilityApplication();
		controller.hasNoDocuments = true;
		controller.comments = 'testing comments';
		PageReference directPage = controller.Save();
		System.assertEquals(null, directPage);
		system.debug('** external: ' + app.External_Comments__c);
		system.debug('** external2: ' + system.now().format('dd/MM/yyyy HH:mm:ss ') + ' ' + userInfo.getName() + ': ' + controller.comments);

		Application__c appAfter = [Select Id, External_Comments__c, RecordTypeId, Type__c, Snapshot_C01__c from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];

		System.assert(appAfter.External_Comments__c.contains(controller.comments), 'The external comments should contain the phrase: testing comments');
		
		Test.stopTest();

	}

	private static testMethod void testCancelCOIWithCommentsAndExternalCommentsAndReviewRecordType(){
		Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
		Application__c app = [Select Id, External_Comments__c, RecordTypeId, Type__c, Snapshot_C01__c from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
		app.External_Comments__c = 'test2';
		app.Type__c = GlobalConstants.CANCELCOI;
		app.RecordTypeId = constants.application_RecordTypeId_Review_Eligibility;
		app.Snapshot_C01__c = true;
		app.Snapshot_C03__c = true;
		app.Snapshot_C04__c = true;
		app.Snapshot_C05__c = true;
		app.Snapshot_C06__c = true;
		app.Snapshot_C07__c = true;
		app.Snapshot_C01_Limit__c = 100000;
		app.Snapshot_C03_Limit__c = 20000;
		app.Snapshot_C04_Limit__c = 20000;
		app.Snapshot_C05_Limit__c = 20000;
		app.Snapshot_C06_Limit__c = 20000;
		app.Snapshot_C07_Limit__c = 20000;
		app.Snapshot_C04_Flat_Rate__c = 2000;
		app.Single_Dwelling__c = true;
		app.Multi_Dwelling__c = true;
		app.Alterations__c = true;
		app.Swimming_Pool__c = true;
		app.Renovation_Non_Structural__c = true;
		app.C07_Other__c = true;
		app.Single_Dwelling_Limit__c = 20000;
		app.Multi_Dwelling_Limit__c = 20000;
		app.Alteration_Limit__c = 20000;
		app.Swimming_Pool_Limit__c = 20000;
		app.Renovation_Non_Structural_Limit__c = 20000;
		app.Category_Limit_Other__c = 20000;
		app.Flat_Rate_Premium_C01__c = 1500;

		app.Snapshot_Billing_Street__c = '32 test street';
		app.Snapshot_Billing_City__c = 'Testville';
		app.Snapshot_Billing_State__c = 'VIC';
		app.Snapshot_Billing_Postal_Code__c = '3000';
		app.Snapshot_Billing_Country__c = 'Australia';
		app.Snapshot_Mailing_Street__c = '31 test street';
		app.Snapshot_Mailing_City__c = 'Testvillee';
		app.Snapshot_Mailing_State__c = 'VIC';
		app.Snapshot_Mailing_Postal_Code__c = '3001';
		app.Snapshot_Mailing_Country__c = 'Australia';
		update app;

		PageReference pageRef = Page.EditEligibilityApplication;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',app.id);
		Test.startTest();		
		EditEligibilityApplication controller = new EditEligibilityApplication();
		controller.hasNoDocuments = true;
		controller.comments = 'testing comments';
		PageReference directPage = controller.Save();
		Application__c appAfter = [Select Id, External_Comments__c, RecordTypeId, Type__c, Snapshot_C01__c from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
		System.assert(appAfter.External_Comments__c.contains('test2'), 'The external comments should contain the phrase: test2');
		System.assert(appAfter.External_Comments__c.contains(controller.comments), 'The external comments should contain the phrase: testing comments');
		System.assertEquals(null, directPage);
		
		Test.stopTest();

	}

	
}