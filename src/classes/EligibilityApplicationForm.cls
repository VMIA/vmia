public without sharing class EligibilityApplicationForm {

    public Account newAccount {get;set;}
    public Account[] currentAccountCheck {get;set;}
    public Contact newContact {get;set;}
    public Contact_Role__c contactRoleRecord {get;set;}
    public Contact[] currentContactCheck {get;set;}
    public Application__c newApplication {get;set;}
    public List<selectOption> AccountTypes {get; set;}
    public boolean isRequired {get; set;}
    public String appID {get;set;}
    public string selectedRecType{get;set;}
    public boolean isPersonAccount{get;set;}
    public boolean errorsFound {get;set;}
    public string emailAddress{get;set;}
    public string requestedLimit{get;set;}
    public boolean hasNoDocuments{get;set;}


	// Code we will invoke on page load.

    public EligibilityApplicationForm() {
        GlobalUtility.forwardToCustomAuthPage();
        newAccount = new Account();
        newContact = new Contact();
        contactRoleRecord = new Contact_Role__c();
        newApplication = new Application__c();
        AccountTypes =  new List<SelectOption>();
        AccountTypes.add(new SelectOption('Partnership','Partnership'));
        AccountTypes.add(new SelectOption('Company','Company'));
        isRequired = False;
        AccountTypes.add(new SelectOption('Sole Trader','Sole Trader'));
        appID = '00';
        newAccount.acn__c = '';
        isPersonAccount = False;
        selectedRecType = 'Partnership';

        errorsFound = False;
        Attachment newatt = new Attachment();
    }

    public PageReference GoToApplyforEligibilityFlow(){
        return new PageReference('/apex/ApplyForEligibility');
    }
	
    public static Boolean isValidEmail(String email) {
		Boolean res = true;
		String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
		Pattern MyPattern = Pattern.compile(emailRegex);
		Matcher MyMatcher = MyPattern.matcher(email);

		if (!MyMatcher.matches()) 
			res = false;

		return res; 
    }

    public boolean validate() {
        try {
                Date.valueOf(newApplication.Date_of_Builders_Application__c);
            } catch(Exception e) {
                System.debug(e.getMessage());
                errorsFound = True;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'User entered date of builder\'s application in incorrect format');
                ApexPages.addMessage(myMsg);
                return true;
            }
			
        try{
            errorsFound = False;
            //ABN check
            if( (!isPersonAccount && newApplication.Builder_Legal_Entity_Name__c ==null) || (newApplication.builder_acn__c ==null && isRequired) || 
                requestedLimit ==null || newContact.firstName ==null || newContact.lastName ==null || newContact.phone ==null ||
                emailAddress ==null || newApplication.Date_of_Builders_Application__c == null){
                errorsFound = True;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'All mandatory fields must be completed');
                ApexPages.addMessage(myMsg);
                return true;
            }
            //ABN Check
            if(newApplication.builder_abn__c!=null ){
               if (!newApplication.builder_abn__c.isNumeric()) {
                    errorsFound = True;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'ABN must only be numerical values');
                    ApexPages.addMessage(myMsg);
                    return true;
                } else if(newApplication.builder_abn__c.length()<>11) {       
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'ABN must be 11 digits');
                    ApexPages.addMessage(myMsg);       
                    errorsFound = True;
                    return true;
                } 
            }

            if(newApplication.Date_of_Builders_Application__c > system.Today()){   
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Date of Builder Application cannot be in the future');
                ApexPages.addMessage(myMsg);       
                errorsFound = True;
                return true;
            }
            
            //ACN Check
            if(isRequired || newApplication.builder_acn__c!=null ){
                if (!newApplication.builder_acn__c.isNumeric()) {
                    errorsFound = True;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'ACN must only be numerical values');
                    ApexPages.addMessage(myMsg);
                    return true;
                } else if(newApplication.builder_acn__c.length()<>9) {         
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'ACN must be 9 numbers');
                    ApexPages.addMessage(myMsg);     
                    errorsFound = True;
                    return true;
                }
            }
            if(!isValidEmail(emailAddress)){
                errorsFound = True;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Email address not in correct format');
                ApexPages.addMessage(myMsg);
                return true;
            }
            if(!requestedLimit.isNumeric()){
                errorsFound = True;
                /* message updated by KATE LUU - SMS BRisbane - 12th June 2017*/
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Requested Builder Construction Limit must only be numerical values');
                ApexPages.addMessage(myMsg);
                return true;
            }

            //Check if account with these entered details exists already
            try{
                if(newApplication.Builder_ABN__c != null && newApplication.builder_acn__c!= null){
                    currentAccountCheck = [SELECT Name, Id, ACN__c, ABN__c, Construction_Limit_Bracket__c, RecordTypeId   FROM Account WHERE (abn__c=:newApplication.Builder_ABN__c OR name=:newApplication.Builder_Legal_Entity_Name__c or acn__c=:newApplication.Builder_ACN__c) AND (Eligibility_Status__c !=: constants.application_Status_Declined AND Eligibility_Status__c!=null)  LIMIT 1];
                }else if(newApplication.Builder_ACN__c != null){
                    currentAccountCheck = [SELECT Name, Id,  ACN__c, ABN__c, Construction_Limit_Bracket__c, RecordTypeId   FROM Account WHERE (acn__c=:newApplication.Builder_ACN__c OR name=:newApplication.Builder_Legal_Entity_Name__c) AND (Eligibility_Status__c !=: constants.application_Status_Declined  AND Eligibility_Status__c!=null)  LIMIT 1];
                }else if(newApplication.builder_abn__c!= null){
                    currentAccountCheck = [SELECT Name, Id,  ACN__c, ABN__c, Construction_Limit_Bracket__c, RecordTypeId   FROM Account WHERE (abn__c=:newApplication.Builder_ABN__c OR name=:newApplication.Builder_Legal_Entity_Name__c) AND (Eligibility_Status__c !=: constants.application_Status_Declined  AND Eligibility_Status__c!=null)  LIMIT 1];
            	}else {
                    currentAccountCheck = [SELECT Name, Id,  ACN__c, ABN__c, Construction_Limit_Bracket__c, RecordTypeId   FROM Account WHERE (name=:newApplication.Builder_Legal_Entity_Name__c) AND (Eligibility_Status__c !=: constants.application_Status_Declined  AND Eligibility_Status__c!=null)  LIMIT 1];
                }
            } catch(DmlException e){
                System.debug('An unexpected error has occurred: ' + e.getMessage());
                /* message updated by KATE LUU - SMS BRisbane - 12th June 2017*/
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error processing builder. Please try again later')); 
                errorsFound = True;
                return true;
            }
            if (currentAccountCheck.size()>0){
                    errorsFound = True;
                    /* message updated by KATE LUU - SMS BRisbane - 12th June 2017*/
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,' This Builder already exists in the system. No new eligibility applications are permitted for existing builders. Please contact the builder ');
                    ApexPages.addMessage(myMsg);
                    return true;
            } 

            if(hasNoDocuments){
                errorsFound = True;
                /* message updated by KATE LUU - SMS BRisbane - 12th June 2017*/
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please attach documents before proceeding');
                ApexPages.addMessage(myMsg);
                return true;
            }


        }catch(DmlException e) {
                errorsFound = True;
                System.debug('HELP2 ERROR: ' + e.getMessage());
                return true;
        }
        return false;
        
    }

    public void insertAttachment(){
        Blob newAttBody= Blob.valueOf(Apexpages.currentPage().getParameters().get('attBody'));
        String newAttName=Apexpages.currentPage().getParameters().get('attName');
        String newAttType=Apexpages.currentPage().getParameters().get('attType');
        Attachment newAtt =  new Attachment();
        newAtt.parentID = newApplication.id;
        system.debug('Att Name is1: ' + newAttName);
        newAtt.body = newAttBody;
        newAtt.Name = newAttName;
        system.debug('Att Name is2: ' + newAtt.name);
        newAtt.ContentType = newAttType;
        try{
            insert newAtt;
        }catch(DmlException e){
            System.debug('An unexpected error has occurred: ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, string.valueOf(e.getMessage()), ''));
            errorsFound = True;
        }
    }

    public PageReference save(){ 
        try{
            validate();

            if(!errorsFound){
                IF(selectedRecType == 'Sole Trader'){
                    isPersonAccount = True;
                    newAccount.FirstName = newContact.FirstName;
                    newAccount.LastName = newContact.LastName;
                    newAccount.Primary_Email__c = emailAddress;
					newAccount.PersonEmail = emailAddress;
                    newAccount.Phone = newContact.Phone;
                    newAccount.Builder_Role__pc = GlobalConstants.ACCOUNTCONTACTROLE_PRIMARY_DIRECTOR;
                    newApplication.Builder_First_Name__c = newAccount.FirstName;
                    newApplication.Builders_Last_name__c = newAccount.LastName;
                }else{
                    newAccount.name = newApplication.Builder_Legal_Entity_Name__c;
                }
				
                newAccount.abn__c = newApplication.builder_abn__c;
                newAccount.acn__c = newApplication.builder_acn__c;
                newAccount.eligibility_status__c = constants.application_Eligibility_Status_UnderApp;
                newAccount.Builder_Eligibility_Status_Reason__c = constants.application_Builder_Eligibility_Status_UnderRev;
                newContact.email = emailAddress;
                newContact.Builder_Role__c = GlobalConstants.ACCOUNTCONTACTROLE_PRIMARY_DIRECTOR;
                
				User u = new User();
                try{
                    u = [SELECT Id,ContactId, accountId,IsActive, Profile.name 
                        FROM User
                        WHERE Id =: UserInfo.getUserID()
                        AND IsActive =: true];
                } catch(DmlException e){
                    System.debug('An unexpected error has occurred: ' + e.getMessage());
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, string.valueOf(e.getMessage()), ''));
                    errorsFound = True;
                    return null;
                }
                Contact ddCon = [select accountId from Contact where id=:u.contactId limit 1];

                newApplication.ownerId = GlobalUtility.getQueues(new list<String>{GlobalConstants.BAT_QUEUE}).get(GlobalConstants.BAT_QUEUE); 
                newApplication.DBI_Distributor__c = UserInfo.getUserID();
                newApplication.DD_Account__c = ddCon.accountId;
                if(u.profile.name == constants.Distributor){
                    newApplication.Application_Status__c = constants.application_Status_new;
                }else{
                   newApplication.Application_Status__c = constants.application_Status_DD_Review; 
                }
                
                newApplication.Application_Source__c = constants.application_Source_Status_New;
                newApplication.Requested_Costruction_Limit__c = Decimal.valueOf(requestedLimit);
                newApplication.Builder_Legal_Entity_Type__c = selectedRecType;
                newAccount.Legal_Business_Type__c = selectedRecType;
                newAccount.agent_contact__c = u.ContactId;
                newAccount.agent__c = ddCon.accountId;
                newApplication.DD_Account__c = ddCon.accountId;

                Boolean isExistingContact = false;

              //Check if Contact with entered details exists in system
                try{
                    currentContactCheck = [SELECT Name, Id,RBP_VBA_Number__c FROM Contact WHERE firstName=:newContact.firstName AND lastName=:newContact.lastName AND email=:newContact.email LIMIT 1];
                    isExistingContact = !currentContactCheck.isEmpty() ;
                } catch(DmlException e){
                    System.debug('An unexpected error has occurred: ' + e.getMessage());
                    /* message updated by KATE LUU - SMS Brisbane - 12 June 2017 */ 
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Error processing registered building practitioner. Please try again later'));
                    errorsFound = True;
                    return null;
                } 

              //Sets account record type to record type chosen by user
                List<RecordType> recType = [Select Id,SobjectType,Name From RecordType WHERE Name =:selectedRecType and SobjectType ='Account'  limit 1];
                if (recType.size()>0){
                    newAccount.RecordTypeId = recType[0].Id;
                }

                Savepoint sp = Database.setSavepoint();
                try{
                    
                    insert newAccount;
                } catch(DmlException e) {
                    System.debug('An unexpected error has occurred: ' + e.getMessage());
                    errorsFound = True;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error creating builder. Please try again later' + e.getMessage()));  
                    return null;   
                }

                if(!String.isBlank(newAccount.id)){
                    if(currentContactCheck.size()>0){
                        contactRoleRecord.Contact__c = currentContactCheck[0].id;
                        contactRoleRecord.Account__c = newAccount.id;
                        contactRoleRecord.Role__c = GlobalConstants.ACCOUNTCONTACTROLE_RBP_PENDING;
                        contactRoleRecord.Start_Date__c = System.Today();
                        try{
                            insert contactRoleRecord;
                        } catch(DmlException e) {
                            System.debug('An unexpected error has occurred: ' + e.getMessage());
                            /* message updated by KATE LUU - SMS Brisbane - 12 June 2017 */ 
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error processing registered building practitioner. Please try again later')); 
                            errorsFound = True;
                            Database.rollback(sp);
                            return null;
                        }
                    }else{
                        if(!isPersonAccount){
							// If the Primary RBP is already a person account then a contact should not be created only a role should be created
                            List<Account> lstAcc = [SELECT Id FROM Account 
															  WHERE ((FirstName = :newContact.FirstName.toLowerCase() AND LastName = :newContact.LastName.toLowerCase()) OR (LastName = :newContact.FirstName.toLowerCase() + ' '  + newContact.LastName.toLowerCase()))
															  AND (PersonEmail = :emailAddress OR Primary_Email__c = :emailAddress) LIMIT 1];
							if(lstAcc.isEmpty()) {
								List<RecordType> recTypeCon = [Select Id,SobjectType,Name From RecordType WHERE Name =:constants.contact_record_Type_Builder and SobjectType ='Contact'  limit 1];
								newContact.accountId = newAccount.id;
								newContact.recordTypeid = recTypeCon[0].id;
								if(!String.isBlank(newApplication.VBA_Number__c))
									newContact.RBP_VBA_Number__c = newApplication.VBA_Number__c;
								try{
									insert newContact;
									contactRoleRecord.Contact__c = newContact.id;
								} catch(DmlException e) {
									System.debug('An unexpected error has occurred: ' + e.getMessage());
									/* message updated by KATE LUU - SMS Brisbane - 12 June 2017 */ 
									ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error creating registered building practitioner. Please try again later' + e.getMessage())); 
									errorsFound = True;
									//Database.rollback(sp);
									return null;
								}
							} else {
								List<Contact> personAccountContact = [SELECT Id FROM Contact WHERE AccountId =: lstAcc[0].id LIMIT 1];
								contactRoleRecord.Contact__c = personAccountContact[0].id;
							}
                        	contactRoleRecord.Account__c = newAccount.id;
                        	contactRoleRecord.Role__c = GlobalConstants.ACCOUNTCONTACTROLE_RBP_PENDING;
                        	contactRoleRecord.Start_Date__c = System.Today();
						
                            try{
                                insert contactRoleRecord;
                            } catch(DmlException e) {
                                System.debug('An unexpected error has occurred: ' + e.getMessage());
                                /* message updated by KATE LUU - SMS Brisbane - 12 June 2017 */ 
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error processing registered building practitioner. Please try again later')); 
                                errorsFound = True;
                                Database.rollback(sp);
                                return null;
                            }
                        }else{
                            List<Contact> personAccountContact = [SELECT Id FROM Contact WHERE AccountId =: newAccount.id LIMIT 1];
                            if(!personAccountContact.isEmpty()){
                                contactRoleRecord.Contact__c = personAccountContact[0].id;
                                contactRoleRecord.Account__c = newAccount.id;
                                contactRoleRecord.Role__c = GlobalConstants.ACCOUNTCONTACTROLE_RBP_PENDING;
                                contactRoleRecord.Start_Date__c = System.Today();
                                try{
                                    insert contactRoleRecord;
                                } catch(DmlException e) {
                                    System.debug('An unexpected error has occurred: ' + e.getMessage());
                                    /* message updated by KATE LUU - SMS Brisbane - 12 June 2017 */ 
                                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error processing registered building practitioner. Please try again later')); 
                                    errorsFound = True;
                                    Database.rollback(sp);
                                    return null;
                                }
                            }
                        }
                    }
                    System.debug('newContact>>>>>>>'+newContact);
                    System.debug('newContactFirst>>>>>>>'+newContact.FirstName);
                    System.debug('isPersonAccount>>>>>>>'+isPersonAccount);
                    System.debug('newAccountFirst>>>>>>>'+newAccount.FirstName);
					newApplication.Builder_number__c = newAccount.Id;
					if(!isPersonAccount){
						newApplication.RBP_name__c = newContact.FirstName + ' ' + newContact.LastName;
						newApplication.RBP_phone__c = newContact.Phone;
						newApplication.RBP_email__c = newContact.Email;
					}else if(isPersonAccount){
						newApplication.RBP_name__c = newAccount.FirstName + ' ' + newAccount.LastName;
						newApplication.RBP_phone__c = newAccount.Phone;
						newApplication.RBP_email__c = newAccount.Primary_Email__c;
					}
					newApplication.VBA_Number__c = isExistingContact ? currentContactCheck[0].RBP_VBA_Number__c : newApplication.VBA_Number__c;
					newApplication.Builder_Status_at_Start_of_Application__c = constants.Application_Eligibility_Status_UnderApp;

					newApplication.Type__c = GlobalConstants.REC_TYPE_APPLICATION_NEW_ELIGIBILITY;

                    System.debug('newApplication>>>>'+newApplication);
					try{                            
						insert newApplication;
					} catch(DmlException e) {
						System.debug('An unexpected error has occurred: ' + e.getMessage());
						ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error creating application. Please try again later' + e.getMessage()));
						errorsFound = True;
						Database.rollback(sp);
						return null;
					}
                } 
            }
        }catch(DmlException e) {
            errorsFound = True;
            System.debug('HELP2 ERROR: ' + e.getMessage());
        }
        return null;
    }

    public PageReference confirmation(){
        if(errorsFound){
            return null;
        }
        PageReference retURL = new PageReference('/apex/EligibilityApplicationConfirmation?id=' + newApplication.id);
        retURL.setRedirect(true);
        return retURL;
    }

    public PageReference cancel(){
        return new PageReference('/apex/DistributorLanding');
    }


	//Sets the required boolean that is used on the ACN field, if Company, ACN is set to required 
    public pageReference setAccountTypeVal(){
        isRequired = False;
        isPersonAccount = False;
        if (selectedRecType.equalsIgnoreCase('Company')){
            isRequired = True;  
        }else if(selectedRecType.equalsIgnoreCase('Sole Trader')){
            isPersonAccount = True;
        }
        else{
            isPersonAccount = False;
            isRequired = False;
        }
        return null;
    }

    //rollback application & account when file size is more than 25MB
    public void rollBackApplication(){
        try{
            Database.delete(contactRoleRecord);
            contactRoleRecord.Id = null;
            if(currentContactCheck.isEmpty()){
                Database.delete(newContact);
                newContact.Id = null;
            }
            Database.delete(newAccount);
            newAccount.Id = null;
            Database.delete(newApplication);
            newApplication.Id = null;
        }
        catch(Exception exp){
            // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(EligibilityApplicationForm.class),
                                GlobalConstants.FN_ROLLBACK_APPLICATION,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }
}