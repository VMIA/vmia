/**
* Date         :  26-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for EligibilityApplicationForm controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class EligibilityApplicationForm_Test {
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        setupData();
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(rexUser); 
        
        Account userBuilderAc = TestUtility.createBusinessAccount('User Builders',accRecTypeMap.get('Company').getRecordTypeId());
        Database.insert(userBuilderAc);
        System.debug('** userBuilderAc ==>'+ userBuilderAc);
        
        Contact userBuilderContact = TestUtility.createContact('User','Builder','user.builder@bornBuildersTest.com',userBuilderAc.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(userBuilderContact);
        System.debug('** userBuilderContact ==>'+ userBuilderContact);
        
        User builderUser = TestUtility.createPortalUser('UserBuil',profileMap.get('Builder'),userBuilderContact.Id);
        Database.insert(builderUser);         
    }    
    
    public static testMethod void saveCompanyTest(){
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',null,Schema.Sobjecttype.Contact.getRecordTypeInfosByName().get('Distributor').getRecordTypeId());
        Database.insert(tonyStark);
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        System.runAs(rexUser){
            PageReference pageRef = Page.ApplyforEligibility;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('attBody', 'Test Attachment'); 
            System.currentPageReference().getParameters().put('attName', 'DocName'); 
            System.currentPageReference().getParameters().put('attType', 'DocType');            
            Test.startTest();
            EligibilityApplicationForm controller = new EligibilityApplicationForm();
            //Set Business details
            controller.selectedRecType ='Company';
            PageReference pg = controller.setAccountTypeVal();
            System.assertEquals(null, pg);
            System.assert(!controller.isPersonAccount);
            System.assert(controller.isRequired);
            
            setParametersForCompany(controller);
            controller.save();
            controller.insertAttachment();
            Test.stopTest();            
        }
    }   
    
    public static testMethod void saveByBuilderUserTest(){
        User builderUser = [SELECT Id,Name FROM User WHERE Name = 'UserBuil' LIMIT 1];
        System.runAs(builderUser){
            PageReference pageRef = Page.ApplyforEligibility;
            Test.startTest();
            EligibilityApplicationForm controller = new EligibilityApplicationForm();
            //Set Business details
            controller.selectedRecType ='Company';
            PageReference pg = controller.setAccountTypeVal();
            setParametersForCompany(controller);
            controller.save();
            Test.stopTest();            
        }
    }       
    
    public static testMethod void saveSoleTraderTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        System.runAs(rexUser){
            Test.startTest();
            EligibilityApplicationForm controller = new EligibilityApplicationForm();
            //Set Business details
            controller.selectedRecType ='Sole Trader';
            PageReference pg = controller.setAccountTypeVal();
            System.assertEquals(null, pg);
            System.assert(controller.isPersonAccount);
            System.assert(!controller.isRequired);
            
            setParametersForCompany(controller);
            controller.save();
            pg = controller.confirmation();
            System.assertEquals('/apex/EligibilityApplicationConfirmation?id=' + controller.newApplication.id, pg.getUrl()); //Confirmation            
            Test.stopTest();            
        }
    }    
    
    public static testMethod void savePartnershipTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        System.runAs(rexUser){
            Test.startTest();
            EligibilityApplicationForm controller = new EligibilityApplicationForm();
            //Set Business details
            controller.selectedRecType ='Partnership';
            PageReference pg = controller.setAccountTypeVal();
            System.assertEquals(null, pg);
            System.assert(!controller.isPersonAccount);
            System.assert(!controller.isRequired);
            
            setParametersForCompany(controller);
            controller.save();
            Test.stopTest();            
        }
    }      
    
    //Data input errors
    public static testMethod void saveCompanyDataValidationErrorTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        System.runAs(rexUser){
            Test.startTest();
            EligibilityApplicationForm controller = new EligibilityApplicationForm();
            //Set Business details
            controller.selectedRecType ='Company';
            PageReference pg = controller.setAccountTypeVal();
            System.assertEquals(null, pg);
            System.assert(!controller.isPersonAccount);
            System.assert(controller.isRequired);
            System.assert(!controller.isPersonAccount);
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[0].getSummary().contains('All mandatory fields must be completed'), 'There should be a pageMessage advising to provide all mandatory fields.');
            setParametersForCompany(controller);
            controller.newApplication.Builder_ABN__c = 'abc';
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[1].getSummary().contains('ABN must only be numerical values'), 'There should be a pageMessage advising ABN must be neumeric.');            
            pg = controller.GoToApplyforEligibilityFlow();
            System.assertEquals('/apex/ApplyForEligibility', pg.getUrl());
            setParametersForCompany(controller);
            controller.newApplication.Builder_ABN__c = '1234';
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[2].getSummary().contains('ABN must be 11 digits'), 'There should be a pageMessage advising ABN must be 11 numbers.');            
            setParametersForCompany(controller);
            controller.newApplication.builder_acn__c = 'abc';
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[3].getSummary().contains('ACN must only be numerical values'), 'There should be a pageMessage advising ACN must be neumeric.');            
            setParametersForCompany(controller);
            controller.newApplication.builder_acn__c = '1234';
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[4].getSummary().contains('ACN must be 9 numbers'), 'There should be a pageMessage advising ACN must be 9 numbers.');            
            setParametersForCompany(controller);
            controller.newApplication.Date_of_Builders_Application__c = date.today()+1; //Provide a future date
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[5].getSummary().contains('Date of Builder Application cannot be in the future'), 'There should be a pageMessage advising application date must not be in future.');            
            setParametersForCompany(controller);
            controller.emailAddress = 'abcXyz'; //Provide a worng email
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[6].getSummary().contains('Email address not in correct format'), 'There should be a pageMessage advising to corect the format of email.');            
            setParametersForCompany(controller);
            controller.requestedLimit = 'abc'; //Provide non neumeric construction limit
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[7].getSummary().contains('Requested Builder Construction Limit must only be numerical values'), 'There should be a pageMessage advising Construction Limit must be neumeric.');            
            setParametersForCompany(controller);
            controller.hasNoDocuments = true; //Set the no documents to true
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[8].getSummary().contains('Please attach documents before proceeding'), 'There should be a pageMessage advising attach document.');         		
            pg = controller.confirmation();
            System.assertEquals(null, pg); //errorsFound
            Test.stopTest();   
        }
    }
    
    //Applying for a builder who already exists
    public static testMethod void saveCompanyExistingTest(){
        User rexUser = [SELECT Id,Name, AccountId FROM User WHERE Name = 'RexSmith' LIMIT 1];
        //Create builder and contact
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Company').getRecordTypeId());
        bornBuilder.ACN__c = '123456789';
        bornBuilder.ABN__c = '12345678901';
        bornBuilder.eligibility_status__c = 'Active';
        Database.insert(bornBuilder);

        System.runAs(rexUser){
            Test.startTest();
            EligibilityApplicationForm controller = new EligibilityApplicationForm();
            
            //Set Business details
            controller.selectedRecType ='Company';
            controller.setAccountTypeVal();
            setParametersForCompany(controller);
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[0].getSummary().contains('This Builder already exists in the system.'), 'There should be a pageMessage informing that this builder already exists.');            
            
            //With ABN set to Null
            controller = new EligibilityApplicationForm();
            //Set Business details
            controller.selectedRecType ='Company';
            controller.setAccountTypeVal();
            setParametersForCompany(controller);
            controller.newApplication.Builder_ABN__c = null;
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[0].getSummary().contains('This Builder already exists in the system.'), 'There should be a pageMessage informing that this builder already exists.');              
            
            //With incorrect date format
            controller = new EligibilityApplicationForm();
            //Set Business details
            controller.selectedRecType ='Company';
            controller.setAccountTypeVal();
            setParametersForCompany(controller);
            controller.newApplication =null;
            Boolean founderrors = controller.validate();
            System.assertEquals(true, founderrors);             

            PageReference pg = controller.cancel();
            System.assertEquals('/apex/DistributorLanding', pg.getUrl());
            Test.stopTest();            
        }
    } 

    public static testMethod void rollbackApplication(){
        Contact tonyStark = TestUtility.createContact('Tony','Starks','tony.stark@bornBuildersTest.com',null,Schema.Sobjecttype.Contact.getRecordTypeInfosByName().get('Distributor').getRecordTypeId());
        Database.insert(tonyStark);
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        System.runAs(rexUser){
            PageReference pageRef = Page.ApplyforEligibility;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('attBody', 'Test Attachment'); 
            System.currentPageReference().getParameters().put('attName', 'DocName'); 
            System.currentPageReference().getParameters().put('attType', 'DocType');            
            Test.startTest();
            EligibilityApplicationForm controller = new EligibilityApplicationForm();
            //Set Business details
            controller.selectedRecType ='Company';
            PageReference pg = controller.setAccountTypeVal();
            System.assertEquals(null, pg);
            System.assert(!controller.isPersonAccount);
            System.assert(controller.isRequired);
            
            setParametersForCompany(controller);
            controller.save();
            controller.rollbackApplication();

            System.assertEquals(controller.contactRoleRecord.id, null);
            System.assertEquals(controller.newAccount.id, null);
            System.assertEquals(controller.newApplication.id, null);

            //test exception in rollback
            controller.rollbackApplication();
            Test.stopTest();            
        }
    }  
    
    //Applying for a builder who already exists
    public static testMethod void saveCompanyExistingForSoleUserTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        //Create builder and contact
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Company').getRecordTypeId());
        bornBuilder.ACN__c = '123456789';
        bornBuilder.ABN__c = '12345678901';
        bornBuilder.eligibility_status__c = 'Active';
        Database.insert(bornBuilder);
        
        System.runAs(rexUser){
            Test.startTest();
            EligibilityApplicationForm controller = new EligibilityApplicationForm();
            
            //Set Sole Trader details
            controller.selectedRecType ='Sole Trader';
            controller.setAccountTypeVal();
            setParametersForCompany(controller);
            controller.newApplication.Builder_ACN__c = null;
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[0].getSummary().contains('This Builder already exists in the system.'), 'There should be a pageMessage informing that this builder already exists.');             
            
            //With both ABN and ACN set to Null
            controller = new EligibilityApplicationForm();
            //Set Business details
            controller.selectedRecType ='Sole Trader';
            controller.setAccountTypeVal();
            setParametersForCompany(controller);
            controller.newApplication.Builder_ACN__c = null;
            controller.newApplication.Builder_ABN__c = null;
            controller.save();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[0].getSummary().contains('This Builder already exists in the system.'), 'There should be a pageMessage informing that this builder already exists.');               
            PageReference pg = controller.cancel();
            System.assertEquals('/apex/DistributorLanding', pg.getUrl());
            Test.stopTest();            
        }
    }
    
    private static void setParametersForCompany(EligibilityApplicationForm controller){
        controller.hasNoDocuments = false;
        controller.newApplication.Builder_Legal_Entity_Name__c = 'Born Builders';
        controller.newApplication.Builder_ACN__c = '123456789';
        controller.newApplication.Builder_ABN__c = '12345678901';
        controller.requestedLimit = '10000';
        
        //Set Registered building practitioner applicant details
        controller.newContact.firstName = 'Tony';
        controller.newContact.lastName = 'Stark';
        controller.newContact.phone = '0412345678';
        controller.emailAddress = 'tony.stark@bornBuildersTest.com';
        controller.newApplication.VBA_Number__c = 'abcxyz';
        controller.newApplication.Date_of_Builders_Application__c = Date.today();
        controller.newApplication.External_Comments__c = 'BornBuilder application';        
    }
}