public without sharing class EligibilityFormConfirmation {
public String appId{get;set;}
public String appName{get;set;}
public boolean newApplication{get;set;}
public String applicationType{get;set;}
public String builderName {get;set;}

    public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return Page.Login;
        }
        else{
            return null;
        }
    }
    
    public PageReference GoToApplyforEligibilityFlow(){
        return new PageReference('/apex/ApplyForEligibility');
    }
    
   

    
    public PageReference home(){
        return new PageReference('/apex/DistributorLanding');
    
    }
    

    public EligibilityFormConfirmation() {
        appId = ApexPages.currentPage().getParameters().get('id');
        newApplication = False;
        applicationType = 'Application for Eligibility';
        System.debug('appId--->'+appId);

        list<Application__c> newApp = [Select id, name, recordtypeId, Builder_Legal_Entity_Name__c, Builder_First_Name__c, Builders_Last_Name__c from Application__c where id =: appId Limit 1];
        builderName = '';

        if(newApp.size() >0){
            if(String.isNotBlank(newApp[0].Builder_First_Name__c) || String.isNotBlank(newApp[0].Builders_Last_Name__c)){
                builderName = newApp[0].Builder_First_Name__c + ' ' + newApp[0].Builders_Last_Name__c;
                builderName = builderName.replace('null', '');
            }else{
                builderName = newApp[0].Builder_Legal_Entity_Name__c;
            }
            
            if(newApp[0].recordtypeId == constants.application_RecordTypeId_New_Eligibility){
                newApplication = True;
                applicationType = 'Application for Eligibility';
            }
            if(newApp[0].recordtypeId == constants.application_RecordTypeId_Review_Eligibility){
                newApplication = False;
                applicationType = 'Application to Review Eligibility';
            }
            if(newApp[0].recordtypeId == constants.application_RecordTypeId_Amend_COI){
                newApplication = False;
                applicationType = 'Application to Amend a Certificate of Insurance';
            }
            if(newApp[0].recordtypeId == constants.application_RecordTypeId_Cancel_COI){
                newApplication = False;
                applicationType = 'Application to Cancel a Certificate of Insurance';
            }
            appName = newApp[0].name;
        }

    }
    
    
    
}