/**
* Date         :  23-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for DistributorAccessBatch controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class EligibilityFormConfirmation_Test {    
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        UserRole batMember = TestUtility.getUserRole('BAT_Member');
        
        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        Database.insert(vmiaInternalUser);
        setupData();
        setupUsers();
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> rateTableRecTypeMap = Schema.Sobjecttype.PremiumRatesTable__c.getRecordTypeInfosByName();
        
        PremiumRatesTable__c flatRate = TestUtility.createFlatRateTable(1500, rateTableRecTypeMap.get('FlatRate').getRecordTypeId());
        Database.insert(flatRate);

        Account texDistributor = TestUtility.createBusinessAccount('Tex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        texDistributor.website = 'www.textDis.com';
        Database.insert(texDistributor);
        System.debug('** texDistributor ==>'+ texDistributor);

        Contact texSmoth = TestUtility.createContact('Tex','Smoth','tex.smoth@texHomeDistributorTest.com',texDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(texSmoth);
        System.debug('** texSmoth ==>'+ texSmoth);

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);        
    }

    @future
    private static void setupUsers(){
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        Contact tonyStark = [SELECT id FROM Contact WHERE FirstName = 'Tony' and LastName = 'Stark' LIMIT 1];
        Contact rexSmith = [SELECT id FROM Contact WHERE FirstName = 'Rex' and LastName = 'Smith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];

        User builderUser = TestUtility.createPortalUser('ExternB',profileMap.get('Builder'),tonyStark.Id);
        Database.insert(builderUser);

        User ddUser = TestUtility.createPortalUser('ExternD',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(ddUser);

        System.RunAs(builderUser) {
            Application__c  app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
            Database.insert(app);
            System.debug('** Application ==>'+ app);
        }
    }

    public static testmethod void testHome(){
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Application__c app = [Select Id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];

        PageReference pageRef = Page.EligibilityApplicationConfirmation;
        System.currentPageReference().getParameters().put('id', app.Id);
        PageReference pageExpected = new PageReference('/apex/DistributorLanding');
        EligibilityFormConfirmation controller = new EligibilityFormConfirmation();
        System.RunAs(new User(Id=UserInfo.getUserId())) {
            Test.startTest();
            PageReference homePage = controller.home();
            System.assertEquals(pageExpected.getURL(), homePage.getURL());
            Test.stopTest(); 
        }  
    }



    private static testMethod void testForwardToCustomAuthPage(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Application__c app = [Select Id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];

        PageReference pageRef = Page.EligibilityApplicationConfirmation;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',app.id);
        Test.startTest();       
        EligibilityFormConfirmation controller = new EligibilityFormConfirmation();
        PageReference directPage = controller.forwardToCustomAuthPage();
        System.assertEquals(null, directPage);
        
        Test.stopTest();

    }
    private static testMethod void testForwardToCustomAuthPageWithGuest(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Application__c app = [Select Id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        User guestUser = [select id, name from User where userType = 'Guest' LIMIT 1];
        PageReference pageRef = Page.EligibilityApplicationConfirmation;
        
        System.runAs(guestUser){
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id',app.id);
            PageReference pageExpected = Page.Login;
            Test.startTest();       
            EligibilityFormConfirmation controller = new EligibilityFormConfirmation();
            PageReference directPage = controller.forwardToCustomAuthPage();
            System.assertEquals(pageExpected.getUrl(), directPage.getUrl());
            Test.stopTest();
        }
        

    }

    private static testMethod void testGoToApplyforEligibilityFlow(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Application__c app = [Select Id from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];

        PageReference pageRef = Page.EligibilityApplicationConfirmation;
        Test.setCurrentPage(pageRef);
        PageReference confirmPageExpected = new PageReference('/apex/ApplyForEligibility');
        ApexPages.currentPage().getParameters().put('Id',app.id);
        Test.startTest();       
        EligibilityFormConfirmation controller = new EligibilityFormConfirmation();
        PageReference directPage = controller.GoToApplyforEligibilityFlow();
        System.assertEquals(confirmPageExpected.getUrl(), directPage.getUrl());
        
        Test.stopTest();

    }

    private static testMethod void testConfirmationWithNewEligibility(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Application__c app = [Select Id, Name, Builder_Legal_Entity_Name__c, External_Comments__c, RecordTypeId, Type__c, Snapshot_C01__c from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        app.RecordTypeId = constants.application_RecordTypeId_New_Eligibility;

        update app;

        PageReference pageRef = Page.EligibilityApplicationConfirmation;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',app.id);
        Test.startTest();       
        EligibilityFormConfirmation controller = new EligibilityFormConfirmation();
        System.assertEquals(app.Name, controller.AppName);
        System.assertEquals(app.Builder_Legal_Entity_Name__c, controller.BuilderName);  
        System.assertEquals('Application for Eligibility', controller.applicationType); 
        Test.stopTest();

    }

    private static testMethod void testConfirmationWithReviewEligibility(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Application__c app = [Select Id, Name, Builder_Legal_Entity_Name__c, External_Comments__c, RecordTypeId, Type__c, Snapshot_C01__c from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        app.RecordTypeId = constants.application_RecordTypeId_Review_Eligibility;

        update app;

        PageReference pageRef = Page.EligibilityApplicationConfirmation;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',app.id);
        Test.startTest();       
        EligibilityFormConfirmation controller = new EligibilityFormConfirmation();
        System.assertEquals(app.Name, controller.AppName);
        System.assertEquals(app.Builder_Legal_Entity_Name__c, controller.BuilderName);  
        System.assertEquals('Application to Review Eligibility', controller.applicationType);       
        Test.stopTest();

    }

    private static testMethod void testConfirmationWithAmendCOI(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Application__c app = [Select Id, Name, Builder_Legal_Entity_Name__c, External_Comments__c, RecordTypeId, Type__c, Snapshot_C01__c from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        app.RecordTypeId = constants.application_RecordTypeId_Amend_COI;

        update app;

        PageReference pageRef = Page.EligibilityApplicationConfirmation;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',app.id);
        Test.startTest();       
        EligibilityFormConfirmation controller = new EligibilityFormConfirmation();
        System.assertEquals(app.Name, controller.AppName);
        System.assertEquals(app.Builder_Legal_Entity_Name__c, controller.BuilderName);  
        System.assertEquals('Application to Amend a Certificate of Insurance', controller.applicationType);     
        Test.stopTest();

    }

    private static testMethod void testConfirmationWithCancelCOI(){
        Account bornBuilder = [SELECT id FROM Account WHERE name = 'Born Builders' LIMIT 1];
        Application__c app = [Select Id, Name, Builder_Legal_Entity_Name__c, External_Comments__c, RecordTypeId, Type__c, Snapshot_C01__c from Application__c where Builder_Number__c =: bornBuilder.Id limit 1];
        app.RecordTypeId = constants.application_RecordTypeId_Cancel_COI;
        app.Builder_First_Name__c = 'Test';
        app.Builders_Last_Name__c = 'Tester';

        update app;

        PageReference pageRef = Page.EligibilityApplicationConfirmation;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',app.id);
        Test.startTest();       
        EligibilityFormConfirmation controller = new EligibilityFormConfirmation();
        System.assertEquals(app.Name, controller.AppName);
        System.assertEquals(app.Builder_First_Name__c + ' ' + app.Builders_Last_Name__c, controller.BuilderName);   
        System.assertEquals('Application to Cancel a Certificate of Insurance', controller.applicationType);        
        Test.stopTest();

    }


    
}