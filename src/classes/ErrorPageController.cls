public with sharing class ErrorPageController {
	public ErrorPageController() {
		
	}

	public PageReference home() {
        return new PageReference('/apex/DistributorLanding');
    }
}