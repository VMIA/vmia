/**
  * Date         :  10-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Controller for ExportToExcel visualforce page which exports sobject records to excel.
  */
public with sharing class ExportToExcelController{

    // page variables
    public String objectLabel {get;set;}
    public List<sObject> records {get;set;}
    public List<Schema.FieldSetMember> objfieldSet {get;set;}
    
    // variables
    private String objectType;
    private String query;
    private Integer row_limit;
    private Integer offsetVal;
    private String fieldSetName;
    private List<String> tskStatus;
    private List<String> tskSubject;
    
    public ExportToExcelController(){
        query = ApexPages.currentPage().getParameters().get('query');
        query = String.escapeSingleQuotes(query);
        
        row_limit = Integer.valueOf(ApexPages.currentPage().getParameters().get('row_limit'));
        offsetVal = Integer.valueOf(ApexPages.currentPage().getParameters().get('offsetVal'));
        objectType = ApexPages.currentPage().getParameters().get('objectType');
        fieldSetName = ApexPages.currentPage().getParameters().get('fieldSetName');
        String statVal = ApexPages.currentPage().getParameters().get('tskStatus');
        String subVal = ApexPages.currentPage().getParameters().get('tskSubject');
        tskStatus = String.isNotBlank(statVal) ? (List<String>)JSON.deserialize(statVal,List<String>.class) : null;
        tskSubject = String.isNotBlank(subVal) ? (List<String>)JSON.deserialize(subVal,List<String>.class) : null;
        
        System.debug('** query ==>'+ query);
        System.debug('** row_limit ==>'+ row_limit);
        System.debug('** offsetVal ==>'+ offsetVal);
        System.debug('** objectType ==>'+ objectType);
        System.debug('** fieldSetName ==>'+ fieldSetName);
        System.debug('** tskStatus ==>'+ tskStatus);
        System.debug('** tskSubject ==>'+ tskSubject);
        
        Schema.DescribeSObjectResult objResult = Schema.getGlobalDescribe().get(objectType).getDescribe();
        objfieldSet = objResult.fieldSets.getMap().get(fieldSetName).getFields();
        objectLabel = objResult.getLabelPlural();
        
        String soql = 'SELECT ' + String.join(objfieldSet,',') + ' FROM ' + objectType;
        System.debug('** soql ==>'+ soql);
        records = Database.query(query);
        System.debug('** records ==>'+ records);
    }
    
}