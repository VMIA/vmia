/**
  * Date         :  27-June-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for ExportToExcelController
  */ 
  
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
private with sharing class ExportToExcelController_Test{

    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> appRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup(){

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);

        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,
                                                        conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        
        Application__c app = TestUtility.createApplication(bornBuilder.Id,rexDistributor.Id,'New Eligibility',
                                                            appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
        app.External_Comments__c = 'DD/Builder comments';
        Database.insert(app);


    }

    @future
    private static void setupUser(){
        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(new List<User>{vmiaInternalUser,rexUser});
    }
    
    /**
      * @description       This method tests scenario of ExportToExcel page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testPageLoad(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        String currentSOQL = 'SELECT Builder__r.Name,Activity_Number__c,Subject,Builder__c,CreatedDate,ActivityDate,Status FROM Task';
        System.runAs(rexUser){
            PageReference pageRef = Page.ExportToExcel;
            pageRef.getParameters().put('query',currentSOQL);
            pageRef.getParameters().put('ROW_LIMIT','5');
            pageRef.getParameters().put('offsetVal','0');
            pageRef.getParameters().put('fieldSetName','Task_Search_Table');
            pageRef.getParameters().put('tskSubject',JSON.serialize(new List<String>{'COI Approved - Notify Builder', 'COI Rejected - Notify Builder'}));
            pageRef.getParameters().put('tskStatus',JSON.serialize(new List<String>{'Not Started, Completed'}));
            pageRef.getParameters().put('objectType','Task');
            pageRef.setRedirect(true);
            Test.setCurrentPage(pageRef);
            
            Test.startTest();
                ExportToExcelController exportController  = new ExportToExcelController();
            Test.stopTest();
        }
    }
}