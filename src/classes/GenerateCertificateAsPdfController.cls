public class GenerateCertificateAsPdfController {
    
    // page variables
    public Boolean displayACN {get;set;}
    public Boolean displayABN {get;set;}
    public String propertyOwner{get;set;}
    public String propertyAddressString {get;set;}
    public list<Certificate__c> cerlist{get;set;}
    
    //variables
    @TestVisible private Id certificateId;

    public PageReference forwardToCustomAuthPage() {
        if(GlobalConstants.GUEST_TYPE.equalsIgnoreCase(UserInfo.getUserType())){
            return Page.Login;
        }
       
        if(cerlist[0].status__c != constants.certificate_status_Issued_Historical &&  
            cerlist[0].status__c != constants.certificate_status_Issued_Works_In_Progress && 
            cerlist[0].status__c != constants.certificate_status_Issued_Works_Complete && 
            cerlist[0].status__c != constants.certificate_status_Certificate_Issued && 
            cerlist[0].status__c != constants.certificate_status_Takeover){
            return Page.ErrorPage;
        } 
        return null;
    }
    
    public GenerateCertificateAsPdfController(ApexPages.StandardController controller) {
        certificateId = controller.getId();
        
        cerlist = new list<Certificate__c>();

        if(String.isNotBlank(certificateId)){
            cerlist = [SELECT Name,Builder__r.Account_Number__c,Builder__r.RecordType.Name,Status__c, Building_Contract_Date__c,
                        PIT_Distributor_Name__c,PIT_Distributor_Address__c,PIT_Distributor_Website__c,PIT_Distributor_Phone__c,PIT_Policy_Number__c,
                        PIT_Policy_Inception_Date__c,PIT_Domestic_Building_Work__c,PIT_At_the_Property__c,PIT_Builder_Name__c,PIT_Builder_ACN__c,
                        PIT_Builder_ABN__c,PIT_Contract_Price__c,PIT_Special_Conditions__c,PIT_Base_DBI_Premium__c,PIT_GST__c,PIT_Stamp_Duty__c,
                        PIT_Total__c,PIT_First_Owner_Name__c,PIT_First_Owner_Address__c,PIT_Second_Owner_Name__c,PIT_Second_Owner_Address__c,
                        PIT_Third_Owner_Name__c,PIT_Third_Owner_Address__c,PIT_Property_Owner_Count__c,PIT_Defects_Inspector_Name__c,
                        PIT_Actual_Completion_Date__c,PIT_Defects_Inspection_Report_Date__c
                        FROM Certificate__c WHERE Id =: certificateId LIMIT 1];

            // ACN should only be displayed for Companies whereas ABN for Partnership and Sole Trader
            if(!cerlist.isEmpty()) {
                if(cerList[0].Builder__r.RecordType.Name == GlobalConstants.ACCOUNT_REC_TYPE_COMPANY) {
                    displayACN = true;
                    displayABN = false;
                } 
                else if(cerList[0].Builder__r.RecordType.Name == GlobalConstants.ACCOUNT_REC_TYPE_PARTNERSHIP 
                            || cerList[0].Builder__r.RecordType.Name == GlobalConstants.ACCOUNT_REC_TYPE_SOLE_TRADER) {
                    displayACN = false;
                    displayABN = true;
                }
            }
            
            propertyAddressString = cerList[0].PIT_At_the_Property__c;
            propertyAddressString = String.isNotBlank(propertyAddressString) ? propertyAddressString.replace(GlobalConstants.AUSTRALIA, GlobalConstants.WHITE_SPACE) : propertyAddressString;
            propertyOwner = String.isNotBlank(cerList[0].PIT_First_Owner_Name__c) ? cerList[0].PIT_First_Owner_Name__c : GlobalConstants.WHITE_SPACE;
            
            propertyOwner = String.isNotBlank(cerList[0].PIT_Second_Owner_Name__c) ? 
                            (propertyOwner + GlobalConstants.COMMA + GlobalConstants.EMPTY_SPACE + cerList[0].PIT_Second_Owner_Name__c) : propertyOwner;
            
            propertyOwner = String.isNotBlank(cerList[0].PIT_Third_Owner_Name__c) ? 
                            (propertyOwner + GlobalConstants.COMMA + GlobalConstants.EMPTY_SPACE + cerList[0].PIT_Third_Owner_Name__c) : propertyOwner;
                            
            if(cerList[0].PIT_Property_Owner_Count__c != null & cerList[0].PIT_Property_Owner_Count__c > 3){
                propertyOwner += GlobalConstants.COMMA + GlobalConstants.EMPTY_SPACE + GlobalConstants.ET_AL;
            }
        }
    }
}