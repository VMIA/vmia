/**
* Date         :  19-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for GenerateCertificatesController controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class GenerateCertificatesController_Test {
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        

        setupData();
        
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);
        
        Certificate__c  cert = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
        Database.insert(cert);
        System.debug('** Certificate ==>'+ cert.id);
        
        Application__c  app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(),cert.Id);
        Database.insert(app);
        System.debug('** Application ==>'+ app);
        
		User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(rexUser);   
    }    
    
    public static testMethod void updateCategoryOfWorkTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        System.runAs(rexUser){
            GenerateCertificatesController certificatesController = getCertificatesController(bornBuilder);
            
            //TakenOver
            certificatesController.certRecord.Taken_Over__c = true;
            certificatesController.updateCategoryOfWork(); 
            
            //Not TakenOver
            certificatesController.certRecord.Taken_Over__c = false;
            certificatesController.updateCategoryOfWork();
        }
    }
    
    public static testMethod void newCertificateTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        System.runAs(rexUser){
            GenerateCertificatesController certificatesController = getCertificatesController(bornBuilder);
            certificatesController.certRecord.Taken_Over__c = true;
            PageReference pg = certificatesController.newCertificate(); 
            System.assertEquals('/apex/COIApplication?cid='+bornBuilder.id, pg.getUrl());
        }
    }    
    
    public static testMethod void addToBasketTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        System.runAs(rexUser){
            GenerateCertificatesController certificatesController = getCertificatesController(bornBuilder);
            certificatesController.certRecord.Taken_Over__c = true;
            PageReference pg = certificatesController.addToBasket(); 
            System.assertEquals('/apex/ShoppingCartPage', pg.getUrl());
        }
    }      

    public static testMethod void payNowTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        System.runAs(rexUser){
            GenerateCertificatesController certificatesController = getCertificatesController(bornBuilder);
            certificatesController.certRecord.Taken_Over__c = true;
            PageReference pg = certificatesController.payNow(); 
            System.assertEquals('/apex/paymentDetailsPage', pg.getUrl());
        }
    }        
    
    
    private static GenerateCertificatesController getCertificatesController(Account bornBuilder){
            PageReference pageRef = Page.COIApplication;
            Test.setCurrentPage(pageRef); 
            System.currentPageReference().getParameters().put('cId', bornBuilder.Id);
            System.assertEquals(bornBuilder.Id, ApexPages.currentPage().getParameters().get('cId'));
            //ApexPages.currentPage().getParameters().put('distributorId', rexDistributor.Id);   //Without a distributor       
            ApexPages.StandardController stdController = new ApexPages.StandardController(new Certificate__c());
            GenerateCertificatesController certificatesController = new GenerateCertificatesController(stdController);
            
            System.debug('@@@@builderRecord: '+certificatesController.builderRecord);
            
            System.assert(!certificatesController.oBJourney);
            System.assertEquals(bornBuilder.Id, certificatesController.builderId);
            System.debug('@@@@@categoryOfWork: '+certificatesController.categoryOfWork); 
        return certificatesController;
    }

    private static testMethod void addOwnerTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];


        System.runAs(rexUser){
            GenerateCertificatesController certificatesController = getCertificatesController(bornBuilder);
            certificatesController.certRecord.Taken_Over__c = true;

            //Data for Individual Property Owner Preparation for Single Dwelling
            certificatesController.multiCertificate = false;
            certificatesController.firstName = 'TestFirst';
            certificatesController.lastName = 'TestLast';
            certificatesController.email = 'TestEmail@gmail.com';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            
            certificatesController.seletecOwnerType = 'Individual';

            PageReference pg = certificatesController.addOwner(); 
            PageReference pgClone = certificatesController.addOwner(); 

            //Data for Organisational Property Owner Preparation for Single Dwelling
            certificatesController.multiCertificate = false;
            certificatesController.legalEntity = 'TestName';
            certificatesController.abn = '16517819651';
            certificatesController.acn = '165178196';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            
            certificatesController.seletecOwnerType = 'Organisation';

            PageReference pgOrg = certificatesController.addOwner(); 
            PageReference pgOrgclone = certificatesController.addOwner(); 



            //Data for Individual Property Owner Preparation for Multi Dwelling
            certificatesController.multiCertificate = true;
            certificatesController.firstName = 'TestFirst';
            certificatesController.lastName = 'TestLast';
            certificatesController.email = 'TestEmail@gmail.com';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            
            certificatesController.seletecOwnerType = 'Individual';

            PageReference pgMultiInd = certificatesController.addOwner(); 
            PageReference pgMultiIndClone = certificatesController.addOwner(); 

            //Data for Organisational Property Owner Preparation for Single Dwelling
            certificatesController.multiCertificate = true;
            certificatesController.legalEntity = 'TestName';
            certificatesController.abn = '16517819651';
            certificatesController.acn = '165178196';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            
            certificatesController.seletecOwnerType = 'Organisation';

            PageReference pgMultiOrg = certificatesController.addOwner(); 
            PageReference pgMultiClone = certificatesController.addOwner(); 
            
        }
    }  

    private static testMethod void reviewAndConfirmTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];


        System.runAs(rexUser){
            GenerateCertificatesController certificatesController = getCertificatesController(bornBuilder);
            ApexPages.StandardController stdController = new ApexPages.StandardController(bornBuilder);
            referralAndValidationCheck ref = new referralAndValidationCheck(stdController);
            System.debug('>>'+certificatesController.extnTwo);

            // Data for Property Record Creation
            Property__c property = TestUtility.createProperty('TestStreet', 'TestSuburb','1234', 'VIC');
            database.insert(property);
            certificatesController.propertyListClone.add(property);
            certificatesController.selectedPropertytoinsuread = '1';
            certificatesController.contractVal = '20000';
            certificatesController.oBJourney = false;

            //Data for Individual Property Owner Preparation for Single Dwelling
            certificatesController.multiCertificate = false;
            certificatesController.firstName = 'TestFirst';
            certificatesController.lastName = 'TestLast';
            certificatesController.email = 'TestEmail@gmail.com';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            
            certificatesController.seletecOwnerType = 'Individual';

            PageReference pg = certificatesController.addOwner(); 

            //Data for Organisational Property Owner Preparation for Single Dwelling
            certificatesController.multiCertificate = false;
            certificatesController.legalEntity = 'TestName';
            certificatesController.abn = '16517819651';
            certificatesController.acn = '165178196';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            certificatesController.street = 'TestStreet';
            certificatesController.suburb = 'TestSuburb';
            certificatesController.state = 'TestState';
            certificatesController.postcode = '1234';
            
            certificatesController.seletecOwnerType = 'Organisation';

            PageReference pgOrg = certificatesController.addOwner();

            certificatesController.reviewAndConfirm() ;

            certificatesController.multiCertificate = false;
            certificatesController.contractVal = '';
            certificatesController.reviewAndConfirm() ;


            certificatesController.street = '';
            certificatesController.suburb = '';
            certificatesController.state = '';
            certificatesController.postcode = '';
            certificatesController.contractVal = '20000';
            certificatesController.multiCertificate = true;
            certificatesController.propertyListClone.clear();
            certificatesController.addressStr = null ;
            certificatesController.reviewAndConfirm() ;

        }    
    }  

    private static testMethod void reviewAndConfirmValidationTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];


        System.runAs(rexUser){
            GenerateCertificatesController certificatesController = getCertificatesController(bornBuilder);
            ApexPages.StandardController stdController = new ApexPages.StandardController(bornBuilder);
            referralAndValidationCheck ref = new referralAndValidationCheck(stdController);
            System.debug('>>'+certificatesController.extnTwo);

            certificatesController.seletecOwnerType = 'Individual';
            certificatesController.contractVal = '20000';
            certificatesController.multiCertificate = true;
            certificatesController.propertyListClone.clear();
            certificatesController.addressStr = null ;
            certificatesController.propertyWrapperList = null;
            certificatesController.reviewAndConfirm() ;


            certificatesController.seletecOwnerType = 'Individual';
            certificatesController.multiCertificate = false;
            certificatesController.propertyListClone = null;
            certificatesController.propertyWrapperList = null;
            certificatesController.reviewAndConfirm() ;

            certificatesController.seletecOwnerType = 'Individual';
            certificatesController.multiCertificate = true;
            certificatesController.addressStr = null ;
            certificatesController.street = 'TestStreet';
            certificatesController.suburb = 'TestSuburb';
            certificatesController.state = 'TestState';
            certificatesController.postcode = '1234';
            
            certificatesController.propertyWrapperList = null;
            certificatesController.reviewAndConfirm() ;

            certificatesController.seletecOwnerType = 'Individual';
            certificatesController.multiCertificate = false;
            certificatesController.propertyOwnerWrapperList = null;
            certificatesController.otherOwnerList = null;
            certificatesController.reviewAndConfirm() ;


            certificatesController.oBJourney = true;
            certificatesController.reviewAndConfirm() ;

            certificatesController.multiCertificate = true;
            certificatesController.street = '';
            certificatesController.suburb = '';
            certificatesController.state = '';
            certificatesController.postcode = '';
            certificatesController.addressStr = null ;
            certificatesController.reviewAndConfirm() ;

            certificatesController.street = 'TestStreet';
            certificatesController.suburb = 'TestSuburb';
            certificatesController.state = 'TestState';
            certificatesController.postcode = '1234';
            certificatesController.multiCertificate = true;
            certificatesController.propertyWrapperList = null;
            certificatesController.reviewAndConfirm() ;

            certificatesController.multiCertificate = false;
            certificatesController.propertyWrapperList = null;
            certificatesController.reviewAndConfirm() ;



        }
    }        

    private static testMethod void propertyInsuredCheckForMultiTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];


        System.runAs(rexUser){
            GenerateCertificatesController certificatesController = getCertificatesController(bornBuilder);

            certificatesController.selectedPropertytoinsuread = '1.2' ;
            certificatesController.propertyInsuredCheckForMulti();

            certificatesController.selectedPropertytoinsuread = '1' ;
            certificatesController.selectedCategory='C03: New Multi-Dwelling Construction' ;
            certificatesController.propertyInsuredCheckForMulti();

            certificatesController.selectedPropertytoinsuread = '3' ;
            certificatesController.selectedCategory='C01: New Single Dwelling Construction' ; 
            certificatesController.propertyInsuredCheckForMulti();

            certificatesController.selectedPropertytoinsuread = '-3' ;
            certificatesController.selectedCategory='C01: New Single Dwelling Construction' ; 
            certificatesController.propertyInsuredCheckForMulti();

            certificatesController.selectedPropertytoinsuread = '1' ;
            certificatesController.selectedCategory='C01: New Single Dwelling Construction' ; 
            certificatesController.propertyInsuredCheckForMulti();

        }
    }  

    private static testMethod void propertyInsuredCheckest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
         Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        Certificate__c  cert = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
        Database.insert(cert);

        System.runAs(rexUser){
            GenerateCertificatesController certificatesController = getCertificatesController(bornBuilder);
            certificatesController.selectedCategory = 'C01: New Single Dwelling Construction';
            certificatesController.propertyInsuredCheck();

            certificatesController.selectedCategory = 'C03: New Multi-Dwelling Construction';
            certificatesController.selectedPropertytoinsuread = '3';
            certificatesController.propertyInsuredCheck();
            
            certificatesController.selectedCategory = 'C04: Alterations/Additions - Structural';
            certificatesController.propertyInsuredCheck();

            certificatesController.selectedCategory = 'C05: Swimming Pools';
            certificatesController.propertyInsuredCheck();

            certificatesController.selectedCategory = 'C06: Renovations & Tradespeople - Non Structural';
            certificatesController.propertyInsuredCheck();

            certificatesController.selectedCategory = 'C07: Other';
            certificatesController.propertyInsuredCheck();

             certificatesController.getStreetTypes();
             certificatesController.getSubCategories();

             certificatesController.selectedSearchType = '--None--' ;
             certificatesController.applyFilter();

             certificatesController.selectedSearchType = 'SPI' ;
             certificatesController.applyFilter();

             certificatesController.selectedSearchType = 'Address' ;
             certificatesController.applyFilter();

             certificatesController.seletecOwnerType = 'Individual' ;
             certificatesController.applyOwnerFilter();

             certificatesController.seletecOwnerType = 'Organisation' ;
             certificatesController.applyOwnerFilter();

             certificatesController.seletecOwnerType = 'Speculative' ;
             certificatesController.applyOwnerFilter();

            
            //certificatesController.cerList.add(cert);

            // certificatesController.submitCertificate();
             //certificatesController.submitCertRedirect();
             certificatesController.paymentRedirect();
             certificatesController.shoppingRedirect();
             //certificatesController.confirmation();
             certificatesController.backpage();
             certificatesController.backToMultiPayment();
             certificatesController.backToCertificateDetail();
             certificatesController.goToShoppingCart();
             certificatesController.backToShoppingDetail();
             certificatesController.returnHome();
             certificatesController.backtoPrevipusPage();

             // Data for Property Record Creation
            Property__c property = TestUtility.createProperty('TestStreet', 'TestSuburb','1234', 'VIC');
            database.insert(property);
            certificatesController.propertyListClone.add(property);
            certificatesController.indexforRemove = 0;
            //certificatesController.removeProperty();


        }
    }
    
     private static testMethod void premiumLogicControlTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];


        System.runAs(rexUser){
            // Data for Property Record Creation
            Property__c property = TestUtility.createProperty('TestStreet', 'TestSuburb','1234', 'VIC');
            database.insert(property);
            GenerateCertificatesController certificatesController = getCertificatesController(bornBuilder);
            ApexPages.StandardController stdController = new ApexPages.StandardController(bornBuilder);
            referralAndValidationCheck ref = new referralAndValidationCheck(stdController);
            System.debug('>>'+certificatesController.extnTwo);

            
            certificatesController.contractVal = '20000';
            certificatesController.selectedCategory = 'C01: New Single Dwelling Construction';
            certificatesController.builderRecord.SD_Flat_Rate_Premium__c = null;
            certificatesController.multiCertificate = false ;

            certificatesController.street = 'TestStreet';
            certificatesController.suburb = 'TestSuburb';
            certificatesController.state = 'TestState';
            certificatesController.postcode = '1234';
            

            certificatesController.selectedCategory = 'C03: New Multi-Dwelling Construction';
            certificatesController.builderRecord.SD_Flat_Rate_Premium__c = null;
            certificatesController.multiCertificate = true ;
            certificatesController.selectedPropertytoinsuread = '3';
            certificatesController.propertyInsuredCheck();

            //Adding the Propery Onwers 
            //Data for Individual Property Owner Preparation for Single Dwelling
            certificatesController.multiCertificate = false;
            certificatesController.firstName = 'TestFirst';
            certificatesController.lastName = 'TestLast';
            certificatesController.email = 'TestEmail@gmail.com';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            
            certificatesController.seletecOwnerType = 'Individual';

            PageReference pg = certificatesController.addOwner(); 

            //Data for Individual Property Owner Preparation for Single Dwelling
            certificatesController.multiCertificate = false;
            certificatesController.firstName = 'TestFirst';
            certificatesController.lastName = 'TestLast';
            certificatesController.email = 'TestEmail1@gmail.com';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            
            certificatesController.seletecOwnerType = 'Individual';

            PageReference pg1 = certificatesController.addOwner(); 

            //Data for Individual Property Owner Preparation for Single Dwelling
            certificatesController.multiCertificate = false;
            certificatesController.firstName = 'TestFirst';
            certificatesController.lastName = 'TestLast';
            certificatesController.email = 'TestEmail2@gmail.com';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            
            certificatesController.seletecOwnerType = 'Individual';

            PageReference pg2 = certificatesController.addOwner(); 

            certificatesController.propertyWrapperList[0].contractValue = 20000 ;
            certificatesController.propertyWrapperList[1].contractValue = 20000 ;
            certificatesController.propertyWrapperList[2].contractValue = 20000 ;
            certificatesController.reviewAndConfirm();

        }
    }  

    private static testMethod void payNowForNormalBuilderTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];


        System.runAs(rexUser){
            
            Certificate__c certificateRecord = new Certificate__c();
            ApexPages.StandardController sc; 
            sc = new ApexPages.StandardController(certificateRecord);
            GenerateCertificatesController certificatesController = new GenerateCertificatesController(sc);
            PageReference pageRef = Page.COIApplication;
            Test.setCurrentPage(pageRef);

            certificatesController.refferedCertificate = false;
            certificatesController.builderid = bornBuilder.Id;
            certificatesController.selectedCategory = 'C01: New Single Dwelling Construction';
            certificatesController.selectedPropertytoinsuread = '1';
            certificatesController.propertyInsuredCheck();

            // Data for Property Record Creation
            Property__c property = TestUtility.createProperty('TestStreet', 'TestSuburb','1234', 'VIC');
            database.insert(property);
            certificatesController.propertyListClone.add(property);
            certificatesController.propertyList.add(property);

            //Data for Individual Property Owner Preparation for Single Dwelling
           
            certificatesController.firstName = 'TestFirst';
            certificatesController.lastName = 'TestLast';
            certificatesController.email = 'TestEmail@gmail.com';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            
            certificatesController.seletecOwnerType = 'Individual';

            PageReference pg = certificatesController.addOwner(); 


            //Data for Individual Property Owner Preparation for Multi Dwelling
            
            certificatesController.firstName = 'TestFirst';
            certificatesController.lastName = 'TestLast';
            certificatesController.email = 'TestEmail@gmail.com';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            
            certificatesController.seletecOwnerType = 'Individual';

            PageReference pgMultiInd = certificatesController.addOwner(); 

            certificatesController.payNow(); 



        }
    } 

    private static testMethod void payNowMultiForNormalBuilderTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];


        System.runAs(rexUser){
            
            Certificate__c certificateRecord = new Certificate__c();
            ApexPages.StandardController sc; 
            sc = new ApexPages.StandardController(certificateRecord);
            GenerateCertificatesController certificatesController = new GenerateCertificatesController(sc);
            PageReference pageRef = Page.COIApplication;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('indexProperty', '0');

            certificatesController.refferedCertificate = false;
            certificatesController.builderid = bornBuilder.Id;
            certificatesController.selectedCategory = 'C03: New Multi-Dwelling Construction';
            certificatesController.selectedPropertytoinsuread = '3';
            certificatesController.propertyInsuredCheckForMulti();

            // Data for Property Record Creation
            Property__c property = TestUtility.createProperty('TestStreet', 'TestSuburb','1234', 'VIC');
            database.insert(property);
            certificatesController.propertyListClone.add(property);
            certificatesController.propertyList.add(property);

            //Data for Individual Property Owner Preparation for Single Dwelling
            
            //certificatesController.addOwnerToUnits() ;
            certificatesController.seletecOwnerType = 'Individual';
            certificatesController.firstName = 'TestFirst';
            certificatesController.lastName = 'TestLast';
            certificatesController.email = 'TestEmail@gmail.com';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            
            PageReference pg = certificatesController.addOwner(); 
            
            certificatesController.legalEntity = 'TestName';
            certificatesController.abn = '16517819651';
            certificatesController.acn = '165178196';
            certificatesController.businessAddress = 'TestStreet';
            certificatesController.businessSuburb = 'TestSuburb';
            certificatesController.businessState = 'TestState';
            certificatesController.businessPostCode = 'TestPostcode';
            certificatesController.businessCountry = 'TestCountry';
            certificatesController.street = 'TestStreet';
            certificatesController.suburb = 'TestSuburb';
            certificatesController.state = 'TestState';
            certificatesController.postcode = '1234';
            
            certificatesController.seletecOwnerType = 'Organisation';
            PageReference pgOne = certificatesController.addOwner(); 

            PageReference pgMultiInd = certificatesController.addOwner(); 

            certificatesController.payNow(); 
            certificatesController.payNow(); 



        }
    }                           
}