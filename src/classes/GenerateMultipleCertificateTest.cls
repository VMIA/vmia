/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GenerateMultipleCertificateTest {
	
	//variables
    private static Map<String, Id> profileMap = new Map<String, Id>();
    private static Map<String, Schema.RecordTypeInfo> accRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    private static Map<String, Schema.RecordTypeInfo> conRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    private static Map<String, Schema.RecordTypeInfo> certRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    private static Map<String, Schema.RecordTypeInfo> appRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    
    static {
        for (Profile prof : [SELECT Name FROM Profile]) {
            profileMap.put(prof.Name, prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup() {
        for (Profile prof : [SELECT Name FROM Profile]) {
            profileMap.put(prof.Name, prof.Id);
        }

        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal', profileMap.get('VMIA BAT Internal'), batMember.Id);
        Database.insert(vmiaInternalUser);

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

    }
    
    @future
    private static void setupData() {
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor', accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);

        Contact rexSmith = TestUtility.createContact('Rex', 'Smith', 'rex.smith@rexHomeDistributorTest.com', rexDistributor.Id,
                           conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders', accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Name = 'Henry Niu';
        bornBuilder.BillingStreet = '140 William St';
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        
        Application__c app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility',
                             appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
        Database.insert(app);
        
        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id, bornBuilder.Agent__c,
                              certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        Database.insert(cert);
        
        BAT_Letter__c batLetter = TestUtility.createBATLetter(bornBuilder.Id, app.Id, cert.Id);
        batLetter.Letter_Description__c = 'Test Description';
        Database.insert(batLetter);    
        
        Property__c property = TestUtility.createProperty('140 William St', 'Melbourne', '3000', 'VIC');
        Database.insert(property);   
    }

    @future
    private static void setupUser() {
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith', profileMap.get('Distributor'), rexSmith.Id);
        Database.insert(rexUser);
    }

	@isTest
    static void testConstructor() {
        Account builder = [SELECT Id FROM Account LIMIT 1];
    	
		PageReference pageRef = Page.GenerateCertificate; 
        Test.setCurrentPage(pageRef); 
        pageRef.getParameters().put('id', builder.id); 
        
        generateMultipleCertificate gmc = new generateMultipleCertificate();
        
        System.assertEquals(builder.Id, gmc.builderId);
        System.assertEquals(false, gmc.noFilter);
        System.assertEquals(false, gmc.spiTypeSearch);
        System.assertEquals(false, gmc.addressTypeSearch);
        System.assertEquals(false, gmc.InCompleteFilter);
        System.assertEquals(false, gmc.proprtyNotFound);
        System.assertEquals(false, gmc.displayProperty);
        System.assertNotEquals(null, gmc.searchType);
        System.assertEquals(3, gmc.searchType.size());
        
        System.assertNotEquals(null, gmc.builderRecord);
        System.assertNotEquals('Henry Niu', gmc.builderRecord.Name);
    }
    
    @isTest
    static void testApplyFilter() {
        generateMultipleCertificate gmc = new generateMultipleCertificate();
        
        gmc.selectedSearchType = '--None--';
        pagereference pr = gmc.applyFilter();
        System.assertEquals(false, gmc.spiTypeSearch);
        System.assertEquals(false, gmc.addressTypeSearch);
        
        gmc.selectedSearchType = 'SPI';
        pr = gmc.applyFilter();
        System.assertEquals(true, gmc.spiTypeSearch);
        
        gmc.selectedSearchType = 'Address';
        pr = gmc.applyFilter();
        System.assertEquals(true, gmc.addressTypeSearch);
    }
    
    @isTest
    static void testSearchProperty() {
        generateMultipleCertificate gmc = new generateMultipleCertificate();
        
        gmc.selectedSearchType = '--None--';
        pagereference pr = gmc.searchProperty();
        System.assertEquals(true, gmc.noFilter);
        
        gmc.selectedSearchType = 'SPI';
        pr = gmc.searchProperty();
        System.assertEquals(true, gmc.InCompleteFilter);
        
        gmc.selectedSearchType = 'Address';
        pr = gmc.searchProperty();
        System.assertEquals(true, gmc.InCompleteFilter);
        
        gmc.selectedSearchType = 'Test';
        pr = gmc.searchProperty();        

        System.assertEquals(0, gmc.propertyList.size());
        System.assertEquals(0, gmc.propertyListClone.size());
    }
    
    @isTest
    static void testRemoveProperty() {
        PageReference pageRef = Page.GenerateCertificate; 
        Test.setCurrentPage(pageRef); 
        pageRef.getParameters().put('index', '1');
        
        Property__c property = [SELECT Id FROM Property__c LIMIT 1];        
        List<Property__c> propertyListClone = new List<Property__c>();
        propertyListClone.add(property);
        
        generateMultipleCertificate gmc = new generateMultipleCertificate(); 
        gmc.propertyListClone = propertyListClone;
        gmc.removeProperty();
        
        System.assertEquals(0, gmc.propertyListClone.size());
    }
}