/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GeneratePendingLOEExtensionTest {
	
	@isTest
    static void testConstructor() {
    	Application__c app = new Application__c();
    	app.Builder_ACN__c = '111111111';
    	app.Builder_ABN__c = '22222222222';
    	
    	ApexPages.StandardController stdController = new ApexPages.StandardController(app);
    	GeneratePendingLOEExtension ext = new GeneratePendingLOEExtension(stdController);
    	
    	System.assertEquals('111111111', ext.builderACN);
    	System.assertEquals('22222222222', ext.builderABN);
    	System.assertEquals(false, ext.hasMailingAddress);
    	System.assertEquals(false, ext.hasFlatRates);
    	System.assertEquals(true, ext.builderHasACN);
    	System.assertEquals(true, ext.builderHasABN);
    	System.assertNotEquals(null, ext.categoriesList);
    	System.assertEquals(0, ext.categoriesList.size());
    }

    @isTest
    static void testBuilderCategory() {
        GeneratePendingLOEExtension.BuilderCategory cat 
        	= new GeneratePendingLOEExtension.BuilderCategory('ABC Ltd', 1000000, 15);
        	
        System.assertEquals('ABC Ltd', cat.categoryName);
       	System.assertEquals(1000000, cat.categoryLimit);
       	System.assertEquals(15, cat.categoryFlatRate);    
    }
}