/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Global Constant Class.
  */  
public with sharing class GlobalConstants {
    
    /*** TriggerHelper Constants  ***/
    public static final String COMMA = ',';
    
    /*** CertificateTriggerHelper Constants  ***/
    public static final String FN_GENERATE_POLICY = 'generatePolicyNumber';
    public static final String FN_UPDATE_CHILD_CERTS = 'updateChildCertificates';
    public static final String FN_CAL_CONSTRUCTION_LIMIT = 'calculateConstructionLimit';
    public static final String FN_CREATE_INVOICE = 'createInvoiceForCertificate';
    public static final String FN_CREATE_DD_TASK = 'createDistributorTask';
    public static final String FN_UPDATE_CERT_STATUS = 'updateCertificateStatus';
    public static final String FN_CREATE_CERT_HISTORY = 'createCertificateHistory';
    public static final string FN_CLONE_ORIGINAL_CERTIFICATE = 'cloneOriginalCertificate';
    public static final String FN_ASSIGN_TO_BAT_QUEUE = 'assignToBATQueue';
    public static final String FN_REPLENISH_CONSTRUCTION_LIMIT  = 'replenishConstructionLimit';
    public static final String FN_CREATE_SNAPSHOT  = 'captureSnapshotFields';
    
    public static final String STAT_MULTI_APPROVED = 'Multi Approved';
    public static final String STAT_AWAITING_PAYMENT = 'Awaiting Payment';
    public static final String STAT_AWAITING_PAYMENT_TASK = 'AwaitingPayment';
    public static final String STAT_AMENDCANCEL_TASK = 'AmendRequest';
    public static final String STAT_AWAITING_REFUND = 'Awaiting Refund';
    public static final String STAT_REJECTED = 'Rejected';
    public static final String STAT_AWAITING_APPROVAL = 'Awaiting Approval';
    public static final String STAT_ISSUED_IN_PROGRESS = 'Issued Works in Progress';
    public static final String STAT_ISSUED_COMPLETED = 'Issued Works Complete';
    public static final String STAT_EXPIRED = 'Expired';
    public static final String STAT_CANCELLED = 'Cancelled';
    public static final String STAT_ISSUED_HISTORICAL = 'Issued - Historical';
    public static final String STAT_TAKEN_OVER = 'Taken Over';
    public static final String STAT_DRAFT = 'Draft';
    public static final String STAT_PAYMENT_FAILED = 'Payment Failed';
    public static final String STAT_REFUND_FAILED = 'Refund Failed';
    public static final String STAT_WITHDRAWN = 'Withdrawn';
    public static final String STAT_DECLINED = 'Declined';
    public static final String SUB_STAT_REFUND = 'Refund In Progress';
    public static final String SUB_STAT_AWAITING_INFORMATION = 'Awaiting Information';
    public static final String SUB_STAT_COI_APPROVED = 'COI Approved';
    public static final String SUB_STAT_COI_REJECTED = 'COI Rejected';

    public static final String APPLICATION_STATUS_COI_REQUEST_APPROVED = 'COI Request Approved';
    public static final String APPLICATION_STATUS_NEW_INFORMATION_RECEIVED = 'New Information Received';
    public static final String APPLICATION_STATUS_NEW = 'New';
    public static final String APPLICATION_STATUS_DD_REV = 'DD Review';
    public static final String APPLICATION_STATUS_ASSIGNED = 'Assigned';
    public static final String APPLICATION_STATUS_IN_PROG = 'In Progress';
    public static final String APPLICATION_STATUS_BUILDER_WITHDRAWN = 'WITHDRAWN';
    public static final String APPLICATION_STATUS_BUILDER_CHANGED = 'Builder Change'; 
    public static final String APPLICATION_STATUS_BUILDER_UNCHANGED = 'Builder Unchanged'; 
    public static final String APPLICATION_STATUS_COI_REQUEST_REJECTED = 'COI Request Rejected';
    public static final String APPLICATION_STATUS_FINAL_DECLINATURE = 'Final Declinature';
    public static final String APPLICATION_BUILDER_ELIGIBILITY_STATUS_UNDERREV = 'Under Review';
    public static final String APPLICATION_BUILDER_RESULT_STATUS_DECLINED = 'Declined';
    public static final String APPLICATION_BUILDER_RESULT_STATUS_CANCELLED = 'Cancelled';
    public static final String VMIA_INTERNAL_MANAGER_PROFILE = 'VMIA Internal Manager';
    public static final String VMIA_BAT_MANAGER_PROFILE = 'VMIA BAT MANAGER';
    public static final string CANCELCOI = 'Cancel COI';
    public static final string ADVERSE_ALERT = 'Adverse Alert';
    public static final string AMENDCOI = 'Amend COI';
    public static final string NEWCOI = 'New COI' ;
    public static final string NEWINVOICE = 'New Invoice' ;
    public static final string NEWINVOICESTR = 'Original' ;
    public static final string AMENDINVOICESTR = 'Amendment' ;
    public static final string CANCELINVOICESTR = 'Cancellation' ;
    public static final string APP_REC_TYPE_REVIEW_ELIGIBILITY = 'Review Eligibility' ;

    public static final string BAT_QUEUE = 'BAT Queue';
    public static final string BAT_MANAGER_QUEUE = 'BAT Manager Queue';

    public static final String BUILDER = 'builder';
    public static final String SUM_VAL = 'sumVal';
    public static final String AUSTRALIA = 'Australia';
    public static final String LINE_BREAK = '<br/>';
    public static final String GUEST_TYPE = 'Guest';

    public static final String ACCOUNT_ELIGIBILITY_REVIEW_FEQ_6 = '6 Months';
    public static final String ACCOUNT_ELIGIBILITY_REVIEW_FEQ_12 = '12 Months';
    public static final String ACCOUNT_ELIGIBILITY_REVIEW_FEQ_24 = '24 Months';
    public static final String ACCOUNT_BUILDER_ELIGIBILITY_STATUS_UNDERREV = 'Under Review';
    public static final String ACCOUNT_STATUS_UNDERAPP = 'Under Application';
    public static final String ACCOUNT_STATUS_WITHDRAWN = 'Withdrawn';
    public static final String ACCOUNT_STATUS_ACTIVE = 'Active';
    public static final String ACCOUNT_STATUS_ACTIVE_RES = 'Active - Restricted';

    public static final String ACCOUNT_TYPE_AMENDCOI = 'Amend COI';
    public static final String ACCOUNT_TYPE_CANCELCOI = 'Cancel COI';
    
    public static final String ACCOUNT_REC_TYPE_COMPANY = 'Company';
    public static final String ACCOUNT_REC_TYPE_PARTNERSHIP = 'Partnership';
    public static final String ACCOUNT_REC_TYPE_SOLE_TRADER = 'Sole Trader';
    
    public static final String REC_TYPE_REF_CERT = 'Referred Certificate';
    public static final String REC_TYPE_CONTAINER = 'Container';
    public static final String REC_TYPE_FLATRATE = 'FlatRate';
    public static final String REC_TYPE_HISTORICAL_CERT = 'Historical Certificate';
    public static final String REC_TYPE_CERTIFICATE = 'Certificate';
    public static final String REC_TYPE_APPLICATION_NEW_ELIGIBILITY ='New Eligibility';
    public static final string DRAFT_HISTORICAL = 'Draft - Historical';
    public static final String REC_TYPE_PERSON_ACCOUNT = 'Person Account';
    public static final String REC_TYPE_DISTRIBUTOR = 'Distributor';
    public static final String STAT_REFERRED = 'Referred';
    
    public static final String CAT_NEW_MULTI_DWELLING = 'C03: New Multi-Dwelling Construction';

    public static final String CERT_INVOICE_FLD = 'InvoiceNo__c';
    public static final String FLD_ID = 'Id';

    public static final String TSK_TYPE_REVIEW = 'Review';
    public static final String TSK_TYPE_AWAITING_INFO = 'AwaitingInfo';
    public static final String TSK_TYPE_APPROVED = 'Approved';
    public static final String TSK_STATUS_OVERDUE = 'Overdue';

    public static final String CERT_TYPE_OBCERT = 'OBCertificate';
    public static final String CERT_TYPE_OBCONTAINER = 'OBContainer';

    public static final String COMMENTS_BY = '[ Comments By ';
    public static final String END_COMMENTS_BY = '] : ';
    public static final String ET_AL = 'et al.';

    public static final String ACCOUNTCONTACTROLE_PRIMARY_DIRECTOR = 'Primary RBP';
    public static final String ACCOUNTCONTACTROLE_PENDING_PRIMARY_DIRECTOR = 'Primary RBP - Pending';
    public static final String ACCOUNTCONTACTROLE_RBP = 'RBP';
    public static final String ACCOUNTCONTACTROLE_EMPLOYEE = 'Employee';
    public static final String ACCOUNTCONTACTROLE_RBP_PENDING = 'RBP - Pending';
    
    public static final Map<String,String> CERT_INV_RECTYPE_MAP = new Map<String,String>{'Cancel COI' => 'Cancel Invoice',
                                                                                            'Amend COI' => 'Amend Invoice'};
    

    public static final Map<String,String> CERT_INV_TYPE_MAP = new Map<String,String>{'Cancel COI' => 'Cancellation',
                                                                                        'Amend COI' => 'Amendment'};


    /* UserTriggerHelper constants */
    public static final String FN_CREATE_DISTRIBUTOR_QUEUE = 'createDistributorQueue';
    public static final String FN_UPDATE_CONTACT_RECORD = 'updateContactRecord';

    public static final String USER_TYPE_POWER_CUSTOMER = 'PowerCustomerSuccess';
    public static final String USER_TYPE_CUSTOMER = 'CustomerSuccess';
    public static final String USER_TYPE_HIGH_VOLUME = 'CspLitePortal';
    public static final String CUSTOMER_PORTAL_TYPE = 'CustomerPortal';
    public static final String QUEUE = 'Queue';
    public static final String ROLE_AND_SUBORDINATES = 'RoleAndSubordinates';

    /* EditEligibilityApplication constants */
    public static final String APP_STATUS_WITHDRAWN = 'Withdrawn';
    public static final String FN_WITHDRAWN_APPLICATION = 'withdrawApplication';
    public static final String FN_ROLLBACK_APPLICATION = 'rollBackApplication';

    /* amendCOIController constants */
    public static final String FN_WITHDRAWN_CERTIFICATE = 'withdrawCertificate';

    /*** ApplicationTriggerHelper Constants  ***/
    public static final String FN_COPY_DETAILS_TO_BUILDER = 'copyDetailsToBuilder';
    public static final String FN_WITHDRAWBUILDER = 'withdrawBuilderOrCertificate';
    public static final String FN_ASSIGN_RBP_TO_HIDDEN_FIELD = 'assignRBPtToHiddenField';

    public static final String LOCKED = 'Locked';
    public static final String EMPTY_SPACE = ' ';
    public static final String WHITE_SPACE = '';
    public static final String NULL_VAL = 'null';

    /* AccountContactRelationTriggerHelper constants */
    public static final String FN_SET_ROLE = 'setRole';

    /* ContactTriggerHelper constants */
    public static final String FN_UPDATE_ROLE = 'updateRole';
    
    public static final String OWNER_BUILDER = 'Owner Builder';
    public static final String CERT_TYPE_OWNER_BUILDER = 'OBCertificate';


    /* DistributorAccessController constants */
    public static final String FN_REMOVE_BUILDER_SHARING = 'removeSharingBuildersWithDistributor';
    public static final String FN_SHARE_BUILDER = 'shareBuildersWithDistributor';
    public static final String FN_SHARE_INVOICE = 'shareInvoiceWithDistributor';
    public static final String FN_SHARE_APPLICATION = 'shareApplicationsWithDistributor';
    public static final String FN_SHARE_CERTIFICATE = 'shareCertificatesWithDistributor';

    /* GeneratePendingLOEExtension constants */
    public static final String FN_CONSTRUCTOR = 'constructor';

    public static final String PENDING_LOE_CATEGORY_DETAILS = 'Pending_LOE_Category_Details';
    public static final String FORWARD_SLASH = ' / ';

    /* SearchTaskController constants */
    public static final String NONE_OPTION = '--None--';

    /* AccountTriggerHelper constants */
    public static final String FN_CREATE_ELIGIBILITY_HISTORY = 'createHistoryEligibility';
    public static final String FN_DISABLE_BUILDER_USER = 'disableBuilderUser';

    /* referralAndValidationCheck constants */
    public static final String REF_REASON_CROSS_BORDER = 'Cross Border Address';
    public static final String WITHDRAW_SUCCESS_STATUS = 'Certificate successfully withdrawn.';
    public static final String WITHDRAW_FAIL_STATUS = 'Unable to withdraw certificate. Please reach out to support team for withdrawal.';

    /* TaskTriggerHelper constants */
    public static final String TSK_STATUS_RESPONDED = 'Responded';
    public static final String TSK_STATUS_COMPLETED = 'Completed';
    public static final String FN_REJECT_APPLICATION = 'rejectApplication';
    public static final String TSK_SUBJ_PENDING_DECLINATURE_INFO_REQ = 'Pending Declinature - More Information Required';
}