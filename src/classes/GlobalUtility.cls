/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Utility class for the application
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public without sharing class GlobalUtility{
    
    /**
      * @description       This method returns the recordtypes for sobject
      * @param             objName - sobject name
      * @return            Map<String,Id>
      * @throws            NA
      */
    public static Map<String,Id> getsObjectRecordTypes(String objName){
        Map<String,Id> recTypeMap = new Map<String,Id>();
        if(String.isNotBlank(objName)){
            // fetch recordtype details from Schema method based on the object api name
            for(Schema.RecordTypeInfo result : Schema.getGlobalDescribe().get(objName).getDescribe().getRecordTypeInfos()){
                recTypeMap.put(result.getName(),result.getRecordTypeId());  // map of record type name and Id
            }
        }
        return recTypeMap;
    }
    
    /**
      * @description       This method returns the recordtypes for sobject
      * @param             objName - sobject name
      * @return            Map<Id,String>
      * @throws            NA
      */
    public static Map<Id,String> getsObjectRecordTypeNames(String objName){
        Map<Id,String> recTypeMap = new Map<Id,String>();
        if(String.isNotBlank(objName)){
            // fetch recordtype details from Schema method based on the object api name
            for(Schema.RecordTypeInfo result : Schema.getGlobalDescribe().get(objName).getDescribe().getRecordTypeInfos()){
                recTypeMap.put(result.getRecordTypeId(),result.getName());  // map of record type name and Id
            }
        }
        return recTypeMap;
    }

    /**
      * @description       This method returns the queue
      * @param             queueList - queue names
      * @return            Map<String,Id>
      * @throws            NA
      */
    public static Map<String,Id> getQueues(List<String> queueList){
        Map<String,ID> recTypeMap = new Map<String,ID>();
        if(!queueList.isEmpty()){
            // fetch recordtype details from Schema method based on the object api name
            for(Group result : [Select id, name from Group where name in : queueList AND Type =: GlobalConstants.QUEUE]){
                recTypeMap.put(result.name,result.id);  // map of record type name and Id
            }
        }
        return recTypeMap;
    }
    
    
    /**
      * @description       This method returns the recordtype ids based search string for an object
      * @param             objName - sobject name, searchString - string to check in recordtype name
      * @return            Id
      * @throws            NA
      */
    public static Set<Id> getRecordTypeIds(String objName,String searchString){
        Set<Id> recTypeIds = new Set<Id>();
        // verify if object api name & search string for recordtype is provided
        if(String.isNotBlank(objName) && String.isNotBlank(searchString)){
            // Iterate over the record types of sObject using schema methods
            for(Schema.RecordTypeInfo result : Schema.getGlobalDescribe().get(objName).getDescribe().getRecordTypeInfos()){
                if(result.getName().contains(searchString)){    // check if record type name contains searchString
                    recTypeIds.add(result.getRecordTypeId());   // add to return set
                }
            }
        }
        return recTypeIds;
    }
    
    /**
      * @description       This method checks if record type is valid for functionality
      * @param             recTypeIds - Set<Id>, recTypeId - Id
      * @return            Boolean
      * @throws            NA
      */
    public static Boolean isValidRecordType(Set<Id> recTypeIds,Id recTypeId){
        Boolean isValid = false;
        if(recTypeIds != null && recTypeIds.contains(recTypeId)){   // verify if set contains the recordtype Id
            isValid = true; // set flag to true
        }
        return isValid;
    }

    

    /**
      * @description       This method returns the query to fetch all fields for sobject
      * @param             objName - sobject name
      * @return            String
      * @throws            NA
      */
    public static String buildAllQuery(String objName){
        List<String> fields = new List<String>();
        String query ;
        if(String.isNotBlank(objName)){
            fields.addAll(Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap().keySet());
            
            if(!fields.isEmpty()){
                query = ' SELECT ' + String.join(fields,GlobalConstants.COMMA) + ' FROM ' + objName;
            }
        }
        return query;
    }

    

    /**
      * @description       This method is returns the distributor queue for a logged in portal user.                       
      * @param             NA 
      * @return            Void
      * @throws            Exception is handled by calling method
      */
    public static Id getDistributorQueue(){
        Id queueId;
        Id roleId;
        // check if distributor & set the distributor role id else
        if(isDistributor){
            roleId = loggedInUser.UserRoleId;
        }
        else if(isBuilder){
            for(User distributor : [SELECT UserRoleId FROM User WHERE AccountId =: loggedInUser.Account.Agent__c and isActive = true LIMIT 1]){
                System.debug('distributor.UserRoleId>>>>>'+distributor.UserRoleId);
                roleId = distributor.UserRoleId;
            }
        }
        
        if(roleId != null){ // fetch group id for distributor role
            for(GroupMember grpMember : [SELECT GroupId FROM GroupMember 
                                            WHERE Group.Type =: GlobalConstants.QUEUE
                                            AND UserOrGroupId IN 
                                            (SELECT Id FROM Group WHERE Group.Type =: GlobalConstants.ROLE_AND_SUBORDINATES 
                                                AND RelatedId =: roleId) LIMIT 1]){
                queueId = grpMember.GroupId;
            }
        }
        System.debug('** queueId ==>'+ queueId);
        return queueId;
    }
    
    /**
      * @description       This method is returns the distributor queue for a the builder for whom the
						   common RBP has created the certificate.
      * @param             NA 
      * @return            Void
      * @throws            Exception is handled by calling method
      */
    public static Id getDistributorQueueForRBP(Id BuilderId){
        Id queueId;
        Id roleId;

        if(loggedInUser.Profile.Name == constants.Common_RBP){
            for(User distributor : [SELECT UserRoleId FROM User WHERE AccountId =: BuilderId and isActive = true LIMIT 1]){
                System.debug('distributor.UserRoleId>>>>>'+distributor.UserRoleId);
                roleId = distributor.UserRoleId;
            }
        }
        
        if(roleId != null){ // fetch group id for distributor role
            for(GroupMember grpMember : [SELECT GroupId FROM GroupMember 
                                            WHERE Group.Type =: GlobalConstants.QUEUE
                                            AND UserOrGroupId IN 
                                            (SELECT Id FROM Group WHERE Group.Type =: GlobalConstants.ROLE_AND_SUBORDINATES 
                                                AND RelatedId =: roleId) LIMIT 1]){
                queueId = grpMember.GroupId;
            }
        }
        System.debug('** queueId ==>'+ queueId);
        return queueId;
    }

    public static PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return Page.Login;
        }
        else{
            return null;
        }
    }
    
    
    /**
      * @description       Returns logged in user's record
      * @param             NA
      * @return            User
      * @throws            NA
      */
    private static User currentUser;
    public static User loggedInUser {
      get {
            if (currentUser == NULL) {
                 currentUser = [SELECT Id, Name, FirstName, LastName, ContactId, Contact.Name, Contact.AccountId, Profile.Name, Profile.UserLicense.Name,
                                    UserRoleId, AccountId,Account.Agent__c, Contact.Account.Name, SmallPhotoUrl, FullPhotoUrl,
                                    Contact.RecordType.Name, Contact.Account.id, Account.Eligibility_Status__c, Account.Builder_Eligibility_Status_Reason__c
                                                 FROM User 
                                                 WHERE Id = :UserInfo.getUserId()];
            }
            return currentUser;
        }
    }

    private static Account tmpBuilder;
    public static Account currentBuilder {
      get {
            Account thisBuilder;
            if (tmpBuilder == NULL) {
              Id recordID = ApexPages.currentPage().getParameters().get('id');
              if(recordID != NULL){
                Schema.SObjectType objType = recordID.getSobjectType();
                String objName = String.valueof(objType);
              
                if(!objName.equalsIgnoreCase(constants.OBJ_Account)) {
                  recordID = loggedInUser.Contact.Account.id;
                }
                                 
              }else{
                recordID = loggedInUser.Contact.Account.id;
              }
              tmpBuilder = [SELECT Id, Name, Agent__c,Eligibility_Status__c, Builder_Eligibility_Status_Reason__c
                                                   FROM Account 
                                                   WHERE Id = :recordID];
            }
            return tmpBuilder;
        }
    }  
    
    /**
      * @description       Returns true when logged in user is distributor
      * @param             NA
      * @return            Boolean
      * @throws            NA
      */
    public static Boolean isDistributor {
        get {
            if(loggedInUser.Profile.Name == constants.Distributor) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
      * @description       Returns true when logged in user is distributorId
      * @param             NA
      * @return            Boolean
      * @throws            NA
      */
    public static Id distributorId {
        get {
            if(loggedInUser.Profile.Name == constants.Distributor) {
                return loggedInUser.Id;
            } else {
                return null;
            }
        }
    }
    
    /**
      * @description       Returns true when logged in user is builder
      * @param             NA
      * @return            Boolean
      * @throws            NA
      */
    public static Boolean isBuilder {
        get {
            if(loggedInUser.Profile.Name == constants.Builder || loggedInUser.Profile.Name == constants.Common_RBP) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    /**
      * @description       Returns true when logged in user is common director
      * @param             NA
      * @return            Boolean
      * @throws            NA
      */
    public static Boolean isCommonDirector {
        get {
            if(loggedInUser.Profile.Name == constants.Common_RBP) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    /**
      * @description       Returns true when logged in user is internal salesforce user
      * @param             NA
      * @return            Boolean
      * @throws            NA
      */
    public static Boolean isInternalUser {
        get {
            if(loggedInUser.Profile.UserLicense.Name == constants.SALESFORCE_USER) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    /**
      * @description       Returns the builder ID if any
      * @param             NA
      * @return            Id
      * @throws            NA
      */
    public static Id builderID {
        get {
            Id builderID;
            Id recordID = ApexPages.currentPage().getParameters().get('id');
            if(recordID != NULL) {
                Schema.SObjectType objType = recordID.getSobjectType();
                String objName = String.valueof(objType);
            
                if(objName.equalsIgnoreCase(constants.OBJ_Application)) {
                    Application__c objApp = [SELECT Builder_Number__c FROM Application__c WHERE Id = :recordID];
                    if(objApp != NULL) {
                        builderID = objApp.Builder_Number__c;
                    }
                } else if(objName.equalsIgnoreCase(constants.OBJ_Certificate)) {
                    Certificate__c objCert = [SELECT Builder__c FROM Certificate__c WHERE Id = :recordID];
                    if(objCert != NULL) {
                        builderID = objCert.Builder__c;
                    }
                } else if(objName.equalsIgnoreCase(constants.OBJ_Invoice)) {
                    Invoice__c objInvoice = [SELECT Purchaser__c FROM Invoice__c WHERE Id = :recordID];
                    if(objInvoice != NULL) {
                        builderID = objInvoice.Purchaser__c;
                    }
                } else if(objName.equalsIgnoreCase(constants.OBJ_Account)) {
                    Account objAcc =  new Account(Id=recordID);
                    if(objAcc.RecordType.Name != constants.Distributor) {
                        builderID = recordID;
                    }
                }
            } else if (isBuilder) {
                builderID = loggedInUser.accountId;
            } else {
              // check if it is a common director
            list<user> userList = [select id, contactId, profile.name from user where id =: UserInfo.getUserId()];
            String userProfileName = userList[0].profile.name;
                    
            if (userProfileName == Constants.Common_Director) {
              builderID = ApexPages.currentPage().getParameters().get('cid');
            }
            }
            return builderID;
        }
    }
    
   // /**
   //   * @description       Returns list of account-contact relationships
   //   * @param             NA
   //   * @return            List
   //   * @throws            NA
   //   */
   // public static List<AccountContactRelation> relatedContacts {
   //   get {
   //       List<AccountContactRelation> lstContacts = new List<AccountContactRelation>();
   //       lstContacts = [SELECT Contact.FirstName, Contact.LastName, Contact.Name,Contact.MailingStreet,Contact.MailingState,Contact.MailingCity,Contact.MailingPostalCode, Contact.MailingCountry,Contact.MobilePhone,Contact.Phone,Contact.Email,Contact.RBP_VBA_Number__c,
   //                               Contact.BirthDate,Contact.RBP_License_First_Issued__c,Contact.Drivers_License_Number__c,Contact.Date_Commenced_with_Company__c,Contact.Department, Roles
   //                               FROM AccountContactRelation
   //                               WHERE AccountID = :builderID];
            //return lstContacts;
   //   }
   // }

    /**
      * @description       Returns list of account-contact relationships
      * @param             NA
      * @return            List
      * @throws            NA
      */
    public static List<Contact_Role__c> relatedContactRoles {
      get {
        List<Contact_Role__c> lstContacts = new List<Contact_Role__c>();
        lstContacts = [SELECT Contact__r.FirstName, Contact__r.LastName, Contact__r.Name,Contact__r.MailingStreet,Contact__r.MailingState,Contact__r.MailingCity,Contact__r.MailingPostalCode, Contact__r.MailingCountry,Contact__r.MobilePhone,Contact__r.Phone,Contact__r.Email,Contact__r.RBP_VBA_Number__c,
                                  Contact__r.BirthDate,Contact__r.RBP_License_First_Issued__c,Contact__r.Drivers_License_Number__c,Contact__r.Date_Commenced_with_Company__c,Contact__r.Department, Role__c, Account__r.Primary_Email__c, Account__r.IsPersonAccount,
                                  Account__r.ShippingStreet, Account__r.ShippingCity, Account__r.ShippingState, Account__r.ShippingPostalCode, Account__r.ShippingCountry
                                  FROM Contact_Role__c
                                  WHERE Account__c = :builderID AND (End_Date__c = NULL OR 	End_Date__c>=TODAY) ];
      return lstContacts;
      }
    }

    /**
      * @description       Deletes application record for upload scenario
      * @param             appIds - List of application Ids to be deleted
      * @return            Void
      * @throws            Might throw exception which is handled by invoked method
      */
    public static void deleteApplicaiton(List<Id> appIds){
        Database.delete(appIds);
    }
    
    public static List<Schema.FieldSetMember> getFieldsFromFieldSet(String objectName,String fieldSet){
    	Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        system.debug('====>' + DescribeSObjectResultObj.FieldSets.getMap().get(fieldSet));
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSet);
        List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
        system.debug('fieldSetMemberList ====>' + fieldSetMemberList);
        return fieldSetObj.getFields();
    }
    
    public static Double getUserTimezoneOffSet(){
	    TimeZone tz = UserInfo.getTimeZone();
        //Milliseconds to Day
        return tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
    }   
    public static String getQueryString(List<Schema.FieldSetMember> readFieldSet,String objectName){
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : readFieldSet) {
            query += f.getFieldPath() + ', ';
        }
        query += 'ID FROM '+objectName;
        System.debug('query = '+query);
        return query;
    }
        public static List<Certificate__Share> getListOfCertificateShare(List<Certificate__c> listOfCertificates, String userID,String accessLevel,Set<String> setOfPropertYOwnerIDs){
    	List<Certificate__Share> listOfCertShare = new List<Certificate__Share>();
    	for(Certificate__c curentCertificate:listOfCertificates ){
    		setOfPropertYOwnerIDs.add(curentCertificate.Primary_Property_Owner_Id__c);
    		Certificate__Share certshare = new Certificate__Share();
    		certshare.ParentId =curentCertificate.Id;
    		certshare.UserOrGroupId = userID;
    		certshare.AccessLevel = accessLevel;
    		certshare.RowCause = Schema.Certificate__Share.RowCause.Manual;
    		listOfCertShare.add(certshare);
    	}
    	return listOfCertShare;
    }
    
    public static List<AccountShare> getListOfAccountShare(Set<String> setOfPropertYOwnerIDs,String userID,String accessLevel){
    	Set<String> setOfContactIDs = new Set<String> ();
    	Set<String> setOfAccountIDs = new Set<String> ();
    	List<AccountShare> listOfAccountShare = new List<AccountShare> ();
    	for(Property_Owners__c	currentPropOwner : [SELECT ID,Contact__c,Property_Owner__c FROM Property_Owners__c WHERE ID IN :setOfPropertYOwnerIDs]){
    		if(currentPropOwner!=null){
    			if(String.isNotBlank(currentPropOwner.Contact__c)){
    				setOfContactIDs.add(currentPropOwner.Contact__c);
    			}
    			if(String.isNotBlank(currentPropOwner.Property_Owner__c)){
    				setOfAccountIDs.add(currentPropOwner.Property_Owner__c);
    			}
    		}
    	}
    	for(Contact currentContact: [SELECT Account.ID FROM CONTACT WHERE ID IN :setOfContactIDs]){
    		setOfAccountIDs.add(currentContact.Account.ID);
    	}
    	
    	for(String currentAccountID: setOfAccountIDs){
    		AccountShare accountShare = new AccountShare(AccountID=currentAccountID, AccountAccessLevel = accessLevel,OpportunityAccessLevel = accessLevel, CaseAccessLevel = accessLevel,RowCause = 'Manual',UserOrGroupId = userID);
			listOfAccountShare.add(accountShare);
    	}
    	return listOfAccountShare;
    }
}