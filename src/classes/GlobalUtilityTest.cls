/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class GlobalUtilityTest {
	
    //variables
    private static Map<String, Id> profileMap = new Map<String, Id>();
    private static Map<String, Schema.RecordTypeInfo> accRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    private static Map<String, Schema.RecordTypeInfo> conRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    private static Map<String, Schema.RecordTypeInfo> certRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    private static Map<String, Schema.RecordTypeInfo> appRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    
    static {
        for (Profile prof : [SELECT Name FROM Profile]) {
            profileMap.put(prof.Name, prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup() {
        for (Profile prof : [SELECT Name FROM Profile]) {
            profileMap.put(prof.Name, prof.Id);
        }

        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal', profileMap.get('VMIA BAT Internal'), batMember.Id);
        Database.insert(vmiaInternalUser);

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

    }
    
    @future
    private static void setupData() {
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor', accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);

        Contact rexSmith = TestUtility.createContact('Rex', 'Smith', 'rex.smith@rexHomeDistributorTest.com', rexDistributor.Id,
                           conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders', accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Name = 'Henry Niu';
        bornBuilder.BillingStreet = '140 William St';
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        
        Application__c app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility',
                             appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
        Database.insert(app);
        
        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id, bornBuilder.Agent__c,
                              certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        Database.insert(cert);
        
        BAT_Letter__c batLetter = TestUtility.createBATLetter(bornBuilder.Id, app.Id, cert.Id);
        batLetter.Letter_Description__c = 'Test Description';
        Database.insert(batLetter);       
    }

    @future
    private static void setupUser() {
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith', profileMap.get('Distributor'), rexSmith.Id);
        Database.insert(rexUser);
    }

	@isTest
    static void testGetsObjectRecordTypes() {
    	Map<String, Id> types = GlobalUtility.getsObjectRecordTypes('Account');
    	
    	System.assertEquals(8, types.size());
    }
    
    @isTest
    static void testGetsObjectRecordTypeNames() {
    	Map<Id, String> types = GlobalUtility.getsObjectRecordTypeNames('Account');
    	
    	System.assertEquals(8, types.size());    	            
    }
    
    @isTest
    static void testGetQueues() {
    	Map<String, Id> types = GlobalUtility.getQueues(new List<String>{'Account'});
    	
    	System.assertEquals(0, types.size());    	            
    }
    
    @isTest
    static void testGetRecordTypeIds() {
    	Set<Id> ids = GlobalUtility.getRecordTypeIds('Account', 'Test');
    	
    	System.assertEquals(0, ids.size());    	            
    }
    
    @isTest
    static void testIsValidRecordType() {
    	Set<Id> recTypeIds = new Set<Id>();
    	Id recTypeId = accRecTypeMap.get('Company').getRecordTypeId();
    	
    	Boolean isValidRecordType = GlobalUtility.isValidRecordType(recTypeIds, recTypeId);
    	
    	System.assertEquals(false, isValidRecordType);    	            
    }
    
    @isTest
    static void testBuildAllQuery() {    	
    	String query = GlobalUtility.buildAllQuery('Account');
    	
    	System.assertNotEquals('test', query);    	            
    }
    
    @isTest
    static void testGetDistributorQueue() {    	
    	Id id = GlobalUtility.getDistributorQueue();
    	
    	System.assertEquals(null, id);    	            
    }
    
    @isTest
    static void testForwardToCustomAuthPage() {    	
    	PageReference pr = GlobalUtility.forwardToCustomAuthPage();
    	
    	System.assertEquals(null, pr);    	            
    }
    
    @isTest
    static void testLoggedInUser() {    
        User runningUser = [SELECT Name FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
    	System.runAs(runningUser){
            User user = GlobalUtility.loggedInUser;
        	
        	System.assertEquals(runningUser.Name, user.Name);    	            
        }
    }
    
    @isTest
    static void testCurrentBuilder() {    	
    	Account builder = [SELECT Id FROM Account LIMIT 1];
    	
		PageReference pageRef = Page.GenerateBATLetter; 
        Test.setCurrentPage(pageRef); 
        pageRef.getParameters().put('id', builder.id);
        
    	builder = GlobalUtility.currentBuilder;
    	
    	System.assertEquals('Rex Home Distributor', builder.Name);    	            
    }
    
    @isTest
    static void testIsDistributor() {    	
    	Boolean isDistributor = GlobalUtility.isDistributor;
    	
    	System.assertEquals(false, isDistributor);    	            
    }
    
    @isTest
    static void testDistributorId() {    	
    	Id distributorId = GlobalUtility.distributorId;
    	
    	System.assertEquals(null, distributorId);    	            
    }
    
    @isTest
    static void testIsBuilder() {    	
    	Boolean isBuilder = GlobalUtility.isBuilder;
    	
    	System.assertEquals(false, isBuilder);    	            
    }
    
    @isTest
    static void testIsCommonDirector() {    	
    	Boolean isCommonDirector = GlobalUtility.isCommonDirector;
    	
    	System.assertEquals(false, isCommonDirector);    	            
    }
    
    @isTest
    static void testIsInternalUser() {    	
    	Boolean isInternalUser = GlobalUtility.isInternalUser;
    	
    	System.assertEquals(true, isInternalUser);    	            
    }
    
    @isTest
    static void testbuilderIDForAccount() {    	
    	Account builder = [SELECT Id FROM Account LIMIT 1];
    	
		PageReference pageRef = Page.GenerateBATLetter; 
        Test.setCurrentPage(pageRef); 
        pageRef.getParameters().put('id', builder.id);
        
        Id builderID = GlobalUtility.builderID;
    	
    	System.assertEquals(builder.Id, builderID);    	            
    }
    
    @isTest
    static void testbuilderIDForApplication() {    	
    	Application__c app = [SELECT Id FROM Application__c LIMIT 1];
    	
		PageReference pageRef = Page.GenerateBATLetter; 
        Test.setCurrentPage(pageRef); 
        pageRef.getParameters().put('id', app.id);
        
        Id builderID = GlobalUtility.builderID;
    	
    	System.assertNotEquals(null, builderID);    	            
    }
    
    @isTest
    static void testbuilderIDForCertificate() {    	
    	Certificate__c cert = [SELECT Id FROM Certificate__c LIMIT 1];
    	
		PageReference pageRef = Page.GenerateBATLetter; 
        Test.setCurrentPage(pageRef); 
        pageRef.getParameters().put('id', cert.id);
        
        Id builderID = GlobalUtility.builderID;
    	
    	System.assertNotEquals(null, builderID);    	            
    }
    
    @isTest
    static void testRelatedContacts() {    	
        List<Contact_Role__c> relatedContacts = GlobalUtility.relatedContactRoles;
    	
    	System.assertNotEquals(null, relatedContacts); 
    	System.assertEquals(0, relatedContacts.size());    	            
    }
}