public without sharing class IncludeHeaderController {
	
	// Definitions
	public String userName {get;set;}
	public String userAccount {get;set;}
	
	public IncludeHeaderController() {
		userName = GlobalUtility.loggedInUser.Name;
		userAccount = GlobalUtility.loggedInUser.Contact.Account.Name;
	}
	
	// User logging out
    public PageReference UserLogout(){
        return new PageReference(constants.LOGOUT_URL);
    }
}