/**
* Date         :  19-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for IncludeHeaderController controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class IncludeHeaderController_Test {
    public static testMethod void UserLogoutTest(){
        IncludeHeaderController con = new IncludeHeaderController();
        PageReference pg = con.UserLogout();
        System.assertEquals(constants.LOGOUT_URL, pg.getUrl());
    }

}