public without sharing class IncludeNavBarController {
	
	public String builderID {get;set;}
	public String distributorId{get;set;}
	public Boolean isBuilder {get;set;}
	public Boolean isDistributor {get;set;}
	public Boolean isCommonDirector {get;set;}
    public Boolean disableCOI {get;set;}
    public Boolean disableReview {get;set;}
    public Boolean isInternalUser {get;set;}
    public Boolean isDDLandingPage {get;set;}
    
	public IncludeNavBarController() {
		builderID = NULL;
		isBuilder = GlobalUtility.isBuilder;
		isDistributor = GlobalUtility.isDistributor;
		isCommonDirector = GlobalUtility.isCommonDirector;
		builderID = GlobalUtility.builderID;
		distributorId = GlobalUtility.distributorId;
		isInternalUser = GlobalUtility.isInternalUser;
		String currentPage = ApexPages.currentPage().getURL();
		if(currentPage.containsIgnoreCase('distributorlanding')){
			isDDLandingPage = true;
		}else {
			isDDLandingPage = false;
		}
		if(	GlobalUtility.currentBuilder.Eligibility_Status__c == Constants.account_Eligibility_Status_Under_App || 
		   		GlobalUtility.currentBuilder.Eligibility_Status__c == Constants.account_Eligibility_Status_Cancelled ||
		   		GlobalUtility.currentBuilder.Eligibility_Status__c == Constants.account_Eligibility_Status_Declined  ||
		   		GlobalUtility.currentBuilder.Eligibility_Status__c == Constants.application_Status_Suspended) {
		    disableReview = True;
		} else {
			disableReview = False;
		}

        if(GlobalUtility.currentBuilder.Eligibility_Status__c == Constants.account_Eligibility_Status_Active_Restricted || 
           GlobalUtility.currentBuilder.Eligibility_Status__c == Constants.account_Eligibility_Status_Active ){
            disableCOI = False;
        }else{
            disableCOI = True;
        } 


        if(GlobalUtility.currentBuilder.agent__c == null){
        	disableCOI = true;        	
        	disableReview = true;
    	}


        
	}
}