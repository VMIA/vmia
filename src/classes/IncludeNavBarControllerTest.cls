/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class IncludeNavBarControllerTest {
	
	//variables
    private static Map<String, Schema.RecordTypeInfo> accRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    
    static {
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
    }
    
	@testSetup
    private static void testDataSetup() {
		Account builder = TestUtility.createBusinessAccount('Rex Home Builder', accRecTypeMap.get('Company').getRecordTypeId());        
        Database.insert(builder);
    }
    
    @isTest
    static void testConstructor() {
    	Account builder = [SELECT Id FROM Account LIMIT 1];
    	 	
    	PageReference pageRef = Page.GenerateInvoice;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', builder.Id);
        
        IncludeNavBarController controller = new IncludeNavBarController();
        
        System.assertEquals(builder.Id, controller.builderID);
        System.assertEquals(null, controller.distributorId);
        System.assertEquals(false, controller.isBuilder);
        System.assertEquals(false, controller.isDistributor);
        System.assertEquals(false, controller.isCommonDirector);
        System.assertEquals(true, controller.disableCOI);
        System.assertEquals(true, controller.disableReview);
        System.assertEquals(true, controller.isInternalUser);
    }
}