/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class InitiateReviewEligibilityControllerTest {

	//variables
    private static Map<String, Schema.RecordTypeInfo> accRecTypeMap = new Map<String, Schema.RecordTypeInfo>();
    
    static {
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
    }
    
	@testSetup
    private static void testDataSetup() {
		Account builder = TestUtility.createBusinessAccount('Rex Home Builder', accRecTypeMap.get('Company').getRecordTypeId());        
        Database.insert(builder);
    }
    
    @isTest
    static void testConstructor() {
        Account builder = [SELECT Id FROM Account LIMIT 1];
    	 	
    	PageReference pageRef = Page.GenerateInvoice;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', builder.Id);
        
        initiateReviewEligibilityController controller = new initiateReviewEligibilityController();
        
        System.assertEquals(builder.Id, controller.builderID);
        System.assertEquals(1, controller.newAccount.size());
        System.assertEquals(builder.Id, controller.newAccount[0].Id);
    }
    
    @isTest
    static void testForwardToCustomAuthPage() {
        initiateReviewEligibilityController controller = new initiateReviewEligibilityController();
        PageReference pr = controller.forwardToCustomAuthPage();
        
        System.assertEquals(null, pr);        
    }
    
    @isTest
    static void testConfirmation() {
        initiateReviewEligibilityController controller = new initiateReviewEligibilityController();
        PageReference pr = controller.confirmation();
        
        System.assertEquals(null, pr);         
    }
    
    @isTest
    static void testSave() {
        Account builder = [SELECT Id, Agent__c FROM Account LIMIT 1];
    	 	
    	PageReference pageRef = Page.GenerateInvoice;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', builder.Id);
        
        initiateReviewEligibilityController controller = new initiateReviewEligibilityController();
        PageReference pr = controller.save();  
        
        //Task task = [SELECT Id FROM Task LIMIT 1];
        
        System.assertNotEquals(null, controller.app);
        //System.assertNotEquals(null, task);
    }
    
    @isTest
    static void testGetCountries() {
        initiateReviewEligibilityController controller = new initiateReviewEligibilityController();
        List<SelectOption> options = controller.getCountries();  
        
        System.assertEquals(23, options.size());        
    }
}