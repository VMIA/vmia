/**
  * Date         :  18-Jun-2017
  * Author       :  SMS Management & Technology
  * Description  :  This Class is used for Triggering the Refund to Westpac and TechOne
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/ 
global class InvoiceGroupCheck
{

   webservice static void IsMemeber(){
    Set<String> groupNames = new Set<String>();
       for (GroupMember gm : [select 
                                 group.name,
                                 group.DeveloperName 
                              from GroupMember 
                              where UserOrGroupId = :UserInfo.getUserId()]) {
           groupNames.add(gm.group.DeveloperName);
       }
    
       System.debug('groupNames:' +groupNames );
       System.debug('checking: ' + groupNames.contains(label.RefundApprovalGroup) );   
       
      if (!groupNames.contains(label.RefundApprovalGroup)){  
               
          throw new applicationException('error'); 
      } 
      
       
    }
    
    
    public class applicationException extends Exception {}
    
}// end global class