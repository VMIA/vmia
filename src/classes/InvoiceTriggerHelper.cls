public with sharing class InvoiceTriggerHelper {
    // variables
    private static Boolean beforeUpdateFirstRun = true;
    private static Boolean afterUpdateFirstRun = true;
    private static Map<String,Id> invRecTypeMap ;
    
    // constructor : initialize necessary variables
    public InvoiceTriggerHelper(){
        // fetch record types for certificate
        invRecTypeMap = GlobalUtility.getsObjectRecordTypes(String.valueOf(Invoice__c.sObjectType));
    }
    
    public static Boolean isBeforeUpdateEventFirstRun(){
        Boolean retVal = beforeUpdateFirstRun;
        beforeUpdateFirstRun = false;
        return retVal;
    }
    
    public static Boolean isAfterUpdateEventFirstRun(){
        Boolean retVal = afterUpdateFirstRun;
        afterUpdateFirstRun = false;
        return retVal;
    }

 	public void generateSnapshotFields(Map<Id,Invoice__c> newInvMap, Map<Id,Invoice__c> oldInvMap){
 		Schema.SObjectType objType;
 		String objectName;
 		List<id> invIdList = new List<Id>();
 		Map<id, id> invoiceAccountIdMap = new Map<id, id>();
 		Map<id, Account> invoiceAccountMap = new Map<id, Account>();
        try{ 
        	for(Invoice__c inv : newInvMap.values()){
        		if(inv.WestpacResponse__c != oldInvMap.get(inv.Id).WestpacResponse__c && inv.WestpacResponse__c == GlobalConstants.TSK_TYPE_APPROVED){
        			invIdList.add(inv.id);
        		}
        	}

        	for (Invoice__c inv : [SELECT Purchaser__r.Name,Purchaser__r.BillingStreet,Purchaser__r.BillingCity, Purchaser__r.Primary_Email__c,
                                                Purchaser__r.BillingState,Purchaser__r.BillingPostalCode,Purchaser__r.Website,
                                                Purchaser__r.Phone, Purchaser__r.RecordType.Name, Purchaser__r.ABN__c, Purchaser__r.ACN__c,
                                                Purchaser__r.Agent__r.Name, Purchaser__r.Agent__r.BillingStreet, Purchaser__r.Agent__r.BillingCity,
                                                Purchaser__r.Agent__r.BillingState,Purchaser__r.Agent__r.BillingPostalCode,Purchaser__r.Agent__r.Website,
                                                Purchaser__r.Agent__r.Phone, Purchaser__r.Account_Number__c, Purchaser__r.IsPersonAccount, Purchaser__r.Agent__r.Primary_Email__c,
                                                Purchaser__r.ShippingStreet, Purchaser__r.ShippingCity, Purchaser__r.ShippingState, Purchaser__r.ShippingPostalCode
                                                FROM Invoice__c WHERE Id IN: invIdList]){
        		Invoice__c newInv = newInvMap.get(inv.Id);
        		if(inv.Purchaser__r.RecordType.Name.equalsIgnoreCase('Distributor')){
        			newInv.PIT_Distributor_Name__c = inv.Purchaser__r.Name;
        			newInv.PIT_Distributor_Phone__c = inv.Purchaser__r.Phone;
        			newInv.PIT_Distributor_Website__c = inv.Purchaser__r.Website;
                    newInv.PIT_Distributor_Email__c = inv.Purchaser__r.Primary_Email__c;
        			if(String.isNotBlank(inv.Purchaser__r.BillingStreet)){
                        newInv.PIT_Distributor_Address__c = inv.Purchaser__r.BillingStreet + GlobalConstants.EMPTY_SPACE ;
                    }
                    newInv.PIT_Distributor_Address__c = String.isNotBlank(inv.Purchaser__r.BillingCity) ? 
                                                            (newInv.PIT_Distributor_Address__c + GlobalConstants.LINE_BREAK + inv.Purchaser__r.BillingCity) : newInv.PIT_Distributor_Address__c;

                    newInv.PIT_Distributor_Address__c = String.isNotBlank(inv.Purchaser__r.BillingState) ? 
                                                            (newInv.PIT_Distributor_Address__c + GlobalConstants.EMPTY_SPACE + inv.Purchaser__r.BillingState) : newInv.PIT_Distributor_Address__c;

                    newInv.PIT_Distributor_Address__c = String.isNotBlank(inv.Purchaser__r.BillingPostalCode) ?   
                                                            (newInv.PIT_Distributor_Address__c + GlobalConstants.EMPTY_SPACE + inv.Purchaser__r.BillingPostalCode)  : newInv.PIT_Distributor_Address__c;                                    
                                                            
        		}else{
        			newInv.PIT_Distributor_Name__c = inv.Purchaser__r.Agent__r.Name;
        			newInv.PIT_Distributor_Phone__c = inv.Purchaser__r.Agent__r.Phone;
        			newInv.PIT_Distributor_Website__c = inv.Purchaser__r.Agent__r.Website;
                    newInv.PIT_Distributor_Email__c = inv.Purchaser__r.Agent__r.Primary_Email__c;
        			if(String.isNotBlank(inv.Purchaser__r.Agent__r.BillingStreet)){
                        newInv.PIT_Distributor_Address__c = inv.Purchaser__r.Agent__r.BillingStreet + GlobalConstants.EMPTY_SPACE ;
                    }
                    newInv.PIT_Distributor_Address__c = String.isNotBlank(inv.Purchaser__r.Agent__r.BillingCity) ? 
                                                            (newInv.PIT_Distributor_Address__c + GlobalConstants.LINE_BREAK + inv.Purchaser__r.Agent__r.BillingCity) : newInv.PIT_Distributor_Address__c;

                    newInv.PIT_Distributor_Address__c = String.isNotBlank(inv.Purchaser__r.Agent__r.BillingState) ? 
                                                            (newInv.PIT_Distributor_Address__c + GlobalConstants.EMPTY_SPACE + inv.Purchaser__r.Agent__r.BillingState) : newInv.PIT_Distributor_Address__c;

                    newInv.PIT_Distributor_Address__c = String.isNotBlank(inv.Purchaser__r.Agent__r.BillingPostalCode) ?   
                                                            (newInv.PIT_Distributor_Address__c + GlobalConstants.EMPTY_SPACE + inv.Purchaser__r.Agent__r.BillingPostalCode)  : newInv.PIT_Distributor_Address__c;                                    
                    
                    newInv.PIT_Builder_Number__c = inv.Purchaser__r.Account_Number__c;
                    newInv.PIT_Builder_Name__c = inv.Purchaser__r.Name;
                    newInv.PIT_Builder_ACN__c = inv.Purchaser__r.ACN__c;
					newInv.PIT_Builder_ABN__c = inv.Purchaser__r.ABN__c;
					
					
                    if(String.isBlank(newInv.PIT_Builder_Address__c)){
						if(String.isNotBlank(inv.Purchaser__r.ShippingStreet)){
	                        newInv.PIT_Builder_Address__c = inv.Purchaser__r.ShippingStreet + GlobalConstants.EMPTY_SPACE ;
	                    }
	                    newInv.PIT_Builder_Address__c = String.isNotBlank(inv.Purchaser__r.ShippingCity) ? 
	                                                            (newInv.PIT_Builder_Address__c + GlobalConstants.LINE_BREAK + inv.Purchaser__r.ShippingCity) : newInv.PIT_Builder_Address__c;
	
	                    newInv.PIT_Builder_Address__c = String.isNotBlank(inv.Purchaser__r.ShippingState) ? 
	                                                            (newInv.PIT_Builder_Address__c + GlobalConstants.EMPTY_SPACE + inv.Purchaser__r.ShippingState) : newInv.PIT_Builder_Address__c;
	
	                    newInv.PIT_Builder_Address__c = String.isNotBlank(inv.Purchaser__r.ShippingPostalCode) ?   
	                                                            (newInv.PIT_Builder_Address__c + GlobalConstants.EMPTY_SPACE + inv.Purchaser__r.ShippingPostalCode)  : newInv.PIT_Builder_Address__c;                                    	
                	}
                	
                	if(String.isBlank(newInv.PIT_Builder_Address__c)){ 
						if(String.isNotBlank(inv.Purchaser__r.BillingStreet)){
	                        newInv.PIT_Builder_Address__c = inv.Purchaser__r.BillingStreet + GlobalConstants.EMPTY_SPACE ;
	                    }
	                    newInv.PIT_Builder_Address__c = String.isNotBlank(inv.Purchaser__r.BillingCity) ? 
	                                                            (newInv.PIT_Builder_Address__c + GlobalConstants.LINE_BREAK + inv.Purchaser__r.BillingCity) : newInv.PIT_Builder_Address__c;
	
	                    newInv.PIT_Builder_Address__c = String.isNotBlank(inv.Purchaser__r.BillingState) ? 
	                                                            (newInv.PIT_Builder_Address__c + GlobalConstants.EMPTY_SPACE + inv.Purchaser__r.BillingState) : newInv.PIT_Builder_Address__c;
	
	                    newInv.PIT_Builder_Address__c = String.isNotBlank(inv.Purchaser__r.BillingPostalCode) ?   
	                                                            (newInv.PIT_Builder_Address__c + GlobalConstants.EMPTY_SPACE + inv.Purchaser__r.BillingPostalCode)  : newInv.PIT_Builder_Address__c;                                    
                	}
                	
                	newInv.PIT_Is_Person_Account__c = inv.Purchaser__r.IsPersonAccount;
        		}

        	}

        }
        catch(Exception exp){
            // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(InvoiceTriggerHelper.class),
                                GlobalConstants.FN_CREATE_SNAPSHOT,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }
    
}