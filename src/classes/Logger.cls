/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Logger class to log info,debug,error details to application log
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public with sharing class Logger{
    
    /**
      * @description       This method is used to log info,debug,error details to application log               
      * @param             log level, class name, log message, integration payload, exception & time taken 
      * @return            Void
      * @throws            NA
      */
    public static void logMessage(String logLevel, String sourceClass, String sourceFunction, 
                                    String referenceId, String referenceInfo, String logMessage, 
                                    String payLoad, Exception exp, long timeTaken){
        
        List<Application_Log__c> applicationLogs = new List<Application_Log__c>();
        
        Application_Log__c appLog = new Application_Log__c();
        appLog.Source__c = sourceClass;
        appLog.Source_Function__c = sourceFunction;
        appLog.Reference_Id__c = referenceId;
        appLog.Reference_Information__c = referenceInfo;
        appLog.Message__c = logMessage;
        appLog.Integration_Payload__c = payLoad;
        
        if(exp != null){
            appLog.Stack_Trace__c = exp.getStackTraceString();
            appLog.Message__c = exp.getMessage();
        }
        
        Logger_Settings__c logSetting = Logger_Settings__c.getInstance();
        if((logSetting.LogLevel_Debug__c && String.valueOf(LoggingLevel.DEBUG).equalsIgnoreCase(logLevel))
            || (logSetting.LogLevel_Error__c && String.valueOf(LoggingLevel.ERROR).equalsIgnoreCase(logLevel))
            || (logSetting.LogLevel_Info__c && String.valueOf(LoggingLevel.INFO).equalsIgnoreCase(logLevel))
            || (logSetting.LogLevel_Warning__c && String.valueOf(LoggingLevel.WARN).equalsIgnoreCase(logLevel))){
            applicationLogs.add(appLog);
        }
        
        logMessage(applicationLogs);
    }
    
    /**
      * @description       This method is used to commit info,debug,error details to application log               
      * @param             appLogs - List of application logs 
      * @return            Void
      * @throws            NA
      */
    public static void logMessage(List<Application_Log__c> appLogs){
        if(appLogs != null & !appLogs.isEmpty()){
            Database.insert(appLogs,false);
        }
    }
}