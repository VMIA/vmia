/**
  * Date         :  07-Jun-2017
  * Author       :  SMS Management & Technology
  * Description  :  Handler Class for PropertyOwner object trigger which includes context-specific methods 
  *                  that are automatically called when a trigger is executed.
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public with sharing class PropertyOwnerTriggerHandler{
    
    // Variables
    private static PropertyOwnerTriggerHelper helper;    // Helper class variable to call methods based on scenario
    
    /**
      * @description       This method is invoked from trigger which in-turn calls the handlers
      *                     for before and after event.
      * @param             NA 
      * @return            Void
      * @throws            NA
      */        
    public static void execute(){
        helper = new PropertyOwnerTriggerHelper();   // instance of helper class for Case trigger

        // Check for event type of trigger
        if(Trigger.isBefore){
            //beforeHandler();        // invoke of before handler
        }
        else if(Trigger.isAfter){
            afterHandler();         // invoke of after handler
        }
    }
    
    /**
      * @description       This method is handler for before events invoked by execute method.
      *                     Method invokes the helper class methods based on the scenario.                       
      * @param             NA 
      * @return            Void
      * @throws            NA
      */  
    //private static void beforeHandler(){
        // Check for type of operation
        //if(Trigger.isInsert){
            // Methods to be invoked for before insert event
            
        //}
        //else if(Trigger.isUpdate && PropertyOwnerTriggerHelper.isBeforeUpdateEventFirstRun()){
            // Methods to be invoked for before update event

            
        //}
        //else if(Trigger.isDelete){
            // Methods to be invoked for before update event
        //}
    //}
    
    /**
      * @description       This method is handler for after events invoked by execute method.
      *                     Method invokes the helper class methods based on the scenario.                          
      * @param             NA 
      * @return            Void
      * @throws            NA
      */  
    private static void afterHandler(){
        // Check for type of operation
        if(Trigger.isInsert){
            // Methods to be invoked for after insert event
            PropertyOwnerTriggerHelper.updateCertificatePropertyOwnerTextField(Trigger.New);
          
        }
        else if(Trigger.isUpdate && CertificateTriggerHelper.isAfterUpdateEventFirstRun()){
            // Methods to be invoked for after update event
         PropertyOwnerTriggerHelper.updateCertificatePropertyOwnerTextField(Trigger.New); 
        
        }else if(Trigger.isDelete){
            // Methods to be invoked for after delete event
        }
        else if(Trigger.isUndelete){
            // Methods to be invoked for after undelete event
        }
    }
}