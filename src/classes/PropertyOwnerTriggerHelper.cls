/**
  * Date         :  
  * Author       :  SMS Management & Technology
  * Description  :  Helper Class for Object trigger
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public with sharing class PropertyOwnerTriggerHelper{
    
    // constants
    

    // variables
    private static Boolean beforeUpdateFirstRun = true;
    private static Boolean afterUpdateFirstRun = true;
    
    
    // constructor : initialize necessary variables
    public PropertyOwnerTriggerHelper(){
        
        
    }
    
    public static Boolean isBeforeUpdateEventFirstRun(){
        Boolean retVal = beforeUpdateFirstRun;
        beforeUpdateFirstRun = false;
        return retVal;
    }
    
    public static Boolean isAfterUpdateEventFirstRun(){
        Boolean retVal = afterUpdateFirstRun;
        afterUpdateFirstRun = false;
        return retVal;
    }
    
    /**
      * @description       This method is invoked from handler for before insert event to 
      *                                           
      * @param             NA 
      * @return            Void
      * @throws            Exception is handled by try-catch block
      */
    public static void updateCertificatePropertyOwnerTextField(List<Property_Owners__c> listPO){
    
        Set<Id> poIds = new Set<Id>();
        List<Certificate__c> certToUpdateFullList = new List<Certificate__c>();
        for (Property_Owners__c rec : listPO){
            poIds.add(rec.Certificate__c);
        }
        System.debug('poIds<><>'+poIds);
        List<Certificate__c> listCert = [Select Property_Owner_Text__c , Property_Owner_Address__c, Property_Owner_Email__c,  Primary_Property_Owner_Id__c 
                From Certificate__c Where Id IN :poIds];
        
        Map<Id, Certificate__c> mapCert = new Map<Id, Certificate__c>(listCert);
        System.debug('mapCert<>'+mapCert);
        Set<Certificate__c> listCertToUpdate = new Set<Certificate__c>();
        for (Property_Owners__c rec : listPO){
            Certificate__c selectedCert = mapCert.get(rec.Certificate__c);
            if (selectedCert!=null){
                if(rec.Property_Owner_Formula__c != null)
                selectedCert.Property_Owner_Text__c = rec.Property_Owner_Formula__c;
                if(rec.Property_Owner_Address_Formula__c != null)
                selectedCert.Property_Owner_Address__c= rec.Property_Owner_Address_Formula__c;
                if(rec.Property_Owner_Email__c != null)
                selectedCert.Property_Owner_Email__c = rec.Property_Owner_Email__c;
                if(rec.Property_Owner_Id__c != null)
                selectedCert.Property_Owner_Record_Id__c = rec.Property_Owner_Id__c;
                if(rec.Id != null)
                selectedCert.Primary_Property_Owner_Id__c= rec.Id;
                
                listCertToUpdate.add(selectedCert);
            }
        }
        
        Set<Certificate__c> setCertToUpdate = new Set<Certificate__c>();
        List<Certificate__c> result = new List<Certificate__c>();
        setCertToUpdate.addAll(listCertToUpdate);
        result.addAll(setCertToUpdate);

        System.debug('listCertToUpdate: ' + result);
        if (result.size() > 0)
        
        certToUpdateFullList.addAll(result);
            update certToUpdateFullList;
        
    }//end method
 
}