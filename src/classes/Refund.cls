/**
  * Date         :  28-Dec-2016
  * Author       :  SMS Management & Technology
  * Description  :  This Class is used for Triggering the Refund to Westpac and TechOne
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/ 
global class Refund
{
    public static list<Invoice__c> invList ;
  public static Invoice__c invoiceRecord ;

  /**
      * @description       This method is used for sending the Refund to Westpac System
      * @param             NA 
      * @return            void
      * @throws            NA
      */ 

    webservice static void triggerRefund(id invRecord) 
    { 
         System.debug('invRecord--->'+invRecord);
         invoiceRecord = new Invoice__c();
         invList = new list<Invoice__c>();
         invList = [select id,name,Total__c,GST__c,Stamp_Duty__c,Status__c,Premium__c,Purchaser__c,Purchaser__r.name,Purchaser__r.Agent__r.name,Parent_Invoice__c,Parent_Invoice__r.name,type__c,Parent_Invoice__r.Westpac_Reference_No__c  from Invoice__c where id =: invRecord];

     string merchant = system.label.supplierBusinessCode  ;
        integer amount; 
        if(integer.valueOf(invList[0].Total__c) < 0){
            amount = integer.valueOf((invList[0].Total__c) *-100 );
        }else{
            amount = integer.valueOf((invList[0].Total__c)*100) ;
        }
        string orderNumber = string.valueOf(invList[0].name);
        string originalReferenceNo = string.valueOf(invList[0].Parent_Invoice__r.Westpac_Reference_No__c);
        //integer originalReferenceNo = 1017938961; 

        System.debug('Refundammount-->'+amount);
        System.debug('RefundorderNumber-->'+orderNumber);
        System.debug('Refundammount-->'+originalReferenceNo);

    if( merchant == null || amount == null || orderNumber == null  || originalReferenceNo == null && !invList.isEmpty()){

        //invList[0].Westpac_Reference_No__c  = string.valueOf(XMLParser.referenceNo) ;
    invoiceRecord = invList[0]; 
        invoiceRecord.WestpacResponse__c  =  'Erred';
        invoiceRecord.WestpacResponseMessage__c  = 'Internal Salesforce Issue';
        invoiceRecord.Status__c = 'Refund Failed';

    }else{
        invoiceRecord = invList[0]; 
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:WestpacRefund');
        req.setMethod('POST');

        string s = '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">'+
    '<S:Body>'+
        '<ns2:processRequest xmlns:ns2="urn:com.qvalent.quickgateway">'+
            '<requestParameter>'+
                '<name>order.type</name>'+
                '<value>refund</value>'+
            '</requestParameter>'+
            '<requestParameter>'+
                '<name>customer.username</name>'+
                '<value>'+
                '{!HTMLENCODE($Credential.username)}'+
                '</value>'+
            '</requestParameter>'+
            '<requestParameter>'+
                '<name>customer.password</name>'+
                '<value>'+
                '{!HTMLENCODE($Credential.password)}'+
                '</value>'+
            '</requestParameter>'+
            '<requestParameter>'+
                '<name>customer.merchant</name>'+
                '<value>'+
                merchant+
                '</value>'+
            '</requestParameter>'+
            '<requestParameter>'+
                '<name>order.amount</name>'+
                '<value>'+
                amount+
                '</value>'+
            '</requestParameter>'+
            '<requestParameter>'+
                '<name>customer.orderNumber</name>'+
                '<value>'+
                orderNumber+
                '</value>'+
            '</requestParameter>'+
            '<requestParameter>'+
                '<name>card.currency</name>'+
                '<value>AUD</value>'+
            '</requestParameter>'+            
            '<requestParameter>'+
                '<name>customer.originalReferenceNo</name>'+
                '<value>'+
                originalReferenceNo + 
                '</value>'+
            '</requestParameter>'+
        '</ns2:processRequest>'+
    '</S:Body>'+
'</S:Envelope>' ;

  system.debug('s>>>'+s); 
    
    req.setBody(s);
    if(!Test.isRunningTest()){
        req.setClientCertificateName(Label.WestpacCertificate);
    }
   // req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.setHeader('Content-Type', 'text/xml;charset=UTF-8');
    req.setHeader('Accept-Encoding', 'gzip,deflate');
    req.setHeader('SOAPAction','""');
    
    System.debug('req>>'+req);
     System.debug('reqBody>>'+req.getbody());

     try{
      if(!Test.isRunningTest()){
          Http http = new Http();
          HTTPResponse res = http.send(req);
          
          System.debug('Here is your response:'+res.getBody());

          string responseStr = string.valueOf(res.getBody());
          XMLParser.parseXml(responseStr,'westpacRefund');
       } 
    } catch(System.CalloutException e){
        invoiceRecord.Westpac_Reference_No__c  = string.valueOf(XMLParser.referenceNo) ;
        invoiceRecord.WestpacResponse__c  = 'Erred' ;
        invoiceRecord.WestpacResponseMessage__c  = string.valueOf(e);
        invoiceRecord.Status__c = 'Refund Failed';
        update invoiceRecord ;
    }

    if(null != XMLParser.summaryCodeNo && XMLParser.summaryCodeNo == 0){

        invoiceRecord.Westpac_Reference_No__c  = string.valueOf(XMLParser.referenceNo) ;
        invoiceRecord.WestpacResponse__c  = 'Approved' ;
        invoiceRecord.WestpacResponseMessage__c  = XMLParser.westpacResponseText ;
        invoiceRecord.Status__c = 'Refund Complete';

    }else if (null != XMLParser.summaryCodeNo && XMLParser.summaryCodeNo == 1){
        invoiceRecord.Westpac_Reference_No__c  = string.valueOf(XMLParser.referenceNo) ;
        invoiceRecord.WestpacResponse__c  = 'Declined' ;
        invoiceRecord.WestpacResponseMessage__c  = XMLParser.westpacResponseText ;
        invoiceRecord.Status__c = 'Refund Failed';

    }else if (null != XMLParser.summaryCodeNo && XMLParser.summaryCodeNo == 2){
        invoiceRecord.Westpac_Reference_No__c  = string.valueOf(XMLParser.referenceNo) ;
        invoiceRecord.WestpacResponse__c  = 'Erred' ;
        invoiceRecord.WestpacResponseMessage__c  = XMLParser.westpacResponseText;
        invoiceRecord.Status__c = 'Refund Failed';

    }else if (null != XMLParser.summaryCodeNo && XMLParser.summaryCodeNo == 3){
       invoiceRecord.Westpac_Reference_No__c  = string.valueOf(XMLParser.referenceNo) ;
        invoiceRecord.WestpacResponse__c  = 'Rejected' ;
        invoiceRecord.WestpacResponseMessage__c  = XMLParser.westpacResponseText ;
       invoiceRecord.Status__c = 'Refund Failed';

    }
    }
  //  update invList[0] ;

    if(null != XMLParser.summaryCodeNo && XMLParser.summaryCodeNo == 0){
      // refundToTechOne(invList[0].id) ;
     refundToTechOne(invoiceRecord);
    }

  invoiceRecord.Adjustedby__c = userinfo.getuserid();  
  update invoiceRecord ;

  
  list<Certificate__c> cList = new List<Certificate__c>();
  list<Certificate__c> originalCertificate = new list<Certificate__c>();
  list<Certificate__c> newcertificate = new list<Certificate__c>(); 
  string recordtypeStr ;
  string typeStr;
  string statusStr;


 
    newcertificate = [select Id,recordtypeId,Type__c,Status__c, Original_COI__c from Certificate__c where InvoiceNo__c =: invoiceRecord.id];
    if(newcertificate !=null && newcertificate[0].Original_COI__c!=null){
        originalCertificate = [select Id,recordtypeId,Type__c,Status__c from Certificate__c where Id =: newcertificate[0].Original_COI__c];
    }else{
        originalCertificate = [select Id,recordtypeId,Type__c,Status__c from Certificate__c where InvoiceNo__c =: invoiceRecord.Parent_Invoice__c];
    }
  
  system.debug('originalCertificate<>>'+originalCertificate);
  system.debug('newcertificate<>>'+newcertificate);
  if(null != originalCertificate && !originalCertificate.isEmpty() && null != newcertificate && !newcertificate.isEmpty()){
     
     if(newcertificate[0].type__c == GlobalConstants.AMENDCOI){
        recordtypeStr = originalCertificate[0].recordtypeId ; 
        typeStr = originalCertificate[0].Type__c ;
        statusStr = originalCertificate[0].Status__c ;

        originalCertificate[0].recordtypeId = newcertificate[0].recordtypeId ;
        originalCertificate[0].Type__c = newcertificate[0].Type__c ;
        originalCertificate[0].Status__c = GlobalConstants.STAT_ISSUED_HISTORICAL ;

        newcertificate[0].recordtypeId = recordtypeStr ;
        newcertificate[0].Type__c = typeStr ;
        newcertificate[0].Status__c = statusStr ;
      }else if(newcertificate[0].type__c == GlobalConstants.CANCELCOI){

        originalCertificate[0].recordtypeId = newcertificate[0].recordtypeId ;
        originalCertificate[0].Type__c = newcertificate[0].Type__c ;
        originalCertificate[0].Status__c = GlobalConstants.STAT_ISSUED_HISTORICAL ;

        newcertificate[0].recordtypeId = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName().get(GlobalConstants.REC_TYPE_CERTIFICATE).getRecordTypeId(); 
        newcertificate[0].Type__c = GlobalConstants.NEWCOI ;
        newcertificate[0].Status__c = GlobalConstants.APPLICATION_BUILDER_RESULT_STATUS_CANCELLED ;

      }
      cList.add(originalCertificate[0]);
      cList.add(newcertificate[0]);
      
   }
System.debug('cList<><>'+cList);
   if(null != cList && !cList.isEmpty()){
    update cList ;
   }
  }

//@future(callout=true)
/**
      * @description       This method is used for sending the Refund to TEchOne System
      * @param             NA 
      * @return            void
      * @throws            NA
      */ 
 public static void refundToTechOne(Invoice__c invRecId){

    //list<Invoice__c> invoiceRectList = [select id ,name, Premium__c,GST__c,Total__c,Stamp_Duty__c,Westpac_Reference_No__c,Parent_Invoice__c,Parent_Invoice__r.Westpac_Reference_No__c,Tech1Response__c,Tech1ResponseMessage__c from Invoice__c where id =: invRecId];

    TechOneInfo__c techOneRecord = TechOneInfo__c.getValues(System.Label.TechOne);
    string premiumAmountCode;
    string gstCode;
    string totalAmountCode;
    string stampDutyCode;
      if(null != techOneRecord){
        premiumAmountCode = techOneRecord.PremiumAmount__c ; 
        gstCode = techOneRecord.GSTAmount__c ; 
        stampDutyCode = techOneRecord.StampDuty__c ; 
        totalAmountCode = techOneRecord.TotalAmount__c ; 
      }

    if(null !=invRecId ){
    decimal premiumAmount = (invRecId.Premium__c)*-1;
    decimal gstAmount = (invRecId.GST__c)*-1;
    decimal totalAmount = (invRecId.Total__c);
    decimal stampDuty= (invRecId.Stamp_Duty__c)*-1 ;
    string DocumentRef2Var = invRecId.Parent_Invoice__r.Westpac_Reference_No__c ;
    string DocumentVar = invRecId.name;
    //string DocumentIdVar = 

Datetime todayDateVar = system.today();
String dateOutput = todayDateVar.format('dd/MM/yyyy');

    if(premiumAmount == null || gstAmount== null || stampDuty== null || totalAmount == null || DocumentVar ==null || DocumentRef2Var==null){

       invoiceRecord.Tech1Response__c = 'Not Initiated';    
       invoiceRecord.Tech1ResponseMessage__c = 'Internal Salesforce Error' ;
        invoiceRecord.Status__c  = 'Refund Complete' ;    
  }else {


    HttpRequest req = new HttpRequest();
    req.setEndpoint('callout:Tech1Access');
    req.setMethod('POST');
    req.setTimeout(Integer.valueOf(Label.TechOne_Timeout));

string s = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://TechnologyOneCorp.com/T1.F1.Public/Services">'+
   '<soapenv:Header/>'+
   '<soapenv:Body>'+
      '<ser:DocumentFile_DoInsert>'+
         '<ser:Request PerformCommit="true">'+
            '<ser:Auth UserId="'+
            '{!HTMLENCODE($Credential.username)}'+
            '" Password="'+
            '{!HTMLENCODE($Credential.password)}'+
            '"'+
            ' Config="'+ Label.TechOne_Config_Name+'" FunctionName="$F1.MGT.DOCINSRT.WS"/>'+
            '<ser:DocumentFileItem ImportName="DBI.REF" FormatName="DBI.REF" VatType="I" >'+
               '<ser:Documents DocumentId="'+
               DocumentVar.right(8)+
               '" DocumentRef1="'+
               DocumentVar+
               '" DocumentRef2="'+
               DocumentRef2Var+
               '" DocumentDate1="'+
               dateOutput+
               '"'+
                ' DocumentFileDate="'+
                dateOutput +
               '">'+
                  '<ser:Lines LineSeqNbr="1" LedgerCode="GL" AccountNbr="'+
                  premiumAmountCode+
                  '" BaseAmount="'+
                  premiumAmount+
                  '" VatExcBaseAmount="'+
                  premiumAmount+
                  '" Narration1="refund" VatRateCode="NA" VatType="E" />'+
                  '<ser:Lines LineSeqNbr="2" LedgerCode="GL" AccountNbr="'+
                  gstCode+
                  '" BaseAmount="'+
                  gstAmount+
                  '" VatExcBaseAmount="'+
                  gstAmount+
                  '" Narration1="refund" VatRateCode="NA" VatType="E"/>'+
                  '<ser:Lines LineSeqNbr="3" LedgerCode="GL" AccountNbr="'+
                  totalAmountCode+
                  '" BaseAmount="'+
                  totalAmount+
                  '" VatExcBaseAmount="'+
                  totalAmount+
                  '" Narration1="refund" VatRateCode="NA" VatType="E"/>'+
                  '<ser:Lines LineSeqNbr="4" LedgerCode="GL" AccountNbr="'+
                  stampDutyCode+
                  '" BaseAmount="'+
                  stampDuty+
                  '" VatExcBaseAmount="'+
                  stampDuty+
                  '" Narration1="refund" VatRateCode="G" VatType="I"/>'+
               '</ser:Documents>'+
            '</ser:DocumentFileItem>'+
         '</ser:Request>'+
      '</ser:DocumentFile_DoInsert>'+
   '</soapenv:Body>'+
'</soapenv:Envelope>' ;
    
    system.debug('Call To Tech1>>>'+s);       
    
    req.setBody(s);
    req.setHeader('Content-Type', 'text/xml;charset=UTF-8');
    req.setHeader('Accept-Encoding', 'gzip,deflate');
    
    
    System.debug('req>>'+req);
     System.debug('reqBody>>'+req.getbody());
     try{
      if(!Test.isRunningTest()){
          Http http = new Http();
          HTTPResponse res = http.send(req);    
          System.debug('Tech1 Refund response:'+res.getBody()); 
          string responseStr = string.valueOf(res.getBody());
          XMLParser.parseXml(responseStr,'Tech1Refund');
      }  

     }catch(System.CalloutException e){
            invoiceRecord.Tech1Response__c = 'UnSuccessful';        
            invoiceRecord.Tech1ResponseMessage__c = string.valueOf(e) ;
            invoiceRecord.Status__c  = 'Refund Complete' ;
            update invoiceRecord ;
    }    

        if(XMLParser.isError){        
            invoiceRecord.Tech1Response__c = 'UnSuccessful';
           invoiceRecord.Tech1ResponseMessage__c = XMLParser.errorMsg ;
            invoiceRecord.Status__c  = 'Refund Complete' ;
        }else{
            invoiceRecord.Tech1Response__c = 'successful' ;
            invoiceRecord.Tech1ResponseMessage__c = 'Integration to Tech1 is successful';
            invoiceRecord.Status__c  = 'Received in TechOne' ;
        }
       } 
       
       /*
        if(!invoiceRectList.isEmpty())
            update invoiceRectList ;
        */
    }


 }

}