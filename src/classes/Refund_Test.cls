/**
* Date         :  24-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for SuccessinvoiceDetailController controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class Refund_Test {
	//variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        setupData();
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
        
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);

        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(rexUser); 

        System.RunAs(rexUser){
        
	        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
	        bornBuilder.Agent__c = rexDistributor.Id;
	        bornBuilder.Agent_Contact__c = rexSmith.Id;
	        Database.insert(bornBuilder);
	        System.debug('** bornBuilder ==>'+ bornBuilder);
	        
	        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
	        Database.insert(tonyStark);
	        System.debug('** tonyStark ==>'+ tonyStark);
	        
	        Invoice__c parentInv = TestUtility.createInvoice(bornBuilder.Id,'New',invRecTypeMap.get('New Invoice').getRecordTypeId());
	        parentInv.Premium__c = 10000.00;
	        parentInv.Westpac_Reference_No__c = '12345123';
	        Database.insert(parentInv);

	        Invoice__c childInv = TestUtility.createInvoice(bornBuilder.Id,'New',invRecTypeMap.get('Amend Invoice').getRecordTypeId());
	        childInv.Premium__c = 10000.00;
	        childInv.Parent_Invoice__c = parentInv.Id;
	        Database.insert(childInv);

	        Certificate__c  originalCert = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
	        originalCert.Status__c = GlobalConstants.STAT_REFERRED;
	        originalCert.InvoiceNo__c = parentInv.Id;
	        originalCert.Certificate_Type__c = 'Certificate';
	        originalCert.Type__c = 'New COI';
	        Database.insert(originalCert);
	        System.debug('** Original Certificate ==>'+ originalCert.id);  

	        Certificate__c  childCert = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Historical Certificate').getRecordTypeId());
	        childCert.Status__c = GlobalConstants.STAT_REFERRED;
	        childCert.InvoiceNo__c = childInv.Id;
	        childCert.Certificate_Type__c = 'Certificate';
	        childCert.Parent_COI__c = originalCert.Id;
	        childCert.Type__c = 'Amend COI';
	        Database.insert(childCert);
	        System.debug('** child Certificate ==>'+ childCert.id); 

	        Invoice__c childInvCancel = TestUtility.createInvoice(bornBuilder.Id,'New',invRecTypeMap.get('Cancel Invoice').getRecordTypeId());
	        childInvCancel.Premium__c = 10000.00;
	        childInvCancel.Parent_Invoice__c = parentInv.Id;
	        Database.insert(childInvCancel);

	        Certificate__c  childCertCancel = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Historical Certificate').getRecordTypeId());
	        childCertCancel.Status__c = GlobalConstants.STAT_REFERRED;
	        childCertCancel.InvoiceNo__c = childInvCancel.Id;
	        childCertCancel.Certificate_Type__c = 'Certificate';
	        childCertCancel.Parent_COI__c = originalCert.Id;
	        childCertCancel.Type__c = 'Cancel COI';
	        Database.insert(childCertCancel);
	        System.debug('** Cancel Certificate ==>'+ childCertCancel.id); 
	        
	        Application__c  app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(),originalCert.Id);
	        Database.insert(app);
	        System.debug('** Application ==>'+ app);        
	        
	        Property__c prop = new Property__c(Builder__c = bornBuilder.Id, Suburb__c='Dockland');
	        Database.insert(prop);
	        System.debug('** Property ==>'+ prop.id);
	        
	        Property_Owners__c propOwn = new Property_Owners__c(Property__c = prop.id, Certificate__c = originalCert.id, property_Owner__c=rexDistributor.Id);
	        Database.insert(propOwn);
	        System.debug('** Property Owner ==>'+ propOwn.id);        
        
        }
        
    } 

    public static testMethod void triggerRefundAmendTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Map<String,Schema.RecordTypeInfo> invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
        Invoice__c invoice = [SELECT Id FROM Invoice__c WHERE Purchaser__c =: bornBuilder.Id and recordTypeId =: invRecTypeMap.get('Amend Invoice').getRecordTypeId() LIMIT 1];
        
        System.runAs(rexUser){
			XMLParser.summaryCodeNo = 0;
        	Refund.triggerRefund(invoice.Id);
        }	
	}

	public static testMethod void triggerRefundAmendCode1Test(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Map<String,Schema.RecordTypeInfo> invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
        Invoice__c invoice = [SELECT Id FROM Invoice__c WHERE Purchaser__c =: bornBuilder.Id and recordTypeId =: invRecTypeMap.get('Amend Invoice').getRecordTypeId() LIMIT 1];
        
        System.runAs(rexUser){
			XMLParser.summaryCodeNo = 1;
        	Refund.triggerRefund(invoice.Id);
        }	
	}

	public static testMethod void triggerRefundAmendCode2Test(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Map<String,Schema.RecordTypeInfo> invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
        Invoice__c invoice = [SELECT Id FROM Invoice__c WHERE Purchaser__c =: bornBuilder.Id and recordTypeId =: invRecTypeMap.get('Amend Invoice').getRecordTypeId() LIMIT 1];
        
        System.runAs(rexUser){
			XMLParser.summaryCodeNo = 2;
        	Refund.triggerRefund(invoice.Id);
        }	
	}

	public static testMethod void triggerRefundAmendCode3Test(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Map<String,Schema.RecordTypeInfo> invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
        Invoice__c invoice = [SELECT Id FROM Invoice__c WHERE Purchaser__c =: bornBuilder.Id and recordTypeId =: invRecTypeMap.get('Amend Invoice').getRecordTypeId() LIMIT 1];
        
        System.runAs(rexUser){
			XMLParser.summaryCodeNo = 3;
        	Refund.triggerRefund(invoice.Id);
        }	
	}

	 public static testMethod void triggerRefundCancelTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Map<String,Schema.RecordTypeInfo> invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
        Invoice__c invoice = [SELECT Id FROM Invoice__c WHERE Purchaser__c =: bornBuilder.Id and recordTypeId =: invRecTypeMap.get('Cancel Invoice').getRecordTypeId() LIMIT 1];
        
        System.runAs(rexUser){
        	Refund.triggerRefund(invoice.Id);
        }	
	}
}