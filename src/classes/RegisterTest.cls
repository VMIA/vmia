/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class RegisterTest {

	@isTest
    static void testGetInstance() {
        Register newRegister = Register.getInstance();
        
        System.assertNotEquals(null, newRegister);        
    }
    
    @isTest
    static void testAdd() {
        Register register = new Register();
        register.add(Type.forName('Account'), 'value1');
        
        System.assertNotEquals(null, register.objMap);
        System.assertEquals('value1', register.objMap.get(Type.forName('Account')));        
    }
    
    @isTest
    static void testGet() {
        Register register = new Register();
        register.add(Type.forName('Account'), 'value1');        
        Object value = register.get(Type.forName('Account'));
        
        System.assertEquals('value1', value);        
    }
}