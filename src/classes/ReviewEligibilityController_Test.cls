/**
  * Date         :  29-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for ReviewEligibilityController
  */ 
  
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
private with sharing class ReviewEligibilityController_Test{
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> certRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> appRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> invRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup(){
        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);

        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,
                                                        conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;

        Account soleOwner = TestUtility.createPersonAccount('Burgundy',accRecTypeMap.get('Person Account').getRecordTypeId());
        soleOwner.FirstName = 'Ron';
        Database.insert(new List<Account>{bornBuilder,soleOwner});

        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,
                                                        conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        
        Application__c app = TestUtility.createApplication(bornBuilder.Id,rexDistributor.Id,'New Eligibility',
                                                            appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
        app.External_Comments__c = 'DD/Builder comments';
        Database.insert(app);

        Task appTsk = TestUtility.createTask(app.Id,rexSmith.Id,bornBuilder.Id);
        appTsk.Description = 'App Description';

        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        cert.External_Comments__c = 'DD/Builder comments';
        cert.Certificate_Type__c = 'Certificate';
        Database.insert(cert);

        Task certTsk = TestUtility.createTask(cert.Id,rexSmith.Id,bornBuilder.Id);
        certTsk.Description = 'Certificate Description';
        certTsk.Subject = Label.Task_Subject_DD_Review;
        Database.insert(new List<Task>{appTsk,certTsk});

        Invoice__c inv = TestUtility.createInvoice(bornBuilder.Id,'New',invRecTypeMap.get('New Invoice').getRecordTypeId());
        Database.insert(inv);

        cert.InvoiceNo__c = inv.Id;
        cert.Type__c = 'Amend COI';
        Database.update(cert);

    }
    
    @future
    private static void setupUser(){
        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(new List<User>{vmiaInternalUser,rexUser});
    }
    
    /**
      * @description       This method tests scenario of ReviewEligibility page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testPageLoad(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        bornBuilder.Construction_Category_Limit_1__c = 50000;
        bornBuilder.Construction_Category_Limit_3__c = 50000;
        bornBuilder.Construction_Category_Limit_4__c = 50000;
        bornBuilder.Construction_Category_Limit_5__c = 50000;
        bornBuilder.Construction_Category_Limit_6__c = 50000;
        bornBuilder.Construction_Category_Limit_7__c = 50000;
        bornBuilder.Construction_Limit__c = 100000;
        bornBuilder.CO1_New_Single_Dwelling__c = true;
        bornBuilder.CO3_New_Multi_Dwelling__c = true;
        bornBuilder.C04_Alterations_Additions__c = true;
        bornBuilder.C05_Swimming_Pools__c = true;
        bornBuilder.C06_Renovations_Tradespeople__c = true;
        bornBuilder.C07_Other__c = true;
        bornBuilder.Eligibility_Status__c = Constants.account_Eligibility_Status_Active;
        bornBuilder.Other_Terms__c = 'Other terms';
        Database.update(bornBuilder);
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        
        System.runAs(rexUser){
            
            PageReference pageRef = Page.ReviewEligibility;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',bornBuilder.Id);
            ApexPages.CurrentPage().getParameters().put('appID',app.Id);
            Test.startTest();
                ReviewEligibilityController referralcontroller  = new ReviewEligibilityController();
                List<SelectOption> items = referralcontroller.getItems();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of ReviewEligibility page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testPageLoadNewApplicationSave(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        bornBuilder.Construction_Category_Limit_1__c = 50000;
        bornBuilder.Construction_Category_Limit_3__c = 50000;
        bornBuilder.Construction_Category_Limit_4__c = 50000;
        bornBuilder.Construction_Category_Limit_5__c = 50000;
        bornBuilder.Construction_Category_Limit_6__c = 50000;
        bornBuilder.Construction_Category_Limit_7__c = 50000;
        bornBuilder.Construction_Limit__c = 100000;
        bornBuilder.CO1_New_Single_Dwelling__c = true;
        bornBuilder.CO3_New_Multi_Dwelling__c = true;
        bornBuilder.C04_Alterations_Additions__c = true;
        bornBuilder.C05_Swimming_Pools__c = true;
        bornBuilder.C06_Renovations_Tradespeople__c = true;
        bornBuilder.C07_Other__c = true;
        bornBuilder.Eligibility_Status__c = Constants.account_Eligibility_Status_Active;
        bornBuilder.Other_Terms__c = 'Other terms';
        Database.update(bornBuilder);
        
        System.runAs(rexUser){
            
            PageReference pageRef = Page.ReviewEligibility;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',bornBuilder.Id);
            ReviewEligibilityController referralcontroller  = new ReviewEligibilityController();
            List<SelectOption> items = referralcontroller.getItems();
            Test.startTest();
                referralcontroller.rbpChanges = false;
                referralcontroller.limitChanges = true;
                referralcontroller.categoryChanges = false;
                referralcontroller.otherRequests = 'Other Requests';
                referralcontroller.save();
            Test.stopTest();
            Integer appCount = [SELECT count() FROM Application__c WHERE Type__c =: Constants.reviewEligibility];
            System.assertEquals(1,appCount);
        }
    }
    
    /**
      * @description       This method tests scenario of cancel on ReviewEligibility page
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testCancel(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        
        System.runAs(rexUser){
            
            PageReference pageRef = Page.ReviewEligibility;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',bornBuilder.Id);
            ApexPages.CurrentPage().getParameters().put('appID',app.Id);
            ReviewEligibilityController reviewController  = new ReviewEligibilityController();
            Test.startTest();
                reviewController.cancel();
            Test.stopTest();
        }
    }
    
    /**
      * @description       This method tests scenario of cancel on ReviewEligibility page
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testConfirmation(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        
        System.runAs(rexUser){
            
            PageReference pageRef = Page.ReviewEligibility;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',bornBuilder.Id);
            ApexPages.CurrentPage().getParameters().put('appID',app.Id);
            ReviewEligibilityController reviewController  = new ReviewEligibilityController();
            Test.startTest();
                reviewController.confirmation();
            Test.stopTest();
        }
    }
    
    /**
      * @description       This method tests scenario of forwardToCustomAuthPage on ReviewEligibility page
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testForwardToCustomAuthPage(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        
        System.runAs(rexUser){
            
            PageReference pageRef = Page.ReviewEligibility;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',bornBuilder.Id);
            ApexPages.CurrentPage().getParameters().put('appID',app.Id);
            ReviewEligibilityController reviewController  = new ReviewEligibilityController();
            Test.startTest();
                reviewController.forwardToCustomAuthPage();
            Test.stopTest();
            System.assert(System.currentPageReference().getURL().contains(Page.ReviewEligibility.getURL()));
        }
    }

    /**
      * @description       This method tests scenario of save on ReviewEligibility page
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSave(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        
        System.runAs(rexUser){
            
            PageReference pageRef = Page.ReviewEligibility;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',bornBuilder.Id);
            ApexPages.CurrentPage().getParameters().put('appID',app.Id);
            ReviewEligibilityController reviewController  = new ReviewEligibilityController();
            Test.startTest();
                reviewController.rbpName = 'Other Name';
                reviewController.rbpEmail = 'Other Email';
                reviewController.rbpPhone = '123456';
                reviewController.eligibilityConditions = 'Other Condtitions';
                reviewController.otherRequests = 'Other Requests';
                reviewController.singleDwellingDisabled = false;
                reviewController.rbpChanges = true;
                reviewController.limitChanges = true;
                reviewController.categoryChanges = true;
                reviewController.hasDocuments = true;
                reviewController.save();
            Test.stopTest();
            System.assert(System.currentPageReference().getURL().contains(Page.ReviewEligibility.getURL()));
        }
    }

    /**
      * @description       This method tests scenario of save on ReviewEligibility page
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSaveNoChange(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        
        System.runAs(rexUser){
            
            PageReference pageRef = Page.ReviewEligibility;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',bornBuilder.Id);
            ApexPages.CurrentPage().getParameters().put('appID',app.Id);
            ReviewEligibilityController reviewController  = new ReviewEligibilityController();
            Test.startTest();
                reviewController.singleDwellingDisabled = false;
                reviewController.rbpChanges = false;
                reviewController.limitChanges = false;
                reviewController.categoryChanges = false;
                reviewController.hasDocuments = false;
                reviewController.save();
            Test.stopTest();
            System.assert(ApexPages.hasMessages());
            System.assertEquals('Please select at least one change you would like to make to the current eligibility',ApexPages.getMessages().get(0).getSummary());
        }
    }

    /**
      * @description       This method tests scenario of save on ReviewEligibility page
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSaveNoOtherRequest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        
        System.runAs(rexUser){
            
            PageReference pageRef = Page.ReviewEligibility;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',bornBuilder.Id);
            ApexPages.CurrentPage().getParameters().put('appID',app.Id);
            ReviewEligibilityController reviewController  = new ReviewEligibilityController();
            Test.startTest();
                reviewController.singleDwellingDisabled = false;
                reviewController.rbpChanges = true;
                reviewController.limitChanges = false;
                reviewController.categoryChanges = false;
                reviewController.hasDocuments = false;
                reviewController.save();
            Test.stopTest();
            System.assert(ApexPages.hasMessages());
            System.assertEquals('Please complete a detailed description of the request',ApexPages.getMessages().get(0).getSummary());
        }
    }

    /**
      * @description       This method tests scenario of save on ReviewEligibility page
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testPageLoadSoleTrader(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account soleOwner = [SELECT Id FROM Account WHERE IsPersonAccount = true LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        
        System.runAs(rexUser){
            
            PageReference pageRef = Page.ReviewEligibility;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id',soleOwner.Id);
            ApexPages.CurrentPage().getParameters().put('appID',app.Id);
            Test.startTest();
                ReviewEligibilityController reviewController  = new ReviewEligibilityController();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of forwardToCustomAuthPage on ReviewEligibility page
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testForwardToCustomAuthPageGuestUser(){
        User guestUser = [SELECT Id,Name FROM User WHERE UserType = 'Guest' LIMIT 1];
        
        System.runAs(guestUser){
            
            PageReference pageRef = Page.ReviewEligibility;
            Test.setCurrentPage(pageRef);
            ReviewEligibilityController reviewController  = new ReviewEligibilityController();
            Test.startTest();
                reviewController.forwardToCustomAuthPage();
            Test.stopTest();
        }
    }
}