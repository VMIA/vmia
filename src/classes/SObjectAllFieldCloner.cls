/**
  * Date         :  18-March-2017
  * Author       :  SMS Management & Technology
  * Description  :  Controller for COI Application Form
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Commentsbuilderid

    
*******************************  History ************************************************/ 

public class SObjectAllFieldCloner {

  // Clone a list of objects to a particular object type
  // Parameters 
  // - List<sObject> sObjects - the list of objects to be cloned 
  // - Schema.SobjectType objectType - the type of object to be cloned.
  // The sObjects you pass in must include the ID field, 
  // and the object must exist already in the database, 
  // otherwise the method will not work.
  public static List<sObject> cloneObjects(List<sObject> sObjects,
                                        Schema.SObjectType objectType,Map<id,string> applicationRequestType){
    
    // A list of IDs representing the objects to clone
    List<Id> sObjectIds = new List<Id>{};
    // A list of fields for the sObject being cloned
    List<String> sObjectFields = new List<String>{};
    // A list of new cloned sObjects
    List<sObject> clonedSObjects = new List<sObject>{};
    
    // Get all the fields from the selected object type using 
    // the get describe method on the object type.
    if(objectType != null){
      sObjectFields.addAll(
        objectType.getDescribe().fields.getMap().keySet());
    }
    
    // If there are no objects sent into the method, 
    // then return an empty list
    if (sObjects != null && 
        !sObjects.isEmpty() && 
        !sObjectFields.isEmpty()){
    
        // Strip down the objects to just a list of Ids.
        for (sObject objectInstance: sObjects){
            sObjectIds.add(objectInstance.Id);
        }

        /* Using the list of sObject IDs and the object type, 
         we can construct a string based SOQL query 
         to retrieve the field values of all the objects.*/

        String allSObjectFieldsQuery = 'SELECT ' + sObjectFields.get(0); 

        for (Integer i=1 ; i < sObjectFields.size() ; i++){
            allSObjectFieldsQuery += ', ' + sObjectFields.get(i);
        }

        allSObjectFieldsQuery += ' FROM ' + 
                               objectType.getDescribe().getName() + 
                               ' WHERE ID IN (\'' + sObjectIds.get(0) + 
                               '\'';

        for (Integer i=1 ; i < sObjectIds.size() ; i++){
            allSObjectFieldsQuery += ', \'' + sObjectIds.get(i) + '\'';
        }

        allSObjectFieldsQuery += ')';
    
        try{

        // Execute the query. For every result returned, 
        // use the clone method on the generic sObject 
        // and add to the collection of cloned objects
            for (SObject sObjectFromDatabase:
                Database.query(allSObjectFieldsQuery)){
                if(string.valueOf(objectType) == 'Certificate__C'){
                    Certificate__C originalCert = (Certificate__C) sObjectFromDatabase;           
                    Certificate__C clonedRecord = (Certificate__C) sObjectFromDatabase.clone(false,true);
                    clonedRecord.Original_COI__c = sObjectFromDatabase.Id;
                    clonedRecord.recordTypeId = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName().get(GlobalConstants.REC_TYPE_HISTORICAL_CERT).getRecordTypeId();
                    clonedRecord.Status__c = GlobalConstants.DRAFT_HISTORICAL;
                    if(applicationRequestType.containsKey(originalCert.id)){
                        //if(applicationRequestType.get(originalCert.id) == GlobalConstants.CANCELCOI){
                        //    clonedRecord.Status__c = GlobalConstants.STAT_ISSUED_HISTORICAL;
                        //}
                        clonedRecord.type__c = applicationRequestType.get(originalCert.id); 
                    }
                    clonedRecord.Policy_Number__c = originalCert.Policy_Number__c ;
                    clonedRecord.Parent_Certificate_Premium__c = originalCert.Premium__c ;  
                    clonedRecord.Date_Issued__c = null;                  
                    clonedSObjects.add(clonedRecord);
                }else{
                    clonedSObjects.add(sObjectFromDatabase.clone(false,true));  
                }

              
            }

        } catch (exception e){
        // Write exception capture method 
        // relevant to your organisation. 
        // Debug message, Apex page message or 
        // generated email are all recommended options.
        }
    }    
    
    // return the cloned sObject collection.
    return clonedSObjects;
  }
}