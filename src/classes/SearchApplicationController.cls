/*
  Date      :  31-Jan-2017
  Author      :   SMS Management & Technology
  Description    :  List of applications with a paginated view
*/
public with sharing class SearchApplicationController {
  
  // Definitions
  public String applicationName {get;set;}
  public String applicationType {get;set;}
  public Application__c objApp {get;set;}
  public Decimal totalPages {get;set;}
  public String selectedStatus {get;set;}
  Public list<Application__c> ApplicationList;
  
  @TestVisible Private String sortDirection = 'ASC';
  @TestVisible private String sortExp = 'Name';
  public String appQuery;
  private String appStatus;
  private Date appDate;
  private Date toDateRange;
  List<Application__c> lstApplication {get;set;}
  @TestVisible private Set<String> statusSet;
  @TestVisible private Set<String> setBuilders;
  @TestVisible private String accId;
  private String currentSOQL;
  public List<Application__c> completeApplicationList {get;set;}
  public static final Integer MAX_RECORDS_PER_PAGE = 10;
  Public String query {get;set;}
  public Boolean disabledNextLast {get; set;}
  public Boolean disabledPrevFirst {get; set;}
  
  Public String dateapp {get;set;}
  Public String ToDateR {get;set;}
  
    
  // Constructor
  public SearchApplicationController(ApexPages.StandardController stdController) {
    objApp = new Application__c();
    statusSet = new Set<String>();
    setBuilders = new Set<String>();
    ApplicationList = new List<Application__c>();
    ApplicationSSC = new ApexPages.StandardSetController(Database.query(qry()));
    completeApplicationList = Database.query(qry());
  }

  public string qry(){
        query = 'SELECT Id, Name, RecordType.Name, Application_Status__c, Certificate__c, Final_Assessment_Summary__c, Builder_Result_Status__c, Final_Declinature_Date__c, Requested_Costruction_Limit__c, Send_PENDING_LOE__c, Pending_Declinature__c,  Other_Reason__c, Project_Contract_Change__c, Property_Address_Change__c, Owner_Details_Change__c, Contract_Terminated__c, Construction_Did_Not_Proceed__c, Hidden_Builder_current_Legal_Name__c, Hidden_Builder_Name__c, Hidden_Builder_ABN__c, Hidden_Builder_ACN__c, Date_of_Builders_Application__c, Builder_Number__c, Builder_Number__r.Name, Application_Source__c, Builder_Legal_Entity_Name__c, Builder_First_Name__c, Builders_Last_Name__c, Reason_For_Cancellation__c, Type__c, External_Comments__c, createdDate, CreatedByID, Builder_Account_Number__c, Legal_Entity_Name__c FROM Application__c';
        userType();
        setFilterCriteria();
        if(sortDirection == 'ASC') {
                    query = query + ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS FIRST';
                } else {
                    query = query + ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS LAST';
                }
        return query;
    }
  
    //sorting - Switching between acsending and descending modes
    public String sortExpression {
        get{
            return sortExp;
        }
        set{
            if (value == sortExp){
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            }
            else{
                sortDirection = 'ASC';
                sortExp = value;
            }
        }
    }
  
    // Set filter criteria on the query 
    public void setFilterCriteria() {
        if (selectedStatus != NULL && selectedStatus != ''){
              string status = selectedStatus;
              status = status.replace('[','');
              status = status.replace(']','');
              string[] statusVals = status.split(',');
              
              for(String s : statusVals){
                  statusSet.add(s.trim());
              }
         }
            
        
        
                
        // Filter criteria - Application Name / Number
        if(applicationName != NULL && applicationName != '') {
          String updatedQuery = updateQuery(query);
          query = updatedQuery + ' Name LIKE \'%'+String.escapeSingleQuotes(applicationName)+'%\'';
        }
            
        // Filter criteria - Application Type
        applicationType = objApp.Type__c;
        if(applicationType != NULL && applicationType != '') {
          String updatedQuery = updateQuery(query);
          //appQuery = updatedQuery + ' Type__c LIKE \'%'+String.escapeSingleQuotes(applicationType)+'%\'';
          query = updatedQuery + ' Type__c =:  applicationType';
        }
            
        // Filter criteria - Application Status    
        if(statusSet.size() > 0) {
          String validStatus = NULL; 
          if(statusSet.size() == 1) {
            for(string s : statusSet) {
              validStatus = s;
            }
          }
          if(validStatus != NULL && validStatus != '' || statusSet.size() > 1) {
            
            System.debug('the test : ' + statusSet);
            String updatedQuery = updateQuery(query);
            query = updatedQuery + ' Application_Status__c IN :statusSet';  
          }
        }
            
        //Filter criteria - Date of Builder Application
        if(objApp.Date_of_Builders_Application__c != NULL && objApp.Date_Started_Trading__c == NULL) {
          String updatedQuery = updateQuery(query);
          appDate = date.newinstance(objApp.Date_of_Builders_Application__c.year(), objApp.Date_of_Builders_Application__c.month(), objApp.Date_of_Builders_Application__c.day());
          dateapp = String.valueof(appDate);
          
          query = updatedQuery + ' Date_of_Builders_Application__c = '+dateapp;
          
          
          
        } else if (objApp.Date_of_Builders_Application__c != NULL && objApp.Date_Started_Trading__c != NULL){
          String updatedQuery = updateQuery(query);
          appDate = date.newinstance(objApp.Date_of_Builders_Application__c.year(), objApp.Date_of_Builders_Application__c.month(), objApp.Date_of_Builders_Application__c.day());
          dateapp = String.valueof(appDate);
          toDateRange = date.newinstance(objApp.Date_Started_Trading__c.year(), objApp.Date_Started_Trading__c.month(), objApp.Date_Started_Trading__c.day());
          toDateR = String.valueof(toDateRange);
          
          
          query = updatedQuery + ' Date_of_Builders_Application__c >= ' + dateapp+ ' AND Date_of_Builders_Application__c <= ' + toDateR;
        }
      }
  
  // Retrieve application based on loggedin user
  public void userType() {
    String userType = GlobalUtility.loggedInUser.Contact.RecordType.Name;
    accId = GlobalUtility.loggedInUser.Contact.AccountId;
      
    if(userType == 'Distributor') {
      // All of distributers builders
      for(Account a : [SELECT Id, Agent__c FROM Account WHERE Agent__c = :accId]) {
        setBuilders.add(a.Id);
      }
      query = query + ' WHERE Builder_Number__c IN :setBuilders';
    } else {
      // Only logged in builder
      query= query + ' WHERE Builder_Number__c = :accId';
    }
  }
  
 // Updating the query for additional filter criteria
    public String updateQuery(String query) {
        if(query.contains('WHERE')) {
            query = query + ' AND';
        } else {
            query = query + ' WHERE';
        }   
        return query;
    }
  
    public ApexPages.StandardSetController ApplicationSSC {
        get {
            if(ApplicationSSC == null) {
                ApplicationSSC = new ApexPages.StandardSetController(ApplicationList);
            }
            Decimal results = ApplicationSSC.getResultSize();
            ApplicationSSC.setPageSize(MAX_RECORDS_PER_PAGE);
            totalPages = (results/MAX_RECORDS_PER_PAGE).round(System.RoundingMode.UP);

            return ApplicationSSC;
        }
        set;
    }
    
    //Search Function
    Public pageReference searchvalues(){
         completeApplicationList = Database.query(qry());
         ApplicationList = Database.query(qry());
        try{
            ApplicationSSC = new ApexPages.StandardSetController(Database.query(qry()));
            ApplicationSSC.setPageSize(MAX_RECORDS_PER_PAGE);
            
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;
        
    }
    
    
    
    //UI Fields
    // Get all application status
    public List<SelectOption> getAppStatus() {
        List<SelectOption> AppStatus = new List<SelectOption>();
        DescribeFieldResult d = Application__c.Application_Status__c.getDescribe();
        AppStatus.add(new SelectOption('None','--None--'));
        
        for (PicklistEntry e : d.getPicklistValues()) {
            if (e.isActive()) {
                AppStatus.add(new SelectOption(e.getValue(), e.getLabel()));
            }   
        }             
        return AppStatus;
    } 
    
    //Pagination
    // Returns the page number of the current page set
    public Integer pageNumber {
        get {
            return ApplicationSSC.getPageNumber();
        }
        set;
    }
    
    public List<Application__c> getApplicationList() {
        if(ApplicationSSC.getHasNext()) {
            disabledNextLast = true;
        } else {
            disabledNextLast = false;
        }
        
        if(ApplicationSSC.getHasPrevious()) {
            disabledPrevFirst = true;
        } else {
            disabledPrevFirst = false;
        }
        
        return ApplicationSSC.getRecords();
    }
    
    // Search values based on the filter
    public void sortValues() {
        searchvalues();
    }
    
    // Field that needs to be sorted
    public String getSortDirection() {
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    
     // Indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return ApplicationSSC.getHasNext();
        }
        set;
    }
    
    // Indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return ApplicationSSC.getHasPrevious();
        }
        set;
    }
    
    public void showFirstRec() {
        ApplicationSSC.first();
    }
    
    public void showPreviousRec() {
        ApplicationSSC.previous();
    }

    public void showNextRec() {
        ApplicationSSC.next();
    }
    
    public void showLastRec() {
        ApplicationSSC.last();
    }
    
    
  

  
  
 
  
  
  
  // Clear filter values on the form
  public void clearValues() {
    applicationName = NULL;
    applicationType = NULL;
    objApp.Date_of_Builders_Application__c = NULL;
    objApp.Date_Started_Trading__c = NULL;
    selectedStatus = NULL;
    statusSet.clear();
    objApp = new Application__c();
    searchvalues();
  }
  
   
    
    public PageReference export() {
        if(objApp.Type__c == '' || objApp.Type__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,'Please Conduct a Search with a specific Type before Exporting'));
            Return null;
        }
        else{
        
        system.debug('type' + objApp.Type__c);
        //PageReference pageRef = Page.ApplicationExport;
        PageReference pageRef = new PageReference(Page.ApplicationExport.getUrl());
        pageRef.getParameters().put('fullExport','FALSE');
        pageRef.getParameters().put('apptype', objApp.Type__c );
        pageRef.setRedirect(false);
        return pageRef;
        }
    }

    public PageReference exportAll() {
        if(objApp.Type__c == '' || objApp.Type__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,'Please Conduct a Search with a specific Type before Exporting'));
            return null;  
        }
        else
        {
        PageReference pageRef = new PageReference(Page.ApplicationExport.getUrl());
        pageRef.getParameters().put('fullExport','TRUE');
        pageRef.getParameters().put('apptype', objApp.Type__c );
        pageRef.setRedirect(false);
        return pageRef;
        }
    }
}