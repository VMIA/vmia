/**
  * Date         :  29-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for SearchApplicationController
  */ 
  
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
global with sharing class SearchApplicationController_Test{
    
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static {
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
    }
    
    @testSetup
	static void init() {		
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor', accRecTypeMap.get('Distributor').getRecordTypeId());
        Account lexDistributor = TestUtility.createBusinessAccount('Lex Distributors', accRecTypeMap.get('Distributor').getRecordTypeId());
        Account texDistributor = TestUtility.createBusinessAccount('Tex Distributors', accRecTypeMap.get('Distributor').getRecordTypeId());

		List<Account> accList = new List<Account>();
        accList.add(rexDistributor);
        accList.add(lexDistributor);
        accList.add(texDistributor);
        Database.insert(accList);
	}
    
    static void testConstructor() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        
        System.assertNotEquals(null, controller.objApp);
        System.assertNotEquals(null, controller.statusSet);
        System.assertNotEquals(null, controller.setBuilders);
        System.assertNotEquals(null, controller.ApplicationList);
        System.assertNotEquals(null, controller.completeApplicationList);
    }
    
    @isTest
    static void testGetStandardSetController() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        ApexPages.StandardSetController ApplicationSSC = controller.ApplicationSSC;
        
        System.assertNotEquals(null, ApplicationSSC);
        System.assertEquals(0, ApplicationSSC.getResultSize());
        System.assertEquals(10, ApplicationSSC.getPageSize());
        System.assertEquals('Name', controller.sortExpression);
        //System.assertEquals(EXPECTED_APP_QUERY, controller.query);
        System.assertEquals(0, controller.totalPages);        
    }
    
    @isTest
    static void testUserType() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        controller.userType();
        
        System.assertEquals(null, controller.accId);
        //System.assertEquals('null WHERE Id = :accId', controller.query);
    }
    
    @isTest
    static void testGetApplicationList() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        List<Application__c> applicationlist = controller.getApplicationList();
        
        System.assertNotEquals(null, applicationlist);
        System.assertEquals(0, applicationlist.size());
    }
    
    @isTest
    static void testgetAppStatus() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        List<SelectOption> options = controller.getAppStatus();
        
        System.assertNotEquals(null, options);
        //System.assertEquals(8, options.size());
    }
    
    @isTest
    static void testSortExpression() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        
        System.assertEquals('Name', controller.sortExpression);
        System.assertEquals('ASC', controller.sortDirection);
        
        controller.sortExpression = 'DESC';
        System.assertEquals('ASC', controller.sortDirection);
        
        controller.sortExpression = NULL;
        System.assertEquals('ASC', controller.sortDirection);
    }
    
    @isTest
    static void testHasNext() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        
        System.assertEquals(false, controller.hasNext);
    }
    
    @isTest
    static void testHasPrevious() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        
        System.assertEquals(false, controller.hasPrevious);
    }
    
    @isTest
    static void testPageNumber() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        
        System.assertEquals(1, controller.pageNumber);
    }
    
    @isTest
    static void testshowFirstRec() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        controller.showFirstRec();
        
        System.assertEquals(false, controller.ApplicationSSC.getHasPrevious());
        System.assertEquals(false, controller.ApplicationSSC.getHasNext());
        System.assertEquals(1, controller.ApplicationSSC.getPageNumber());
        
        //Account accout = (Account)controller.con.getRecord();
        //System.assertEquals('Rex Home Distributor', account.Name);
        //System.assertEquals(accRecTypeMap.get('Distributor').getRecordTypeId(), account.RecordTypeId);
    }
    
    @isTest
    static void testshowLastRec() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        controller.showLastRec();
        
        System.assertEquals(false, controller.ApplicationSSC.getHasPrevious());
        System.assertEquals(false, controller.ApplicationSSC.getHasNext());
        System.assertEquals(1, controller.ApplicationSSC.getPageNumber());
    }
    
    @isTest
    static void testshowPreviousRec() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        controller.showPreviousRec();
        
		System.assertEquals(false, controller.ApplicationSSC.getHasPrevious());
        System.assertEquals(false, controller.ApplicationSSC.getHasNext());
        System.assertEquals(1, controller.ApplicationSSC.getPageNumber());
    }
    
    @isTest
    static void testshowNextRec() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        controller.showNextRec();
        
        System.assertEquals(false, controller.ApplicationSSC.getHasPrevious());
        System.assertEquals(false, controller.ApplicationSSC.getHasNext());
        System.assertEquals(1, controller.ApplicationSSC.getPageNumber());
    }
    
    @isTest
    static void testCancel() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        controller.clearvalues();
        
        System.assertEquals(false, controller.ApplicationSSC.getHasPrevious());
        System.assertEquals(false, controller.ApplicationSSC.getHasNext());
        System.assertEquals(1, controller.ApplicationSSC.getPageNumber());
    }
    
    @isTest
    static void testUpdateQuery() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        String query = controller.updateQuery('SELECT ID FROM ACCOUNT');
        
        System.assertEquals('SELECT ID FROM ACCOUNT WHERE', query);
    }
    
    @isTest
    static void testGetSortDirection() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        
        System.assertEquals('ASC', controller.getSortDirection());
    }
    
    @isTest
    static void testSearchValues1() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        controller.searchValues();
        controller.sortvalues();
        
        System.assertNotEquals(null, controller.ApplicationSSC);
        System.assertEquals(0, controller.statusSet.size());
        
    }
    
    @isTest
    static void testSearchValues2() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        controller.applicationName = 'A-0001';
        controller.objApp.Type__c = 'Amend COI';
        controller.objApp.Date_of_Builders_Application__c = date.today();
        controller.selectedStatus = 'New';
        controller.objApp.Date_Started_Trading__c = date.today();
        
        controller.searchValues();
        
        
        System.assertNotEquals(null, controller.ApplicationSSC);
        
        
    }
    
    @isTest
    static void testSearchValues3() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        controller.applicationName = 'A-0001';
        controller.objApp.Type__c = 'Amend COI';
        controller.objApp.Date_of_Builders_Application__c = date.today();
        controller.selectedStatus = 'New';
        controller.objApp.Date_Started_Trading__c = NULL;
        
        controller.searchValues();
        System.assertNotEquals(null, controller.ApplicationSSC);
        
        
    }
    
    @isTest
    static void testExport() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        controller.objApp.Type__c = 'Amend COI';
        
        PageReference pageReference = controller.export();
        
        
        System.assertEquals('FALSE', pageReference.getParameters().get('fullExport'));
        System.assertEquals(False, pageReference.getRedirect());
    }
    
    @isTest
    static void testExportnoObjectType() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        PageReference pageReference = controller.export();
    }
    
    @isTest
    static void testExportAll() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        controller.objApp.Type__c = 'Amend COI';
        PageReference pageReference = controller.exportAll();
        
        
        System.assertEquals('TRUE', pageReference.getParameters().get('fullExport'));
        System.assertEquals(False, pageReference.getRedirect());
    }
    
    @isTest
    static void testExportAllnoobjectType() {
        ApexPages.StandardController stdController = null;
        SearchApplicationController controller = new SearchApplicationController(stdController);
        PageReference pageReference = controller.exportAll();
    }
    
    
}