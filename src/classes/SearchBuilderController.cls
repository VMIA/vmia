/*
    Date            :   07-Mar-2017
    Author          :   SMS Management & Technology
    Description     :   List of builders with a paginated view
*/
public with sharing class SearchBuilderController {
    
    // Definitions
    
    public String accName {get;set;}
    public Decimal accLimit {get;set;}
    public Account objAcc {get;set;}
    public String selectedStatus {get;set;}
    public String selectedType {get;set;}
    public Decimal totalPages {get;set;}
    @TestVisible private String sortDirection = 'ASC';
    @TestVisible private String sortExp = 'Name';
    Public List<Account> BuilderList;
    Public List<Account> completeBuilderList {get;set;}
    @TestVisible private Set<String> statusSet;
    @TestVisible private Set<String> typeSet;
    @TestVisible private Set<String> setBuilders;
    @TestVisible private String accId;
    public String query {get;set;}
    @TestVisible private Date eligDate;
    @TestVisible private Date toDateRange;
    Public String EDate {get;set;}
    Public String ToDateR {get;set;}
    
    public Boolean disabledNextLast {get; set;}
    public Boolean disabledPrevFirst {get; set;}
    public static final Integer MAX_RECORDS_PER_PAGE = 10;
   // public static final String LIMIT_10K = ' LIMIT 10000'; // set to 10K due to Standardset pagination limit : https://developer.salesforce.com/docs/atlas.en-us.pages.meta/pages/apex_pages_standardsetcontroller.htm
    
    private string currentSOQL;
    
    // Constructor
    public SearchBuilderController(ApexPages.StandardController stdController) {
        objAcc = new Account();
        statusSet = new Set<String>();
        typeSet = new Set<String>();
        setBuilders = new Set<String>(); 
        BuilderList = new List<Account>();
        currentSOQL = qry();
        //BuilderSSC = new ApexPages.StandardSetController(Database.query(qry()));
        BuilderSSC = new ApexPages.StandardSetController(Database.getQueryLocator( currentSOQL + ' '+ label.Record_Limit));
        //completeBuilderList = Database.query(qry());
    }
    
    
    public string qry(){
        query = 'SELECT Id, IsPersonAccount, ';
        for(Schema.FieldSetMember field : Schema.sObjectType.Account.fieldsets.builder_export.getFields()){
            query += field.getFieldPath() + ',';
        }
        query = query.substring(0,query.lastIndexOf(','));
                query += ' FROM Account';        
        
        userType();
        setFilterCriteria();
        System.debug('++++++++++ The query : ' + query);
        if(sortDirection == 'ASC') {
                    query = query + ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS FIRST';
                } else {
                    query = query + ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS LAST';
                }
        return query;
    }

    // Switching between acsending and descending modes
    public String sortExpression{
        get{
            return sortExp;
        }
        set{
            if (value == sortExp){
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';    
            }
            else{
                sortDirection = 'ASC';
                sortExp = value;
            }
        }
    }
    
    // Set filter criteria on the query 
    public void setFilterCriteria() {
        
        if (selectedType != NULL && selectedType != ''){
            string type = selectedType;
            type = type.replace('[','');
            type = type.replace(']','');
            string[] typeVals = type.split(',');
            
            for(String s : typeVals){
                typeSet.add(s.trim());
            }
        }
        
        if (selectedStatus != NULL && selectedStatus != ''){
            string status = selectedStatus;
            status = status.replace('[','');
            status = status.replace(']','');
            string[] statusVals = status.split(',');
            
            for(String s : statusVals){
                statusSet.add(s.trim());
            }
        }
        
        

        
       
        
        
        // Filter criteria - Account Name
        if(accName != NULL && accName != '') {
            String updatedQuery = updateQuery(query);
            query = updatedQuery + ' Name LIKE \'%'+String.escapeSingleQuotes(accName)+'%\'';
        }
        
        
        // Filter criteria - Builder Type   
        if(typeSet.size() > 0) {
            String validType = NULL; 
            if(typeSet.size() == 1) {
                for(string s : typeSet) {
                    validType = s;
                }
            }
            
            if((validType != NULL && validType != '' && validType != 'None') || typeSet.size() > 1) {
                System.debug('the test 111: ' + typeSet);
                String updatedQuery = updateQuery(query);
                query = updatedQuery + ' Legal_Business_Type__c = ' + '\'' +String.escapeSingleQuotes(selectedType)+ '\'';  
            }
        }
        
        // Filter criteria - Builder Status      
        if(statusSet.size() > 0) {
            String validStatus = NULL; 
            if(statusSet.size() == 1) {
                for(string s : statusSet) {
                    validStatus = s;
                }
            }
            
            if((validStatus != NULL && validStatus != '' && validStatus != 'None') || statusSet.size() > 1) {
                System.debug('the test 111: ' + statusSet);
                String updatedQuery = updateQuery(query);
                query = updatedQuery + ' Eligibility_Status__c = ' + '\'' +String.escapeSingleQuotes(selectedStatus)+ '\'';  
            }
        }
        
        // Filter criteria - Construction Limit
        if(accLimit > 0) {
            String updatedQuery = updateQuery(query);
            query = updatedQuery + ' Construction_Limit__c = :accLimit';
        }
        
        //Filter criteria - Date of Original Eligibility
        if(objAcc.Original_Eligibility_Approval_Date__c != NULL && objAcc.Date_Started_Trading__c == NULL) {
            String updatedQuery = updateQuery(query);
            eligDate = date.newinstance(objAcc.Original_Eligibility_Approval_Date__c.year(), objAcc.Original_Eligibility_Approval_Date__c.month(), objAcc.Original_Eligibility_Approval_Date__c.day());
            Edate = String.valueof(eligdate);
            System.debug('++++++++++ The eligdate : ' + Edate);
            query = updatedQuery + ' Original_Eligibility_Approval_Date__c = ' + EDate  ;  
        } else if (objAcc.Original_Eligibility_Approval_Date__c != NULL && objAcc.Date_Started_Trading__c != NULL){
            String updatedQuery = updateQuery(query);
            eligDate = date.newinstance(objAcc.Original_Eligibility_Approval_Date__c.year(), objAcc.Original_Eligibility_Approval_Date__c.month(), objAcc.Original_Eligibility_Approval_Date__c.day());
            Edate = String.valueof(eligdate);
            ToDateRange = date.newinstance(objAcc.Date_Started_Trading__c.year(), objAcc.Date_Started_Trading__c.month(), objAcc.Date_Started_Trading__c.day());
            toDateR = String.valueof(toDateRange );
            query = updatedQuery + ' Original_Eligibility_Approval_Date__c >= ' + EDate +' AND Original_Eligibility_Approval_Date__c <= ' + toDateR;
        }
        
        System.debug('query<><>'+query);
    }
    
    // Retrieve builder based on loggedin user
    public void userType() {
        String userType = GlobalUtility.loggedInUser.Contact.RecordType.Name;
        accId = GlobalUtility.loggedInUser.Contact.AccountId;
        
        list<user> userList = [select id, contactId, profile.name from user where id =: UserInfo.getUserId()];
        String userProfileName = userList[0].profile.name;
                
        if (userProfileName == Constants.Common_RBP) {         
            for (Contact_Role__c acr : 
                    [SELECT Account__c  
                     FROM Contact_Role__c 
                     WHERE Contact__c =: userList[0].contactId AND (Role__c =: Constants.Primary_RBP OR Role__c =: Constants.EmployeeRole)]) {
                setBuilders.add(acr.Account__c);     
            }
            
            query = query + ' WHERE Id IN :setBuilders';
        } else if (userType == 'Distributor') {
            // All of distributers builders
            for (Account a : [SELECT Id, Agent__c FROM Account WHERE Agent__c = :accId]) {
                setBuilders.add(a.Id);
            }
            query = query + ' WHERE Id IN :setBuilders';
        } else {
            // Only logged in builder
            query = query + ' WHERE Id = :accId';
        }
    }
    
    
    // Updating the query for additional filter criteria
    public String updateQuery(String query) {
        if(query.contains('WHERE')) {
            query = query + ' AND';
        } else {
            query = query + ' WHERE';
        }   
        return query;
    }
    
    //StandardSet
    public ApexPages.StandardSetController BuilderSSC {
        get {
            if(BuilderSSC == null) {
                //BuilderSSC = new ApexPages.StandardSetController(BuilderList);
                currentSOQL = qry();
                BuilderSSC = new ApexPages.StandardSetController(Database.getQueryLocator(currentSOQL  + ' '+ label.Record_Limit));
            }
            Decimal results = BuilderSSC.getResultSize();
            BuilderSSC.setPageSize(MAX_RECORDS_PER_PAGE);
            totalPages = (results/MAX_RECORDS_PER_PAGE).round(System.RoundingMode.UP);

            return BuilderSSC;
        }
        set;
    }
    
    
    //Search Function
    Public pageReference searchvalues(){
         
        //completeBuilderList = Database.query(qry());
        //BuilderList = Database.query(qry());
        try{
            //BuilderSSC = new ApexPages.StandardSetController(Database.query(qry()));
            currentSOQL = qry();
            BuilderSSC = new ApexPages.StandardSetController(Database.getQueryLocator(currentSOQL + ' '+ label.Record_Limit));
            BuilderSSC.setPageSize(MAX_RECORDS_PER_PAGE);
            
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;
        
    }
    
    
    //Pagination
    // Returns the page number of the current page set
    public Integer pageNumber {
        get {
            return BuilderSSC.getPageNumber();
        }
        set;
    }
    
    public List<Account> getBuilderList() {
        if(BuilderSSC.getHasNext()) {
            disabledNextLast = true;
        } else {
            disabledNextLast = false;
        }
        
        if(BuilderSSC.getHasPrevious()) {
            disabledPrevFirst = true;
        } else {
            disabledPrevFirst = false;
        }
        
        return BuilderSSC.getRecords();
    }
    
    // Search values based on the filter
    public void sortValues() {
        searchvalues();
    }
    
    // Field that needs to be sorted
    public String getSortDirection() {
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    
     // Indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return BuilderSSC.getHasNext();
        }
        set;
    }
    
    // Indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return BuilderSSC.getHasPrevious();
        }
        set;
    }
    
    public void showFirstRec() {
        BuilderSSC.first();
    }
    
    public void showPreviousRec() {
        BuilderSSC.previous();
    }

    public void showNextRec() {
        BuilderSSC.next();
    }
    
    public void showLastRec() {
        BuilderSSC.last();
    }
    
    
    // Get all eligibility status
    public List<SelectOption> getAccStatus() {
        List<SelectOption> AccStatus = new List<SelectOption>();
        DescribeFieldResult d = Account.Eligibility_Status__c.getDescribe();
        AccStatus.add(new SelectOption('None','--None--'));
        
        for (PicklistEntry e : d.getPicklistValues()) {
            if (e.isActive()) {
                AccStatus.add(new SelectOption(e.getValue(), e.getLabel()));
            }   
        }              
        return AccStatus;
    }
    
    // Get legal business type
    public List<SelectOption> getAccType() {
        List<SelectOption> AccType = new List<SelectOption>();
        DescribeFieldResult d = Account.Legal_Business_Type__c.getDescribe();
        AccType.add(new SelectOption('None','--None--'));
        
        for (PicklistEntry e : d.getPicklistValues()) {
            if (e.isActive()) {
                AccType.add(new SelectOption(e.getValue(), e.getLabel()));
            }   
        }              
        return AccType;
    }
    
    
    
    
    
    
    
    
    // Clear filter values on the form
    public void clearValues() {
        BuilderSSC= Null;
        selectedType = NULL;
        selectedStatus = NULL;
        accName = NULL;
        accLimit = NULL;
        objAcc.Original_Eligibility_Approval_Date__c = NULL;
        objAcc.Date_Started_Trading__c = NULL;
        typeSet.clear();
        statusSet.clear();
        searchValues();
    }

    public PageReference export() {        
        PageReference pageRef = Page.BuilderExport;
        pageRef.getParameters().put('fullExport','FALSE');
        pageRef.setRedirect(false);
        return pageRef;
    }

    public PageReference exportAll() { 
        //currentSOQL = new SearchBuilderController(new ApexPages.StandardController(new Account())).qry();
        System.debug('** currentSOQL ==>'+currentSOQL);
        completeBuilderList = Database.query(currentSOQL + ' LIMIT 1000');     
        PageReference pageRef = Page.BuilderExport;
        pageRef.getParameters().put('fullExport','TRUE');
        pageRef.setRedirect(false);
        return pageRef;
    }
}