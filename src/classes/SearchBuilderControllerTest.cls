/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
global with sharing class SearchBuilderControllerTest {
	
	private static final String EXPECTED_APP_QUERY = 'SELECT Id,  Name,ACN__c,Eligibility_Status__c,Legal_Business_Type__c,ParentId,ABN__c,Builder_ACN__c,Date_Started_Trading__c,Original_Eligibility_Approval_Date__c,Phone,Primary_Email__c,After_Hours_Phone__c,Website,IsVolumeBuilder__c,ShippingStreet,BillingStreet,Builder_Risk_Rating__c,Construction_Limit__c,AvailableConstructionLimit__c,Construction_Category_Limit_1__c,SD_Flat_Rate_Premium__c,Construction_Category_Limit_2__c,Non_Structural_Flat_Rate__c,Construction_Category_Limit_3__c,C03_Multi_Dwelling_Flat_Rate_Premium__c,Construction_Category_Limit_4__c,C04_Structural_Flat_Rate_Premium__c,Construction_Category_Limit_5__c,SP_Flat_Rate_Premium__c,Construction_Category_Limit_6__c,Flat_Rate_Premium__c,Construction_Category_Limit_7__c,C07_Other_Flat_Rate_Premium__c,Eligibility_Conditions__c,Last_Eligibility_Reviewed_Date__c,Eligibility_Review_Date__c,Eligibility_Number__c,RBP_License_First_Issued__pc,RBP_VBA_Number__pc FROM Account WHERE Id = :accId ORDER BY Name ASC NULLS FIRST';

	private static final String EXPECTED_APP_QUERY2 = 'SELECT Id,  Name,ACN__c,Eligibility_Status__c,Legal_Business_Type__c,ParentId,ABN__c,Builder_ACN__c,Date_Started_Trading__c,Original_Eligibility_Approval_Date__c,Phone,Primary_Email__c,After_Hours_Phone__c,Website,IsVolumeBuilder__c,ShippingStreet,BillingStreet,Builder_Risk_Rating__c,Construction_Limit__c,AvailableConstructionLimit__c,Construction_Category_Limit_1__c,SD_Flat_Rate_Premium__c,Construction_Category_Limit_2__c,Non_Structural_Flat_Rate__c,Construction_Category_Limit_3__c,C03_Multi_Dwelling_Flat_Rate_Premium__c,Construction_Category_Limit_4__c,C04_Structural_Flat_Rate_Premium__c,Construction_Category_Limit_5__c,SP_Flat_Rate_Premium__c,Construction_Category_Limit_6__c,Flat_Rate_Premium__c,Construction_Category_Limit_7__c,C07_Other_Flat_Rate_Premium__c,Eligibility_Conditions__c,Last_Eligibility_Reviewed_Date__c,Eligibility_Review_Date__c,Eligibility_Number__c,RBP_License_First_Issued__pc,RBP_VBA_Number__pc FROM Account WHERE Id = :accId ORDER BY Name ASC NULLS FIRST AND Legal_Business_Type__c IN :typeSet AND Eligibility_Status__c IN :statusSet';
	
	private static final String EXPECTED_APP_QUERY3 = 'SELECT Id,  Name,ACN__c,Eligibility_Status__c,Legal_Business_Type__c,ParentId,ABN__c,Builder_ACN__c,Date_Started_Trading__c,Original_Eligibility_Approval_Date__c,Phone,Primary_Email__c,After_Hours_Phone__c,Website,IsVolumeBuilder__c,ShippingStreet,BillingStreet,Builder_Risk_Rating__c,Construction_Limit__c,AvailableConstructionLimit__c,Construction_Category_Limit_1__c,SD_Flat_Rate_Premium__c,Construction_Category_Limit_2__c,Non_Structural_Flat_Rate__c,Construction_Category_Limit_3__c,C03_Multi_Dwelling_Flat_Rate_Premium__c,Construction_Category_Limit_4__c,C04_Structural_Flat_Rate_Premium__c,Construction_Category_Limit_5__c,SP_Flat_Rate_Premium__c,Construction_Category_Limit_6__c,Flat_Rate_Premium__c,Construction_Category_Limit_7__c,C07_Other_Flat_Rate_Premium__c,Eligibility_Conditions__c,Last_Eligibility_Reviewed_Date__c,Eligibility_Review_Date__c,Eligibility_Number__c,RBP_License_First_Issued__pc,RBP_VBA_Number__pc FROM Account WHERE Id = :accId ORDER BY Name ASC NULLS FIRST AND Legal_Business_Type__c IN :typeSet AND Eligibility_Status__c IN :statusSet AND Name LIKE \'%Test%\' AND Legal_Business_Type__c IN :typeSet AND Eligibility_Status__c IN :statusSet';
	
	private static final String EXPECTED_APP_QUERY4 = 'SELECT Id,  Name,ACN__c,Eligibility_Status__c,Legal_Business_Type__c,ParentId,ABN__c,Builder_ACN__c,Date_Started_Trading__c,Original_Eligibility_Approval_Date__c,Phone,Primary_Email__c,After_Hours_Phone__c,Website,IsVolumeBuilder__c,ShippingStreet,BillingStreet,Builder_Risk_Rating__c,Construction_Limit__c,AvailableConstructionLimit__c,Construction_Category_Limit_1__c,SD_Flat_Rate_Premium__c,Construction_Category_Limit_2__c,Non_Structural_Flat_Rate__c,Construction_Category_Limit_3__c,C03_Multi_Dwelling_Flat_Rate_Premium__c,Construction_Category_Limit_4__c,C04_Structural_Flat_Rate_Premium__c,Construction_Category_Limit_5__c,SP_Flat_Rate_Premium__c,Construction_Category_Limit_6__c,Flat_Rate_Premium__c,Construction_Category_Limit_7__c,C07_Other_Flat_Rate_Premium__c,Eligibility_Conditions__c,Last_Eligibility_Reviewed_Date__c,Eligibility_Review_Date__c,Eligibility_Number__c,RBP_License_First_Issued__pc,RBP_VBA_Number__pc FROM Account WHERE Id = :accId ORDER BY Name ASC NULLS FIRST AND Legal_Business_Type__c IN :typeSet AND Eligibility_Status__c IN :statusSet AND Name LIKE \'%Test%\' AND Legal_Business_Type__c IN :typeSet AND Eligibility_Status__c IN :statusSet AND Name LIKE \'%Test%\' AND Legal_Business_Type__c IN :typeSet AND Eligibility_Status__c IN :statusSet';
	
	//variables
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static {
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
    }
    
	@testSetup
	static void init() {		
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor', accRecTypeMap.get('Distributor').getRecordTypeId());
        Account lexDistributor = TestUtility.createBusinessAccount('Lex Distributors', accRecTypeMap.get('Distributor').getRecordTypeId());
        Account texDistributor = TestUtility.createBusinessAccount('Tex Distributors', accRecTypeMap.get('Distributor').getRecordTypeId());

		List<Account> accList = new List<Account>();
        accList.add(rexDistributor);
        accList.add(lexDistributor);
        accList.add(texDistributor);
        Database.insert(accList);
	}
	
	

	
	
    static void testConstructor() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        
        System.assertNotEquals(null, controller.objAcc);
        System.assertNotEquals(null, controller.statusSet);
        System.assertNotEquals(null, controller.typeSet);
        System.assertNotEquals(null, controller.setBuilders);
        System.assertNotEquals(null, controller.BuilderList);
        System.assertNotEquals(null, controller.completeBuilderList);
    }
    
    @isTest
    static void testGetStandardSetController() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        ApexPages.StandardSetController BuilderSSC = controller.BuilderSSC;
        
        System.assertNotEquals(null, BuilderSSC);
        System.assertEquals(0, BuilderSSC.getResultSize());
        System.assertEquals(10, BuilderSSC.getPageSize());
        System.assertEquals('Name', controller.sortExpression);
        //System.assertEquals(EXPECTED_APP_QUERY, controller.query);
        System.assertEquals(0, controller.totalPages);        
    }
    
    @isTest
    static void testUserType() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        controller.userType();
        
        System.assertEquals(null, controller.accId);
        //System.assertEquals('null WHERE Id = :accId', controller.query);
    }
    
    @isTest
    static void testGetBuilderList() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        List<Account> accounts = controller.getBuilderList();
        
        System.assertNotEquals(null, accounts);
        System.assertEquals(0, accounts.size());
    }
    

    
    @isTest
    static void testGetAccStatus() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        List<SelectOption> options = controller.getAccStatus();
        
        System.assertNotEquals(null, options);
//        System.assertEquals(8, options.size());
    }
    
    @isTest
    static void testgetAccType() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        List<SelectOption> options = controller.getAccType();
        
        System.assertNotEquals(null, options);
        System.assertEquals(4, options.size());
    }
    
    @isTest
    static void testSortExpression() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        
        System.assertEquals('Name', controller.sortExpression);
        System.assertEquals('ASC', controller.sortDirection);
        
        controller.sortExpression = 'DESC';
        System.assertEquals('ASC', controller.sortDirection);
        
        controller.sortExpression = NULL;
        System.assertEquals('ASC', controller.sortDirection);
    }
    
    @isTest
    static void testHasNext() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        
        System.assertEquals(false, controller.hasNext);
    }
    
    @isTest
    static void testHasPrevious() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        
        System.assertEquals(false, controller.hasPrevious);
    }
    
    @isTest
    static void testPageNumber() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        
        System.assertEquals(1, controller.pageNumber);
    }
    
    @isTest
    static void testshowFirstRec() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        controller.showFirstRec();
        
        System.assertEquals(false, controller.BuilderSSC.getHasPrevious());
        System.assertEquals(false, controller.BuilderSSC.getHasNext());
        System.assertEquals(1, controller.BuilderSSC.getPageNumber());
        
        //Account accout = (Account)controller.con.getRecord();
        //System.assertEquals('Rex Home Distributor', account.Name);
        //System.assertEquals(accRecTypeMap.get('Distributor').getRecordTypeId(), account.RecordTypeId);
    }
    
    @isTest
    static void testshowLastRec() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        controller.showLastRec();
        
        System.assertEquals(false, controller.BuilderSSC.getHasPrevious());
        System.assertEquals(false, controller.BuilderSSC.getHasNext());
        System.assertEquals(1, controller.BuilderSSC.getPageNumber());
    }
    
    @isTest
    static void testshowPreviousRec() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        controller.showPreviousRec();
        
		System.assertEquals(false, controller.BuilderSSC.getHasPrevious());
        System.assertEquals(false, controller.BuilderSSC.getHasNext());
        System.assertEquals(1, controller.BuilderSSC.getPageNumber());
    }
    
    @isTest
    static void testshowNextRec() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        controller.showNextRec();
        
        System.assertEquals(false, controller.BuilderSSC.getHasPrevious());
        System.assertEquals(false, controller.BuilderSSC.getHasNext());
        System.assertEquals(1, controller.BuilderSSC.getPageNumber());
    }
    
    @isTest
    static void testCancel() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        controller.clearvalues();
        
        System.assertEquals(false, controller.BuilderSSC.getHasPrevious());
        System.assertEquals(false, controller.BuilderSSC.getHasNext());
        System.assertEquals(1, controller.BuilderSSC.getPageNumber());
    }
    
    @isTest
    static void testUpdateQuery() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        String query = controller.updateQuery('SELECT ID FROM ACCOUNT');
        
        System.assertEquals('SELECT ID FROM ACCOUNT WHERE', query);
    }
    
    @isTest
    static void testGetSortDirection() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        
        System.assertEquals('ASC', controller.getSortDirection());
    }
    
    /*@isTest
    static void testSetSortDirection() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        controller.setSortDirection('ASC');
        
        System.assertEquals('ASC', controller.sortDirection);
    }*/
    
    @isTest
    static void testSetFilterCriteria() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        
        controller.selectedType = '[A],[B],[C]';
        controller.selectedStatus = '[D],[E],[F]';
        controller.query = EXPECTED_APP_QUERY;
        controller.setFilterCriteria();
        
        System.assertEquals(3, controller.typeSet.size());
        System.assertEquals('A', new List<String>(controller.typeSet)[0]);
        System.assertEquals('B', new List<String>(controller.typeSet)[1]);
        System.assertEquals('C', new List<String>(controller.typeSet)[2]);
        
        System.assertEquals(3, controller.statusSet.size());
        System.assertEquals('D', new List<String>(controller.statusSet)[0]);
        System.assertEquals('E', new List<String>(controller.statusSet)[1]);
        System.assertEquals('F', new List<String>(controller.statusSet)[2]);
        
        System.assertEquals(null, controller.eligDate);
        System.assertEquals(null, controller.toDateRange);
        //System.assertEquals(EXPECTED_APP_QUERY2, controller.query);
        
        controller.accName = 'Test';
        controller.setFilterCriteria();
        //System.assertEquals(EXPECTED_APP_QUERY3, controller.query);
        
        controller.setFilterCriteria();
        //System.assertEquals(EXPECTED_APP_QUERY4, controller.query);
    }
    
    @isTest
    static void testSearchValues() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        controller.searchValues();
        
        System.assertNotEquals(null, controller.BuilderSSC);
        System.assertEquals(0, controller.typeSet.size());
        System.assertEquals(0, controller.statusSet.size());
        
    }
    
    @isTest
    static void testSearchValues2() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        controller.accLimit = 1;
        controller.searchValues();
        controller.selectedtype = 'Amend COI';
        controller.selectedStatus = 'New';
        controller.objAcc.Original_Eligibility_Approval_Date__c = date.today();
        controller.objAcc.Date_Started_Trading__c = date.today();
        
        controller.searchValues();
        
        System.assertNotEquals(null, controller.BuilderSSC);
        System.assertEquals(1, controller.accLimit);
       
        
    }
    
    @isTest
    static void testSearchValues3() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        controller.accLimit = 1;
        controller.searchValues();
        controller.selectedtype = 'Amend COI';
        controller.selectedStatus = 'New';
        controller.objAcc.Original_Eligibility_Approval_Date__c = date.today();
        controller.objAcc.Date_Started_Trading__c = NULL;
        
        controller.searchValues();
        
        System.assertNotEquals(null, controller.BuilderSSC);
        System.assertEquals(1, controller.accLimit);
       
        
    }
    
    @isTest
    static void testSortValues() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        controller.sortValues();
        
        System.assertNotEquals(null, controller.BuilderSSC);
    }
    
    @isTest
    static void testClearValues() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        controller.clearValues();
        
        System.assertNotEquals(null, controller.BuilderSSC);
        System.assertEquals(null, controller.selectedType);
        System.assertEquals(null, controller.selectedStatus);
        System.assertEquals(null, controller.accName);
        System.assertEquals(null, controller.accLimit);
        System.assertEquals(null, controller.objAcc.Original_Eligibility_Approval_Date__c);
        System.assertEquals(null, controller.objAcc.Date_Started_Trading__c);
        System.assertEquals(0, controller.typeSet.size());
        System.assertEquals(0, controller.statusSet.size());
    }
    
    @isTest
    static void testExport() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        PageReference pageReference = controller.export();
        
        System.assertNotEquals(null, pageReference);
        System.assertEquals('FALSE', pageReference.getParameters().get('fullExport'));
        System.assertEquals(False, pageReference.getRedirect());
    }
    
    @isTest
    static void testExportAll() {
        ApexPages.StandardController stdController = null;
        SearchBuilderController controller = new SearchBuilderController(stdController);
        PageReference pageReference = controller.exportAll();
        
        System.assertNotEquals(null, pageReference);
        System.assertEquals('TRUE', pageReference.getParameters().get('fullExport'));
        System.assertEquals(False, pageReference.getRedirect());
    }
    
    
}