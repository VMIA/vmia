/*
    Date            :  31-Jan-2017
    Author          :   SMS Management & Technology
    Description     :  List of COI with a paginated view
    History:
    -------------------------------------------------
    31-Jul-2017     : V.Jaiswal     #1275 Certificate Search and Export issue
*/
public with sharing class SearchCertificatesController {
    
    //constants
    @testVisible private static final String CERT_RECTYPE_CONTAINER = 'Container';
    @testVisible private static final String CERT_RECTYPE_Historical = GlobalConstants.REC_TYPE_HISTORICAL_CERT;
    
    // Definitions
    public String builderName {get;set;}
    public String propertyOwner {get;set;}
    public String CertificationNo {get;set;}
    public String CertificationType {get;set;}
    public String selectedStatus {get;set;}
    public String selectedType {get;set;}
    public Decimal totalPages {get;set;}
    public Decimal fromPrice {get;set;}
    public String fromPriceString {get;set;}
    public Decimal toPrice {get;set;}
    public String toPriceString {get;set;}
    public Date fromIssueDate {get;set;}
    public Date toIssueDate {get;set;}
    Set<string> issuedHistorical{get;set;}
    @testVisible private Set<String> statusSet;
    @testVisible private Set<String> typeSet;
    @testVisible private String sortDirection = 'ASC';
    @testVisible private String sortExp = 'Name';
    @testVisible private String appQuery;
    @testVisible private String appStatus;
    public Certificate__c objCert {get;set;}
    //#1275 added transient variable to exculde list from view state
    @testVisible transient List<Certificate__c> lstCertification {get;set;} 
   
    @testVisible private Set<String> setBuilders;
    @testVisible private String accId;
    //#1275 added transient variable to exculde list from view state
    transient  List<Certificate__c> completeCertificateList {get;set;}
    Public Id containerRecordType;
    public set<id> excludeRecordType{get;set;}
    
    // Constructor
    public SearchCertificatesController(ApexPages.StandardController stdController) {
        objCert = new Certificate__c();
        statusSet = new Set<String>();
        typeSet = new Set<String>();
        setBuilders = new Set<String>();
        fromPriceString = NULL;
        toPriceString = NULL;
        
        lstCertification = new List<Certificate__c>();
        issuedHistorical = new Set<string>();
        excludeRecordType = new Set<Id>();
        appQuery = deafaultQuery();
        System.debug('setBuilders>>>>>>>>'+setBuilders);
        System.debug('** appQuery  ==>'+ appQuery);
        
        con = new ApexPages.StandardSetController(Database.query(appQuery + ' '+ label.Record_Limit));
        completeCertificateList = String.isNotBlank(appQuery) ? Database.Query(appQuery + ' '+ label.Record_Limit) : new List<Certificate__c>();
    }
    
    public string deafaultQuery(){
        
        string myDefaultQuery; 
         
         myDefaultQuery= 'SELECT Actual_Completion_Date__c,Actual_Start_Date__c,Amended_Premium__c,Builder__c,Building_Contract_Date__c,Permit_Issue_Date__c,Building_Permit_Number__c, Property_Owner_Text__c,';
               myDefaultQuery+= 'Category_of_Building_Works__c,Certificate_Type__c,Change_Request__c,Conditions__c,Consent_Date__c,Consent_Number__c,Contract_Price__c,CreatedById,CreatedDate,Current_Issued_Certificate__c, Builder__r.Name,';
               myDefaultQuery+= 'Date_Issued__c,Date_Updated__c,Defects_Inspection_Completion_Date__c,Defects_Inspection_Report_Date__c,Defects_Inspector_Name__c,Description_of_non_residential_works__c, Builder__r.Account_Number__c,';
               myDefaultQuery+= 'Description_of_Works__c,Distributor__c,Estimated_Completion_Date__c,Estimated_Start_Date__c,Estimated_Value_of_Non_Res_works__c,External_Comments__c,Further_Information_Description__c,';
               myDefaultQuery+= 'Further_Information_Summary__c,GST__c,Has_work_Completed__c,Have_Works_Commenced__c,Hidden_Most_Recent_Amend__c,Id,Inspector_VBA_Registration_Number__c,InvoiceNo__c,';
               myDefaultQuery+= 'IsDeleted,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Levies__c,Name,New_Information_Entered__c,Number_of_Properties__c,';
               myDefaultQuery+= 'Occupancy_Permit_Date__c,Original_COI__c,Out_Of_Date__c,OwnerId,Parent_Certificate_Premium__c,Parent_Certificate_Total__c,Parent_COI__c,PayStatus__c,';
               myDefaultQuery+= 'Policy_Number__c,Premium__c,PropertyAddressHidden__c,Property_Address__c,Property_Owner__c,Property__c,RecordTypeId,Referral_Reason__c,Referral_Requested__c,';
               myDefaultQuery+= 'Replenish_Kitty__c,Speculative__c,StampDuty__c,Status__c,Sub_Categories__c,Sub_Status__c,SuccessEmailSent__c,SystemModstamp,System_Generated_Referral_Reason__c,Invoice_No_Formula__c,Created_by_Formula__c, ';
               myDefaultQuery+= 'Taken_Over__c,testFormula__c,Total__c,TypeofCover__c,Type_of_Building_Contract__c,Type__c,Unit__c,PIT_At_the_Property__c,PIT_First_Owner_Name__c FROM Certificate__c';
                
                String userType = GlobalUtility.loggedInUser.Contact.RecordType.Name;
        accId = GlobalUtility.loggedInUser.Contact.AccountId;
        
        list<user> userList = [select id, contactId, profile.name from user where id =: UserInfo.getUserId()];
        String userProfileName = userList[0].profile.name;

        //user type
        if (userProfileName == Constants.Common_RBP) {         
            for (Contact_Role__c acr : 
                    [SELECT Account__c  
                     FROM Contact_Role__c 
                     WHERE Contact__c =: userList[0].contactId AND (Role__c =: Constants.Primary_RBP OR Role__c =: Constants.EmployeeRole)]) {
                setBuilders.add(acr.Account__c);     
            }
            
            myDefaultQuery+= ' WHERE Builder__c IN :setBuilders';            
        } else if (userType == 'Distributor') {
            // All of distributers builders
            for (Account a : [SELECT Id, Agent__c FROM Account WHERE Agent__c = :accId]) {
                setBuilders.add(a.Id);
            }
            myDefaultQuery += ' WHERE (Builder__c IN :setBuilders OR Distributor__c = :accId)';
        } else {
            // Only logged in builder
            myDefaultQuery += ' WHERE Builder__c = :accId';
        }

        System.debug('myDefaultQuery<>'+myDefaultQuery);
        System.debug('setBuilders<><>'+setBuilders);
        //end user type
        
        //search filter criteria
                 
        if (selectedType != NULL && selectedType != ''){
            string type = selectedType;
            type = type.replace('[','');
            type = type.replace(']','');
            string[] typeVals = type.split(',');
            
            for(String s : typeVals){
                typeSet.add(s.trim());
            }
        }
        
        if (selectedStatus != NULL && selectedStatus != ''){
            string status = selectedStatus;
            status = status.replace('[','');
            status = status.replace(']','');
            string[] statusVals = status.split(',');
            
            for(String s : statusVals){
                statusSet.add(s.trim());
            }
        }
        
        // Filter criteria - Builder Name
        if(builderName != NULL && builderName != '') {
            String updatedQuery = updateQuery(myDefaultQuery);
            myDefaultQuery = updatedQuery + ' Builder__r.Name LIKE \'%'+String.escapeSingleQuotes(builderName)+'%\'';
        }
        
        // Filter criteria - Certificate Number
        if(CertificationNo != NULL && CertificationNo != '') {
            String updatedQuery = updateQuery(myDefaultQuery);
            CertificationNo = CertificationNo.trim();
            PolicyNumber__c policySetting = PolicyNumber__c.getOrgDefaults(); 
            if(null != policySetting){
                if(CertificationNo.startsWith(policySetting.Prefix__c)){
                    myDefaultQuery = updatedQuery + ' Policy_Number__c = :CertificationNo'; 
                } else{
                    myDefaultQuery = updatedQuery + ' name = :CertificationNo'; 
                }
            }
        }
        
        // Filter criteria - Property Address
        if(objCert.Property_Address__c != NULL && objCert.Property_Address__c != '') {
            String updatedQuery = updateQuery(myDefaultQuery);
            myDefaultQuery = updatedQuery + ' Property_Address__c LIKE \'%'+String.escapeSingleQuotes(objCert.Property_Address__c)+'%\'';  
        }
        
        // Filter criteria - Property Owner
        if(propertyOwner != NULL && propertyOwner != '') {
            String updatedQuery = updateQuery(myDefaultQuery);
            myDefaultQuery = updatedQuery + ' Property_Owner_Text__c LIKE \'%'+String.escapeSingleQuotes(propertyOwner)+'%\'';  
        }
        
        // Filter criteria - Category of works   
        if(typeSet.size() > 0) {
            String validType = NULL; 
            if(typeSet.size() == 1) {
                for(string s : typeSet) {
                    validType = s;
                }
            }
            
            if((validType != NULL && validType != '' && validType != 'None') || typeSet.size() > 1) {
                String updatedQuery = updateQuery(myDefaultQuery);
                myDefaultQuery = updatedQuery + ' Category_of_Building_Works__c IN :typeSet';  
            }
        }
        
        // Filter criteria - status     
        if(statusSet.size() > 0) {
            String validStatus = NULL; 
            if(statusSet.size() == 1) {
                for(string s : statusSet) {
                    validStatus = s;
                }
            }
            
            if((validStatus != NULL && validStatus != '' && validStatus != 'None') || statusSet.size() > 1) {
                String updatedQuery = updateQuery(myDefaultQuery);
                myDefaultQuery = updatedQuery + ' Status__c IN :statusSet';  
            }
        }
        
        // Filter criteria - Contract Price
        if(null != fromPriceString){
            fromPriceString = fromPriceString.replace(',','');
            try{
                fromPrice = Decimal.valueOf(fromPriceString);  
            }catch(Exception e){
                fromPrice = 0;
            }
        }
        if(null != toPriceString){
            toPriceString = toPriceString.replace(',','');
            try{
                toPrice = Decimal.valueOf(toPriceString);
            }catch(Exception e){
                toPrice = 0;
            }
        }
        system.debug('@@1=' + toPrice);
        system.debug('@@2=' + fromPrice);
        if(fromPrice > 0 && toPrice <= 0) {
            String updatedQuery = updateQuery(myDefaultQuery);
            myDefaultQuery = updatedQuery + ' Contract_Price__c = :fromPrice';  
        } else if (fromPrice >= 0 && toPrice > 0){
            String updatedQuery = updateQuery(myDefaultQuery);
            myDefaultQuery = updatedQuery + ' Contract_Price__c >= :fromPrice AND Contract_Price__c <= :toPrice';
        }
        
        //Filter criteria - Date Issued
        fromIssueDate = objCert.Date_Issued__c;
        toIssueDate = objCert.Actual_Start_Date__c; // Using the Actual_Start_Date__c to accept input using date picker
        
        if(fromIssueDate != NULL && toIssueDate == NULL) {
            String updatedQuery = updateQuery(myDefaultQuery);
            myDefaultQuery = updatedQuery + ' Date_Issued__c = :fromIssueDate';  
        } else if (fromIssueDate != NULL && toIssueDate != NULL){
            String updatedQuery = updateQuery(myDefaultQuery);
            myDefaultQuery = updatedQuery + ' Date_Issued__c >= :fromIssueDate AND Date_Issued__c <= :toIssueDate';
        }
        
        //exclude container recordtype
        containerRecordType = Schema.SObjectType.Certificate__c.RecordTypeInfosByName.get('Container').RecordTypeId;

        excludeRecordType.add(containerRecordType);
        id historicalId = Schema.SObjectType.Certificate__c.RecordTypeInfosByName.get(GlobalConstants.REC_TYPE_HISTORICAL_CERT).RecordTypeId;
        excludeRecordType.add(historicalId);

        System.debug('** containerRecordType ==>'+ containerRecordType);
        if(containerRecordType != NULL){
            String updatedQuery = updateQuery(myDefaultQuery);
            myDefaultQuery = updatedQuery + ' RecordTypeId !=: excludeRecordType';
            System.debug('** appquerywithfilter ==>'+ myDefaultQuery);
        }

         String updatedQuery = updateQuery(myDefaultQuery); 
         
         issuedHistorical.add(GlobalConstants.STAT_ISSUED_HISTORICAL);
         issuedHistorical.add(GlobalConstants.DRAFT_HISTORICAL);
         issuedHistorical.add(GlobalConstants.SUB_STAT_REFUND);
        
         myDefaultQuery = updatedQuery + ' status__c !=: issuedHistorical';  

         System.debug('myDefaultQuery>>><<'+myDefaultQuery);    
                
        //end search filter criterua
         
                if(sortDirection == 'ASC') {
                    myDefaultQuery += ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS FIRST';
                } else {
                    myDefaultQuery += ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS LAST';
                }
                
                system.debug('**sortDirection: '+sortDirection);
                system.debug('**sortExpression: '+sortExpression);
                System.debug('**myDefaultQuery: '+myDefaultQuery);
                
        return myDefaultQuery;
    }
    
    public SearchCertificatesController() {
    }
    
    // Instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController con {
       get {
            if(con == null) {
                con = new ApexPages.StandardSetController(lstCertification);
            }
            Decimal results = con.getResultSize();
            con.setPageSize(10);
            totalPages = (results/10).round(System.RoundingMode.UP);

            return con;
        }
        set;
    }
    
    public List<Certificate__c> getlstCertification() {
        return con.getRecords() ;
    }
        
    
    public List<Certificate__c> getCompleteCertificateList() {
        System.debug('** con ==>'+ con);
        completeCertificateList = new List<Certificate__c>();
        System.debug('!!appQuery: '+appQuery);
        completeCertificateList = Database.query(appQuery + ' '+ label.Record_Limit);
        return completeCertificateList;
    }
    
    // Retrieve COI based on loggedin user
    public void userType() {
        String userType = GlobalUtility.loggedInUser.Contact.RecordType.Name;
        accId = GlobalUtility.loggedInUser.Contact.AccountId;
        
        list<user> userList = [select id, contactId, profile.name from user where id =: UserInfo.getUserId()];
        String userProfileName = userList[0].profile.name;

        if (userProfileName == Constants.Common_Director) {         
            for (Contact_Role__c acr : 
                    [SELECT Account__c  
                     FROM Contact_Role__c 
                     WHERE Contact__c =: userList[0].contactId AND (Role__c =: Constants.Primary_RBP OR Role__c =: Constants.EmployeeRole)]) {
                setBuilders.add(acr.Account__c);     
            }
            
            appQuery = appQuery + ' WHERE Builder__c IN :setBuilders';            
        } else if (userType == 'Distributor') {
            // All of distributers builders
            for (Account a : [SELECT Id, Agent__c FROM Account WHERE Agent__c = :accId]) {
                setBuilders.add(a.Id);
            }
            appQuery = appQuery + ' WHERE (Builder__c IN :setBuilders OR Distributor__c = :accId)';
        } else {
            // Only logged in builder
            appQuery = appQuery + ' WHERE Builder__c = :accId';
        }
                
    }
    
     
    // Get all certificate status
    public List<SelectOption> getCertStatus() {
        List<SelectOption> CertStatus = new List<SelectOption>();
        DescribeFieldResult d = Certificate__c.Status__c.getDescribe();
        CertStatus.add(new SelectOption('None','--None--'));
        
        for (PicklistEntry e : d.getPicklistValues()) {
            if (e.isActive()) {
                CertStatus.add(new SelectOption(e.getValue(), e.getLabel()));
            }   
        }              
        return CertStatus;
    }
    
    // Get all category of building works
    public List<SelectOption> getCertCategory() {
        List<SelectOption> CertCategory = new List<SelectOption>();
        DescribeFieldResult d = Certificate__c.Category_of_Building_Works__c.getDescribe();
        CertCategory.add(new SelectOption('None','--None--'));
        
        for (PicklistEntry e : d.getPicklistValues()) {
            if (e.isActive()) {
                CertCategory.add(new SelectOption(e.getValue(), e.getLabel()));
            }   
        }              
        return CertCategory;
    }
    
    // Switching between acsending and descending modes
    public String sortExpression { get{
        return sortExp;
    } set {
        if (value == sortExp)
            sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
        else
            sortDirection = 'ASC';
        sortExp = value;
    }
                                 }
    
    // Indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
    
    // Indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
    
    // Returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
    
    // Returns the first page of records
    public void first() {
        con.first();
    }
    
    // Returns the last page of records
    public void last() {
        con.last();
    }
    
    // Returns the previous page of records
    public void previous() {
        con.previous();
    }
    
    // Returns the next page of records
    public void next() {
        con.next();
    }
    
    // Returns the PageReference of the original page, if known, or the home page.
    public void cancel() {
        con.cancel();
    }
    
    // Updating the query for additional filter criteria
    public String updateQuery(String query) {
        if(query.contains('WHERE')) {
            query = query + ' AND';
        } else {
            query = query + ' WHERE';
        }  
        return query;
    }
    
    // Field that needs to be sorted
    public String getSortDirection() {
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    
    // Setting the direction ascending /descending for sorting
    public void setSortDirection(String value) { 
        sortDirection = value;
    }
    
    // Set filter criteria on the query 
    public void setFilterCriteria() {
        
        if (selectedType != NULL && selectedType != ''){
            string type = selectedType;
            type = type.replace('[','');
            type = type.replace(']','');
            string[] typeVals = type.split(',');
            
            for(String s : typeVals){
                typeSet.add(s.trim());
            }
        }
        
        if (selectedStatus != NULL && selectedStatus != ''){
            string status = selectedStatus;
            status = status.replace('[','');
            status = status.replace(']','');
            string[] statusVals = status.split(',');
            
            for(String s : statusVals){
                statusSet.add(s.trim());
            }
        }
        
        // Filter criteria - Builder Name
        if(builderName != NULL && builderName != '') {
            String updatedQuery = updateQuery(appQuery);
            appQuery = updatedQuery + ' Builder__r.Name LIKE \'%'+String.escapeSingleQuotes(builderName)+'%\'';
        }
        
        // Filter criteria - Certificate Number
        if(CertificationNo != NULL && CertificationNo != '') {
            String updatedQuery = updateQuery(appQuery);
            CertificationNo = CertificationNo.trim();
            PolicyNumber__c policySetting = PolicyNumber__c.getOrgDefaults(); 
            if(null != policySetting){
                if(CertificationNo.startsWith(policySetting.Prefix__c)){
                    appQuery = updatedQuery + ' Policy_Number__c = :CertificationNo'; 
                } else{
                    appQuery = updatedQuery + ' name = :CertificationNo'; 
                }
            }
        }
        
        // Filter criteria - Property Address
        if(objCert.Property_Address__c != NULL && objCert.Property_Address__c != '') {
            String updatedQuery = updateQuery(appQuery);
            appQuery = updatedQuery + ' Property_Address__c LIKE \'%'+String.escapeSingleQuotes(objCert.Property_Address__c)+'%\'';  
        }
        
        // Filter criteria - Property Owner
        if(propertyOwner != NULL && propertyOwner != '') {
            String updatedQuery = updateQuery(appQuery);
            appQuery = updatedQuery + ' Property_Owner_Text__c LIKE \'%'+String.escapeSingleQuotes(propertyOwner)+'%\'';  
        }
        
        // Filter criteria - Category of works   
        if(typeSet.size() > 0) {
            String validType = NULL; 
            if(typeSet.size() == 1) {
                for(string s : typeSet) {
                    validType = s;
                }
            }
            
            if((validType != NULL && validType != '' && validType != 'None') || typeSet.size() > 1) {
                String updatedQuery = updateQuery(appQuery);
                appQuery = updatedQuery + ' Category_of_Building_Works__c IN :typeSet';  
            }
        }
        
        // Filter criteria - status     
        if(statusSet.size() > 0) {
            String validStatus = NULL; 
            if(statusSet.size() == 1) {
                for(string s : statusSet) {
                    validStatus = s;
                }
            }
            
            if((validStatus != NULL && validStatus != '' && validStatus != 'None') || statusSet.size() > 1) {
                String updatedQuery = updateQuery(appQuery);
                appQuery = updatedQuery + ' Status__c IN :statusSet';  
            }
        }
        
        // Filter criteria - Contract Price
        if(null != fromPriceString){
            fromPriceString = fromPriceString.replace(',','');
            try{
                fromPrice = Decimal.valueOf(fromPriceString);  
            }catch(Exception e){
                fromPrice = 0;
            }
        }
        if(null != toPriceString){
            toPriceString = toPriceString.replace(',','');
            try{
                toPrice = Decimal.valueOf(toPriceString);
            }catch(Exception e){
                toPrice = 0;
            }
        }
        system.debug('@@1=' + toPrice);
        system.debug('@@2=' + fromPrice);
        if(fromPrice > 0 && toPrice <= 0) {
            String updatedQuery = updateQuery(appQuery);
            appQuery = updatedQuery + ' Contract_Price__c = :fromPrice';  
        } else if (fromPrice >= 0 && toPrice > 0){
            String updatedQuery = updateQuery(appQuery);
            appQuery = updatedQuery + ' Contract_Price__c >= :fromPrice AND Contract_Price__c <= :toPrice';
        }
        
        //Filter criteria - Date Issued
        fromIssueDate = objCert.Date_Issued__c;
        toIssueDate = objCert.Actual_Start_Date__c; // Using the Actual_Start_Date__c to accept input using date picker
        
        if(fromIssueDate != NULL && toIssueDate == NULL) {
            String updatedQuery = updateQuery(appQuery);
            appQuery = updatedQuery + ' Date_Issued__c = :fromIssueDate';  
        } else if (fromIssueDate != NULL && toIssueDate != NULL){
            String updatedQuery = updateQuery(appQuery);
            appQuery = updatedQuery + ' Date_Issued__c >= :fromIssueDate AND Date_Issued__c <= :toIssueDate';
        }
    }
    
    // Search values based on the filter
    public void searchValues() {
        //con = NULL;
        typeSet.clear();
        statusSet.clear();
        
        
         appQuery = deafaultQuery();        
         lstCertification = Database.query(appQuery + ' '+ label.Record_Limit); 
        try{
            con = new ApexPages.StandardSetController(Database.query(appQuery + ' '+  label.Record_Limit));                       
            System.debug('** searchresult  ==>'+ lstCertification.size());
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
                
    }
    
    // Search values based on the filter
    public void sortValues() {
        //con = NULL;
        system.debug('**sortValues: '+ sortExpression);
        searchValues();
    }
    
    // Clear filter values on the form
    public void clearValues() {
        con = NULL;
        builderName = NULL;
        CertificationNo = NULL;
        propertyOwner = NULL;
        fromPrice = NULL;
        fromPriceString = NULL;
        toPrice = NULL;
        fromIssueDate = NULL;
        toPriceString = NULL;
        toIssueDate = NULL;
        objCert = new Certificate__c();
        selectedStatus = NULL;
        selectedType = NULL;
        typeSet.clear();
        statusSet.clear();
        searchValues();
    }

    
     public PageReference export() {        

        PageReference pageRef = new PageReference(Page.CertificatesExport.getUrl());
        pageRef.getParameters().put('fullExport','False');
        pageRef.setRedirect(false);
        return pageRef;
    }

    public PageReference exportAll() {        
        PageReference pageRef = new PageReference(Page.CertificatesExport.getUrl());
        pageRef.getParameters().put('fullExport','TRUE');
        pageRef.setRedirect(false);
        return pageRef;
    }
    
}