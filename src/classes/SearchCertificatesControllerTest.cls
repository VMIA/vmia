/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The   class annotation indicates this class only contains test
 * methods. Classes defined with the   annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest 
private class SearchCertificatesControllerTest {

    private static final String EXPECTED_APP_QUERY = 'SELECT Id, Name, Builder__c, Builder__r.Name, Category_of_Building_Works__c, Status__c, Contract_Price__c, Date_Issued__c, Property_Address__c, Property_Owner__c, Property_Owner__r.Name, Conditions__c, Distributor__c, Policy_Number__c, Certificate_Type__c FROM Certificate__c WHERE Builder__c = :accId AND RecordType.Name !=: CERT_RECTYPE_CONTAINER AND RecordType.Name != :CERT_RECTYPE_Historical ORDER BY Name ASC NULLS FIRST';

    private static final String EXPECTED_APP_QUERY2 = 'SELECT Id, Name, Builder__c, Builder__r.Name, Category_of_Building_Works__c, Status__c, Contract_Price__c, Date_Issued__c, Property_Address__c, Property_Owner__c, Property_Owner__r.Name, Conditions__c, Distributor__c, Policy_Number__c, Certificate_Type__c FROM Certificate__c WHERE Builder__c = :accId AND RecordType.Name !=: CERT_RECTYPE_CONTAINER AND RecordType.Name != :CERT_RECTYPE_Historical ORDER BY Name ASC NULLS FIRST AND Category_of_Building_Works__c IN :typeSet AND Status__c IN :statusSet';
    
    private static final String EXPECTED_APP_QUERY3 = 'SELECT Id, Name, Builder__c, Builder__r.Name, Category_of_Building_Works__c, Status__c, Contract_Price__c, Date_Issued__c, Property_Address__c, Property_Owner__c, Property_Owner__r.Name, Conditions__c, Distributor__c, Policy_Number__c, Certificate_Type__c FROM Certificate__c WHERE Builder__c = :accId AND RecordType.Name !=: CERT_RECTYPE_CONTAINER AND RecordType.Name != :CERT_RECTYPE_Historical ORDER BY Name ASC NULLS FIRST AND Category_of_Building_Works__c IN :typeSet AND Status__c IN :statusSet AND Builder__r.Name LIKE \'%Test%\' AND Category_of_Building_Works__c IN :typeSet AND Status__c IN :statusSet';
    
    private static final String EXPECTED_APP_QUERY4 = 'SELECT Id, Name, Builder__c, Builder__r.Name, Category_of_Building_Works__c, Status__c, Contract_Price__c, Date_Issued__c, Property_Address__c, Property_Owner__c, Property_Owner__r.Name, Conditions__c, Distributor__c, Policy_Number__c, Certificate_Type__c FROM Certificate__c WHERE Builder__c = :accId AND RecordType.Name !=: CERT_RECTYPE_CONTAINER AND RecordType.Name != :CERT_RECTYPE_Historical ORDER BY Name ASC NULLS FIRST AND Category_of_Building_Works__c IN :typeSet AND Status__c IN :statusSet AND Builder__r.Name LIKE \'%Test%\' AND Category_of_Building_Works__c IN :typeSet AND Status__c IN :statusSet AND Builder__r.Name LIKE \'%Test%\' AND name = :CertificationNo AND Category_of_Building_Works__c IN :typeSet AND Status__c IN :statusSet';
    
    //variables
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static {
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
    }
    
    @testSetup
    static void init() {        
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor', accRecTypeMap.get('Distributor').getRecordTypeId());
        Account lexDistributor = TestUtility.createBusinessAccount('Lex Distributors', accRecTypeMap.get('Distributor').getRecordTypeId());
        Account texDistributor = TestUtility.createBusinessAccount('Tex Distributors', accRecTypeMap.get('Distributor').getRecordTypeId());

        List<Account> accList = new List<Account>();
        accList.add(rexDistributor);
        accList.add(lexDistributor);
        accList.add(texDistributor);
        Database.insert(accList);
    }
    
     
     static testmethod void testConstructor() {
        Account a= TestUtility.createBusinessAccount('Rex Home Distributor', accRecTypeMap.get('Distributor').getRecordTypeId());
        insert a;

        Contact c = New Contact(FirstName = 'Distributor', LastName = 'Test', AccountID = a.id,RBP_VBA_Number__c='DB-U 121231');
        insert c;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Distributor' LIMIT 1];
  
        User pageUser = new User(LastName = 'LIVESTON',
                     FirstName='JASON',
                     Alias = 'jliv',
                     Email = 'jason.liveston@asdf.com',
                     Username = 'jason.liveston@asdf.com',
                     ProfileId = profileId.id,
                     TimeZoneSidKey = 'GMT',
                     LanguageLocaleKey = 'en_US',
                     EmailEncodingKey = 'UTF-8',
                     LocaleSidKey = 'en_US',
                     ContactId = c.Id
                     
                     );

        database.insert(pageUser);          
        
        System.runAs(pageUser){
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        controller.selectedType = '[A],[B],[C]';
        controller.selectedStatus = '[D],[E],[F]';
        controller.builderName = 'Test';
        controller.CertificationNo = '123';
        controller.objCert = new Certificate__c();
        controller.objCert.Unit__c = 'address';
        controller.propertyOwner = 'test';
        controller.fromPriceString = '123';
        controller.toPriceString = '123';
        controller.fromIssueDate = System.Today();
        controller.toIssueDate = null;
        controller.CertificationNo = 'Test';
        insert new PolicyNumber__c(Prefix__c = '01');
        
        controller.deafaultQuery();
             
        System.assertNotEquals(null, controller.objCert);
        System.assertNotEquals(null, controller.statusSet);
        System.assertNotEquals(null, controller.typeSet);
        System.assertNotEquals(null, controller.setBuilders);
        }
    }
    
     
     static testmethod void testGetStandardSetController() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        ApexPages.StandardSetController con = controller.con;
        
        System.assertNotEquals(null, con);
        System.assertEquals(0, con.getResultSize());
        System.assertEquals(10, con.getPageSize());
        System.assertEquals('Name', controller.sortExpression);
        //System.assertEquals(EXPECTED_APP_QUERY, controller.appQuery);
        System.assertEquals(0, controller.totalPages);        
    }
    
     
     static testmethod void testUserType() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        controller.userType();
        
        System.assertEquals(null, controller.accId);
        //System.assertEquals('null WHERE Builder__c = :accId', controller.appQuery);
    }
    
     
     static testmethod void testGetlstCertification() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        List<Certificate__c> certs = controller.getlstCertification();
        
        System.assertNotEquals(null, certs);
        System.assertEquals(0, certs.size());
    }
    
     
     static testmethod void testGetCertStatus() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        List<SelectOption> options = controller.getCertStatus();
        
        System.assertNotEquals(null, options);
        //System.assertEquals(14, options.size());
    }
    
     
     static testmethod void testGetCertCategory() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        List<SelectOption> options = controller.getCertCategory();
        
        System.assertNotEquals(null, options);
        System.assertEquals(7, options.size());
    }
    
     
     static testmethod void testSortExpression() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        
        System.assertEquals('Name', controller.sortExpression);
        System.assertEquals('ASC', controller.sortDirection);
        
        controller.sortExpression = 'DESC';
        System.assertEquals('ASC', controller.sortDirection);
    }
    
     
     static testmethod void testHasNext() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        
        System.assertEquals(false, controller.hasNext);
    }
    
     
     static testmethod void testHasPrevious() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        
        System.assertEquals(false, controller.hasPrevious);
    }
    
     
     static testmethod void testPageNumber() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        
        System.assertEquals(1, controller.pageNumber);
    }
    
     
     static testmethod void testFirst() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        controller.first();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
        
        //Account accout = (Account)controller.con.getRecord();
        //System.assertEquals('Rex Home Distributor', account.Name);
        //System.assertEquals(accRecTypeMap.get('Distributor').getRecordTypeId(), account.RecordTypeId);
    }
    
     
     static testmethod void testLast() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        controller.last();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
    }
    
     
     static testmethod void testPrevious() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        controller.previous();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
    }
    
     
     static testmethod void testNext() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        controller.next();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
    }
    
     
     static testmethod void testConcel() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        controller.cancel();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
    }
    
     
     static testmethod void testUpdateQuery() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        String query = controller.updateQuery('SELECT ID FROM ACCOUNT');
        
        System.assertEquals('SELECT ID FROM ACCOUNT WHERE', query);
    }
    
     
     static testmethod void testGetSortDirection() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        
        System.assertEquals('ASC', controller.getSortDirection());
    }
    
     
     static testmethod void testSetSortDirection() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        controller.setSortDirection('ASC');
        
        System.assertEquals('ASC', controller.sortDirection);
    }
    
     
     static testmethod void testSetFilterCriteria() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        
        controller.selectedType = '[A],[B],[C]';
        controller.selectedStatus = '[D],[E],[F]';
        controller.appQuery = EXPECTED_APP_QUERY;
        controller.setFilterCriteria();
        
        System.assertEquals(3, controller.typeSet.size());
        System.assertEquals('A', new List<String>(controller.typeSet)[0]);
        System.assertEquals('B', new List<String>(controller.typeSet)[1]);
        System.assertEquals('C', new List<String>(controller.typeSet)[2]);
        
        System.assertEquals(3, controller.statusSet.size());
        System.assertEquals('D', new List<String>(controller.statusSet)[0]);
        System.assertEquals('E', new List<String>(controller.statusSet)[1]);
        System.assertEquals('F', new List<String>(controller.statusSet)[2]);
        
        System.assertEquals(null, controller.fromIssueDate);
        System.assertEquals(null, controller.toIssueDate);
        System.assertEquals(EXPECTED_APP_QUERY2, controller.appQuery);
        
        controller.builderName = 'Test';
        controller.setFilterCriteria();
        System.assertEquals(EXPECTED_APP_QUERY3, controller.appQuery);
        
        controller.CertificationNo = 'Test';
        insert new PolicyNumber__c(Prefix__c = '01');
        controller.setFilterCriteria();
        System.assertEquals(EXPECTED_APP_QUERY4, controller.appQuery);
    }
    
     
     static testmethod void testSearchValues() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        controller.searchValues();
        
        System.assertNotEquals(null, controller.con);
        System.assertEquals(0, controller.typeSet.size());
        System.assertEquals(0, controller.statusSet.size());
    }
    
     
     static testmethod void testSortValues() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        controller.sortValues();
        
        System.assertNotEquals(null, controller.con);
    }
    
     
     static testmethod void testClearValues() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
        controller.clearValues();
        
        System.assertNotEquals(null, controller.con);
        System.assertEquals(null, controller.selectedType);
        System.assertEquals(null, controller.selectedStatus);
        System.assertEquals(null, controller.builderName);
        System.assertEquals(null, controller.CertificationNo);
        System.assertEquals(null, controller.propertyOwner);
        System.assertEquals(null, controller.fromPrice);
        System.assertEquals(null, controller.toPrice);
        System.assertEquals(null, controller.fromIssueDate);
        System.assertEquals(null, controller.toIssueDate);        
        System.assertNotEquals(null, controller.objCert);
        System.assertEquals(0, controller.typeSet.size());
        System.assertEquals(0, controller.statusSet.size());
    }
    
    static testmethod void testExport() {
        ApexPages.StandardController stdController = null;
        SearchCertificatesController controller = new SearchCertificatesController(stdController);
                
        System.assertNotEquals(null, controller.export());
        System.assertNotEquals(null, controller.exportAll());
        
    }
    
     
     
}