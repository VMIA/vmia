public with sharing class SearchContactController {


    public String contactName {get;set;}    
    public String accountName {get;set;}
    public String selectedAccountType {get;set;}
    public String selectedBuilderRole {get;set;}
    public Set<String> builderRoleSet {get;set;}
    public Set<String> accTypeSet {get;set;}
    public Set<String> setBuilders {get; set;}

    Public String query {get;set;}
    public List<Contact> lstContact;
    Public list<Contact> completelstContact {get;set;}
    public static final Integer MAX_RECORDS_PER_PAGE = 10;
    Public String sortDirection = 'ASC';
    Public String sortExp = 'Name';
    public Decimal totalPages {get;set;}

    public Boolean disabledNextLast {get; set;}
    public Boolean disabledPrevFirst {get; set;}
    public string contactId{get;set;}
    public string accId{get;set;}
    


    // Constructor
    public SearchContactController(ApexPages.StandardController stdController) {
        lstContact = new List<Contact>();
        setContlr = new ApexPages.StandardSetController(Database.query(qry() + ' '+ label.Record_Limit));
        completelstContact = Database.query(qry() + ' '+ label.Record_Limit);
        System.debug('** excelquery  ==>'+ query + ' '+ label.Record_Limit);
        System.debug('** excelsearchresult  ==>'+ lstContact.size());

    }
    
    public void userType() {
        String userType = GlobalUtility.loggedInUser.Contact.RecordType.Name;
        accId = GlobalUtility.loggedInUser.Contact.AccountId; 
        String userProfile = GlobalUtility.loggedInUser.Profile.Name;
        contactId = GlobalUtility.loggedInUser.ContactId;
        
        setBuilders = new Set<String>();
        
        if(userProfile == Constants.Distributor) {
          // All of distributers builders
          for(Account a : [SELECT Id, Agent__c FROM Account WHERE Agent__c = :accId]) {
            setBuilders.add(a.Id);
          }
          query = query + ' WHERE AccountId IN :setBuilders';
        }else if (userProfile == Constants.Common_RBP){
            for (Contact_Role__c acr : 
                    [SELECT Account__c  
                     FROM Contact_Role__c 
                     WHERE Contact__c =: contactId AND (Role__c =: Constants.Primary_RBP OR Role__c =: Constants.EmployeeRole)]) {
                setBuilders.add(acr.Account__c);     
            }
            query = query + ' WHERE AccountId IN :setBuilders';
        } 
        else {
          // Only logged in builder
          query= query + ' WHERE AccountId = :accId';
        }
    }

    public string qry(){
        query = 'SELECT Id, Name, Builder_Role__c, Account.Name, Account.RecordType.Name,Legal_Entity_Name_Formula__c  FROM Contact';
        userType();
        setFilterCriteria();
        if(sortDirection == 'ASC') {
            query = query + ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS FIRST';
        } else {
            query = query + ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS LAST';
        }

        return query;
    }
    //sorting
    public String sortExpression {
        get{
            return sortExp;
        }
        set{
            if (value == sortExp){
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            }
            else{
                sortDirection = 'ASC';
                sortExp = value;
            }
        }
    }


    //set filter criteria
    public void setFilterCriteria() {
        if(!string.isblank(contactName)) {          
            query = updateQuery(query) + ' Name LIKE \'%'+String.escapeSingleQuotes(contactName )+'%\'';
        }          
        
        builderRoleSet = new Set<String>();
        if (selectedBuilderRole != NULL && selectedBuilderRole != ''){
            string bRole = selectedBuilderRole;
            bRole = bRole.replace('[','');
            bRole = bRole.replace(']','');
            string[] builderRoleVals = bRole.split(',');
            
            for(String s : builderRoleVals){
                builderRoleSet.add(s.trim());
            }
        }
        
         // Filter criteria - Builder Role 
        if(builderRoleSet.size() > 0) {
            String validType = NULL; 
            if(builderRoleSet.size() == 1) {
                for(string s : builderRoleSet) {
                    validType = s;
                }
            }
            
            if((validType != NULL && validType != '' && validType != 'None') || builderRoleSet.size() > 1) {                
                query = updateQuery(query) + ' Builder_Role__c IN :builderRoleSet';  
            }
        }

        if(!string.isblank(accountName)) {          
            query = updateQuery(query) + ' Account.Name LIKE \'%'+String.escapeSingleQuotes(accountName)+'%\'';
        }

        accTypeSet = new Set<String>();
        if (selectedAccountType != NULL && selectedAccountType != ''){
            string accType = selectedAccountType;
            accType = accType.replace('[','');
            accType = accType.replace(']','');
            string[] accountTypeVals = accType.split(',');
            
            for(String s : accountTypeVals){
                accTypeSet.add(s.trim());
            }
        }
        
         // Filter criteria - Builder Role 
        if(accTypeSet.size() > 0) {
            String validType = NULL; 
            if(accTypeSet.size() == 1) {
                for(string s : accTypeSet) { 
                    validType = s;
                }
            }
            
            if((validType != NULL && validType != '' && validType != 'None') || accTypeSet.size() > 1) {                
                query = updateQuery(query) + ' Account.RecordType.Name IN :accTypeSet';  
            }
        }
    }

    private String accType;
    // Get all Account Type
    public List<SelectOption> getAccountType() {
        List<SelectOption> accType = new List<SelectOption>();
        Schema.DescribeSObjectResult R = Account.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
        accType.add(new SelectOption('None','--None--'));

        for (Schema.RecordTypeInfo e : RT) {

            accType.add(new SelectOption(e.getRecordTypeId(), e.getName()));

        }             
        return accType;
    } 

    private String builderRole;
    // Get all Account Type
    public List<SelectOption> getBuilderRole() {
        List<SelectOption> builderRole = new List<SelectOption>();
        DescribeFieldResult d = Contact.Builder_Role__c.getDescribe();
        builderRole.add(new SelectOption('None','--None--'));

        for (PicklistEntry e : d.getPicklistValues()) {

            builderRole.add(new SelectOption(e.getValue(), e.getLabel()));

        }             
        return builderRole ;
    }

    // Updating the query for additional filter criteria
    public String updateQuery(String query) {
        if(query.contains('WHERE')) {
            query = query + ' AND';
        } else {
            query = query + ' WHERE';
        }   
        return query;
    }


    //StandardSet
    public ApexPages.StandardSetController setContlr {
        get {
            if(setContlr == null) {
                setContlr = new ApexPages.StandardSetController(lstContact);
            }
            Decimal results = setContlr.getResultSize();
            setContlr.setPageSize(MAX_RECORDS_PER_PAGE);
            totalPages = (results/MAX_RECORDS_PER_PAGE).round(System.RoundingMode.UP);

            return setContlr;
        }
        set;
    }

    //Search Function
    Public pageReference searchAction(){
        completelstContact = Database.query(qry());
        lstContact = Database.query(qry());
        try{
            setContlr = new ApexPages.StandardSetController(Database.query(qry()));
            setContlr.setPageSize(MAX_RECORDS_PER_PAGE);
            System.debug('** query  ==>'+ query);
            System.debug('** searchresult  ==>'+ lstContact.size());
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;

    }

    //Pagination
    // Returns the page number of the current page set
    public Integer pageNumber {
        get {
            return setContlr.getPageNumber();
        }
        set;
    }

    public List<Contact> getlstContact() {
        if(setContlr.getHasNext()) {
            disabledNextLast = true;
        } else {
            disabledNextLast = false;
        }

        if(setContlr.getHasPrevious()) {
            disabledPrevFirst = true;
        } else {
            disabledPrevFirst = false;
        }

        return setContlr.getRecords();
    }

    // Search values based on the filter
    public void sortValues() {
        setContlr = NULL;
        searchAction();
    }

    // Field that needs to be sorted
    public String getSortDirection() {
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }

    // Indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return setContlr.getHasNext();
        }
        set;
    }

    // Indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return setContlr.getHasPrevious();
        }
        set;
    }

    public void showFirstRec() {
        setContlr.first();
    }

    public void showPreviousRec() {
        setContlr.previous();
    }

    public void showNextRec() {
        setContlr.next();
    }

    public void showLastRec() {
        setContlr.last();
    }
    
     public void clearValues(){
        contactName = null;   
        accountName = null;
        selectedBuilderRole = null;
        selectedAccountType = null;
        searchAction();
    }


    //Export
    public PageReference export() {

        System.debug('** excelsearchresult  ==>'+ lstContact.size());
        PageReference pageRef = new PageReference(Page.ContactExport.getUrl());
        pageRef.getParameters().put('fullExport','False');
        pageRef.setRedirect(false);
        return pageRef;
    }

    public PageReference exportAll() {
        System.debug('** excelsearchresult  ==>'+ completelstContact.size());
        PageReference pageRef = new PageReference(Page.ContactExport.getUrl());
        pageRef.getParameters().put('fullExport','TRUE');
        pageRef.setRedirect(false);
        return pageRef;
    }

}