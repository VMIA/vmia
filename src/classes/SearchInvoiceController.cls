/*
  Date      :  07-Jun-2017
  Author      :   SMS Management & Technology
  Description    :  List of applications with a paginated view
*/

public with sharing class SearchInvoiceController {
    
    //Definitions
    public String InvoiceNumber {get;set;}
    public Invoice__c ObjInv {get;set;}
    Public String query {get;set;}
    @TestVisible private Set<String> setBuilders;
    @TestVisible private String sortExp = 'Name';
    @TestVisible private String sortDirection = 'ASC';
    @TestVisible private String accId;
    public Decimal totalPages {get;set;}
    
    Public String PurchaseUser {get;set;}
    public String InvRecType {get; set;}
    public static final Integer MAX_RECORDS_PER_PAGE = 10;
    public Boolean disabledNextLast {get; set;}
    public Boolean disabledPrevFirst {get; set;}
    Public list<Invoice__c> InvoiceList;
    public List<Invoice__c> completeInvoiceList {get;set;}
    
    
    public Date InvoiceDate {get;set;}
    Public String InvDate {get;set;}
    
    Public Date ToDate {get;set;}
    Public String TDate {get;set;}
    
   
    
    public SearchInvoiceController(ApexPages.StandardController stdController) {
        InvoiceNumber = NULL;
        objInv = new Invoice__c();
        setBuilders = new Set<String>(); 
        InvoiceList = new List<Invoice__c>();
        InvoiceSSC = new ApexPages.StandardSetController(Database.query(qry()));
        completeInvoiceList = Database.query(qry());
       
        
    }
    
    
    public string qry(){
        query = 'SELECT Id, Purchaser__r.Name, ';
        for(Schema.FieldSetMember field : Schema.sObjectType.Invoice__c.fieldsets.Invoice_Export.getFields()){
                    query += field.getFieldPath() + ',';
                }
            query = query.substring(0,query.lastIndexOf(','));
            query += ' FROM Invoice__c';
        
        setFilterCriteria();
        if(sortDirection == 'ASC') {
                    query = query + ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS FIRST';
                } else {
                    query = query + ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS LAST';
                }
        return query;
    }
    
    
    //sorting - Switching between acsending and descending modes
    public String sortExpression {
        get{
            return sortExp;
        }
        set{
            if (value == sortExp){
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            }
            else{
                sortDirection = 'ASC';
                sortExp = value;
            }
        }
    }
    
    // Set filter criteria on the query 
    public void setFilterCriteria() {
        //Filter on status
        String updateTheQuery = updateQuery(query);
        query = updateTheQuery + ' Status__c != \'Draft\' AND Status__c != \'Payment In Progress\'' ;
        // Filter criteria - Invoice Number
        if(InvoiceNumber != NULL) {
            String updatedQuery = updateQuery(query);
            query = updatedQuery + ' Name LIKE \'%'+String.escapeSingleQuotes(InvoiceNumber)+'%\'';
        }
        
        // Filter criteria - Invoice Type
        if(InvRecType != Null) {
          String updatedQuery = updateQuery(query);
          query = updatedQuery + ' Type__c LIKE \'%'+String.escapeSingleQuotes(InvRecType)+'%\'';  
        }
        
        //Filter criteria - Date of Builder Application
        
        ToDate =    ObjInv.Invoice_Date__c;
        
        system.debug ('ObjInv.PurchaseDate__c ==>'+ ObjInv.PurchaseDate__c );
        
        if(ObjInv.Invoice_Date__c != NULL && ObjInv.PurchaseDate__c == NULL) {
            String updatedQuery = updateQuery(query);
            
            InvoiceDate = date.newinstance(ObjInv.Invoice_Date__c .year(), ObjInv.Invoice_Date__c .month(), ObjInv.Invoice_Date__c .day());
            InvDate = String.valueof(InvoiceDate );
            query = updatedQuery + ' Invoice_Date__c = ' + InvDate;  
        }
        else if (ObjInv.Invoice_Date__c != NULL && ObjInv.PurchaseDate__c != NULL){
            String updatedQuery = updateQuery(query);
            InvoiceDate = date.newinstance(ObjInv.Invoice_Date__c .year(), ObjInv.Invoice_Date__c .month(), ObjInv.Invoice_Date__c .day());
            InvDate = String.valueof(InvoiceDate );
            ToDate = date.newinstance(ObjInv.PurchaseDate__c .year(), ObjInv.PurchaseDate__c .month(), ObjInv.PurchaseDate__c .day());
            TDate = String.valueof(ToDate );
            query = updatedQuery + ' Invoice_Date__c >= ' + InvDate + ' AND Invoice_Date__c <= ' + TDate;  
        }
        
        //Filter criteria - Date of Builder Application
        if(PurchaseUser != NULL ) {
            String updatedQuery = updateQuery(query);
            query = updatedQuery + ' Purchaser__r.name LIKE \'%'+String.escapeSingleQuotes(PurchaseUser)+'%\'';  
        }
    }
    
    // Updating the query for additional filter criteria
    public String updateQuery(String query) {
        if(query.contains('WHERE')) {
            query = query + ' AND';
        } else {
            query = query + ' WHERE';
        }   
        return query;
    }
    
    public ApexPages.StandardSetController InvoiceSSC {
        get {
            if(InvoiceSSC == null) {
                InvoiceSSC = new ApexPages.StandardSetController(InvoiceList);
            }
            Decimal results = InvoiceSSC.getResultSize();
            InvoiceSSC.setPageSize(MAX_RECORDS_PER_PAGE);
            totalPages = (results/MAX_RECORDS_PER_PAGE).round(System.RoundingMode.UP);

            return InvoiceSSC;
        }
        set;
    }
    
    
    //Search Function
    Public pageReference searchvalues(){
         completeInvoiceList = Database.query(qry());
         InvoiceList = Database.query(qry());
        try{
            InvoiceSSC = new ApexPages.StandardSetController(Database.query(qry()));
            InvoiceSSC.setPageSize(MAX_RECORDS_PER_PAGE);
            
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;
        
    }
    
    
    
    
    //Type Picklist
    public List<SelectOption> getInvoiceType() {
        List<SelectOption> InvType = new List<SelectOption>();
        DescribeFieldResult d = Invoice__c.Type__c.getDescribe();
        
        //InvType.add(new SelectOption('', '--None--'));
        for (PicklistEntry e : d.getPicklistValues()) {
            if (e.isActive()) {
                InvType.add(new SelectOption(e.getValue(), e.getLabel()));
            }
        }
        
        return InvType;
    }
    
    
    //Pagination
    // Returns the page number of the current page set
    public Integer pageNumber {
        get {
            return InvoiceSSC.getPageNumber();
        }
        set;
    }
    
    public List<Invoice__c> getInvoiceList() {
        if(InvoiceSSC.getHasNext()) {
            disabledNextLast = true;
        } else {
            disabledNextLast = false;
        }
        
        if(InvoiceSSC.getHasPrevious()) {
            disabledPrevFirst = true;
        } else {
            disabledPrevFirst = false;
        }
        
        return InvoiceSSC.getRecords();
    }
    
    // Search values based on the filter
    public void sortValues() {
        searchvalues();
    }
    
    // Field that needs to be sorted
    public String getSortDirection() {
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    
     // Indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return InvoiceSSC.getHasNext();
        }
        set;
    }
    
    // Indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return InvoiceSSC.getHasPrevious();
        }
        set;
    }
    
    public void showFirstRec() {
        InvoiceSSC.first();
    }
    
    public void showPreviousRec() {
       InvoiceSSC.previous();
    }

    public void showNextRec() {
       InvoiceSSC.next();
    }
    
    public void showLastRec() {
        InvoiceSSC.last();
    }
    
    //end of pagination
    
    
    
    
    
    //clear search filter attribute
    public void clearValues() {
        InvoiceNumber = NULL;
        InvRecType = NULL;
        objInv = new Invoice__c();
        ObjInv.Type__c = null;
        ObjInv.Invoice_Date__c = Null;
        ObjInv.PurchaseDate__c = Null;
        searchvalues();
    }
    
    
    
    
    //Export Current List to excel
    public PageReference export() { 
        PageReference pageRef = new PageReference(Page.InvoiceExport.getUrl());
        pageRef.getParameters().put('fullExport','FALSE');
        pageRef.setRedirect(false);
        return pageRef;
    }
    
    //Export Full List to excel
    public PageReference exportAll() { 
        PageReference pageRef = new PageReference(Page.InvoiceExport.getUrl());
        pageRef.getParameters().put('fullExport','TRUE');
        pageRef.setRedirect(false);
        return pageRef;
    }


}