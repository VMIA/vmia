/*
  Date      :  31-Jan-2017
  Author      :   SMS Management & Technology
  Description    :  List of applications with a paginated view
*/
public without sharing class SearchLetterController {
  
  // Definitions
  public String letterName {get;set;}
  public String letterType {get;set;}
  public BAT_Letter__c objLetter {get;set;}
  public Decimal totalPages {get;set;}
  public String selectedStatus {get;set;}
  @testVisible private String sortDirection = 'ASC';
  @testVisible private String sortExp = 'Name';
  @testVisible private String letterQuery;
  @testVisible private String letterStatus;
  public Date letterDate {get;set;}
  public Date toDateRange {get;set;}
  @testVisible List<BAT_Letter__c> lstLetter {get;set;}
  @testVisible private Set<String> typeSet;
  @testVisible private Set<String> setBuilders;
  @testVisible private String accId;
  @testVisible private String currentSOQL;
  public String subjectSearch {get;set;}
    
  // Constructor
  public SearchLetterController (ApexPages.StandardController stdController) {
    objLetter = new BAT_Letter__c();
    typeSet = new Set<String>();
    setBuilders = new Set<String>();  
  }

  public SearchLetterController () {
  }
  
  // Instantiate the StandardSetController from a query locator
  public ApexPages.StandardSetController con {
    get {
      if(con == NULL) {
        // Default field to sort - Name
        if(sortExpression == NULL) {
          sortExpression = 'Name';
        }

        // Defining the query based on the filter criteria
        letterQuery = 'SELECT Id, Name, letter_subject__c, createdDate, builder__c, builder__r.name, application__c, application__r.name, certificate__c, certificate__r.name FROM BAT_Letter__c';
        System.debug('here2++++++++++ The query : ' + letterQuery);
        userType();
        setFilterCriteria();
        
        if(sortDirection == 'ASC') {
          letterQuery = letterQuery + ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS FIRST';
        } else {
          letterQuery = letterQuery + ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS LAST';
        }
        currentSOQL = letterQuery;
        con = new ApexPages.StandardSetController(Database.Query(letterQuery));
        
        System.debug('++++++++++ The query : ' + letterQuery);
        
        // Sets the number of records in each page set
        Decimal results = con.getResultSize();
        Integer PageSize = 10;
        con.setPageSize(PageSize);
        
        // Determining the number of pages
        totalPages = (results/PageSize).round(System.RoundingMode.UP);
      }
      return con;
    }
    set;
  }
  
  // Retrieve application based on loggedin user
  public void userType() {
    String userType = GlobalUtility.loggedInUser.Contact.RecordType.Name;
    accId = GlobalUtility.loggedInUser.Contact.AccountId;
      
    if(userType == 'Distributor') {
      // All of distributers builders
      for(Account a : [SELECT Id, Agent__c FROM Account WHERE Agent__c = :accId]) {
        setBuilders.add(a.Id);
      }
      letterQuery = letterQuery + ' WHERE Builder__c IN :setBuilders';
    } else {
      // Only logged in builder
      letterQuery = letterQuery + ' WHERE Builder__c = :accId';
    }
  }
  
  // Returns a list of wrapper objects for the sObjects in the current page set
  public List<BAT_Letter__c> getlstLetters() {
    lstLetter = new List<BAT_Letter__c>();
    lstLetter = con.getRecords();
    return lstLetter;
  }
  
    // Get all application status

    //public List<SelectOption> getAppStatus() {
    //    List<SelectOption> AppStatus = new List<SelectOption>();
    //    DescribeFieldResult d = Application__c.Application_Status__c.getDescribe();
    //    AppStatus.add(new SelectOption('None','--None--'));
        
    //    for (PicklistEntry e : d.getPicklistValues()) {
    //        if (e.isActive()) {
    //            AppStatus.add(new SelectOption(e.getValue(), e.getLabel()));
    //        }   
    //    }             
    //    return AppStatus;
    //} 
    
  // Switching between acsending and descending modes
  public String sortExpression { get{
        return sortExp;
    } set {
    if (value == sortExp)
      sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
    else
      sortDirection = 'ASC';
      sortExp = value;
    }
  }
  
  // Indicates whether there are more records after the current page set.
  public Boolean hasNext {
    get {
      return con.getHasNext();
    }
    set;
  }

  // Indicates whether there are more records before the current page set.
  public Boolean hasPrevious {
    get {
      return con.getHasPrevious();
    }
    set;
  }

  // Returns the page number of the current page set
  public Integer pageNumber {
    get {
      return con.getPageNumber();
    }
    set;
  }

  // Returns the first page of records
  public void first() {
   con.first();
  }

  // Returns the last page of records
  public void last() {
   con.last();
  }

  // Returns the previous page of records
  public void previous() {
   con.previous();
  }

  // Returns the next page of records
  public void next() {
   con.next();
  }

  // Returns the PageReference of the original page, if known, or the home page.
  public void cancel() {
   con.cancel();
  }
  
  // Updating the query for additional filter criteria
  public String updateQuery(String query) {
    if(query.contains('WHERE')) {
      query = query + ' AND';
    } else {
      query = query + ' WHERE';
    }  
    return query;
  }
  
  // Field that needs to be sorted
  public String getSortDirection() {
    if (sortExpression == null || sortExpression == '')
    return 'ASC';
    else
    return sortDirection;
  }

  // Setting the direction ascending /descending for sorting
  public void setSortDirection(String value) { 
    sortDirection = value;
  }
  
  // Set filter criteria on the query 
  public void setFilterCriteria() {
    if(!String.isBlank(objLetter.letter_subject__c)) {
      String updatedQuery = updateQuery(letterQuery);
      letterQuery = updatedQuery + ' letter_subject__c LIKE \'%'+String.escapeSingleQuotes(objLetter.letter_subject__c)+'%\'';
    }
        
    letterDate =  objLetter.date_sent__c;
    toDateRange = objLetter.Hidden_to_date__c; // Using today's date to accept input using date picker
            
    // Filter criteria - Application Name / Number
    if(letterName != NULL && letterName != '') {
      String updatedQuery = updateQuery(letterQuery);
      letterQuery = updatedQuery + ' Name LIKE \'%'+String.escapeSingleQuotes(letterName)+'%\'';
    }
        
    // Filter criteria - Application Type
    //if(letterType != NULL && letterType != '') {
    //  String updatedQuery = updateQuery(letterQuery);
    //  letterQuery = updatedQuery + ' RecordType.Name LIKE \'%'+String.escapeSingleQuotes(letterType)+'%\'';  
    //}
        
    // Filter criteria - Application Status    
    //if(statusSet.size() > 0) {
    //  String validStatus = NULL; 
    //  if(statusSet.size() == 1) {
    //    for(string s : statusSet) {
    //      validStatus = s;
    //    }
    //  }
    //  if(validStatus != NULL && validStatus != '' || statusSet.size() > 1) {
        
    //    System.debug('the test : ' + statusSet);
    //    String updatedQuery = updateQuery(letterQuery);
    //    letterQuery = updatedQuery + ' Application_Status__c IN :statusSet';  
    //  }
    //}
        
    //Filter criteria - Date of Builder Application
    if(letterDate != NULL) {
      if (toDateRange != NULL){
        String updatedQuery = updateQuery(letterQuery);
        letterQuery = updatedQuery + ' createdDate >= :letterDate AND createdDate <= :toDateRange';
      }else{
        String updatedQuery = updateQuery(letterQuery);
        letterQuery = updatedQuery + ' createdDate = :letterDate';  
      }
    }else if (toDateRange != NULL){
      String updatedQuery = updateQuery(letterQuery);
      letterQuery = updatedQuery + ' createdDate <= :toDateRange';
    }
  }

  
  // Search values based on the filter
  public void searchValues() {
    con = NULL;
    typeSet.clear();
  }
  
  // Search values based on the filter
  public void sortValues() {
    con = NULL;
  }
  
  // Clear filter values on the form
  public void clearValues() {
    letterName = NULL;
    letterType = NULL;
    //objLetter.createdDate = NULL;
    letterDate = NULL;
    toDateRange = NULL;

    subjectSearch = NULL;
    typeSet.clear();
    con = NULL;
    objLetter = new BAT_Letter__c();
  }
  
}