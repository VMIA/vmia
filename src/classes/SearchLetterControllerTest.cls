/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SearchLetterControllerTest {

	//variables
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static {
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
    }
    
	@testSetup
	static void init() {		
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor', accRecTypeMap.get('Distributor').getRecordTypeId());
        Account lexDistributor = TestUtility.createBusinessAccount('Lex Distributors', accRecTypeMap.get('Distributor').getRecordTypeId());
        Account texDistributor = TestUtility.createBusinessAccount('Tex Distributors', accRecTypeMap.get('Distributor').getRecordTypeId());

		List<Account> accList = new List<Account>();
        accList.add(rexDistributor);
        accList.add(lexDistributor);
        accList.add(texDistributor);
        Database.insert(accList);
	}
	
	@isTest
    static void testDefaultConstructor() {
        SearchLetterController controller = new SearchLetterController();        
        System.assertNotEquals(null, controller);
    }
	
	@isTest
    static void testConstructor() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        
        System.assertNotEquals(null, controller.sortExpression);
        System.assertEquals(null, controller.letterQuery);
        System.assertEquals(null, controller.currentSOQL);
        System.assertEquals(null, controller.totalPages);
    }
    
    @isTest
    static void testUserType() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        controller.userType();
        
        System.assertEquals(null, controller.accId);
        System.assertEquals('null WHERE Builder__c = :accId', controller.letterQuery);
    }
    
    @isTest
    static void testGetlstLetters() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        List<BAT_Letter__c> letters = controller.getlstLetters();
        
        System.assertNotEquals(null, letters);
        System.assertEquals(0, letters.size());
    }
    
    @isTest
    static void testSortExpression() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        
        System.assertEquals('Name', controller.sortExpression);
        System.assertEquals('ASC', controller.sortDirection);
        
        controller.sortExpression = 'DESC';
        System.assertEquals('ASC', controller.sortDirection);
    }
    
    @isTest
    static void testHasNext() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        
        System.assertEquals(false, controller.hasNext);
    }
    
    @isTest
    static void testHasPrevious() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        
        System.assertEquals(false, controller.hasPrevious);
    }
    
    @isTest
    static void testPageNumber() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        
        System.assertEquals(1, controller.pageNumber);
    }
    
    @isTest
    static void testFirst() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        controller.first();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());        
    }
    
    @isTest
    static void testLast() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        controller.last();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
    }
    
    @isTest
    static void testPrevious() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        controller.previous();
        
		System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
    }
    
    @isTest
    static void testNext() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        controller.next();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
    }
    
    @isTest
    static void testConcel() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        controller.cancel();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
    }
    
    @isTest
    static void testUpdateQuery() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        String query = controller.updateQuery('SELECT ID FROM ACCOUNT');
        
        System.assertEquals('SELECT ID FROM ACCOUNT WHERE', query);
    }
    
    @isTest
    static void testGetSortDirection() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        
        System.assertEquals('ASC', controller.getSortDirection());
    }
    
    @isTest
    static void testSetSortDirection() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        controller.setSortDirection('ASC');
        
        System.assertEquals('ASC', controller.sortDirection);
    }
    
    @isTest
    static void testSetFilterCriteria() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        controller.objLetter.letter_subject__c = 'Test Subject';
        controller.letterQuery = 'SELECT Id FROM Account LIMIT 1';
        
        controller.letterName = 'Test Letter';
        controller.setFilterCriteria();        
        System.assertEquals('SELECT Id FROM Account LIMIT 1 WHERE letter_subject__c LIKE \'%Test Subject%\' AND Name LIKE \'%Test Letter%\'', 
        	controller.letterQuery);
        
        controller.letterDate = Date.parse('21/05/2017');
        controller.setFilterCriteria();        
        System.assertEquals('SELECT Id FROM Account LIMIT 1 WHERE letter_subject__c LIKE \'%Test Subject%\' AND Name LIKE \'%Test Letter%\' AND letter_subject__c LIKE \'%Test Subject%\' AND Name LIKE \'%Test Letter%\'', 
        	controller.letterQuery);
    }
    
    @isTest
    static void testSearchValues() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        controller.searchValues();
        
        System.assertNotEquals(null, controller.con);
        System.assertEquals(0, controller.typeSet.size());
    }
    
    @isTest
    static void testSortValues() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        controller.sortValues();
        
        System.assertNotEquals(null, controller.con);
    }
    
    @isTest
    static void testClearValues() {
        ApexPages.StandardController stdController = null;
        SearchLetterController controller = new SearchLetterController(stdController);
        controller.clearValues();
        
        System.assertNotEquals(null, controller.con);
        System.assertEquals(null, controller.letterName);
        System.assertEquals(null, controller.letterType);
        System.assertEquals(null, controller.letterDate);
        System.assertEquals(null, controller.toDateRange);
        System.assertEquals(0, controller.typeSet.size());
    }
}