@isTest
private class SearchPropertyOwnerController_Test {
	
	private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
	private static User pageUser;
    
	static {
    	accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
    }
    
    @testSetup
	static void init() {    
		Account a= TestUtility.createBusinessAccount('Rex Home Distributor', accRecTypeMap.get('Distributor').getRecordTypeId());
	    insert a;

	    Contact c = New Contact(FirstName = 'Distributor', LastName = 'Test', AccountID = a.id, RBP_VBA_Number__c = 'DB-U 123456');
	    insert c;

	    Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Distributor' LIMIT 1];

	    pageUser = new User(LastName = 'LIVESTON',
							FirstName='JASON',
							Alias = 'jliv',
							Email = 'jason.liveston@asdf.com',
							Username = 'jason.liveston@asdf.com',
							ProfileId = profileId.id,
							TimeZoneSidKey = 'GMT',
							LanguageLocaleKey = 'en_US',
							EmailEncodingKey = 'UTF-8',
							LocaleSidKey = 'en_US',
							ContactId = c.Id

							);

	    database.insert(pageUser);          
	}

    
    static void testConstructor() {
        // System.runAs(pageUser){        query = 'Select Id, Name,  Street_Full__c, Suburb__c, City__c, State__c, Postcode__c, Country__c from Property__c';

        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        
        System.assertNotEquals(null, controller.PStreet);
        System.assertNotEquals(null, controller.PSuburb);
        System.assertNotEquals(null, controller.PCity);
        System.assertNotEquals(null, controller.PState);
        System.assertNotEquals(null, controller.PPostcode);
        System.assertNotEquals(null, controller.PCountry);
        // }
    }
    
    @isTest
    static void testGetStandardSetController() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        ApexPages.StandardSetController PropertySSC = controller.PropertySSC;
        
        //System.assertNotEquals(null, PropertySSC);
        //System.assertEquals(0, PropertySSC.getResultSize());
        //System.assertEquals(10, PropertySSC.getPageSize());
        //System.assertEquals(0, controller.totalPages);        
    }
     @isTest
    static void testSortExpression() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        
        System.assertEquals('ASC', controller.sortDirection);
        
        controller.sortExpression = 'DESC';
        System.assertEquals('ASC', controller.sortDirection);
        
        controller.sortExpression = NULL;
        System.assertEquals('ASC', controller.sortDirection);
    }
    
    @isTest
    static void testHasNext() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        
        System.assertEquals(false, controller.hasNext);
    }
    
    @isTest
    static void testHasPrevious() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        
        System.assertEquals(false, controller.hasPrevious);
    }
    
    @isTest
    static void testPageNumber() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        
        System.assertEquals(1, controller.pageNumber);
    }
    
    @isTest
    static void testshowFirstRec() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        controller.showFirstRec();
        
        System.assertEquals(false, controller.PropertySSC.getHasPrevious());
        System.assertEquals(false, controller.PropertySSC.getHasNext());
        System.assertEquals(1, controller.PropertySSC.getPageNumber());
        
        
    }
    
    @isTest
    static void testshowLastRec() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        controller.showLastRec();
        
        System.assertEquals(false, controller.PropertySSC.getHasPrevious());
        System.assertEquals(false, controller.PropertySSC.getHasNext());
        System.assertEquals(1, controller.PropertySSC.getPageNumber());
    }
    
    @isTest
    static void testshowPreviousRec() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        controller.showPreviousRec();
        
        System.assertEquals(false, controller.PropertySSC.getHasPrevious());
        System.assertEquals(false, controller.PropertySSC.getHasNext());
        System.assertEquals(1, controller.PropertySSC.getPageNumber());
    }
    
    @isTest
    static void testshowNextRec() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        controller.showNextRec();
        
        System.assertEquals(false, controller.PropertySSC.getHasPrevious());
        System.assertEquals(false, controller.PropertySSC.getHasNext());
        System.assertEquals(1, controller.PropertySSC.getPageNumber());
        List<Property__c> getListProperty = controller.getPropertyList();
        system.assertNotEquals(getListProperty, null);
    }
    
    @isTest
    static void testCancel() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        controller.clearvalues();
        
        System.assertEquals(false, controller.PropertySSC.getHasPrevious());
        System.assertEquals(false, controller.PropertySSC.getHasNext());
        System.assertEquals(1, controller.PropertySSC.getPageNumber());
    }

    @isTest
    static void testGetPropertyList() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        List<Property__c> propertyList = controller.PropertyList;
        System.assertNotEquals(null, propertyList);
        System.assertEquals(0, propertyList.size());
    }

     @isTest
    static void testSearchValues() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        controller.searchproperties();
        System.assertNotEquals(null, controller.PropertySSC);
        
        
    }
    
    @isTest
    static void testSearchValues2() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        controller.PStreet = 'testStreet';
        controller.PSuburb = 'test';
        controller.PCity = 'test';
        controller.PState = 'VIC';
        controller.PPostcode = '3000';
        controller.PCountry = 'Australia';
        
        
        controller.searchproperties();
        
        System.assertNotEquals(null, controller.PropertySSC);    
        
    }
    
    
    @isTest
    static void testSortValues() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        controller.sortValues();
        
        System.assertNotEquals(null, controller.PropertySSC);
    }
    
    @isTest
    static void testClearValues() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        controller.clearValues();

    }
    
    @isTest
    static void testExport() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        PageReference pageReference = controller.export();
        
        //System.assertNotEquals(null, pageReference);
        //System.assertEquals('FALSE', pageReference.getParameters().get('fullExport'));
        //System.assertEquals(False, pageReference.getRedirect());
    }
    
    @isTest
    static void testExportAll() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        PageReference pageReference = controller.exportAll();
        
        System.assertNotEquals(null, pageReference);
        System.assertEquals('TRUE', pageReference.getParameters().get('fullExport'));
        System.assertEquals(False, pageReference.getRedirect());
    }

    @isTest
    static void testUpdateQuery() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        String query = controller.updateQuery('SELECT ID FROM PROPERTY__c');
        
        System.assertEquals('SELECT ID FROM PROPERTY__c WHERE', query);
    }
    
    @isTest
    static void testGetSortDirection() {
        ApexPages.StandardController stdController = null;
        SearchPropertyOwnerController controller = new SearchPropertyOwnerController(stdController);
        
        System.assertEquals('ASC', controller.getSortDirection());
    }
}