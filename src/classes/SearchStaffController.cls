/*
	Date			:	02-Feb-2017
	Author			: 	SMS Management & Technology
	Description		:	Displays the staff list for the distributor
	
					-	All contacts of record type distributor for the logged-in
					    user's accout count as the staff list for the distributor.
*/
public without sharing class SearchStaffController {
	
	// Definitions
	@testVisible private String appQuery;
	@testVisible private String sortExp = 'Name';
	@testVisible private String sortDirection = 'ASC';
	@testVisible private String contactType = 'Distributor';
	public Decimal totalPages {get;set;}
	@testVisible List<Contact> lstApplication {get;set;}
		
	// Constructor
	public SearchStaffController() {
	}
	
	// Instantiate the StandardSetController from a query locator
	public ApexPages.StandardSetController con {
		get {
			if(con == NULL) {
				// Default field to sort - Name
				if(sortExpression == NULL) {
					sortExpression = 'Name';
				}

				// Defining the query based on the filter criteria
				Id accid = GlobalUtility.loggedInUser.Contact.AccountId;
				appQuery = 'SELECT Id, Name, Email, Phone, Department, AccountID FROM Contact WHERE RecordType.Name = :contactType AND AccountID = :accid';
				
				if(sortDirection == 'ASC') {
					appQuery = appQuery + ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS FIRST';
				} else {
					appQuery = appQuery + ' ORDER BY ' + sortExpression  + ' ' + sortDirection + ' NULLS LAST';
				}
				
				con = new ApexPages.StandardSetController(Database.Query(appQuery));
				
				// Sets the number of records in each page set
				Decimal results = con.getResultSize();
				Integer PageSize = 10;
				con.setPageSize(PageSize);
				
				// Determining the number of pages
				totalPages = (results/PageSize).round(System.RoundingMode.UP);
			}
			return con;
		}
		set;
	}

	// Returns a list of wrapper objects for the sObjects in the current page set
	public List<Contact> getlstApplication() {
		lstApplication = new List<Contact>();
		lstApplication = con.getRecords();
		return lstApplication;
	}

	// Switching between acsending and descending modes
	public String sortExpression { get{
        return sortExp;
    } set {
		if (value == sortExp)
			sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
		else
			sortDirection = 'ASC';
			sortExp = value;
		}
	}
	
	// Indicates whether there are more records after the current page set.
	public Boolean hasNext {
		get {
			return con.getHasNext();
		}
		set;
	}

	// Indicates whether there are more records before the current page set.
	public Boolean hasPrevious {
		get {
			return con.getHasPrevious();
		}
		set;
	}

	// Returns the page number of the current page set
	public Integer pageNumber {
		get {
			return con.getPageNumber();
		}
		set;
	}

	// Returns the first page of records
	public void first() {
	 con.first();
	}

	// Returns the last page of records
	public void last() {
	 con.last();
	}

	// Returns the previous page of records
	public void previous() {
	 con.previous();
	}

	// Returns the next page of records
	public void next() {
	 con.next();
	}

	// Returns the PageReference of the original page, if known, or the home page.
	public void cancel() {
	 con.cancel();
	}
	
	// Field that needs to be sorted
	public String getSortDirection() {
    if (sortExpression == null || sortExpression == '')
		return 'ASC';
    else
		return sortDirection;
	}

	// Setting the direction ascending /descending for sorting
	public void setSortDirection(String value) { 
		sortDirection = value;
	}
	
	// Search values based on the filter
	public void sortValues() {
		con = NULL;
	}
}