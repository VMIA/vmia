/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SearchStaffControllerTest {

	//variables
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static {
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
    }
    
	@testSetup
	static void init() {		
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor', accRecTypeMap.get('Distributor').getRecordTypeId());
        Account lexDistributor = TestUtility.createBusinessAccount('Lex Distributors', accRecTypeMap.get('Distributor').getRecordTypeId());
        Account texDistributor = TestUtility.createBusinessAccount('Tex Distributors', accRecTypeMap.get('Distributor').getRecordTypeId());

		List<Account> accList = new List<Account>();
        accList.add(rexDistributor);
        accList.add(lexDistributor);
        accList.add(texDistributor);
        Database.insert(accList);
	}
	
	@isTest
    static void testDefaultConstructor() {
        SearchStaffController controller = new SearchStaffController();        
        System.assertNotEquals(null, controller);
    }
    
    @isTest
    static void testGetStandardSetController() {
        SearchStaffController controller = new SearchStaffController();
        ApexPages.StandardSetController con = controller.con;
        
        System.assertNotEquals(null, con);
        System.assertEquals(0, con.getResultSize());
        System.assertEquals(10, con.getPageSize());
        System.assertEquals('Name', controller.sortExpression);
        System.assertEquals('SELECT Id, Name, Email, Phone, Department, AccountID FROM Contact WHERE RecordType.Name = :contactType AND AccountID = :accid ORDER BY Name ASC NULLS FIRST', 
        	controller.appQuery);
        System.assertEquals(0, controller.totalPages);        
    }
    
    @isTest
    static void testGetlstApplication() {
        SearchStaffController controller = new SearchStaffController();
        List<Contact> contacts = controller.getlstApplication();
        
        System.assertNotEquals(null, contacts);
        System.assertEquals(0, contacts.size());
    }
    
    @isTest
    static void testSortExpression() {
        SearchStaffController controller = new SearchStaffController();
        
        System.assertEquals('Name', controller.sortExpression);
        System.assertEquals('ASC', controller.sortDirection);
        
        controller.sortExpression = 'DESC';
        System.assertEquals('ASC', controller.sortDirection);
    }
    
    @isTest
    static void testHasNext() {
        SearchStaffController controller = new SearchStaffController();
        
        System.assertEquals(false, controller.hasNext);
    }
    
    @isTest
    static void testHasPrevious() {
        SearchStaffController controller = new SearchStaffController();
        
        System.assertEquals(false, controller.hasPrevious);
    }
    
    @isTest
    static void testPageNumber() {
        SearchStaffController controller = new SearchStaffController();
        
        System.assertEquals(1, controller.pageNumber);
    }
    
    @isTest
    static void testFirst() {
        SearchStaffController controller = new SearchStaffController();
        controller.first();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
        
        //Account accout = (Account)controller.con.getRecord();
        //System.assertEquals('Rex Home Distributor', account.Name);
        //System.assertEquals(accRecTypeMap.get('Distributor').getRecordTypeId(), account.RecordTypeId);
    }
    
    @isTest
    static void testLast() {
        SearchStaffController controller = new SearchStaffController();
        controller.last();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
    }
    
    @isTest
    static void testPrevious() {
        SearchStaffController controller = new SearchStaffController();
        controller.previous();
        
		System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
    }
    
    @isTest
    static void testNext() {
        SearchStaffController controller = new SearchStaffController();
        controller.next();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
    }
    
    @isTest
    static void testConcel() {
        SearchStaffController controller = new SearchStaffController();
        controller.cancel();
        
        System.assertEquals(false, controller.con.getHasPrevious());
        System.assertEquals(false, controller.con.getHasNext());
        System.assertEquals(1, controller.con.getPageNumber());
    }
        
    @isTest
    static void testGetSortDirection() {
        SearchStaffController controller = new SearchStaffController();
        
        System.assertEquals('ASC', controller.getSortDirection());
    }
    
    @isTest
    static void testSetSortDirection() {
        SearchStaffController controller = new SearchStaffController();
        controller.setSortDirection('ASC');
        
        System.assertEquals('ASC', controller.sortDirection);
    }
    
    @isTest
    static void testSortValues() {
        SearchStaffController controller = new SearchStaffController();
        controller.sortValues();
        
        System.assertNotEquals(null, controller.con);
    }
 }