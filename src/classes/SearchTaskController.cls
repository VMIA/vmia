/**
  * Date         :  31-Jan-2017
  * Author       :  SMS Management & Technology
  * Description  :  List of Tasks with a paginated view
  */
public with sharing class SearchTaskController{

    public void sortValues() {
        searchTasks(true);         
    }

    
    //constants
    private static String COMMA = ',';
    private static String BUILDER_NAME = 'Builder__r.Name';
    private static String EMPTY_SPACE = '';
    private static String WHITE_SPACE = ' ';
    private static String NONE = '--None--';
    private static String QUERY_TSK_SELECT = 'SELECT ';
    private static String TSK_SUBJ_COI_APPROVED = 'COI Approved - Notify Builder';
    private static String TSK_SUBJ_COI_REJECTED = 'COI Rejected - Notify Builder';
    private static String TSK_SUBJ_REVIEW_APP = 'Review application created by builder';
    private static String QUERY_TSK_FROM = ' FROM Task ';
    private static String STRING_TYPE = 'String';
    private static String DATE_TYPE = 'Date';
    private static String SOQL_COUNT = ' count() ';
    private static String AND_OPERATOR = 'AND';
    private static String WHERE_CLAUSE = 'WHERE';
    private static String LIKE_PERCENT_START = ' LIKE \'%';
    private static String PERCENT_END = '%\'';
    private static String EQUALS_QUOTE_START = '= :';
    private static String QUOTE_END = '\'';
    private static String BUILDER_CHECK = BUILDER_NAME + LIKE_PERCENT_START + 'builderName' + PERCENT_END;
    private static String SUBJECT_CHECK = ' Subject IN: validSubj ';
    private static String STATUS_CHECK = ' Status IN: validStatus ';
    private static String CREATEDDATE_START_CHECK = ' DAY_ONLY(CreatedDate) >=: createdDateStart ';
    private static String CREATEDDATE_END_CHECK = ' DAY_ONLY(CreatedDate) <=: createdDateEnd ';
    private static String ACTIVITYDATE_START_CHECK = ' ActivityDate >=: activityDateStart ';
    private static String ACTIVITYDATE_END_CHECK = ' ActivityDate <=: activityDateEnd ';
    private static String DISTRIBUTOR_CHECK = ' Builder__r.Agent__c =: distributorId ';
    private static String SOQL_ORDER_OFFSET1 = ' ORDER BY ';
    private static String SOQL_ORDER_OFFSET2 = '  LIMIT : ROW_LIMIT OFFSET : offsetVal';
    private static Integer ROW_LIMIT = 10; //list_size
    
    //page variables
    public Date activityDateStart {get;set;}
    public Date activityDateEnd {get;set;}
    public Date createdDateStart {get;set;}
    public Date createdDateEnd {get;set;}
    public List<Task> taskList {get;set;}
    public Integer totalRows {get;set;} //total_size
    public Integer pageNumber {get;set;}
    public Integer currentPage {get;set;}
    public String builderName {get;set;}
    public List<String> tskSubject {get;set;}
    public List<String> tskStatus {get;set;}
    public id distId {get;set;}
    
    public String sortDirection {
        get {
            if (string.IsBlank(sortDirection)) { sortDirection = 'ASC'; }
            return sortDirection;
        }
        set;}
        
        
    //sorting
    Public String sortExp = 'ActivityDate';
    public String sortExpression {
        get{
            if (string.IsBlank(sortExp)) { sortExp = 'ActivityDate'; }
            
            return sortExp;
        }
        set{    
            System.debug('value: '+value);
            if (value == sortExp){
                sortDirection = (sortDirection == 'ASC') ? 'DESC' : 'ASC';
            }
            else{
                sortDirection = 'ASC';
                sortExp = value;
            }
            System.debug('sortExp: '+sortExp);
        }
    }
    
    
    // variables
    private Date activityDate;
    private Integer offsetVal; //counter
    private String totalRowsQuery;
    private Boolean onLoad;
    private String currentSOQL;
    private Id distributorId;
    private Set<String> validStatus;
    private Set<String> validSubj;
    
    // form picklist for subject field
    public List<SelectOption> getTaskSubject(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(EMPTY_SPACE,GlobalConstants.NONE_OPTION));
        options.add(new SelectOption(TSK_SUBJ_COI_APPROVED,TSK_SUBJ_COI_APPROVED));
        options.add(new SelectOption(TSK_SUBJ_COI_REJECTED,TSK_SUBJ_COI_REJECTED));
        options.add(new SelectOption(TSK_SUBJ_REVIEW_APP,TSK_SUBJ_REVIEW_APP));
        options.add(new SelectOption(Label.Task_Subject_Builder_Deadline,Label.Task_Subject_Builder_Deadline));
        options.add(new SelectOption(Label.Task_Subject_Builder_Departure,Label.Task_Subject_Builder_Departure));
        options.add(new SelectOption(Label.Task_Subject_Builder_Event,Label.Task_Subject_Builder_Event));
        options.add(new SelectOption(Label.Task_Subject_Builder_Outcome,Label.Task_Subject_Builder_Outcome));
        options.add(new SelectOption(Label.Task_Subject_DD_Review,Label.Task_Subject_DD_Review));
        options.add(new SelectOption(Label.Task_Subject_Refund_Success,Label.Task_Subject_Refund_Success));
        options.add(new SelectOption(Label.Task_Subject_Request_for_Information,Label.Task_Subject_Request_for_Information));
        options.add(new SelectOption(Label.Task_Subject_Welcome_Builder,Label.Task_Subject_Welcome_Builder));
        return options;
    }

    // form picklist for status field
    public List<SelectOption> getTaskStatus(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(EMPTY_SPACE,GlobalConstants.NONE_OPTION));
        for( Schema.PicklistEntry f : Task.Status.getDescribe().getPicklistValues()){
          options.add(new SelectOption(f.getValue(),f.getLabel()));
        } 
        return options;
    }
    
    // constructor
    public SearchTaskController(){
        taskList = new List<Task>();
        offsetVal = 0;
        totalRows = 0;
        pageNumber = 0;
        currentPage = 1;
        onLoad = true;
        if(GlobalUtility.isDistributor){
            distId = GlobalUtility.loggedInUser.AccountId;
        }
        //distributorId  = GlobalUtility.isDistributor ? GlobalUtility.loggedinUser.AccountId : null;
        searchTasks(true);  // display tasks on page load
    }
    
    /**
      * @description       This method is invoked from SearchTask page to fetch tasks based on the filters.
      * @param             NA 
      * @return            void
      * @throws            Method might throw Exception which is handled by try-catch block & appropriate 
                           error message displayed to end user
      */ 
    public void search(){
        offsetVal = 0;
        currentPage = 1;
        onLoad = false;
        searchTasks(true);  // simulate task search from button click
    }
    
    /**
      * @description       This method is invoked from search method & pagination methodsto fetch tasks based on the filters.
      * @param             NA 
      * @return            void
      * @throws            Method might throw Exception which is handled by try-catch block & appropriate 
                           error message displayed to end user
      */ 
    private void searchTasks(Boolean isButtonClick){
        try{
            String query = buildQuery();
            System.debug('** query ==>'+ query);
            System.debug('** totalRowsQuery ==>'+ totalRowsQuery);
                                    
            currentSOQL = query;
            taskList = Database.query(query);
            
            if(isButtonClick){
                totalRows = Database.countQuery(totalRowsQuery);
                //totalRows = taskList.size();
               
            }
            System.debug('** totalRows ==>'+ totalRows);
            pageNumber = Math.mod(totalRows,ROW_LIMIT) > 0 ? (Math.abs(totalRows/ROW_LIMIT) + 1) : Math.abs(totalRows/ROW_LIMIT);
            System.debug('** pageNumber ==>'+ pageNumber );
        }
        catch(Exception exp){          
            /* messsage updated by KATE LUU - SMS Brisbane - 12June2017*/  
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Error occurred, please try again later ' );
            ApexPages.addMessage(myMsg);      
            //ApexPages.addMessages(exp);      
        }
    }
    
    /**
      * @description       This method is invoked from searchTasks method to build dynamic SOQL for task based on the filter selected by user.
      * @param             NA 
      * @return            String
      * @throws            Method might throw Exception which is handled by calling method
      */ 
    private String buildQuery(){
        String query = QUERY_TSK_SELECT;
        totalRowsQuery = QUERY_TSK_SELECT + SOQL_COUNT;
        query += BUILDER_NAME + COMMA;
        for(Schema.FieldSetMember field : SObjectType.Task.FieldSets.Task_Search_Table.getFields()){
            query += field.getFieldPath() + COMMA;
        }
        query = query.substring(0,query.lastIndexOf(COMMA));
        query += QUERY_TSK_FROM;
        totalRowsQuery += QUERY_TSK_FROM;
        query = onLoad ? (query + WHITE_SPACE + SOQL_ORDER_OFFSET1 + sortExpression + WHITE_SPACE + sortDirection + SOQL_ORDER_OFFSET2) : setFilter(query) ;
        
        
        System.debug('buildQuery sortDirection: '+sortDirection);
        System.debug('buildQuery sortExpression: '+sortExpression);
        
        return query;
    }
    
    /**
      * @description       This method is invoked from buildQuery method to set filters to base query based on user input
      * @param             String - query 
      * @return            String
      * @throws            Method might throw Exception which is handled by calling method
      */ 
    private String setFilter(String query){
        String soqlFilter = EMPTY_SPACE ;
        System.debug('** Subject ==>'+ tskSubject);
        System.debug('** Status ==>'+ tskStatus);
        validSubj = new Set<String>();
        validStatus = new Set<String>();
        for(String sub : tskSubject){
            if(!EMPTY_SPACE.equalsIgnoreCase(sub)){
                validSubj.add(sub);
            }
        }
        
        for(String status : tskStatus){
            if(!EMPTY_SPACE.equalsIgnoreCase(status)){
                validStatus.add(status);
            }
        }
        System.debug('** validSubj ==>'+ validSubj);
        System.debug('** validSubj ==>'+ validSubj);
        soqlFilter = (String.isNotBlank(builderName)) ? 
                        !soqlFilter.containsIgnoreCase(WHERE_CLAUSE) ? 
                            (soqlFilter + WHITE_SPACE + WHERE_CLAUSE + WHITE_SPACE + BUILDER_NAME + LIKE_PERCENT_START + String.escapeSingleQuotes(builderName) + PERCENT_END + AND_OPERATOR + WHITE_SPACE) : 
                            (soqlFilter + BUILDER_NAME + LIKE_PERCENT_START + String.escapeSingleQuotes(builderName) + PERCENT_END + AND_OPERATOR + WHITE_SPACE) : soqlFilter;
        soqlFilter = (createdDateStart != null) ? 
                        !soqlFilter.containsIgnoreCase(WHERE_CLAUSE) ? 
                            (soqlFilter + WHITE_SPACE + WHERE_CLAUSE + CREATEDDATE_START_CHECK + AND_OPERATOR + WHITE_SPACE):
                            (soqlFilter + CREATEDDATE_START_CHECK + AND_OPERATOR + WHITE_SPACE) : soqlFilter;
        
        soqlFilter = (createdDateEnd != null) ? 
                        !soqlFilter.containsIgnoreCase(WHERE_CLAUSE) ? 
                            (soqlFilter + WHITE_SPACE + WHERE_CLAUSE + CREATEDDATE_END_CHECK + AND_OPERATOR + WHITE_SPACE):
                            (soqlFilter + CREATEDDATE_END_CHECK + AND_OPERATOR + WHITE_SPACE) : soqlFilter;
        
        soqlFilter = (activityDateStart != null) ? 
                        !soqlFilter.containsIgnoreCase(WHERE_CLAUSE) ? 
                            (soqlFilter + WHITE_SPACE + WHERE_CLAUSE + ACTIVITYDATE_START_CHECK + AND_OPERATOR + WHITE_SPACE):
                            (soqlFilter + ACTIVITYDATE_START_CHECK + AND_OPERATOR + WHITE_SPACE) : soqlFilter;

        soqlFilter = (activityDateEnd != null) ? 
                        !soqlFilter.containsIgnoreCase(WHERE_CLAUSE) ? 
                            (soqlFilter + WHITE_SPACE + WHERE_CLAUSE + ACTIVITYDATE_END_CHECK + AND_OPERATOR + WHITE_SPACE):
                            (soqlFilter + ACTIVITYDATE_END_CHECK + AND_OPERATOR + WHITE_SPACE) : soqlFilter;

        soqlFilter = (validSubj != null && !validSubj.isEmpty()) ? 
                        !soqlFilter.containsIgnoreCase(WHERE_CLAUSE) ? (soqlFilter + WHITE_SPACE + WHERE_CLAUSE + SUBJECT_CHECK + AND_OPERATOR + WHITE_SPACE) :
                        (soqlFilter + WHITE_SPACE + SUBJECT_CHECK + AND_OPERATOR + WHITE_SPACE) : soqlFilter;
        
        soqlFilter = (validStatus != null && !validStatus.isEmpty()) ? 
                        !soqlFilter.containsIgnoreCase(WHERE_CLAUSE) ? (soqlFilter + WHITE_SPACE + WHERE_CLAUSE + STATUS_CHECK + AND_OPERATOR + WHITE_SPACE) :
                        (soqlFilter + WHITE_SPACE + STATUS_CHECK + AND_OPERATOR + WHITE_SPACE) : soqlFilter;

        System.debug('** distributorId ==>'+ distributorId);
        soqlFilter = (distributorId != null) ? 
                        !soqlFilter.containsIgnoreCase(WHERE_CLAUSE) ? (soqlFilter + WHITE_SPACE + WHERE_CLAUSE + AND_OPERATOR + WHITE_SPACE) :
                        (soqlFilter + WHITE_SPACE + AND_OPERATOR + WHITE_SPACE) : soqlFilter;

        soqlFilter = soqlFilter.lastIndexOf(AND_OPERATOR) > 0 ? soqlFilter.substring(0,soqlFilter.lastIndexOf(AND_OPERATOR)) : soqlFilter;
        
        System.debug('sortDirection: '+sortDirection);
        System.debug('sortExpression: '+sortExpression);
        
        totalRowsQuery += soqlFilter; 
        System.debug('** totalRowsQuery ==>'+ totalRowsQuery);
        return (query + soqlFilter + SOQL_ORDER_OFFSET1 + sortExpression + WHITE_SPACE + sortDirection + SOQL_ORDER_OFFSET2);
    }
    
    /**
      * @description       This method is invoked from SearchTask page to navigate to first page in pagination
      * @param             NA 
      * @return            void
      * @throws            Method might throw Exception which is handled by searchTasks method
      */
    public void first(){
        offsetVal = 0;
        currentPage = 1;
        searchTasks(false);
    }
    
    /**
      * @description       This method is invoked from SearchTask page to navigate to next page in pagination
      * @param             NA 
      * @return            void
      * @throws            Method might throw Exception which is handled by searchTasks method
      */
    public void next(){
        //if(!gethasNext()){
            offsetVal += ROW_LIMIT;
            currentPage ++;
            searchTasks(false);
        //}
    }
    
    /**
      * @description       This method is invoked from SearchTask page to navigate to previous page in pagination
      * @param             NA 
      * @return            void
      * @throws            Method might throw Exception which is handled by searchTasks method
      */
    public void previous(){
        if(!gethasPrevious()){
            offsetVal -= ROW_LIMIT;
            currentPage --;
            searchTasks(false);
        }
    }
    
    /**
      * @description       This method is invoked from SearchTask page to navigate to last page in pagination
      * @param             NA 
      * @return            void
      * @throws            Method might throw Exception which is handled by searchTasks method
      */
    public void last(){
        //if(!gethasNext()){
            //Integer modVal = math.mod(totalRows,ROW_LIMIT);
            //offsetVal = modVal > 0 ? totalRows - math.mod(totalRows,ROW_LIMIT) : (offsetVal+ ROW_LIMIT);
            offsetVal = totalRows - math.mod(totalRows,ROW_LIMIT);
            System.debug('** offsetVal ==>'+ offsetVal);
            currentPage = pageNumber;
            searchTasks(false);
        //}
    }
    
    // return true if offset is zero
    public Boolean gethasPrevious(){
        return (offsetVal == 0);
    }
    
    // return true if totalRows is less than offset and limit
    public Boolean gethasNext(){
        return (offsetVal + ROW_LIMIT < totalRows);
    }
    
    public PageReference export(){
        PageReference pageRef = Page.ExportToExcel;
        pageRef.getParameters().put('query',currentSOQL);
        pageRef.getParameters().put('ROW_LIMIT',String.valueOf(ROW_LIMIT));
        pageRef.getParameters().put('offsetVal',String.valueOf(offsetVal));
        pageRef.getParameters().put('fieldSetName','Task_Search_Table');
        pageRef.getParameters().put('tskSubject',JSON.serialize(tskSubject));
        pageRef.getParameters().put('tskStatus',JSON.serialize(tskStatus));
        pageRef.getParameters().put('objectType','Task');
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference exportAll(){
        currentSOQL = currentSOQL.subString(0,currentSOQL.indexOf('LIMIT'));
        return export();
    }
}