/**
  * Date         :  29-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for SearchTaskController
  */ 
  
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
private with sharing class SearchTaskController_Test{
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> appRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup(){

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);

        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,
                                                        conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        
        Application__c app = TestUtility.createApplication(bornBuilder.Id,rexDistributor.Id,'New Eligibility',
                                                            appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
        app.External_Comments__c = 'DD/Builder comments';
        Database.insert(app);


    }

    @future
    private static void setupUser(){
        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(new List<User>{vmiaInternalUser,rexUser});
    }
    
    /**
      * @description       This method tests scenario of SearchTask page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testPageLoad(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.SearchTask;
            Test.setCurrentPage(pageRef);
            Test.startTest();
                SearchTaskController searchController  = new SearchTaskController();
                System.assert(!searchController.getTaskSubject().isEmpty());
                System.assert(!searchController.getTaskStatus().isEmpty());
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of SearchTask page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSearch(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.SearchTask;
            Test.setCurrentPage(pageRef);
            Test.startTest();
                SearchTaskController searchController  = new SearchTaskController();
                searchController.builderName = 'Born';
                searchController.tskSubject = new List<String>{'Review Referred COI Application','COI Rejected - Notify Builder','COI Approved - Notify Builder','VBA Number Required'};
                searchController.tskStatus = new List<String>{'Responded','Not Started','Completed'};
                searchController.createdDateStart = System.today().addDays(-2);
                searchController.createdDateEnd = System.today().addDays(2);
                searchController.activityDateStart = System.today().addDays(-2);
                searchController.activityDateEnd = System.today().addDays(2);
                searchController.search();
                System.assert(searchController.taskList.isEmpty());
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of SearchTask page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSearchByStatus(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.SearchTask;
            Test.setCurrentPage(pageRef);
            Test.startTest();
                SearchTaskController searchController  = new SearchTaskController();
                searchController.tskSubject = new List<String>();
                searchController.tskStatus = new List<String>{'Responded','Not Started','Completed'};
                searchController.search();
                System.assert(searchController.taskList.isEmpty());
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of SearchTask page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSearchByCreatedDateStart(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.SearchTask;
            Test.setCurrentPage(pageRef);
            Test.startTest();
                SearchTaskController searchController  = new SearchTaskController();
                searchController.tskSubject = new List<String>();
                searchController.tskStatus = new List<String>();
                searchController.createdDateStart = System.today().addDays(-2);
                searchController.search();
                System.assert(searchController.taskList.isEmpty());
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of SearchTask page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSearchByCreatedDateEnd(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.SearchTask;
            Test.setCurrentPage(pageRef);
            Test.startTest();
                SearchTaskController searchController  = new SearchTaskController();
                searchController.tskSubject = new List<String>();
                searchController.tskStatus = new List<String>();
                searchController.createdDateEnd = System.today().addDays(2);
                searchController.search();
                System.assert(searchController.taskList.isEmpty());
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of SearchTask page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSearchByActivityDateStart(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.SearchTask;
            Test.setCurrentPage(pageRef);
            Test.startTest();
                SearchTaskController searchController  = new SearchTaskController();
                searchController.tskSubject = new List<String>();
                searchController.tskStatus = new List<String>();
                searchController.activityDateStart = System.today().addDays(-2);
                searchController.search();
                System.assert(searchController.taskList.isEmpty());
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of SearchTask page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSearchByActivityDateEnd(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.SearchTask;
            Test.setCurrentPage(pageRef);
            Test.startTest();
                SearchTaskController searchController  = new SearchTaskController();
                searchController.tskSubject = new List<String>();
                searchController.tskStatus = new List<String>();
                searchController.activityDateEnd = System.today().addDays(2);
                searchController.search();
                System.assert(searchController.taskList.isEmpty());
            Test.stopTest();
        }
    }
    
    /**
      * @description       This method tests scenario of pagination in task search
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testPaginationMethods(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.SearchTask;
            Test.setCurrentPage(pageRef);
            SearchTaskController searchController  = new SearchTaskController();
            Test.startTest();
                System.assertEquals(false,searchController.gethasNext());
                System.assertEquals(true,searchController.gethasPrevious());
                System.assertEquals(0,searchController.pageNumber);
                searchController.first();
                searchController.last();
                searchController.previous();
                searchController.next();
            Test.stopTest();
        }
    }
    
    /**
      * @description       This method tests scenario of export
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testExport(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.SearchTask;
            Test.setCurrentPage(pageRef);
            Test.startTest();
                SearchTaskController searchController  = new SearchTaskController();
                searchController.export();
                searchController.exportAll();
            Test.stopTest();
        }
    }

    /**
      * @description       This method tests scenario of SearchTask page load
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testSearchWithoutInput(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];

        System.runAs(rexUser){
            PageReference pageRef = Page.SearchTask;
            Test.setCurrentPage(pageRef);
            Test.startTest();
                SearchTaskController searchController  = new SearchTaskController();
                searchController.search();
            Test.stopTest();
            System.assert(ApexPages.hasMessages());
        }
    }
}