/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class SendEmailTest {

    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> certRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> invRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> appRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> premiumRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
        
    static{
        for (Profile prof : [SELECT Name FROM Profile]) {
            profileMap.put(prof.Name,prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        premiumRecTypeMap = Schema.Sobjecttype.PremiumRatesTable__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup(){
        for (Profile prof : [SELECT Name FROM Profile]) {
            profileMap.put(prof.Name,prof.Id);
        }

        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        Database.insert(vmiaInternalUser);

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);

        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(rexUser);  

        System.runAs(rexUser) {         
            Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Partnership').getRecordTypeId());
            bornBuilder.Agent__c = rexDistributor.Id;
            bornBuilder.Agent_Contact__c = rexSmith.Id;         
            Database.insert(bornBuilder);
            
            Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
            Database.insert(tonyStark);
            
            Certificate__c  cert = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
            Database.insert(cert);
    
            Application__c  app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(),cert.Id);
            app.OwnerId = rexUser.Id ;
            app.RBP_Name__c = 'Tony Stark';
            app.New_Information_Entered__c = System.today()-5;
            app.Hidden_submitted__c= false;
            app.Requires_Approval__c = false;
            app.Application_Status__c = 'New';
            Database.insert(app);
        }      
    }
    
    @isTest
    static void testConstructor() {
        Account builder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
            
        PageReference pageRef = Page.GenerateInvoice;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', builder.Id);
        
        sendEmail controller = new sendEmail();
        
        System.assertNotEquals(null, controller.account);
        System.assertEquals(builder.Id, controller.account.Id);
        System.assertEquals(null, controller.body);
    }
    
    @isTest
    static void testGetAccount() {
        Account builder = [SELECT Id FROM Account LIMIT 1];
            
        PageReference pageRef = Page.GenerateInvoice;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', builder.Id);
        
        sendEmail controller = new sendEmail();
        
        System.assertNotEquals(null, controller.getAccount());
        System.assertEquals(builder.Id, controller.getAccount().Id);
    }
    
    @isTest
    static void testCreateTask() {
        User rexUser = [SELECT Id FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Application__c  app = [SELECT Id FROM Application__c LIMIT 1];
        
        Test.startTest();
        sendEmail.createTask(String.valueOf(app.Id), String.valueOf(rexUser.Id));
        Test.stopTest();
        
        Task task = [SELECT Id FROM Task LIMIT 1];
        
        System.assertNotEquals(null, task);        
    }
    
    @isTest
    static void testCreateCertTask() {
        User rexUser = [SELECT Id FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Certificate__c  cert = [SELECT Id FROM Certificate__c LIMIT 1];        
        
        Test.startTest();
        sendEmail.createCertTask(String.valueOf(cert.Id), String.valueOf(rexUser.Id), 'Test Subject', 'Test Desc');
        Test.stopTest();
        
        Task task = [SELECT Id FROM Task LIMIT 1];
        
        System.assertNotEquals(null, task);
    }
    
    @isTest
    static void testCreateNewTask() {
        Application__c  app = [SELECT Id FROM Application__c LIMIT 1];
        User rexUser = [SELECT Id FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        
        Test.startTest();
        sendEmail.createNewTask(String.valueOf(app.Id), String.valueOf(rexUser.Id), String.valueOf(bornBuilder.Id), 'Builder Legal Name', 'Requested Subject', 'Requested Description', 2);
        Test.stopTest();
        
        Task task = [SELECT Id FROM Task LIMIT 1];
        
        System.assertNotEquals(null, task);
    }
}