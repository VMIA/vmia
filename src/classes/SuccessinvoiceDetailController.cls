public without sharing class SuccessinvoiceDetailController {
    
    // constants
    private static String SEMI_COLON = ';';
    private static String ID_VAR = 'id';
    private static String PDF_FORMAT = '.pdf';
    private static String EMAIL_SUBJECT = 'Certificate of Insurance';
    private static String EMAIL_BODY = ' Hi, <br/> Your Certificate of Insurance application was SUCCESSFULLY COMPLETED. <br/> Please See Attachment in this email. ';
    
    public String invoiceId{get;set;}
    public string certificateId{get;set;}
    public string paytype;
    //public string invoiceNumber{get;set;}
    public static list<Certificate__c> cerlistForTech1{get;set;}
    public list<Certificate__c> cerlist{get;set;}
    public static list<Invoice__c> invList{get;set;}
    //public Boolean multipleCertificateFlag{get;set;}
    
   
    //public string builderrecordId{get;set;}
    public static string receiptNumber{get;set;}
    public static string paymentReference{get;set;}
    public string paymentAmount{get;set;}
    public static string summaryCode;
    public static string responseDescription ;
    public string builderName{get;set;}
    
   public static decimal premiumAmount =0;
   public static decimal gstAmount=0;
   public static decimal stampDuty=0;
   public static decimal totalAmount=0 ;

   public string errorMsg ;
   public Boolean isError;
   public static Boolean successfulPayment{get;set;}
   
   public String emailAddress {get;set;}
   public Boolean includeCerts {get;set;}
   public Boolean OBCertificate {get;set;}
   public Boolean showCertificate{get;set;}

    public SuccessinvoiceDetailController(){

        invoiceId= ApexPages.CurrentPage().getParameters().get('id');
        paytype= ApexPages.CurrentPage().getParameters().get('paytype');
        receiptNumber = ApexPages.CurrentPage().getParameters().get('receiptNumber');
        paymentReference = ApexPages.CurrentPage().getParameters().get('paymentReference');
        paymentAmount = ApexPages.CurrentPage().getParameters().get('paymentAmount');
        summaryCode = ApexPages.CurrentPage().getParameters().get('summaryCode');
        responseDescription = ApexPages.CurrentPage().getParameters().get('responseDescription');

        certificateId = '';
        builderName = '';
        showCertificate = false;

        isError = false;
        successfulPayment = false;
        OBCertificate = false;

        //multipleCertificateFlag = false ;
        cerlist = new list<Certificate__c>();
        cerlistForTech1 = new list<Certificate__c>();
       


          if(invoiceId != null ){
                invList = [Select id,Name,Adjustedby__c,Adjustedby__r.name, Premium__c,Stamp_Duty__c,GST__c,Total__c,Invoice_Date__c,PurchaseDate__c, createdBy.Name  from Invoice__c where id =: invoiceId] ;
          }
          System.debug('invList<><<'+invList);
          if(null != invList && !invList.isEmpty() && receiptNumber != null){
            invList[0].PurchaseDate__c =  System.today();
          }



          for(Certificate__c cer :[Select id,Name,Status__c, Category_of_Building_Works__c, Property_Address__c, InvoiceNo__c,InvoiceNo__r.Westpac_Reference_No__c,Policy_Number__c,Type__c,Certificate_Type__c,Total__c,
                                      Builder__c, Builder__r.Name, Builder__r.Primary_Email__c, Builder__r.Agent__r.name, PropertyAddressHidden__c,
                                      Date_Issued__c, Distributor__r.Name,
                                      (SELECT Property_Owner__r.Name, Contact__r.Name FROM PropertyOwners__r)
                                      from Certificate__c where InvoiceNo__c =: invoiceId and (Certificate_Type__c = 'Certificate' or Certificate_Type__c = 'OBCertificate')]){
             if(cer.Date_Issued__c == null && receiptNumber != null){
                cer.Status__c = 'Issued Works in Progress';
                cer.Date_Issued__c = system.Today();
              }
              if(cer.Status__c != GlobalConstants.STAT_ISSUED_HISTORICAL){
                showCertificate= true;
              }
              cerlist.add(cer);
              emailAddress = String.isBlank(emailAddress) ? cer.Builder__r.Primary_Email__c : emailAddress;
          }
          if(!cerList.isEmpty()){
              if(!cerList[0].PropertyOwners__r.isEmpty()){
                  if (cerList[0].PropertyOwners__r[0].Contact__r.Name != null){
                      builderName = cerList[0].PropertyOwners__r[0].Contact__r.Name;
                  } else if(cerList[0].PropertyOwners__r[0].Property_Owner__r.Name != null){
                      builderName = cerList[0].PropertyOwners__r[0].Property_Owner__r.Name;
                  }
              }
          }
          

          System.debug('cerlist>'+cerlist);

          if(!cerlist.isEmpty()){
            if(cerlist[0].Certificate_Type__c == GlobalConstants.CERT_TYPE_OWNER_BUILDER) {
              OBCertificate = true;
            } else {
              OBCertificate = false;
            }
              if(receiptNumber != null)
                cerlistForTech1.addAll(cerlist);
          }
        
      

    }
 
@future(callout=true)
public static void SendWelcomeLetter(String jsonString){

    List<Certificate__c> cerlistForTech1 = (List<Certificate__c>)JSON.deserialize(jsonString, List<Certificate__c>.class);
    System.debug('Inside the Send Welcome Letter');
    Set<id> cerId = new Set<Id>();
    System.debug('receiptNumber<><>'+receiptNumber);
  try{
     if(null != cerlistForTech1 && !cerlistForTech1.isEmpty() && receiptNumber != null){


      
      //List<Messaging.SingleEmailMessage>﻿ mails = new List<Messaging.SingleEmailMessage>﻿();
      list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();

       EmailTemplate objEmailTemplate=[select id, name from EmailTemplate where Name='Welcome Owner Pack'];

        EmailTemplate coiTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName =: Label.DBI_COI LIMIT 1];
        OrgWideEmailAddress orgAddress = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName =: Label.NO_REPLY_VMIA LIMIT 1];
                    

        for(Certificate__c retCert : [Select id, Builder__c,Policy_Number__c,Welcome_Pack_Sent__c,Property_Owner_Email__c, Property_Owner_Record_Id__c, Primary_Property_Owner_Id__c,Property_Owner_Address__c From Certificate__c
            Where Id IN :cerlistForTech1]){
          cerId.add(retCert.Id);
        if(retCert.Property_Owner_Email__c != NULL && retCert.Property_Owner_Address__c != null && !retCert.Welcome_Pack_Sent__c){

              System.debug('retCert.Id: '+retCert.Id);
              System.debug('retCert.Property_Owner_Record_Id__c: '+retCert.Property_Owner_Record_Id__c);
              List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();

              Messaging.EmailFileAttachment certAttachment = new Messaging.EmailFileAttachment();
              PageReference certPDF = Page.GenerateCertificate;
              certPDF.getParameters().put(ID_VAR,retCert.Id);
              certPDF.setRedirect(true);
              Blob certPDFContent ;
              if(!Test.isRunningTest()){
                 certPDFContent = certPDF.getContentAsPDF();
              }
              else{
                  certPDFContent = Blob.valueOf('UNIT.TEST'); //Added to facilitate unit testing
              }
              System.debug('certPDFContent<><>'+certPDFContent);
              certAttachment.setFileName(retCert.Policy_Number__c + PDF_FORMAT);
              certAttachment.setBody(certPDFContent);        
              emailAttachments.add(certAttachment);


              PageReference ref = Page.WelcomeLetter;
              ref.getParameters().put('Id', retCert.Id);
              ref.getParameters().put('POwnerid', retCert.Primary_Property_Owner_Id__c );
              ref.setRedirect(true);
              Blob b = ref.getContentAsPDF();

              Messaging.EmailFileAttachment efa1 = new Messaging.EmailFileAttachment();
              efa1.setFileName('WelcomeLetter.pdf');
              efa1.setBody(b);
              emailAttachments.add(efa1);

              

              System.debug('emailAttachments<>><'+emailAttachments);

              Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
              
              System.debug('retCert.Property_Owner_Email__c<>'+retCert.Property_Owner_Email__c);
              System.debug('retCert.objEmailTemplate<>'+objEmailTemplate.Id);
              
              email= Messaging.renderStoredEmailTemplate(objEmailTemplate.id,null,retCert.Builder__c);
              email.setToAddresses(new String[] { retCert.Property_Owner_Email__c });
              email.setFileAttachments(emailAttachments);
              email.setOrgWideEmailAddressId(orgAddress.Id);
              mails.add(email);
         }      

        }

        system.debug('mails<><>'+mails);
        Messaging.sendEmail(mails); 

        for(Certificate__c cert : cerlistForTech1){
            cert.Welcome_Pack_Sent__c = true;
        }
        update cerlistForTech1 ;
        //initAction(cerId);

      }

    }catch(Exception exp){
            System.debug('** exp ==>'+ exp.getstacktracestring());
          //  Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf('SuccessinvoiceDetailController.class'),
                         //      'SendWelcomeLetter',null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
 }

@future(callout=true)
    public static void  goToTechOne() {
    
    System.debug('Future Call to TechOne');
   
    list<Invoice__c> invoiceRectList = new list<Invoice__c>(); 
    successfulPayment = false;
    string premiumAmountCode;
    string gstCode;
    string totalAmountCode;
    string stampDutyCode;
      TechOneInfo__c techOneRecord = TechOneInfo__c.getValues(System.Label.TechOne);

      if(null != techOneRecord){
        premiumAmountCode = techOneRecord.PremiumAmount__c ; 
        gstCode = techOneRecord.GSTAmount__c ; 
        stampDutyCode = techOneRecord.StampDuty__c ; 
        totalAmountCode = techOneRecord.TotalAmount__c ; 
      }

     

    if(null != cerlistForTech1 && !cerlistForTech1.isEmpty()){

        for(Invoice__c inv: [select id, Tech1Response__c,Westpac_Reference_No__c,Premium__c,GST__c,Stamp_Duty__c,Total__c from Invoice__c where id =: cerlistForTech1[0].InvoiceNo__c]){
            invoiceRectList.add(inv);
        }
    }
   
       

if(null != summaryCode && summaryCode.equalsIgnoreCase('0') && null != invoiceRectList && !invoiceRectList.isEmpty()){ 
  /*
   invoiceRectList[0].WestpacResponse__c = 'Approved' ; 
   invoiceRectList[0].Westpac_Reference_No__c = receiptNumber ;    
   invoiceRectList[0].WestpacResponseMessage__c  = responseDescription ; 
   invoiceRectList[0].Status__c  = 'Sent To TechOne' ;     
   */
  successfulPayment = true;

  System.debug('First');
  System.debug('invoiceRectList[0]:::'+invoiceRectList[0]);

if(null !=invoiceRectList && (invoiceRectList[0].Tech1Response__c == null || invoiceRectList[0].Tech1Response__c == '' ||  invoiceRectList[0].Tech1Response__c == 'Not Initiated')){

System.debug('Second');
premiumAmount = -invoiceRectList[0].Premium__c ;
gstAmount = -(invoiceRectList[0].GST__c) ;
stampDuty = -(invoiceRectList[0].Stamp_Duty__c) ;
totalAmount = (invoiceRectList[0].Total__c) ;

string DocumentVar = paymentReference;
string DocumentRef1Var = paymentReference;
string DocumentRef2Var = receiptNumber ;



Datetime todayDateVar = system.today() ;
String dateOutput = todayDateVar.format('dd/MM/yyyy');
System.debug('dateOutput>>'+dateOutput);
System.debug('DocumentRef2Var>>'+DocumentRef2Var);
if(premiumAmount == null || gstAmount== null || stampDuty== null || totalAmount == null || DocumentVar ==null || DocumentRef2Var==null){

    invoiceRectList[0].Tech1Response__c = 'Not Initiated';    
   invoiceRectList[0].Tech1ResponseMessage__c = 'Internal Salesforce Error' ;
   invoiceRectList[0].Status__c  = 'Payment Complete' ;    
   System.debug('Third');
  }else {

    //Savepoint sp = Database.setSavepoint();
    System.debug('Fourth');

    HttpRequest req = new HttpRequest();
    
    req.setEndpoint('callout:Tech1Access'); 
    req.setMethod('POST');
    req.setTimeout(Integer.valueOf(Label.TechOne_Timeout));

  string ss = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://TechnologyOneCorp.com/T1.F1.Public/Services">'+
   '<soapenv:Header/>'+
   '<soapenv:Body>'+
      '<ser:DocumentFile_DoInsert>'+
         '<ser:Request PerformCommit="true">'+
            '<ser:Auth UserId="'+ 
                '{!HTMLENCODE($Credential.username)}' +
                '"'+
                ' Password="'+
                '{!HTMLENCODE($Credential.password)}' +
                '"'+
                ' Config="'+ Label.TechOne_Config_Name+'" FunctionName="$F1.MGT.DOCINSRT.WS"/>'+
                '<ser:DocumentFileItem ImportName="DBI.PAYT" FormatName="DBI.PAYT" VatType="I" >'+
                '<ser:Documents DocumentId="'+
                DocumentVar.right(8)+
                '"'+
                ' DocumentRef1="'+
                DocumentRef1Var+
                '"'+
                ' DocumentRef2="'+
                DocumentRef2Var+
                '"'+
                ' DocumentDate1="'+
                dateOutput+
                '"'+
                ' DocumentFileDate="'+
                dateOutput +
                '">'+
                '<ser:Lines LineSeqNbr="1" LedgerCode="GL" AccountNbr="'+
                premiumAmountCode+
                '" BaseAmount="'+
                premiumAmount+
                '" VatExcBaseAmount="'+
                premiumAmount +
                '" Narration1="purchase" VatRateCode="NA" VatType="E" />'+
                '<ser:Lines LineSeqNbr="2" LedgerCode="GL" AccountNbr="'+
                gstCode+
                '" BaseAmount="'+
                gstAmount+
                '" VatExcBaseAmount="'+
                gstAmount+
                '" Narration1="purchase" VatRateCode="NA" VatType="E"/>'+
                '<ser:Lines LineSeqNbr="3" LedgerCode="GL" AccountNbr="'+
                totalAmountCode+
                '" BaseAmount="'+
                totalAmount+
                '" VatExcBaseAmount="'+
                totalAmount+
                '" Narration1="purchase" VatRateCode="NA" VatType="E"/>'+
                '<ser:Lines LineSeqNbr="4" LedgerCode="GL" AccountNbr="'+
                stampDutyCode+
                '" BaseAmount="'+
                stampDuty+
                '" VatExcBaseAmount="'+
                stampDuty+
                '" Narration1="purchase" VatRateCode="G" VatType="I"/>'+
                 '</ser:Documents>'+
            '</ser:DocumentFileItem>'+
         '</ser:Request>'+
      '</ser:DocumentFile_DoInsert>'+
   '</soapenv:Body>'+
'</soapenv:Envelope>';


    system.debug('New Payment Request to Tech1'+ss); 
      
    
    req.setBody(ss);
   // req.setClientCertificateName('certest');
   // req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.setHeader('Content-Type', 'text/xml;charset=UTF-8');
    req.setHeader('Accept-Encoding', 'gzip,deflate');
    //req.setHeader('SOAPAction','""');
    
    System.debug('req>>'+req);
     System.debug('reqBody>>'+req.getbody());
    Http http = new Http();

        try{
          
            if(!Test.isRunningTest()){
                  HTTPResponse res = http.send(req);
                  System.debug('Here is your response:'+res.getBody());    
        
                  string responseStr = string.valueOf(res.getBody());
                  XMLParser.parseXml(responseStr,'NewPaymentToTech1');
                  
                  system.debug('isError>>>>>'+XMLParser.isError);
                  system.debug('errorMsg>>>>>'+XMLParser.errorMsg);                
            }

          if(XMLParser.isError){
              System.debug('Inside If');
             invoiceRectList[0].Tech1Response__c = 'UnSuccessful'; 
             invoiceRectList[0].Tech1ResponseMessage__c = XMLParser.errorMsg ;
             invoiceRectList[0].Status__c  = 'Payment Complete - Error Received from TechOne' ;
             invoiceRectList[0].PurchaseDate__c =  System.today();
          }else{
              System.debug('Inside Else');
              invoiceRectList[0].Tech1Response__c = 'Successful'; 
              invoiceRectList[0].Tech1ResponseMessage__c = 'Integration to Tech1 is Successful' ;
              invoiceRectList[0].Status__c  = 'Payment Complete - Received in TechOne Successfully' ;
              invoiceRectList[0].PurchaseDate__c =  System.today();
          }

          invoiceRectList[0].WestpacResponse__c = 'Approved' ; 
         invoiceRectList[0].Westpac_Reference_No__c = receiptNumber ;    
         invoiceRectList[0].WestpacResponseMessage__c  = responseDescription ; 
         invoiceRectList[0].PurchaseDate__c =  System.today();
          


        } catch(System.CalloutException e){
            invoiceRectList[0].Tech1Response__c = 'UnSuccessful';        
            invoiceRectList[0].Tech1ResponseMessage__c = string.valueOf(e) ;
            invoiceRectList[0].Status__c  = 'Payment Complete - Error in Sending To TechOne' ;
            invoiceRectList[0].WestpacResponse__c = 'Approved' ; 
            invoiceRectList[0].Westpac_Reference_No__c = receiptNumber ;    
            invoiceRectList[0].WestpacResponseMessage__c  = responseDescription;
            invoiceRectList[0].PurchaseDate__c =  System.today();

            update invoiceRectList[0] ;
            // Database.rollback(sp);
        }
            

          

    }
    
    //System.debug('invoiceRectList[0]::::'+invoiceRectList[0]);
    //update invoiceRectList[0] ;
 }
}else if(null != summaryCode && summaryCode.equalsIgnoreCase('1') && null != invoiceRectList) {
    invoiceRectList[0].WestpacResponse__c = 'Declined' ;
    invoiceRectList[0].Tech1Response__c = 'Not Initiated' ;
    invoiceRectList[0].Westpac_Reference_No__c = receiptNumber ;    
    invoiceRectList[0].WestpacResponseMessage__c  = responseDescription ;  
    invoiceRectList[0].Status__c  = 'Payment Failed' ; 
} else if(null != summaryCode && summaryCode.equalsIgnoreCase('2') && null != invoiceRectList){
    invoiceRectList[0].WestpacResponse__c = 'Error' ;
    invoiceRectList[0].Westpac_Reference_No__c = receiptNumber ;    
   invoiceRectList[0].WestpacResponseMessage__c  = responseDescription ;   
   invoiceRectList[0].Tech1Response__c = 'Not Initiated' ;
   invoiceRectList[0].Status__c  = 'Payment Failed' ;
  }else if(null != summaryCode && summaryCode.equalsIgnoreCase('3') && null != invoiceRectList){
    invoiceRectList[0].WestpacResponse__c = 'Rejected' ;
    invoiceRectList[0].Westpac_Reference_No__c = receiptNumber ;    
   invoiceRectList[0].WestpacResponseMessage__c  = responseDescription ;   
   invoiceRectList[0].Tech1Response__c = 'Not Initiated' ;
   invoiceRectList[0].Status__c  = 'Payment Failed' ;
  }


if(!invoiceRectList.isEmpty())
  update invoiceRectList ;
  
  if(!cerlistForTech1.isEmpty()){
    List<Database.SaveResult> results = Database.update(cerlistForTech1,false) ;
    if(results[0].isSuccess()){
        //ndWelcomeLetter(cerlistForTech1);
    }
    
    
  }
  


 }

    /**
      * @description       This method is invoked from Invoice detail page to send copy of certificates to customer
      * @param             NA 
      * @return            void
      * @throws            Method might thow exception which is handled by try-catch block
      */ 
    public void sendCopyOfCertificatesAndInvoice(){
        try{
            System.debug('** invoiceId ==>'+ invoiceId);
            System.debug('** cerList ==>'+ cerList);

            if(String.isNotBlank(emailAddress)){
                List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
                if(cerList != null && !cerList.isEmpty()){
                    Messaging.EmailFileAttachment certAttachment;
                    Blob certPDFContent;
                    for(Certificate__c cert : cerList){
                        PageReference certPDF = Page.GenerateCertificate;
                        certPDF.getParameters().put(ID_VAR,cert.Id);
                        certPDF.setRedirect(true);
                        if(!Test.isRunningTest()){
                            certPDFContent = certPDF.getContentAsPDF();
                        }
                        else{
                            certPDFContent = Blob.valueOf('UNIT.TEST'); //Added to facilitate unit testing
                        }
                        certAttachment = new Messaging.EmailFileAttachment();
                        certAttachment.setFileName(cert.Policy_Number__c + PDF_FORMAT);
                        certAttachment.setBody(certPDFContent);
                        emailAttachments.add(certAttachment);
                    }
                    
                    if(includeCerts){   // send invoice if selected
                        invList = [SELECT Name,Invoice_Date__c,Premium__c,Stamp_Duty__c,GST__c,Total__c,PurchaseDate__c,Purchaser__c 
                                    FROM Invoice__c WHERE Id =: invoiceId LIMIT 1];
                        // generate invoice pdf
                        PageReference invoicePDF = Page.GenerateInvoice;
                        invoicePDF.getParameters().put(ID_VAR,invoiceId);
                        invoicePDF.setRedirect(true);
                        Blob invoicePDFContent;
                        if(!Test.isRunningTest()){
                            invoicePDFContent = invoicePDF.getContentAsPDF();
                        }
                        else{
                            invoicePDFContent = Blob.valueOf('UNIT.TEST'); //Added to facilitate unit testing
                        }
                        Messaging.EmailFileAttachment invoiceAttachment = new Messaging.EmailFileAttachment();
                        invoiceAttachment.setFileName(invList.get(0).Name + PDF_FORMAT);
                        invoiceAttachment.setBody(invoicePDFContent);
                        emailAttachments.add(invoiceAttachment);
                    }
                    EmailTemplate coiTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName =: Label.DBI_COI LIMIT 1];
                    OrgWideEmailAddress orgAddress = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName =: Label.NO_REPLY_VMIA LIMIT 1];
                    // send email to user
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    email = Messaging.renderStoredEmailTemplate(coiTemplate.Id,null,invList.get(0).Purchaser__c);
                    email.setFileAttachments(emailAttachments);
                    email.setToAddresses(new List<String>(emailAddress.split(SEMI_COLON)));
                    email.setOrgWideEmailAddressId(orgAddress.Id);

                    Messaging.SendEmailResult [] emailResults = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); 
                    for(Messaging.SendEmailResult result : emailResults){
                        if(result.isSuccess()){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.Email_Success));
                        }
                    }
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Enter_Email_Address));
            }
        }
        catch(Exception exp){
            System.debug('** exp ==>'+ exp.getstacktracestring());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Generic_ERROR));
        }
    }
    
}