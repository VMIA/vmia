/**
* Date         :  24-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for SuccessinvoiceDetailController controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class SuccessinvoiceDetailController_Test {
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        setupData();
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
        
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);
        
        Invoice__c inv = TestUtility.createInvoice(bornBuilder.Id,'New',invRecTypeMap.get('New Invoice').getRecordTypeId());
        inv.Premium__c = 10000.00;
        Database.insert(inv);
        
        Certificate__c  cert = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
        cert.Status__c = GlobalConstants.STAT_REFERRED;
        cert.InvoiceNo__c = inv.Id;
        cert.Certificate_Type__c = 'Certificate';
        Database.insert(cert);
        System.debug('** Certificate ==>'+ cert.id);  
        
        Application__c  app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(),cert.Id);
        Database.insert(app);
        System.debug('** Application ==>'+ app);        
        
        Property__c prop = new Property__c(Builder__c = bornBuilder.Id, Suburb__c='Dockland');
        Database.insert(prop);
        System.debug('** Property ==>'+ prop.id);
        
        Property_Owners__c propOwn = new Property_Owners__c(Property__c = prop.id, Certificate__c = cert.id, property_Owner__c=rexDistributor.Id);
        Database.insert(propOwn);
        System.debug('** Property Owner ==>'+ propOwn.id);        
        
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(rexUser); 
        /*
List<Certificate__c> cerList = [Select id,Name, Category_of_Building_Works__c, Property_Address__c, InvoiceNo__c,Policy_Number__c,Type__c,Certificate_Type__c,Total__c,
Builder__c, Builder__r.Name, Builder__r.Trading_Name__c, Builder__r.Primary_Email__c, Builder__r.Agent__r.name, PropertyAddressHidden__c,
Date_Issued__c, Distributor__r.Name,
(SELECT Property_Owner__r.Name, Contact__r.Name FROM PropertyOwners__r)
from Certificate__c where InvoiceNo__c =: inv.id and (Certificate_Type__c = 'Certificate' or Certificate_Type__c = 'OBCertificate')];
System.debug('Cert Prop Owner Contact: '+cerList[0].PropertyOwners__r[0].Contact__r.Name);*/
        
        
    } 
    
    public static testMethod void goToTechOneApprovedTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Invoice__c invoice = [SELECT Id FROM Invoice__c WHERE Purchaser__c =: bornBuilder.Id LIMIT 1];
        
        System.runAs(rexUser){
            PageReference pageRef = Page.InvoiceDetail;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters
            System.currentPageReference().getParameters().put('id', invoice.Id);
            System.currentPageReference().getParameters().put('paytype', 'multi');
            System.currentPageReference().getParameters().put('receiptNumber', 'invoice123');
            System.currentPageReference().getParameters().put('paymentReference', 'pay123');
            System.currentPageReference().getParameters().put('paymentAmount', '10000.00');
            System.currentPageReference().getParameters().put('summaryCode', '0'); //Identifies a success scenario
            System.currentPageReference().getParameters().put('responseDescription', 'payment success');
            Test.startTest();
            SuccessinvoiceDetailController controller = new SuccessinvoiceDetailController();
            controller.emailAddress = 'abc@xyz.com';
            controller.includeCerts = true;
            SuccessinvoiceDetailController.goToTechOne();
            controller.sendCopyOfCertificatesAndInvoice();
            Test.stopTest();
        }
    }
    
    
    public static testMethod void goToTechOneApprovedWithoutEmailAddressTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Invoice__c invoice = [SELECT Id FROM Invoice__c WHERE Purchaser__c =: bornBuilder.Id LIMIT 1];
        
        System.runAs(rexUser){
            PageReference pageRef = Page.InvoiceDetail;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters
            System.currentPageReference().getParameters().put('id', invoice.Id);
            System.currentPageReference().getParameters().put('paytype', 'multi');
            System.currentPageReference().getParameters().put('receiptNumber', 'invoice123');
            System.currentPageReference().getParameters().put('paymentReference', 'pay123');
            System.currentPageReference().getParameters().put('paymentAmount', '10000.00');
            System.currentPageReference().getParameters().put('summaryCode', '0'); //Identifies a success scenario
            System.currentPageReference().getParameters().put('responseDescription', 'payment success');
            Test.startTest();
            SuccessinvoiceDetailController controller = new SuccessinvoiceDetailController();
            controller.includeCerts = true;
            SuccessinvoiceDetailController.goToTechOne();
            controller.sendCopyOfCertificatesAndInvoice();
            System.assert(Apexpages.hasMessages() && Apexpages.getMessages()[0].getSummary().contains(Label.Enter_Email_Address), 'There should be a pageMessage advising to enter an email address.');
            Test.stopTest();
        }
    }    
    
    public static testMethod void goToTechOneApprovedWithoutReceiptNumberTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Invoice__c invoice = [SELECT Id FROM Invoice__c WHERE Purchaser__c =: bornBuilder.Id LIMIT 1];
        
        Certificate__c cert = [Select id, Certificate_Type__c from Certificate__c where Builder__c =: bornBuilder.Id limit 1]; 
        cert.Certificate_Type__c = GlobalConstants.CERT_TYPE_OWNER_BUILDER;
        Database.update(cert);
        
        System.runAs(rexUser){
            PageReference pageRef = Page.InvoiceDetail;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters
            System.currentPageReference().getParameters().put('id', invoice.Id);
            System.currentPageReference().getParameters().put('paytype', 'multi');
            System.currentPageReference().getParameters().put('paymentReference', 'pay123');
            System.currentPageReference().getParameters().put('paymentAmount', '10000.00');
            System.currentPageReference().getParameters().put('summaryCode', '0'); //Identifies a success scenario
            System.currentPageReference().getParameters().put('responseDescription', 'payment success');
            Test.startTest();
            SuccessinvoiceDetailController controller = new SuccessinvoiceDetailController();
            System.assertEquals(null, SuccessinvoiceDetailController.receiptNumber);
            controller.emailAddress = 'abc@xyz.com';
            controller.includeCerts = true;
            SuccessinvoiceDetailController.goToTechOne();
            controller.sendCopyOfCertificatesAndInvoice();
            Test.stopTest();
        }
    }    
    
    public static testMethod void goToTechOneDeclinedTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Invoice__c invoice = [SELECT Id FROM Invoice__c WHERE Purchaser__c =: bornBuilder.Id LIMIT 1];
        
        System.runAs(rexUser){
            PageReference pageRef = Page.InvoiceDetail;
            Test.setCurrentPage(pageRef); 
            
            //Set url parameters
            System.currentPageReference().getParameters().put('id', invoice.Id);
            System.currentPageReference().getParameters().put('paytype', 'multi');
            System.currentPageReference().getParameters().put('receiptNumber', 'invoice123');
            System.currentPageReference().getParameters().put('paymentReference', 'pay123');
            System.currentPageReference().getParameters().put('paymentAmount', '10000.00');
            System.currentPageReference().getParameters().put('summaryCode', '1'); //Identifies a declined scenario
            System.currentPageReference().getParameters().put('responseDescription', 'payment success');
            Test.startTest();
            SuccessinvoiceDetailController controller = new SuccessinvoiceDetailController();
            SuccessinvoiceDetailController.goToTechOne();
            Test.stopTest();
        }
    }
    
    public static testMethod void goToTechOneErrorTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Invoice__c invoice = [SELECT Id FROM Invoice__c WHERE Purchaser__c =: bornBuilder.Id LIMIT 1];
        
        Property_Owners__c propOwner = [Select Id from Property_Owners__c LIMIT 1];
        Contact con = [Select Id from Contact where AccountId = :bornBuilder.Id LIMIT 1];
        propOwner.Contact__c = con.Id;
        Database.update(propOwner);
        
        System.runAs(rexUser){
            PageReference pageRef = Page.InvoiceDetail;
            Test.setCurrentPage(pageRef); 
            
            //Set url parameters
            System.currentPageReference().getParameters().put('id', invoice.Id);
            System.currentPageReference().getParameters().put('paytype', 'multi');
            System.currentPageReference().getParameters().put('receiptNumber', 'invoice123');
            System.currentPageReference().getParameters().put('paymentReference', 'pay123');
            System.currentPageReference().getParameters().put('paymentAmount', '10000.00');
            System.currentPageReference().getParameters().put('summaryCode', '2'); //Identifies a Error scenario
            System.currentPageReference().getParameters().put('responseDescription', 'payment success');
            Test.startTest();
            SuccessinvoiceDetailController controller = new SuccessinvoiceDetailController();
            SuccessinvoiceDetailController.goToTechOne();
            Test.stopTest();
        }
    }  
    
    public static testMethod void goToTechOneRejectedTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Invoice__c invoice = [SELECT Id FROM Invoice__c WHERE Purchaser__c =: bornBuilder.Id LIMIT 1];
        System.runAs(rexUser){
            PageReference pageRef = Page.InvoiceDetail;
            Test.setCurrentPage(pageRef);
            
            //Set url parameters			
            System.currentPageReference().getParameters().put('id', invoice.Id);
            System.currentPageReference().getParameters().put('paytype', 'multi');
            System.currentPageReference().getParameters().put('receiptNumber', 'invoice123');
            System.currentPageReference().getParameters().put('paymentReference', 'pay123');
            System.currentPageReference().getParameters().put('paymentAmount', '10000.00');
            System.currentPageReference().getParameters().put('summaryCode', '3'); //Identifies a Rejected scenario
            System.currentPageReference().getParameters().put('responseDescription', 'payment success');
            Test.startTest();			
            SuccessinvoiceDetailController controller = new SuccessinvoiceDetailController();
            SuccessinvoiceDetailController.goToTechOne();
            Test.stopTest();
        }
    }     
}