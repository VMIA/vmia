/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Helper Class for Task trigger
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public with sharing class TaskTriggerHelper{
    
    // variables
    private static Boolean beforeUpdateFirstRun = true;
    private static Boolean afterUpdateFirstRun = true;
    
    public static Boolean isBeforeUpdateEventFirstRun(){
        Boolean retVal = beforeUpdateFirstRun;
        beforeUpdateFirstRun = false;
        return retVal;
    }
    
    public static Boolean isAfterUpdateEventFirstRun(){
        Boolean retVal = afterUpdateFirstRun;
        afterUpdateFirstRun = false;
        return retVal;
    }

    /**
      * @description       This method is invoked from handler for before insert event to 
      *                    make tasks related to application and certificates public
      * @param             NA 
      * @return            Void
      * @throws            Exception is handled by try-catch block
      */
    public void makeTasksPublic(List<Task> tskList){
        for(Task tsk : tskList) {
            if(tsk.whatId != null){
                Schema.SObjectType objType = tsk.whatId.getSobjectType();
                if(objType == Application__c.sobjectType || objType == Certificate__c.sObjectType){
                    tsk.IsVisibleInSelfService = true; 
                }
            }
        }
    }
    
    /**
      * @description       This method is invoked from handler for before update event to 
      *                    reject any application on expiry
      * @param             NA 
      * @return            Void
      * @throws            Exception is handled by try-catch block
      */
    public void rejectApplication(List<Task> tskList){
        try{
            Map<Id,Task> appTaskMap = new Map<Id,Task>();
            List<Application__c> appList = new List<Application__c>();
            
            for(Task tsk : tskList){
                if(tsk.Task_Expired__c){
                    if(tsk.WhatId != null){
                        appTaskMap.put(tsk.WhatId,tsk);
                    }
                    tsk.Status = (!GlobalConstants.TSK_STATUS_RESPONDED.equalsIgnoreCase(tsk.Status) 
                                    && !GlobalConstants.TSK_STATUS_COMPLETED.equalsIgnoreCase(tsk.Status)) ? GlobalConstants.TSK_STATUS_OVERDUE : tsk.Status;
                }
            }
            
            if(!appTaskMap.isEmpty()){
                for(Application__c app : [SELECT Application_Status__c,External_Comments__c,Requires_Approval__c 
                                            FROM Application__c WHERE Id IN: appTaskMap.keyset()]){
                    app.Application_Status__c = GlobalConstants.APPLICATION_STATUS_NEW_INFORMATION_RECEIVED;
                    Task appTask = appTaskMap.get(app.Id);
                    if(String.isNotBlank(app.External_Comments__c)){
                        app.External_Comments__c += '\n' + '\n' + System.now().format('dd/MM/yyyy HH:mm:ss ') + ' SYSTEM: TASK ' + appTask.Activity_Number__c + ' IS NOW OVERDUE WITHOUT COMPLETION';
                    }
                    else{
                        app.External_Comments__c += System.now().format('dd/MM/yyyy HH:mm:ss ') + ' SYSTEM: TASK ' + appTask.Activity_Number__c + ' IS NOW OVERDUE WITHOUT COMPLETION';
                    }
                    
                    if(GlobalConstants.TSK_SUBJ_PENDING_DECLINATURE_INFO_REQ.equalsIgnoreCase(appTask.Subject)){
                        app.Requires_Approval__c = true;
                    }
                    
                    appList.add(app);
                }
                
                if(!appList.isEmpty()){
                    Database.update(appList);
                }
            }
        }
        catch(Exception exp){
            // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(TaskTriggerHelper.class),
                                GlobalConstants.FN_REJECT_APPLICATION,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }
    
}