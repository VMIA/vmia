/**
  * Date         :  30-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for TaskTrigger
  */ 
  
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
private with sharing class TaskTrigger_Test{
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> certRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> appRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup(){

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);

        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,
                                                        conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexSmith.AccountId;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        
        Application__c app = TestUtility.createApplication(bornBuilder.Id,rexDistributor.Id,'New Eligibility',
                                                            appRecTypeMap.get('New Eligibility').getRecordTypeId(),null);
        app.External_Comments__c = 'DD/Builder comments';
        Database.insert(app);

        Certificate__c cert = TestUtility.createCertificate(bornBuilder.Id,bornBuilder.Agent__c,
                                                                certRecTypeMap.get(GlobalConstants.REC_TYPE_REF_CERT).getRecordTypeId());
        cert.External_Comments__c = 'DD/Builder comments';
        Database.insert(cert);

    }

    @future
    private static void setupUser(){
        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        
        Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(new List<User>{vmiaInternalUser,rexUser});
    }
    
    /**
      * @description       This method tests scenario of task creation for certificate
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testTaskCreationCertificate(){
        User rexUser = [SELECT Id,Name,ContactId FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Certificate__c cert = [SELECT Id FROM Certificate__c LIMIT 1];
        cert.OwnerId = rexUser.Id;
        Database.update(cert);
        
        System.runAs(rexUser){
            Task certTsk = TestUtility.createTask(cert.Id,rexUser.ContactId,bornBuilder.Id);
            certTsk.Description = 'Cert Description';
            Test.startTest();
                Database.insert(certTsk);
            Test.stopTest();
            
            Task tskAfterInsert = [SELECT IsVisibleInSelfService FROM Task WHERE WhatId =: cert.Id LIMIT 1];
            System.assertEquals(true,tskAfterInsert.IsVisibleInSelfService);
        }
    }
    
    /**
      * @description       This method tests scenario of task creation for application
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testTaskCreationApplication(){
        User rexUser = [SELECT Id,Name,ContactId FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        app.OwnerId = rexUser.Id;
        Database.update(app);

        System.runAs(rexUser){
            Task appTsk = TestUtility.createTask(app.Id,rexUser.ContactId,bornBuilder.Id);
            appTsk.Description = 'App Description';
            Test.startTest();
                Database.insert(appTsk);
            Test.stopTest();
            
            Task tskAfterInsert = [SELECT IsVisibleInSelfService FROM Task WHERE WhatId =: app.Id LIMIT 1];
            System.assertEquals(true,tskAfterInsert.IsVisibleInSelfService);
        }
    }
    
    /**
      * @description       This method tests scenario of task expiry to reject application
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testTaskExpiryRejectApplication(){
        User rexUser = [SELECT Id,Name,ContactId FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        app.OwnerId = rexUser.Id;
        Database.update(app);

        Task appTsk = TestUtility.createTask(app.Id,rexUser.ContactId,bornBuilder.Id);
        appTsk.Description = 'App Description';
        Database.insert(appTsk);
        System.runAs(rexUser){
            Test.startTest();
                appTsk.Task_Expired__c = true;
                Database.update(appTsk);
            Test.stopTest();
            
            Task tskAfterUpdate = [SELECT Status FROM Task WHERE WhatId =: app.Id LIMIT 1];
            System.assertEquals(GlobalConstants.TSK_STATUS_OVERDUE,tskAfterUpdate.Status);
            Application__c appAfterUpdate = [SELECT Application_Status__c FROM Application__c LIMIT 1];
            System.assertEquals(GlobalConstants.APPLICATION_STATUS_NEW_INFORMATION_RECEIVED,appAfterUpdate.Application_Status__c);
        }
    }

    /**
      * @description       This method tests scenario of task expiry to reject application
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testTaskExpiryRejectApplicationNoExternalComments(){
        User rexUser = [SELECT Id,Name,ContactId FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        app.OwnerId = rexUser.Id;
        app.External_Comments__c = null;
        Database.update(app);

        Task appTsk = TestUtility.createTask(app.Id,rexUser.ContactId,bornBuilder.Id);
        appTsk.Description = 'App Description';
        Database.insert(appTsk);
        System.runAs(rexUser){
            Test.startTest();
                appTsk.Task_Expired__c = true;
                Database.update(appTsk);
            Test.stopTest();
            
            Task tskAfterUpdate = [SELECT Status FROM Task WHERE WhatId =: app.Id LIMIT 1];
            System.assertEquals(GlobalConstants.TSK_STATUS_OVERDUE,tskAfterUpdate.Status);
            Application__c appAfterUpdate = [SELECT Application_Status__c FROM Application__c LIMIT 1];
            System.assertEquals(GlobalConstants.APPLICATION_STATUS_NEW_INFORMATION_RECEIVED,appAfterUpdate.Application_Status__c);
        }
    }

    /**
      * @description       This method tests scenario of task expiry of pending declinature application
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testTaskExpiryPendingDeclinatureApplication(){
        User rexUser = [SELECT Id,Name,ContactId FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        app.OwnerId = rexUser.Id;
        Database.update(app);
        
        Task appTsk = TestUtility.createTask(app.Id,rexUser.ContactId,bornBuilder.Id);
        appTsk.Description = 'App Description';
        appTsk.Subject = GlobalConstants.TSK_SUBJ_PENDING_DECLINATURE_INFO_REQ;
        Database.insert(appTsk);
        System.runAs(rexUser){
            Test.startTest();
                appTsk.Task_Expired__c = true;
                Database.update(appTsk);
            Test.stopTest();
            
            Task tskAfterUpdate = [SELECT Status FROM Task WHERE WhatId =: app.Id LIMIT 1];
            System.assertEquals(GlobalConstants.TSK_STATUS_OVERDUE,tskAfterUpdate.Status);
            Application__c appAfterUpdate = [SELECT Requires_Approval__c,Application_Status__c FROM Application__c LIMIT 1];
            System.assertEquals(GlobalConstants.APPLICATION_STATUS_NEW_INFORMATION_RECEIVED,appAfterUpdate.Application_Status__c);
            System.assertEquals(true,appAfterUpdate.Requires_Approval__c);
        }
    }

    /**
      * @description       Test method to validate deletion and undelete of task
      *                     Scenario is not part of requirement, created for code coverage purpose only
      * @param             NA
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testTaskDeleteUndelete(){
        User rexUser = [SELECT Id,Name,ContactId FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        app.OwnerId = rexUser.Id;
        Database.update(app);

        System.runAs(rexUser){
            Task appTsk = TestUtility.createTask(app.Id,rexUser.ContactId,bornBuilder.Id);
            appTsk.Description = 'App Description';
            Database.insert(appTsk);
            Test.startTest();
                Database.delete(appTsk);
                Database.undelete(appTsk);
            Test.stopTest();
        }
    }
    
    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testRejectApplicationExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            TaskTriggerHelper helper = new TaskTriggerHelper();
            Test.startTest();
                helper.rejectApplication(null);
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }
}