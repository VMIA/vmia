@isTest
public class TestDataFactory {
    public static List<Account> createDistributorAccounts(integer numAccts){
        List<Account> accts = new List<Account>();
        List<RecordType> recTypeDist = [Select Id,SobjectType,Name From RecordType WHERE Name ='Distributor' and SobjectType ='Account'  limit 1];
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestDistAccount' + i, RecordTypeId = recTypeDist[0].id);
            accts.add(a);
        }
        
        return accts;
    }   

     public static List<Contact> createDistributorContacts(integer numContacts){
        List<Contact> cons = new List<Contact>();
        List<RecordType> recTypeDist = [Select Id,SobjectType,Name From RecordType WHERE Name ='Distributor' and SobjectType ='Contact'  limit 1];
        for(Integer i=0;i<numContacts;i++) {
            Contact c = new Contact(RecordTypeId = recTypeDist[0].id);
            cons.add(c);
        }
        
        return cons;
    } 

    public static List<Contact> createBuilderContacts(integer numContacts){
        List<Contact> cons = new List<Contact>();
        List<RecordType> recTypeBuild = [Select Id,SobjectType,Name From RecordType WHERE Name ='Builder' and SobjectType ='Contact'  limit 1];
        for(Integer i=0;i<numContacts;i++) {
            Contact c = new Contact(RecordTypeId = recTypeBuild[0].id);
            cons.add(c);
        }
        
        return cons;
    }  

     public static List<Certificate__c> createCertificates(integer numCertificates){

     return null ;	
     }	

     public static List<Application__C> createApplications(integer numApplication){

     return null ;	
     }

    public static List<Account> createBuilderAccounts(integer numAccts){
        List<Account> accts = new List<Account>();
        List<RecordType> recTypeBuild = [Select Id,SobjectType,Name From RecordType WHERE Name ='Company' and SobjectType ='Account'  limit 1];
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestBuildAccount' + i, RecordTypeId = recTypeBuild[0].id, Construction_Limit__c = 1000000, ABN__c = '12134532412', ACN__c = '121345324', Builder_Risk_Rating__c = 'C', Eligibility_Status__c='Under Application', CO1_New_Single_Dwelling__c = True, Construction_Category_Limit_1__c = 20000 );
            accts.add(a);
        }
        
        return accts;
    }	
}