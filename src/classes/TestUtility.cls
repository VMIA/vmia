/**
  * Date         :  15-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Utility class for Test classes
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/ 
@isTest
public with sharing class TestUtility{
    
    // Constants
    private static final String USR_TMP_EMAIL = 'us.name@vmail.com';
    private static final String ENCODE_KEY = 'UTF-8';
    private static final String TIME_ZONE = 'Australia/Sydney';
    private static final String LOCALE_KEY = 'en_US';
    private static final String USER_POSTFIX = '@testorg.com';
    private static final String POLICY_PREFIX = 'C3';
    
    /**
      * @description       This method fetch User role based on developer name                    
      * @param             roleDevName    
      * @return            UserRole
      * @throws            NA
      */
    public static UserRole getUserRole(String roleDevName){
        UserRole role = [SELECT Id,DeveloperName FROM UserRole WHERE DeveloperName =: roleDevName LIMIT 1];
        return role;
    }
    
    /**
      * @description       This method creates user for testing                     
      * @param             lastName , profileId, roleId    
      * @return            User
      * @throws            NA
      */
    public static User createUser(String lastName,Id profileId,Id roleId){
        User usr = new User(alias = lastName, email= USR_TMP_EMAIL,
                            emailencodingkey = ENCODE_KEY, lastname = lastName,
                            timezonesidkey = TIME_ZONE,
                            languagelocalekey = LOCALE_KEY,
                            localesidkey = LOCALE_KEY, profileid = profileId,
                            UserRoleId = roleId,
                            username = lastName + System.currentTimeMillis() + USER_POSTFIX);
        return usr;
    }
    
    /**
      * @description       This method creates custom settings required for application                     
      * @param             NA    
      * @return            void
      * @throws            NA
      */
    public static void createCustomSettings(){
        /** custom setting for application log **/
        Logger_Settings__c setting = new Logger_Settings__c(LogLevel_Error__c = true);
        Database.upsert(setting);
        
        /** custom setting for policy number generation **/
        PolicyNumber__c policyNumber = new PolicyNumber__c(Prefix__c = POLICY_PREFIX, CurrentValue__c = 100000,
                                                            RecordType__c = GlobalConstants.REC_TYPE_CONTAINER + GlobalConstants.COMMA + GlobalConstants.REC_TYPE_HISTORICAL_CERT);
        Database.upsert(policyNumber);

        /** custom setting for Portal Queue Objects used for apex queue creation **/
        List<Portal_Queue_Objects__c> portalQueueObjs = new List<Portal_Queue_Objects__c>();
        Portal_Queue_Objects__c appObj = new Portal_Queue_Objects__c(Enable__c = true,Name = String.valueOf(Application__c.class),
                                                                        Object_API_Name__c = String.valueOf(Application__c.class));
        Portal_Queue_Objects__c certObj = new Portal_Queue_Objects__c(Enable__c = true,Name = String.valueOf(Certificate__c.class),
                                                                        Object_API_Name__c = String.valueOf(Certificate__c.class));
        Portal_Queue_Objects__c invObj = new Portal_Queue_Objects__c(Enable__c = true,Name = String.valueOf(Invoice__c.class),
                                                                        Object_API_Name__c = String.valueOf(Invoice__c.class));
        portalQueueObjs.add(appObj);
        portalQueueObjs.add(certObj);
        portalQueueObjs.add(invObj);

        Database.upsert(portalQueueObjs);

        /** Custom setting for TechOne **/
        TechOneInfo__c techOne = new TechOneInfo__c(Name = 'TechOne',PremiumAmount__c = '4000', GSTAmount__c = '2240', StampDuty__c = '2260', TotalAmount__c = '1035');
        Database.upsert(techOne);
    }

    /**
      * @description       This method creates portal user for testing                     
      * @param             lastName , profileId, contactId    
      * @return            User
      * @throws            NA
      */
    public static User createPortalUser(String lastName,Id profileId,Id contactId){
        User usr = createUser(lastName,profileId,null); 
        usr.ContactId = contactId;
        return usr;
    }

    /**
      * @description       This method creates business account for testing                     
      * @param             name , rectTypeId    
      * @return            Account
      * @throws            NA
      */
    public static Account createBusinessAccount(String name,Id recTypeId){
        Account acc = new Account(Name = name,RecordTypeId = recTypeId);
        return acc;
    }

    /**
      * @description       This method creates Person account for testing                     
      * @param             lastName , rectTypeId    
      * @return            Account
      * @throws            NA
      */
    public static Account createPersonAccount(String lastName,Id recTypeId){
        Account acc = new Account(LastName = lastName,RecordTypeId = recTypeId);
        return acc;
    }

    /**
      * @description       This method creates contact for testing                     
      * @param             firstName, lastName , email, accId , recTypeId   
      * @return            Contact
      * @throws            NA
      */
    public static Contact createContact(String firstName,String lastName,String email,Id accId,Id recTypeId){
        Contact con = new Contact(FirstName = firstName,LastName = lastName,Email = email,AccountId = accId,RecordTypeId = recTypeId);
        return con;
    }

    /**
      * @description       This method creates Certificate for testing                     
      * @param             builderId, distributorId , recTypeId    
      * @return            Certificate__c
      * @throws            NA
      */
    public static Certificate__c createCertificate(Id builderId,Id distributorId,Id recTypeId){
        Certificate__c cert = new Certificate__c(Builder__c = builderId,Distributor__c = distributorId,RecordTypeId = recTypeId);
        return cert;
    }

    /**
      * @description       This method creates Invoice for testing                     
      * @param             purchaserId, type , recTypeId    
      * @return            Invoice__c
      * @throws            NA
      */
    public static Invoice__c createInvoice(Id purchaserId,String type,Id recTypeId){
        Invoice__c invoice = new Invoice__c(Purchaser__c = purchaserId,Type__c = type,RecordTypeId = recTypeId);
        return invoice;
    }

    /**
      * @description       This method creates Application for testing                     
      * @param             builderId, distributorId , type, recTypeId, certificateId    
      * @return            Application__c
      * @throws            NA
      */
    public static Application__c createApplication(Id builderId,Id distributorId,String type,Id recTypeId,Id certificateId){
        Application__c app = new Application__c(Builder_Number__c = builderId,DD_Account__c = distributorId, Type__c = type,RecordTypeId = recTypeId,Certificate__c = certificateId);
        return app;
    }
    
      /**
      * @description       This method creates PropertyOnwer for testing                     
      * @param             accountId, certificateId    
      * @return            Property_Owner__c
      * @throws            NA
      */
    public static Property_Owners__c createPropertyOwner(Id accountId, Id certificateId){
        Property_Owners__c po = new Property_Owners__c(Property_Owner__c = accountId, Certificate__c = certificateId);
        return po;
    }

    /**
      * @description       This method creates Contact Role for testing                     
      * @param             accountId, certificateId    
      * @return            Property_Owner__c
      * @throws            NA
      */
    public static Contact_Role__c createContactRole(Id accountId, Id contactId,string roleStr){
        Contact_Role__c co = new Contact_Role__c(Account__c = accountId, Contact__c = contactId , Role__c = roleStr);
        return co;
    }

    /**
      * @description       This method creates BAT Letter for testing                     
      * @param             builderId, applicationId, certificateId   
      * @return            BAT_Letter__c
      * @throws            NA
      */
    public static BAT_Letter__c createBATLetter(Id builderId,Id applicationId,Id certificateId){
        BAT_Letter__c batLetter = new BAT_Letter__c(Builder__c = builderId,Application__c = applicationId,Certificate__c = certificateId);
        return batLetter;
    }
    /**
      * @description       This method creates property record for testing                     
      * @param             streetVar, suburbVar,postCodeVar ,StateVar    
      * @return            Property__c
      * @throws            NA
      */
     public static Property__c createProperty(String streetVar, String suburbVar,String postCodeVar, String StateVar){ 
        Property__c property = new Property__c(Street_Full__c = streetVar, Suburb__c = suburbVar, Postcode__c= postCodeVar,State__c=StateVar);
        return property ;
     }

    /**
      * @description       This method creates Property Owner for testing                     
      * @param             ownerId, certificateId,,propertyId     
      * @return            Property_Owners__c
      * @throws            NA
      */
    public static Property_Owners__c createPropertyOwner(Id ownerId,Id certificateId ,Id propertyId){
        Property_Owners__c propertyOwner = new Property_Owners__c(Certificate__c = certificateId,Property__c = propertyId,Contact__c = ownerId);
        return propertyOwner;
    }

    /**
      * @description       This method creates a Flat Rate for testing                     
      * @param             premium, recTypeId    
      * @return            Property_Owners__c
      * @throws            NA
      */
    public static PremiumRatesTable__c createFlatRateTable(Decimal premium, Id recTypeId){
        PremiumRatesTable__c flatRate = new PremiumRatesTable__c(Premium__c = premium, RecordTypeId = recTypeId);
        return flatRate;
    }

    /**
      * @description       This method creates Task for testing                     
      * @param             whatIdVal, whoIdVal,,builderId     
      * @return            Task
      * @throws            NA
      */
    public static Task createTask(Id whatIdVal,Id whoIdVal ,Id builderId){
        Task tsk = new Task(WhatId = whatIdVal,WhoId = whoIdVal,Builder__c = builderId);
        return tsk;
    }
}