/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Helper Class for all triggers in org to verify if triggers are activated.
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public with sharing class TriggerHelper{
    
    public static Boolean isTriggerDisabled;
    
    public static Boolean isTriggerDisabled(String objType){
        if(isTriggerDisabled == null){
            isTriggerDisabled = checkTriggerDisabled(objType);
        }
        System.debug('** Triggers Disabled ==>'+isTriggerDisabled);
        return isTriggerDisabled;
    }

    /**
      * @description       This method checks if trigger execution should be disabled for the object
                           for the execution user
      * @param             objType 
      * @return            Boolean
      * @throws            NA
      */        
    private static Boolean checkTriggerDisabled(String objType){
        Boolean isDisabled = false;
        Set<String> objSet = new Set<String>();
        Set<String> userIds = new Set<String>();
        Set<String> profileIds = new Set<String>();
        if(String.isNotBlank(objType)){
            SystemSwitch__c custSettings = SystemSwitch__c.getInstance();   // get instance of custom setting
            if(custSettings != null){
                String objsDisabled = custSettings.ObjectTriggersToBeDisabled__c != null ? custSettings.ObjectTriggersToBeDisabled__c.trim() : null ;
                String disabledforUsers = custSettings.Users__c != null ? custSettings.Users__c.trim() : null ;
                String disabledforProfiles = custSettings.Profiles__c != null ? custSettings.Profiles__c.trim() : null ;                        
                if(String.isNotBlank(objsDisabled)){
                    objSet.addAll(trimStringList(objsDisabled.split(GlobalConstants.COMMA)));   // fetch all objects to be disabled from setting
                    userIds.addAll(trimStringList(disabledforUsers.split(GlobalConstants.COMMA)));  // fetch all users for triggers to be disabled from setting
                    profileIds.addAll(trimStringList(disabledforProfiles.split(GlobalConstants.COMMA)));    // fetch all users for triggers to be disabled from setting
                    // check if trigger execution should be bypassed for the object for current user or profile
                    if(objSet.contains(objType) && custSettings.DisableTriggers__c
                        && ((userIds.isEmpty() && profileIds.isEmpty()) ||
                            ((!userIds.isEmpty() && userIds.contains(UserInfo.getUserId())) 
                                || (!profileIds.isEmpty() && profileIds.contains(UserInfo.getProfileId()))))){
                        isDisabled = true;
                    }
                }
            }
        }
        System.debug('** Triggers Disabled ==>'+isDisabled);
        return isDisabled;
    }
    
    /**
      * @description       This method is used to trim content of list
      * @param             strList - List<String> 
      * @return            Void
      * @throws            NA
      */    
    public static List<String> trimStringList(List<String> strList){
        List<String> retList = new List<String>();
        for(String str : strList){
            retList.add(str.trim());
        }
        return retList;
    }
}