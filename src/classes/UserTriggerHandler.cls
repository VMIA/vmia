/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Handler Class for User object trigger which includes context-specific methods 
                    that are automatically called when a trigger is executed.
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public with sharing class UserTriggerHandler{
    
    // Variables
    private static UserTriggerHelper helper;    // Helper class variable to call methods based on scenario
    
    /**
      * @description       This method is invoked from trigger which in-turn calls the handlers
                           for before and after event.
      * @param             NA 
      * @return            Void
      * @throws            NA
      */        
    public static void execute(){
        helper = new UserTriggerHelper();   // instance of helper class for Case trigger

        // Check for event type of trigger
        if(Trigger.isBefore){
            beforeHandler();        // invoke of before handler
        }
        else if(Trigger.isAfter){
            afterHandler();         // invoke of after handler
        }
    }
    
    /**
      * @description       This method is handler for before events invoked by execute method.
                           Method invokes the helper class methods based on the scenario.                       
      * @param             NA 
      * @return            Void
      * @throws            NA
      */  
    private static void beforeHandler(){
        // Check for type of operation
        if(Trigger.isInsert){
            // Methods to be invoked for before insert event
        }
        else if(Trigger.isUpdate && UserTriggerHelper.isBeforeUpdateEventFirstRun()){
            // Methods to be invoked for before update event
        }
    }
    
    /**
      * @description       This method is handler for after events invoked by execute method.
                           Method invokes the helper class methods based on the scenario.                          
      * @param             NA 
      * @return            Void
      * @throws            NA
      */  
    private static void afterHandler(){
        // Check for type of operation
        if(Trigger.isInsert){
            // Methods to be invoked for after insert event
            helper.createDistributorQueue(Trigger.new);   // create queue for distributors
            //helper.updateContactRecord(Trigger.new);
      helper.accessToDirectorController((Map<Id,User>)Trigger.newMap,null);
        }
        else if(Trigger.isUpdate && UserTriggerHelper.isAfterUpdateEventFirstRun()){
            // Methods to be invoked for after update event
            //helper.updateContactRecord(Trigger.new);
       helper.accessToDirectorController((Map<Id,User>)Trigger.newMap,(Map<Id,User>)Trigger.oldMap);
        }
    }
}