/**
  * Date         :  30-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Helper Class for User trigger
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
public with sharing class UserTriggerHelper{
    
    // variables
    private static Boolean beforeUpdateFirstRun = true;
    private static Boolean afterUpdateFirstRun = true;

    public static Boolean isBeforeUpdateEventFirstRun(){
        Boolean retVal = beforeUpdateFirstRun;
        beforeUpdateFirstRun = false;
        return retVal;
    }
    
    public static Boolean isAfterUpdateEventFirstRun(){
        Boolean retVal = afterUpdateFirstRun;
        afterUpdateFirstRun = false;
        return retVal;
    }
    
     /**
      * @description       This method is invoked from handler for after insert event to create queue for distributor
      *                     account & associate distributor role to it.
      * @param             NA 
      * @return            Void
      * @throws            Exception is handled by try-catch block
      */
    public void createDistributorQueue(List<User> userList){
        try{
            Set<Id> roleIds = new Set<Id>();
            Map<String,String> roleNameMap = new Map<String,String>();
            Map<Id,User> validUsers = new Map<Id,User>();
            Set<String> grpTypes= new Set<String>{GlobalConstants.QUEUE,GlobalConstants.ROLE_AND_SUBORDINATES};
            // fetch user roles details
            for(User usr : [SELECT UserRoleId,UserRole.Name,UserRole.DeveloperName,IsPortalEnabled,UserType FROM User WHERE Id IN: userList]){
                if(usr.IsPortalEnabled && usr.UserRoleId != null 
                    && GlobalConstants.USER_TYPE_POWER_CUSTOMER.equalsIgnoreCase(usr.UserType)){
                    roleIds.add(usr.UserRoleId);
                    String devNameUntrimmed = usr.UserRole.DeveloperName.replaceAll('[^A-Za-z .]','');
                    String devNameTrimmed;
                    if(devNameUntrimmed.length() >40){
                        devNameTrimmed = devNameUntrimmed.substring(0,40);
                    }else{
                        devNameTrimmed = devNameUntrimmed;
                    }

                    String uroleName = usr.UserRole.Name;
                    String uRoleNameTrimmed;
                    if(uroleName.length() >40){
                        uRoleNameTrimmed = uroleName.substring(0,40);
                    }else{
                        uRoleNameTrimmed = uroleName;
                    }


                    roleNameMap.put(devNameTrimmed,uRoleNameTrimmed);
                }
            }
            
            Map<String,Id> roleGroupMap = new Map<String,Id>();
            Map<String,Id> queueMap = new Map<String,Id>();
            // form role group & queue maps
            for(Group grp : [SELECT Name,DeveloperName,RelatedId,Type FROM Group WHERE Type IN: grpTypes]){
                if(GlobalConstants.ROLE_AND_SUBORDINATES.equalsIgnoreCase(grp.Type) && roleIds.contains(grp.RelatedId)){
                    roleGroupMap.put(grp.DeveloperName,grp.Id);
                }
                else if(GlobalConstants.QUEUE.equalsIgnoreCase(grp.Type)){
                    queueMap.put(grp.DeveloperName,grp.Id);
                }
            }
            
            if(!queueMap.isEmpty()){    // check if queueMap is empty
                Map<String,Group> queuesToCreate = new Map<String,Group>();
                List<GroupMember> queueMembers = new List<GroupMember>();
                // create queue if not available
                for(String roleName : roleNameMap.keyset()){
                    if(!queueMap.containsKey(roleName)){
                        Group grp = new Group(Type = GlobalConstants.QUEUE,DeveloperName = roleName,Name = roleNameMap.get(roleName));
                        queuesToCreate.put(grp.DeveloperName,grp);
                    }
                }

                if(!queuesToCreate.isEmpty()){
                    Database.insert(queuesToCreate.values());   // create queues
                    List<QueueSObject> queueObjects = new List<QueueSObject>();
                    // links objects to queues based on custom setting if enabled
                    for(Group grp : queuesToCreate.values()){
                        for(Portal_Queue_Objects__c objectRec : Portal_Queue_Objects__c.getAll().values()){
                            if(objectRec.Enable__c){
                                QueueSObject queueObject = new QueueSObject(QueueId = grp.Id,SObjectType = objectRec.Object_API_Name__c);
                                queueObjects.add(queueObject);
                            }
                        }
                    }
                    
                    if(!queueObjects.isEmpty()){
                        Database.insert(queueObjects);
                    }
                    // associate roles to queues
                    for(String roleName : roleNameMap.keyset()){
                        if(!queueMap.containsKey(roleName)){
                            GroupMember grpMember = new GroupMember(UserOrGroupId = roleGroupMap.get(roleName),
                                                                        GroupId = queuesToCreate.get(roleName).Id);
                            queueMembers.add(grpMember);
                        }
                    }
                    /*if(!queueMembers.isEmpty() && !System.isFuture()){
                        addQueueMembers(JSON.serialize(queueMembers));
                    }*/
                    if(!queueMembers.isEmpty()){
                        Database.insert(queueMembers);
                    }
                }
            }
        }
        catch(Exception exp){
            // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(UserTriggerHelper.class),
                                GlobalConstants.FN_CREATE_DISTRIBUTOR_QUEUE,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }
    
    @future
    private static void addQueueMembers(String queueMembersString){
        List<GroupMember> queueMembers = (List<GroupMember>)JSON.deserialize(queueMembersString,(List<GroupMember>.class));
        if(!queueMembers.isEmpty()){
            Database.insert(queueMembers);
        }
    }

    /**
      * @description       This method is invoked from handler for after insert/update event to capture some information
      *                      on contact level.
      * @param             NA 
      * @return            Void
      * @throws            Exception is handled by try-catch block
      */
    public void updateContactRecord(List<User> userList){
        try{
            System.debug('trigger.isUpdate<>'+trigger.isUpdate);             
            
            Set<id> userId = new set<id>();
            for(user usr : userList){
                if(usr.contactId != null){
                    userId.add(usr.id);
                }
            }
            System.debug('** userId<>>>'+userId);
            if(!userId.isEmpty() && !System.isFuture()){
                System.debug('** Calling the Future Method');
                updateContactRecordFuture(userId);
            }             
        
        }
        catch (Exception exp){
            // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(UserTriggerHelper.class),
                                GlobalConstants.FN_UPDATE_CONTACT_RECORD,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }   

    } 

    /**
      * @description       This method is invoked from handler for after update event to capture some information
      *                      on contact level.
      * @param             NA 
      * @return            Void
      * @throws            Exception is handled by try-catch block
      */
    @future  
    public static void updateContactRecordFuture(set<id> userId){
        try{
            Map<id,user> userMap = new Map<id,user>();
            list<contact> contactList = new list<contact>();

            for(User usr : [SELECT id,username,contactId,IsPortalEnabled,Profile.UserLicense.Name FROM User WHERE Id IN: userId and isActive  = true and contactId != null and IsPortalEnabled = true]){
                userMap.put(usr.contactId,usr);
            }

            for(contact con : [Select id , Community_User__c,Username__c from contact where id IN : userMap.keySet()]){
                if(userMap.containsKey(con.id)){
                    user uRecord = userMap.get(con.id);
                    con.Community_User__c = true;
                    con.Username__c = uRecord.username ;
                    con.User_License__c = uRecord.Profile.UserLicense.Name ;
                    contactList.add(con);
                }
            }
            System.debug('** contactList<>'+contactList);

            if(!contactList.isEmpty()){
                Database.update(contactList);
            }
        }
        catch (Exception exp){
            // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(UserTriggerHelper.class),
                                GlobalConstants.FN_UPDATE_CONTACT_RECORD,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        } 
    }
    /**
      * @description       This method is invoked from handler for after insert/update event to capture some information
      *                     
      * @param             NA 
      * @return            Void
      * @throws            Exception is handled by try-catch block
      */
    public void accessToDirectorController(Map<Id,User> newUserMap,Map<Id,User> oldMap){
        set<id> userId = new Set<id>();
        Map<id,string> userWithProfileName = new Map<id,string>();
        for(user usr : [Select Id,Profile.name from User where ID IN : newUserMap.keySet()]){
            userWithProfileName.put(usr.Id,usr.Profile.name);
        }
        for(user u: newUserMap.values()){
             User oldUser = (oldMap != null) ? oldMap.get(u.Id) : null;     // get old certificate  
             string profileName = userWithProfileName.get(u.id);
            if(oldUser == null && profileName.equalsIgnoreCase(Constants.Common_RBP)){
                System.debug('Inside If');
                userId.add(u.Id);
            }else if(null != oldUser && oldUser.profileId != u.ProfileId ){
                 System.debug('Inside Else');
                 System.debug('userWithProfileName<>'+userWithProfileName);
                if(userWithProfileName.containsKey(u.Id)){
                    string userProfile = userWithProfileName.get(u.Id);
                    System.debug('userProfile<>'+userProfile);
                    if(userProfile.equalsIgnoreCase(Constants.Common_RBP)){
                        userId.add(u.Id);
                    }
                }
            }
        }
        System.debug('userId<>'+userId);
        if(!Test.isRunningTest()){
            if(!userId.isEmpty())
                provideAccessToDirector(userId);
        }    
    }    

    /**
      * @description       This method is invoked from handler for after update event to capture some information
      *                      on c
      * @param             NA 
      * @return            Void
      * @throws            Exception is handled by try-catch block
      */
    @future  
    public static void provideAccessToDirector(set<id> userId){

        Map<id,id> userWithContactMap = new Map<id,id>();

        for(user u : [Select id,contactId from User where ID IN :userId]){
            userWithContactMap.put(u.Id,u.contactId);
        }

        System.debug('userWithContactMap<>'+userWithContactMap);
        Map<id,List<id>> directorsWithBuildersMap = new Map<id,list<id>>();
        Set<id> builderSet = new Set<id>();

        for(Contact_Role__c cRole :[Select id,Account__c,Contact__c,Role__c from Contact_Role__c where Contact__c IN : userWithContactMap.values() and Role__c =: Constants.Primary_RBP]){
            builderSet.add(cRole.Account__c);
            if(directorsWithBuildersMap.containsKey(cRole.Contact__c)){
                list<id> builders = new List<id>();
                builders = directorsWithBuildersMap.get(cRole.Contact__c);
                builders.add(cRole.Account__c);
                directorsWithBuildersMap.put(cRole.Contact__c,builders);

            }else{
                list<id> builders = new List<id>();
                builders.add(cRole.Account__c);
                directorsWithBuildersMap.put(cRole.Contact__c,builders);

           }
        }
        System.debug('directorsWithBuildersMap<>'+directorsWithBuildersMap);
        //Builder With Certificate List
        Map<id,List<certificate__C>> builderWithCertificates = new Map<id,List<certificate__c>>();
        Map<id,List<id>> builderWithInvoicess = new Map<id,List<id>>();
        Map<id,List<Certificate__c>> directorWithCertificatesMap = new Map<id,List<Certificate__c>>();
        Map<id,List<id>> directorWithinvoicesMap =  new Map<id,List<id>>();

        for(Certificate__c c : [Select Id , Builder__c,InvoiceNo__c from Certificate__c where Builder__c IN :builderSet ]){

            if(builderWithCertificates.containsKey(c.Builder__c)){
                list<Certificate__c> cList = new List<Certificate__c>();
                cList = builderWithCertificates.get(c.Builder__c);
                cList.add(c);
                builderWithCertificates.put(c.Builder__c,cList);
            }else{
                list<Certificate__c> cList = new List<Certificate__c>();
                cList.add(c);
                builderWithCertificates.put(c.Builder__c,cList);
            }

            if(c.InvoiceNo__c != null){
                if(builderWithInvoicess.containsKey(c.Builder__c)){
                    list<id> invList = new List<id>();
                    invList = builderWithInvoicess.get(c.Builder__c);
                    invList.add(c.InvoiceNo__c);
                    builderWithInvoicess.put(c.Builder__c,invList);
                }else{
                    list<id> invList = new List<id>();
                     invList.add(c.InvoiceNo__c);
                    builderWithInvoicess.put(c.Builder__c,invList);
                }
            }    
        }
        System.debug('builderWithInvoicess<>'+builderWithInvoicess);

        for(id directorId : directorsWithBuildersMap.keySet()){
            for(id builderId : directorsWithBuildersMap.get(directorId)){
                if(builderWithCertificates.containsKey(builderId)){
                    directorWithCertificatesMap.put(directorId,builderWithCertificates.get(builderId));
                }
                if(builderWithInvoicess.containsKey(builderId)){
                    directorWithinvoicesMap.put(directorId,builderWithInvoicess.get(builderId));
                }
            }
        }


        System.debug('directorWithinvoicesMap<>'+directorWithinvoicesMap);

        //Code for Sharing the Applications with Distributors

        Map<id,list<Application__c>>buildersWithApplications =new Map<id,list<Application__c>>();
        Map<id,list<Application__c>> directorWithApplicationsMap = new Map<id,list<Application__c>>();

        for(Application__c app : [Select id,Builder_Number__c from Application__c where Builder_Number__c IN : builderSet]){
            if(buildersWithApplications.containsKey(app.Builder_Number__c)){
                    list<Application__c> Applications = new List<Application__c>();
                    Applications = buildersWithApplications.get(app.Builder_Number__c);
                    Applications.add(app);
                    buildersWithApplications.put(app.Builder_Number__c,Applications);
                }else{
                   list<Application__c> Applications = new List<Application__c>();
                    Applications.add(app);
                    buildersWithApplications.put(app.Builder_Number__c,Applications);
                }
        }

        System.debug('buildersWithApplications<>><'+buildersWithApplications);
        for(id directorId : directorsWithBuildersMap.keySet()){
            for(id builderId : directorsWithBuildersMap.get(directorId)){
                if(buildersWithApplications.containsKey(builderId)){
                    list<Application__c> appList = buildersWithApplications.get(builderId);
                    directorWithApplicationsMap.put(directorId,appList);
                }
                
            }
        }    

        System.debug('directorWithCertificatesMap<>'+directorWithCertificatesMap);
        System.debug('directorWithApplicationsMap<>'+directorWithApplicationsMap);
        System.debug('directorWithinvoicesMap<>'+directorWithinvoicesMap);
        System.debug('directorsWithBuildersMap<>'+directorsWithBuildersMap);
        
        CommonDirectorAccessController.shareCertificatesWithCommonDirector(directorWithCertificatesMap);
        CommonDirectorAccessController.shareApplicationsWithCommonDirector(directorWithApplicationsMap);
        CommonDirectorAccessController.shareInvoiceWithCommonDirector(directorWithinvoicesMap);
        CommonDirectorAccessController.shareBuilderWithCommonDirectorOnUserTrigger(directorsWithBuildersMap);


   }     

}