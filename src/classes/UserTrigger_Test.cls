/**
  * Date         :  15-May-2017
  * Author       :  SMS Management & Technology
  * Description  :  Test Class for User trigger
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/   
@isTest
private with sharing class UserTrigger_Test{
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
    }

    @testSetup
    private static void testDataSetup(){
        UserRole batMember = TestUtility.getUserRole('BAT_Member');

        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        Database.insert(vmiaInternalUser);

        setupData();
        
    }

    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);

        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);

        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);

        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);
    }
    
    /**
      * @description       This method tests scenario of queue creation for distributor user
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testDistributorQueueCreation(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
            User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
            Test.startTest();
                Database.insert(rexUser);
            Test.stopTest();
            User rexUserAfterInsert = [SELECT Username,UserRoleId,UserRole.DeveloperName,Contact.Username__c FROM User WHERE LastName = 'RexSmith' LIMIT 1];
            System.assert(rexUserAfterInsert.UserRoleId != null);
            Group distributorRoleGroup = [SELECT DeveloperName FROM Group 
                                            WHERE Type =: GlobalConstants.ROLE_AND_SUBORDINATES AND RelatedId =: rexUserAfterInsert.UserRoleId LIMIT 1];
            System.assert(distributorRoleGroup != null);
            Group distributorQueue = [SELECT DeveloperName FROM Group WHERE Type =: GlobalConstants.QUEUE AND DeveloperName =: rexUserAfterInsert.UserRole.DeveloperName LIMIT 1];
            System.assert(distributorQueue != null);
            Integer grpMemberCount = [SELECT count() FROM GroupMember 
                                        WHERE UserOrGroupId =: distributorRoleGroup.Id AND GroupId =: distributorQueue.Id LIMIT 1];
            System.assertEquals(1,grpMemberCount);
        }
    }

    /**
      * @description       This method tests scenario of contact update on user record update
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    /*private static testMethod void testUpdateContactRecord(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            Contact rexSmith = [SELECT AccountId FROM Contact WHERE FirstName = 'Rex' AND LastName = 'Smith' LIMIT 1];
            User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
            Database.insert(rexUser);
            Test.startTest();
                rexUser.UserName += '.test';
                Database.update(rexUser);
            Test.stopTest();
            User rexUserAfterUpdate = [SELECT UserName,Contact.Username__c FROM User WHERE LastName = 'RexSmith' LIMIT 1];
            System.assert(rexUserAfterUpdate.UserName == rexUserAfterUpdate.Contact.Username__c);
        }
        
    }*/

    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testcreateDistributorQueueExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            UserTriggerHelper helper = new UserTriggerHelper();
            Test.startTest();
                helper.createDistributorQueue(null);
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }

    /**
      * @description       This method is for covering exception and not a valid business scenario
      * @param             NA     
      * @return            Void
      * @throws            NA
      */
    private static testMethod void testUpdateContactRecordExceptionScenario(){
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(runningUser){
            UserTriggerHelper helper = new UserTriggerHelper();
            Test.startTest();
                helper.updateContactRecord(null);
            Test.stopTest();
            Integer expCount = [SELECT count() FROM Application_Log__c];
            System.assertEquals(1,expCount);
        }
    }

}