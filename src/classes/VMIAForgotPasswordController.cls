/*
    Date            :   23-Feb-2017
    Author          :   SMS Management & Technology
    Description     :   Forgot password controller
*/
public class VMIAForgotPasswordController {
    // Declarations
    public String userName {get;set;}
    public boolean displaymsg {get; set;}
    public boolean resetSuccess {get;set;}
    public List<User> lstUser {get; set;}
    
    public VMIAForgotPasswordController() {
        displaymsg = false;
        resetSuccess = false;
    }
    
    // Forgot Password - redirect to confimation page
    public PageReference forgotPassword() {
        List<User> lstUser = [Select Id, username from User where username = :userName];

        if (lstUser.size() > 0) {      
            boolean success = Site.forgotPassword(userName);
            if (success) {   
                resetSuccess = true; 
                displaymsg = false;    
            }   
        } else { // Failure - Set error message
            displaymsg = true;
        }
        return null;
    }
    
    public PageReference cancel() {
        PageReference pr = Page.Login;
        pr.setRedirect(true);
        return pr;
    }
}