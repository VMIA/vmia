@isTest
private class VMIAForgotPasswordController_Test {
	private static Map<String,Id> profileMap = new Map<String,Id>();
    private static Map<String,Schema.RecordTypeInfo> accRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> conRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> certRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> invRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> appRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    private static Map<String,Schema.RecordTypeInfo> premiumRecTypeMap = new Map<String,Schema.RecordTypeInfo>();
    
    
    static{
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
        appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        premiumRecTypeMap = Schema.Sobjecttype.PremiumRatesTable__c.getRecordTypeInfosByName();
    }
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }

        UserRole batMember = TestUtility.getUserRole('BAT_Member');
        UserRole batManager = TestUtility.getUserRole('BAT_Manager');
        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        Database.insert(vmiaInternalUser);

        User vmiaInternalManagerUser = TestUtility.createUser('IntMan',profileMap.get('VMIA Internal Manager'),batManager.Id);
        Database.insert(vmiaInternalManagerUser);

        setupData();     // data loaded for testing in a future method to avoid MIXED_DML

        //setupUser();    // user loaded for testing in a future method to avoid MIXED_DML

    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();

        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);

        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(rexUser);  

    }

	@isTest static void testForgotPassword() {
		User rexUser = [SELECT Id,Name, Username FROM User WHERE Name = 'RexSmith' LIMIT 1];
		Test.startTest();
		VMIAForgotPasswordController controller = new VMIAForgotPasswordController();
		List<User> getUsers = controller.lstUser;
		controller.username = rexUser.Username;
		PageReference ref = controller.ForgotPassword();
		system.assertEquals(ref, null);
		//system.assertEquals(controller.displaymsg, false);
		controller.username = 'test';
		ref = controller.ForgotPassword();
		system.assertEquals(controller.displaymsg, true);
		Test.stopTest();
	}
	
	@isTest static void testCancel() {
		PageReference pr = Page.Login;
		Test.startTest();
		VMIAForgotPasswordController controller = new VMIAForgotPasswordController();
		PageReference ref = controller.cancel();
		system.assertEquals(ref.getUrl(), pr.getUrl());
		Test.stopTest();
	}
	
}