public with sharing class WelcomeLetterEx {

    Private final Certificate__c Cert;
    Public Contact POwnerContact {get; set;}
    Public Account POwnerAccount {get; set;}
    Public List<Property_Owners__c> POwner {get; set;}
    public string propertyOwnerName{get;set;}

    public WelcomeLetterEx(ApexPages.StandardController controller) {
        this.Cert = (Certificate__c)controller.getRecord();
        id POwnerid = apexpages.currentpage().getparameters().get('POwnerid'); 
         system.debug('POwnerid  ' + POwnerid );
        system.debug('cert.id ' + cert.id);
        POwner = [select id, Contact__r.id, Property_Owner__r.id FROM Property_Owners__c WHERE id =: POwnerid ];
        system.debug('POwner.Contact__r.id ' + POwner[0].Contact__r.id);
        if(POwner[0].Contact__r.id != NULL){
            POwnerContact = [Select id,firstName, lastName, mailingstreet, mailingcity, mailingstate, mailingpostalcode, mailingcountry FROM contact where id=: POwner[0].Contact__r.id];
            propertyOwnerName = POwnerContact.firstName;
        }
        if(POwner[0].Property_Owner__r.id != NULL){
            POwnerAccount = [Select id, firstName, lastName, name, billingAddress, PersonMailingAddress, billingstreet, Personmailingstreet, billingcity, Personmailingcity, billingstate, Personmailingstate, billingpostalcode, 
                Personmailingpostalcode, billingcountry, Personmailingcountry FROM Account where id=: POwner[0].Property_Owner__r.id];
                propertyOwnerName = POwnerAccount.firstName;
        }
    }

}