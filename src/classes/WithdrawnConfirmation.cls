public without sharing class WithdrawnConfirmation{
public String appId{get;set;}
public String certId{get;set;}
public boolean isApplicationRecord{get;set;}
public List<Application__c> appToWithdraw {get;set;}
public List<Certificate__c> certToWithdraw {get;set;}

    public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return Page.Login;
        }
        else{
            return null;
        }
    }
    
    public PageReference WithdrawObject(){
        PageReference withdrawRedirect = new PageReference('');
        if(isApplicationRecord){
            list<Application__c> appToWithdraw = [Select Application_Status__c, Builder_Number__c from Application__c where id =: appId Limit 1];
            if(appToWithdraw.size()>0){
                appToWithdraw[0].Application_Status__c = GlobalConstants.APPLICATION_STATUS_BUILDER_WITHDRAWN;
                update appToWithdraw;
                
                try {
	                Account appBuilder = new Account(Id = appToWithdraw[0].Builder_Number__c);
	                appBuilder.Builder_Eligibility_Status_Reason__c	= NULL;
	                update appBuilder;
                }catch(exception e) {
                	System.debug('+++++ Builder eligibility status update failure on withdrawal');
                }
            }
            withdrawRedirect = new PageReference('/apex/EditEligibilityApplication?id='+appId);
        }else{
            list<Certificate__c> certToWithdraw = [Select Status__c from Certificate__c where id =: certId Limit 1];
            if(certToWithdraw.size()>0){
                certToWithdraw[0].Status__c = GlobalConstants.APPLICATION_STATUS_BUILDER_WITHDRAWN;
                update certToWithdraw; 
            }
            
            withdrawRedirect = new PageReference('/apex/RequestAmendCOI?id='+certId);
        }
        return withdrawRedirect;
    }
    
        public PageReference Cancel(){
            PageReference withdrawRedirect = new PageReference('');
            if(isApplicationRecord){
                withdrawRedirect = new PageReference('/apex/EditEligibilityApplication?id='+appId);
            }else{
                withdrawRedirect = new PageReference('/apex/RequestAmendCOI?id='+certId);
            }
            return withdrawRedirect;
        
        }
    

    public WithdrawnConfirmation() {
        appId = ApexPages.currentPage().getParameters().get('appId');
        certId = ApexPages.currentPage().getParameters().get('certId');
        certToWithdraw = new List<Certificate__c>();
        appToWithdraw = new List<Application__c>();
        if(!String.isBlank(appId)){
            isApplicationRecord = True;
        }else{
            isApplicationRecord = False;
        }
    }
    
    
    
}