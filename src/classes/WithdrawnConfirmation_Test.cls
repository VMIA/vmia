/**
* Date         :  19-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for WithdrawnConfirmation controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class WithdrawnConfirmation_Test {
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        UserRole batMember = TestUtility.getUserRole('BAT_Member');
        
        List<User> userList = new List<User>();
        User vmiaInternalUser = TestUtility.createUser('Internal',profileMap.get('VMIA BAT Internal'),batMember.Id);
        Database.insert(vmiaInternalUser);
        setupData();
        
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);
        
        Certificate__c  cert = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
        Database.insert(cert);
        System.debug('** Certificate ==>'+ cert.id);
        
        Application__c  app = TestUtility.createApplication(bornBuilder.Id, rexDistributor.Id, 'New Eligibility', appRecTypeMap.get('New Eligibility').getRecordTypeId(),cert.Id);
        Database.insert(app);
        System.debug('** Application ==>'+ app);
    }    
    
    public static testMethod void forwardToCustomAuthPageGuestUserTest(){
        User user = [select Id from User where userType = 'Guest' LIMIT 1];
        System.runAs(user) {
            PageReference pageRef = Page.WithdrawConfirmation;
            Test.setCurrentPage(pageRef);
            Test.startTest();           
            WithdrawnConfirmation controller = new WithdrawnConfirmation();
            PageReference pg = controller.forwardToCustomAuthPage();
            System.assertEquals(Page.Login.getURL(), pg.getURL());
            Test.stopTest();
        }
    }
    
    public static testMethod void forwardToCustomAuthPageInternalUserTest(){
        User user = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(user){
            PageReference pageRef = Page.WithdrawConfirmation;
            Test.setCurrentPage(pageRef);
            Test.startTest();           
            WithdrawnConfirmation controller = new WithdrawnConfirmation();
            PageReference pg = controller.forwardToCustomAuthPage();
            System.assertEquals(null, pg);
            Test.stopTest();                
        }     
    }    
    
    public static testMethod void WithdrawObjectWithApplicationTest(){
        User user = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(user){
            Contact tonyStark = [SELECT AccountId FROM Contact WHERE FirstName = 'Tony' AND LastName = 'Stark' LIMIT 1];
            Application__c app = [Select id from Application__c limit 1];
            Certificate__c cert = [Select id from Certificate__c limit 1];
            PageReference pageRef = Page.WithdrawConfirmation;
            Test.setCurrentPage(pageRef);  
            
            //Set url parameters                
            ApexPages.currentPage().getParameters().put('appId', app.Id);
            ApexPages.currentPage().getParameters().put('certId', cert.Id);
            Test.startTest();            
            WithdrawnConfirmation controller = new WithdrawnConfirmation();
            PageReference pg = controller.WithdrawObject();
            System.assertEquals('/apex/EditEligibilityApplication?id='+app.Id, pg.getUrl());
            Test.stopTest();
        }
    }
    
    public static testMethod void WithdrawObjectWithoutApplicationTest(){
        User user = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(user){
            Contact tonyStark = [SELECT AccountId FROM Contact WHERE FirstName = 'Tony' AND LastName = 'Stark' LIMIT 1];
            Certificate__c cert = [Select id from Certificate__c limit 1];
            PageReference pageRef = Page.WithdrawConfirmation;
            Test.setCurrentPage(pageRef);  
            
            //Set url parameters            
            ApexPages.currentPage().getParameters().put('certId', cert.Id);
            Test.startTest();            
            WithdrawnConfirmation controller = new WithdrawnConfirmation();
            PageReference pg = controller.WithdrawObject();
            System.assertEquals('/apex/RequestAmendCOI?id='+cert.Id, pg.getUrl());
            Test.stopTest();
        }
    }
    public static testMethod void CancelWithAppTest(){
        User user = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(user){
            Contact tonyStark = [SELECT AccountId FROM Contact WHERE FirstName = 'Tony' AND LastName = 'Stark' LIMIT 1];
            Application__c app = [Select id from Application__c limit 1];
            Certificate__c cert = [Select id from Certificate__c limit 1];
            PageReference pageRef = Page.WithdrawConfirmation;
            Test.setCurrentPage(pageRef);  
            
            //Set url parameters            
            ApexPages.currentPage().getParameters().put('appId', app.Id);
            ApexPages.currentPage().getParameters().put('certId', cert.Id);
            Test.startTest(); 
            WithdrawnConfirmation controller = new WithdrawnConfirmation();
            PageReference pg = controller.cancel();
            System.assertEquals('/apex/EditEligibilityApplication?id='+app.Id, pg.getUrl());
            Test.stopTest();
        }
    }
    
    public static testMethod void CancelWithoutAppTest(){
        User user = [SELECT Id FROM User WHERE Profile.Name = 'VMIA BAT Internal' AND LastName = 'Internal' LIMIT 1];
        System.runAs(user){
            Contact tonyStark = [SELECT AccountId FROM Contact WHERE FirstName = 'Tony' AND LastName = 'Stark' LIMIT 1];
            Certificate__c cert = [Select id from Certificate__c limit 1];
            PageReference pageRef = Page.WithdrawConfirmation;
            Test.setCurrentPage(pageRef);  
            
            //Set url parameters            
            ApexPages.currentPage().getParameters().put('certId', cert.Id);
            Test.startTest(); 
            WithdrawnConfirmation controller = new WithdrawnConfirmation();
            PageReference pg = controller.cancel();
            System.assertEquals('/apex/RequestAmendCOI?id='+cert.Id, pg.getUrl());
            Test.stopTest();
        }
    }    
}