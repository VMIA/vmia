/**
  * Date         :  28-Dec-2016
  * Author       :  SMS Management & Technology
  * Description  :  This Class is used for Parsing the XML response from External System
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/ 
public class XMLParser{
public static string errorMsg ;
public static Boolean isError=false ;
public static integer integerXmlTag = 0;
public static Boolean foundReferenceNumber = false;
public static integer referenceNo ;
public static Boolean westpacResponsecode = false;
public static integer summaryCodeNo ;
public static Boolean westpacResponseTextBol = false;
public static string westpacResponseText ;

/**
      * @description       This method is a parent method which trigger the respectice XML parser based on the external System
      * @param             string responseXml,string type
      * @return            void
      * @throws            NA
      */ 
	public  static void parseXml(string responseXml,string type){

        DOM.Document xmlDOC = new DOM.Document();
        xmlDOC.load(responseXml);

        DOM.XMLNode rootElement = xmlDOC.getRootElement();

        System.debug('rootElement>>'+rootElement);

        string outxmlstring=String.valueof(xmlDOC.getRootElement().getName());

        if(type=='westpacRefund'){

            for(DOM.XMLNode xmlnodeobj:xmlDOC.getRootElement().getChildElements())
            {        
         
                System.debug('xmlnodeobj>>'+xmlnodeobj);
                XMLParser.loadChildsForWRefund(xmlnodeobj);         
             } 
        } else if(type=='Tech1Refund' || type=='NewPaymentToTech1'){
            for(DOM.XMLNode xmlnodeobj:xmlDOC.getRootElement().getChildElements())
            {        
         
                 System.debug('xmlnodeobj>>'+xmlnodeobj);
                XMLParser.loadChildsForTech1Refund(xmlnodeobj);         
            }    
        }

         

    }
    /**
      * @description       This method is used for parsing the XML received from TechOne
      * @param             DOM.XMLNode xmlnode
      * @return            void
      * @throws            NA
      */ 

    public  static void loadChildsForTech1Refund(DOM.XMLNode xmlnode)
    {   string outxmlstring ;
        string spiStr ;
        integer k = 0;
        //errorMsg='ErrorCode : ';
        integerXmlTag++;
        for(Dom.XMLNode child : xmlnode.getChildElements())
        {	
        	System.debug('child::'+child);
          if(child.getText()!= null)

          {
            System.debug('child.getName()>>'+child.getName());
            System.debug('child.getText()>>'+child.getText());
            System.debug('child.getAttributeCount()>>'+child.getAttributeCount());
            system.debug('Children>>'+child.getChildElements());          
            
            
             if (null !=child.getName() && child.getName() == 'ErrorItem' && child.getAttributeCount() > 0) { 
                for (Integer i = 0; i< child.getAttributeCount(); i++ ) {
                    system.debug('key>>'+child.getAttributeKeyAt(i));
                    system.debug('nameSpace>>'+child.getAttributeKeyNsAt(i));
                    system.debug('value>>'+child.getAttributeValue(child.getAttributeKeyAt(i), child.getAttributeKeyNsAt(i)));
                    spiStr = child.getAttributeValue(child.getAttributeKeyAt(i), child.getAttributeKeyNsAt(i)) ;
                    system.debug('Children'+child.getChildren());

                    if(child.getAttributeKeyAt(i) == 'ErrorCode'){
                        errorMsg = 'ErrorCode : ' + child.getAttributeValue(child.getAttributeKeyAt(i), child.getAttributeKeyNsAt(i)) ;
                        isError = true;
                        System.debug('errorMsg1'+errorMsg);
                    }else if (child.getAttributeKeyAt(i) == 'Description'){
                        errorMsg += ' Error Description : ' + child.getAttributeValue(child.getAttributeKeyAt(i), child.getAttributeKeyNsAt(i)) ;
                        System.debug('errorMsg2'+errorMsg);
                    }
                   
                }  
            }
          }
          
          loadChildsForTech1Refund(child);       
        }

    }
    /**
      * @description       This method is used for parsing the XML received from TechOne for Refund
      * @param             DOM.XMLNode xmlnode
      * @return            void
      * @throws            NA
      */ 

    public  static void loadChildsForWRefund(DOM.XMLNode xmlnode)
    {   string outxmlstring ;
        string spiStr ;
        integer k = 0;
        //errorMsg='ErrorCode : ';
        integerXmlTag++;
        for(Dom.XMLNode child : xmlnode.getChildElements())
        {   
            System.debug('child::'+child);
          if(child.getText()!= null)

          {
            System.debug('child.getName()>>'+child.getName());
            System.debug('child.getText()>>'+child.getText());
            System.debug('child.getAttributeCount()>>'+child.getAttributeCount());
            system.debug('Children>>'+child.getChildElements());
           
           if (null !=child.getName() && child.getText() == 'response.summaryCode' ) { 
                westpacResponsecode = true;                
            }

            if(null !=child.getName() && child.getName() == 'value' && westpacResponsecode){
                westpacResponsecode = false;
                summaryCodeNo = integer.valueOf(child.getText()) ;
            }  

            if (null !=child.getName() && child.getText() == 'response.text' ) { 
                westpacResponseTextBol = true;                
            }

            if(null !=child.getName() && child.getName() == 'value' && westpacResponseTextBol){
                westpacResponseTextBol = false;
                westpacResponseText = child.getText() ;
            } 
            
            if (null !=child.getName() && child.getText() == 'response.referenceNo' ) { 
                foundReferenceNumber = true;                
            }

            if(null !=child.getName() && child.getName() == 'value' && foundReferenceNumber){
                foundReferenceNumber = false;
                referenceNo = integer.valueOf(child.getText()) ;
            }   

            System.debug('referenceNo::'+referenceNo);
          }
          
          loadChildsForWRefund(child);       
        }

    }
}