public with sharing class AmendCOIController{
    public List<Account> newAccount {get;set;}
    public list<Certificate__c> currentCert{get;set;}
    public list<Certificate__c> currentCertName{get;set;}
    public list<Property__c> currentProperty{get;set;}
    public List<Attachment> attachments{get;set;}
    public list<Property_Owners__c> currentPropertyOwners{get;set;}
    //public Certificate__c newCert{get;set;}
    public string certID {get;set;}
    public Application__c newApplication {get;set;}
    public List<User> dbiUser {get;set;}
    public string attachmentParentID{get;set;}
    public string currentInvoiceID{get;set;}
    public List<PropertyOwnerWrapper> propOwnerWrapper {get;set;}
    public Integer propOwnerWrapperListSize {get;set;}
    public Integer currentPropertyListSize {get;set;}

    public boolean hasDocuments{get;set;}
    public boolean limitChanges{get;set;}
    public boolean categoryChanges{get;set;}
    public boolean rbpChanges{get;set;}
    public boolean contractTerminated{get;set;}
    public boolean constructionDidNotProceed{get;set;}
    public boolean otherReason{get;set;}
    public boolean hasErrors{get;set;}
    public boolean worksCompleted{get;set;}
    public boolean acceptCompletion{get;set;}
    public boolean worksInProgress{get;set;}
    public boolean showDownloadButton{get;set;}
    public boolean currentOrCancelledDetails{get;set;}

    public String otherRequests {get;set;}
    public boolean hasText {get;set;}
    public boolean showSave{get;set;}
    public boolean isCancelCOI{get;set;}
    public String choice{get;set;}
    public DateTime buildingContractDate {get;set;}
    public String buildingContractDateString {get;set;}
    public DateTime buildingPermitIssuedDate{get;set;}
    public String buildingPermitIssuedDateString{get;set;}
    public DateTime actualStartDate{get;set;}
    public String actualStartDateString{get;set;}
    public DateTime actualCompletionDate{get;set;}
    public String actualCompletionDateString{get;set;}
    public DateTime estStartDate{get;set;}
    public String estStartDateString{get;set;}
    public DateTime estCompletionDate{get;set;}
    public String estCompletionDateString{get;set;}
    public Boolean foundAttachment{get;set;}
    public Boolean OBuilderCertificate{get;set;}
    public Boolean showWithdrawBtn {get;set;}
    public Boolean certFound {get;set;}
    public Boolean isEligibleforAmendCancel {get;set;}
    public Boolean isDistributor {get;set;}
    public String amendPanelTitle {get;set;}
    public String commentsTitle {get;set;}
    public list<certificate__c> certificateHistory{get;set;}
    public boolean certificateHistoryBoolean{get;set;}
    public boolean newCertficate{get;set;}
    public boolean currentAmendOrCancelApplications {get;set;}
    public List<String> currentApplicationStatus = new List<String>{GlobalConstants.APPLICATION_STATUS_NEW, GlobalConstants.APPLICATION_STATUS_ASSIGNED, GlobalConstants.APPLICATION_STATUS_IN_PROG, GlobalConstants.SUB_STAT_AWAITING_INFORMATION, GlobalConstants.APPLICATION_STATUS_NEW_INFORMATION_RECEIVED, GlobalConstants.APPLICATION_STATUS_DD_REV};
    //public String withdrawMessage {get;set;}
    public list<string> ownerNameList {get;set;}

// Code we will invoke on page load. If it is a guest, forward to login page, otherwise if builder is not active or cert not issued, direct away from page.
    public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return new PageReference('/CustomLoginPage');
        }
        /*
        else if(newAccount.size()>0 && currentCert.size()>0){
            if(newAccount[0].Eligibility_Status__c !=constants.account_Eligibility_Status_Active && newAccount[0].Eligibility_Status__c != constants.account_Eligibility_Status_Active_Restricted){
                return new PageReference('/apex/DistributorLanding');
            }
            if(currentCert[0].Status__c != constants.certificate_status_Issued_Works_In_Progress && currentCert[0].Status__c !=constants.certificate_status_Issued_Works_Complete && currentCert[0].Status__c !=constants.certificate_status_Certificate_Issued && currentCert[0].status__c != constants.certificate_status_Takeover ){
                return new PageReference('/apex/DistributorLanding');
            }
            return null;
        }else{
                return new PageReference('/apex/DistributorLanding');
        }
        */
        return null;
    }

    //public void withdrawCertificate(){
    //    try{
    //        currentCert[0].Status__c = GlobalConstants.APP_STATUS_WITHDRAWN;
    //        showWithdrawBtn = false;
    //        Database.update(currentCert[0]);
    //        withdrawMessage = Label.COI_Withdraw_Success;
    //    }
    //    catch(Exception exp){
    //        withdrawMessage = Label.COI_Withdraw_Fail;
    //        // log all the exceptions to application log
    //        Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(amendCOIController.class),
    //                            GlobalConstants.FN_WITHDRAWN_CERTIFICATE,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
            
    //    }
    //}

    public PageReference withdrawCertificate(){
        return new PageReference('/WithdrawConfirmation?certId=' + currentCert[0].id);
    }

    public PageReference generateCert(){
        PageReference certPage;
        if(currentCert[0].Certificate_Type__c == 'OBCertificate'){
            certPage = new PageReference('/GenerateOwnerBuilderCert?id=' + currentCert[0].id);
        }else{
            certPage = new PageReference('/GenerateCertificate?id=' + currentCert[0].id);
        }
        return certPage;
    }


    public PageReference save(){
        hasErrors = False;

        System.debug('choice<>'+choice);
        System.debug('isCancelCOI<>'+isCancelCOI);
        System.debug('acceptCompletion<>'+acceptCompletion);
        if(choice == 'Cancel'){
            isCancelCOI = True;
        }else{
            isCancelCOI = False;
        }

        if(otherRequests=='' || otherRequests==null){
            hasErrors = True;
            /* edited by KATE LUU- SMS Brisbane - 7 June2017 */
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please provide a reason for the request ');
            ApexPages.addMessage(myMsg);
        }
        else if(choice==null || choice==''){
            hasErrors = True; 
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please select the reason for the request ');
            ApexPages.addMessage(myMsg);
        }else if(limitChanges==False && categoryChanges==False && rbpChanges==False && choice=='Amend'){
            hasErrors = True;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please select a reason to AMEND this certificate');
            ApexPages.addMessage(myMsg);
        }else if(contractTerminated==False && constructionDidNotProceed==False && otherReason==False && choice=='Cancel'){
            hasErrors = True;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please select a reason to CANCEL this certificate.');
            ApexPages.addMessage(myMsg);
        }else{
            List<RecordType> recType = [Select Id,SobjectType,Name From RecordType WHERE Name =:constants.amendCOI and SobjectType ='Application__c'  limit 1];
            List<RecordType> recTypeCancel = [Select Id,SobjectType,Name From RecordType WHERE Name =:constants.cancelCOI and SobjectType ='Application__c'  limit 1];
            //List<RecordType> certRecType = [Select Id,SobjectType,Name From RecordType WHERE Name ='Historical Certificate' and SobjectType ='Certificate__c'  limit 1];
            newApplication.Application_Source__c =  constants.application_source_DD_Initiated;
            newApplication.DD_Account__c =  currentCert[0].Distributor__c;
            if(isCancelCOI){
                System.debug('Inside cancel');
                if(recTypeCancel.size()>0){
                    newApplication.recordtypeid = recTypeCancel[0].id;
                    newApplication.type__c = constants.cancelCOI ;
                    newApplication.Contract_Terminated__c = contractTerminated ;
                    newApplication.Construction_Did_Not_Proceed__c = constructionDidNotProceed ;
                    newApplication.Other_Reason__c = otherReason;
                }
            }else if(choice == 'Complete'){
                if(acceptCompletion){
                    currentCert[0].Actual_Completion_Date__c = System.today();
                    currentCert[0].Has_work_Completed__c = True;
                    system.debug('here2');
                    currentCert[0].Status__c = constants.certificate_status_Issued_Works_Complete;
                    if(String.isBlank(currentCert[0].External_Comments__c)){
                        currentCert[0].External_Comments__c = system.now().format('dd/MM/yyyy HH:mm:ss ') + ' ' + userInfo.getName() + ': ' + otherRequests;                        
                    }else{
                        currentCert[0].External_Comments__c += ' \n ' + ' \n ' + system.now().format('dd/MM/yyyy HH:mm:ss ') + ' ' + userInfo.getName() + ': ' + otherRequests;
                    }
                    system.debug('here3: ' + currentCert[0].external_comments__c);
                    attachmentParentID = currentCert[0].id;
                    try{

                        update currentCert;
                    }catch(DmlException e){
                       System.debug('An unexpected error has occurred: ' + e.getMessage());
                       hasErrors = True;
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Failed to update certificate. Please try again');
                        ApexPages.addMessage(myMsg);
                    }
                    return null;
                    }else{
                        hasErrors = True;
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please accept the terms before proceeding');
                        ApexPages.addMessage(myMsg);
                        return null;
                    }

            }else{
                System.debug('Inside Amend');
                if(recType.size()>0){
                    newApplication.recordtypeid = recType[0].id;
                    newApplication.type__c = constants.amendCOI ;
                    newApplication.Project_Contract_Change__c = limitChanges;
                    newApplication.Property_Address_Change__C = categoryChanges;
                    newApplication.Owner_Details_Change__c = rbpChanges ;
                }
            }
            newApplication.External_Comments__c = system.now().format('dd/MM/yyyy HH:mm:ss ') + ' ' + userInfo.getName() + ': ' + otherRequests;

            newApplication.certificate__c = certID;
            newApplication.ownerId = GlobalUtility.getQueues(new list<String>{GlobalConstants.BAT_QUEUE}).get(GlobalConstants.BAT_QUEUE); 
            User u = new User();
            try{
                u = [SELECT Id,ContactId, accountId,IsActive, Profile.name 
                    FROM User
                    WHERE Id =: UserInfo.getUserID()
                    AND IsActive =: true];
            } catch(DmlException e){
                System.debug('An unexpected error has occurred: ' + e.getMessage());
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, string.valueOf(e.getMessage()), ''));
                hasErrors = True;
                return null;
            }
            if(u.profile.name == constants.Distributor){
                newApplication.Application_Status__c = constants.application_Status_new;
            }else{
               newApplication.Application_Status__c = constants.application_Status_DD_Review; 
            }    
            
            try{
                insert newApplication;
            }catch(DmlException e){
               System.debug('An unexpected error has occurred: ' + e.getMessage());
            }
            attachmentParentID = newApplication.id;
            

            if (newAccount.size() > 0){
                newAccount[0].Builder_Eligibility_Status_Reason__c = constants.application_Builder_Eligibility_Status_UnderRev;

                try{
                    update newAccount;
                }catch(DmlException ex){
                   System.debug('An unexpected error has occurred: ' + ex.getMessage());
                }
            }
        }

        

        return null;
    }


    public PageReference confirmation(){
        if(choice == 'Complete'){
                return new PageReference('/apex/CertificateSubmit?id=' + attachmentParentID);

            }else{
                return new PageReference('/apex/EligibilityApplicationConfirmation?id=' + attachmentParentID);
            }
    }

    public PageReference cancel(){
        return new PageReference('/apex/DistributorLanding');

    }

    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        if(isDistributor){
            options.add(new SelectOption('Amend','Amend this Certificate of Insurance'));
            options.add(new SelectOption('Cancel','Cancel this Certificate of Insurance'));
        }
        if(worksInProgress){
            options.add(new SelectOption('Complete','Notify of Completion of Building Works'));
        }
        return options;
    }
/*
    public String getChoice() {
        return choice;
    }
    public void setChoice(String choice) { 
        this.choice = choice; 
    }
*/

    public AmendCOIController() {
        hasDocuments = false;
        hasText = false;
        showSave = false;
        isCancelCOI = false;
        hasErrors = false;
        certFound = False;
        worksInProgress = False;
        
        limitChanges = False;
        categoryChanges = False;
        rbpChanges = False;
        contractTerminated = False;
        constructionDidNotProceed = False;
        otherReason = False;
        buildingContractDateString = '';
        buildingPermitIssuedDateString = '';
        actualStartDateString = '';
        actualCompletionDateString = '';
        currentInvoiceID = '';
        OBuilderCertificate = false;
        currentPropertyOwners = new List<Property_Owners__c>();
        currentProperty = new list<Property__c>();
        Property__c tempProp = new Property__c();
        Property_Owners__c tempPropOwner = new Property_Owners__c();
        currentProperty.add(tempProp);
        //currentPropertyOwners.add(tempPropOwner);
        propOwnerWrapper = new List<PropertyOwnerWrapper>();
        currentPropertyListSize = 0;
        propOwnerWrapperListSize = 0;
        certificateHistory = new list<certificate__c>();
        certificateHistoryBoolean = false;
        newCertficate = false;
        currentAmendOrCancelApplications = false; 
        ownerNameList = new list<string>();

        isDistributor = GlobalUtility.isDistributor;
        List<String> failedInvoiceStatuses = new List<String>{GlobalConstants.STAT_DRAFT, GlobalConstants.STAT_PAYMENT_FAILED, GlobalConstants.STAT_REFUND_FAILED};

        //setSave();
        newApplication = new Application__c();
        newAccount = new List<Account>();
        attachments = new list<Attachment>();
        certID = ApexPages.currentPage().getParameters().get('id');
        if(ApexPages.currentPage().getParameters().get('cancel') == 'True'){
            isCancelCOI = True;
        }

        currentCertName = [Select name, builder__r.Name, builder__r.FirstName, builder__r.LastName from Certificate__c where id=:certID  limit 1]; 
        currentCert = [Select id, builder__c,Type__c,name, Parent_COI__c, Certificate_Type__c,Referral_Reason__c, Taken_Over__c, Building_Permit_Number__c, Referral_Requested__c, Estimated_Value_of_Non_Res_works__c, 
        Permit_Issue_Date__c, Hidden_Most_Recent_Amend__c, Contract_Price__c, TypeofCover__c, Date_Issued__c, Property_Address__c, 
        Building_Contract_Date__c, Levies__c, Premium__c, GST__c, StampDuty__c, Total__c, InvoiceNo__c, InvoiceNo__r.Name,InvoiceNo__r.Type__c, Actual_Start_Date__c,  Actual_Completion_Date__c, 
        Estimated_Start_Date__c, Category_of_Building_Works__c, Description_of_Works__c, Description_of_non_residential_works__c, Speculative__c, 
        Type_of_Building_Contract__c, Number_of_Properties__c, Estimated_Completion_Date__c, External_Comments__c, Have_Works_Commenced__c, Has_work_Completed__c, property__c,
        status__c, sub_categories__c, Out_Of_Date__c, Policy_Number__c,Distributor__c,Property_Owner_Email__c,PIT_First_Owner_Address__c,PIT_At_the_Property__c,PIT_First_Owner_Name__c,PIT_Second_Owner_Name__c,PIT_Third_Owner_Name__c,
        (SELECT Application_Status__c FROM Applications__r WHERE Application_Status__c IN: currentApplicationStatus)
        from Certificate__c where id=:certID  limit 1]; 
        

        if(currentCert.size()>0){
            certFound = True;
            currentAmendOrCancelApplications = (currentCert[0].Applications__r.isEmpty()) ? false : true;
            System.debug('currentCert[0].InvoiceNo__r.Type__c<>'+currentCert[0].InvoiceNo__r.Type__c);
            if(null != currentCert[0].InvoiceNo__r && currentCert[0].InvoiceNo__r.Type__c == GlobalConstants.APPLICATION_STATUS_NEW ){
                newCertficate = true;
            }   

            string policyNumberStr =  currentCert[0].Policy_Number__c ;
            set<string> cerStatusSet = new Set<string>();
            cerStatusSet.add(GlobalConstants.DRAFT_HISTORICAL);
            cerStatusSet.add(GlobalConstants.STAT_AWAITING_PAYMENT);
            cerStatusSet.add(GlobalConstants.SUB_STAT_REFUND);
            
            for(Certificate__c c : [Select id from Certificate__c where Policy_Number__c =:policyNumberStr and Status__c IN: (cerStatusSet) ]){
                currentAmendOrCancelApplications = true;
            }

            System.debug('newCertficate<>'+newCertficate);
            set<string> certificateStatusSet = new Set<string>();
            certificateStatusSet.add(constants.certificate_status_Issued_Works_In_Progress);
            certificateStatusSet.add(constants.certificate_status_Issued_Historical);
            certificateStatusSet.add(constants.certificate_status_Issued_Works_Complete);
            certificateStatusSet.add(constants.application_Builder_Result_Status_Cancelled);

            if(currentCert[0].PIT_First_Owner_Name__c != null)
                ownerNameList.add(currentCert[0].PIT_First_Owner_Name__c);

            if(currentCert[0].PIT_Second_Owner_Name__c != null)
                ownerNameList.add(currentCert[0].PIT_Second_Owner_Name__c);

            if(currentCert[0].PIT_Third_Owner_Name__c != null)
                ownerNameList.add(currentCert[0].PIT_Third_Owner_Name__c);

            set<id> invoiceIdSet = new Set<Id>();    
            if(newCertficate){
                certificateHistory = [Select Id,createdDate,InvoiceNo__c,InvoiceNo__r.name,InvoiceNo__r.Type__c,InvoiceNo__r.PurchaseDate__c,InvoiceNo__r.Total__c from Certificate__c where id =: currentCert[0].id AND InvoiceNo__r.Status__c NOT IN: failedInvoiceStatuses AND Status__c IN: certificateStatusSet];
            }else{
                for(certificate__c cer : [Select Id,createdDate,InvoiceNo__c,InvoiceNo__r.name,InvoiceNo__r.Type__c,InvoiceNo__r.PurchaseDate__c,InvoiceNo__r.Total__c from Certificate__c where Policy_Number__c =: currentCert[0].Policy_Number__c AND InvoiceNo__r.Status__c NOT IN: failedInvoiceStatuses AND Status__c IN: certificateStatusSet order by createdDate desc]){
                    
                    if(!invoiceIdSet.contains(cer.InvoiceNo__c))
                        certificateHistory.add(cer);

                        invoiceIdSet.add(cer.InvoiceNo__c);
                    
                }
            }   
            System.debug('certificateHistory<>'+certificateHistory);
            if(null != certificateHistory && !certificateHistory.isEmpty()){
                certificateHistoryBoolean = true;
            }
            
            newAccount = [Select id, Agent_contact__c, agent__c, ABN__c, vba_number__c, ACN__c, AvailableConstructionLimit__c, Builder_Risk_Rating__c, 
            Eligibility_Status__c, Construction_Limit__c, Flat_Rate_Premium__c, SD_Flat_Rate_Premium__c, SP_Flat_Rate_Premium__c,
            UsedConstructionLimit__c, Original_Eligibility_Approval_Date__c, CO1_New_Single_Dwelling__c, CO3_New_Multi_Dwelling__c,
            C04_Alterations_Additions__c, C05_Swimming_Pools__c, C06_Renovations_Tradespeople__c, C07_Other__c, C08_All_Cover__c,
            Construction_Category_Limit_1__c, Construction_Category_Limit_3__c, Construction_Category_Limit_4__c,
            Construction_Category_Limit_5__c, Construction_Category_Limit_6__c, Other_Construction_Category_Limit__c
            from Account where id=:currentCert[0].builder__c  limit 1]; 

            showWithdrawBtn = (String.isNotBlank(currentCert[0].Status__c) 
                                && ((GlobalConstants.STAT_AWAITING_PAYMENT.equalsIgnoreCase(currentCert[0].Status__c)) 
                                || (GlobalConstants.STAT_REFERRED.equalsIgnoreCase(currentCert[0].Status__c)))) ? true : false;
            
            

            if(currentCert[0].Building_Contract_Date__c != null){
                buildingContractDate =currentCert[0].Building_Contract_Date__c;
                buildingContractDateString = buildingContractDate.format('dd/MM/yyyy');
            }

            if(currentCert[0].Permit_Issue_Date__c != null){
                buildingPermitIssuedDate = currentCert[0].Permit_Issue_Date__c;
                buildingPermitIssuedDateString = buildingPermitIssuedDate.format('dd/MM/yyyy');
            }
            if(currentCert[0].Actual_Start_Date__c != null){
                actualStartDate = currentCert[0].Actual_Start_Date__c;
                actualStartDateString = actualStartDate.format('dd/MM/yyyy');
            }
            if(currentCert[0].Actual_Completion_Date__c != null){
                actualCompletionDate = currentCert[0].Actual_Completion_Date__c;
                actualCompletionDateString = actualCompletionDate.format('dd/MM/yyyy');
            }
            if(currentCert[0].Estimated_Start_Date__c != null){
                estStartDate = currentCert[0].Estimated_Start_Date__c;
                estStartDateString = estStartDate.format('dd/MM/yyyy');
            }
            if(currentCert[0].Estimated_Completion_Date__c != null){
                estCompletionDate = currentCert[0].Estimated_Completion_Date__c;
                estCompletionDateString = estCompletionDate.format('dd/MM/yyyy');
            }
            worksInProgress = False;
            if(currentCert[0].status__c == constants.certificate_status_Issued_Works_In_Progress){
                worksInProgress = true;
            }

            if(currentCert[0].status__c == constants.certificate_status_Issued_Works_In_Progress || currentCert[0].status__c == constants.certificate_status_Takeover ||  currentCert[0].status__c == constants.certificate_status_Issued_Works_Complete ){
                showDownloadButton = True;
            }

            if(currentCert[0].status__c == constants.certificate_status_Issued_Works_In_Progress || currentCert[0].status__c == constants.certificate_status_Takeover ||  currentCert[0].status__c == constants.certificate_status_Issued_Works_Complete || currentCert[0].status__c == constants.certificate_status_Issued_Historical || currentCert[0].status__c == GlobalConstants.STAT_CANCELLED){
                currentOrCancelledDetails = True;
            }

            
            
            currentInvoiceID = currentCert[0].InvoiceNo__c;
            worksCompleted = currentCert[0].Has_work_Completed__c;


            System.debug('currentCert[0].id::>>'+currentCert[0].id);

           //currentProperty = [select Lot_Number__c, Plan_Number__c, Property_Address__c, SPI__c, PropertyOwner__c from Property__c where id=:currentCert[0].property__c];

            if(!currentCert[0].Speculative__c){
                //currentPropertyOwners = [select id, name, Property__c, property_Owner__r.Primary_Email__c, RELATIONSHIP_BETWEEN_OWNER_AND_APPLICANT__c,property_Owner__r.BillingStreet ,property_Owner__r.IsPersonAccount , property_Owner__r.Shippingstreet,property_Owner__r.ShippingCity,property_Owner__r.ShippingState,property_Owner__r.ShippingCountry,property_Owner__r.ShippingPostalCode,property_Owner__r.name, property_Owner__r.PersonEmail,property_Owner__r.PersonMailingStreet, property_Owner__r.PersonMailingCity, property_Owner__r.PersonMailingState, property_Owner__r.PersonMailingPostalCode, contact__c,contact__r.name, contact__r.email, contact__r.phone, contact__r.mailingstreet, contact__r.mailingState, contact__r.mailingCity, contact__r.mailingPostalCode from Property_Owners__c where certificate__c =:currentCert[0].id];
                for(Property_Owners__c prop : [SELECT id, Name, Property__c, property_Owner__r.Primary_Email__c, 
                                                    RELATIONSHIP_BETWEEN_OWNER_AND_APPLICANT__c,property_Owner__r.BillingStreet, property_Owner__r.BillingCountry, 
                                                    property_Owner__r.BillingCity, property_Owner__r.BillingState, property_Owner__r.BillingPostalCode,
                                                    property_Owner__r.IsPersonAccount , property_Owner__r.Shippingstreet,property_Owner__r.ShippingCity,
                                                    property_Owner__r.ShippingState,property_Owner__r.ShippingCountry,property_Owner__r.ShippingPostalCode,
                                                    property_Owner__r.Name, property_Owner__r.PersonEmail,property_Owner__r.PersonMailingStreet, 
                                                    property_Owner__r.PersonMailingCity, property_Owner__r.PersonMailingState, 
                                                    property_Owner__r.PersonMailingPostalCode,property_Owner__r.PersonMailingCountry, Contact__c,contact__r.Account.Name, 
                                                    contact__r.Account.Primary_Email__c, contact__r.Account.ShippingStreet, contact__r.Account.ShippingState, 
                                                    contact__r.Account.ShippingCity, Contact__r.Account.ShippingPostalCode, Contact__r.Account.ShippingCountry,
                                                    contact__r.Account.BillingStreet, contact__r.Account.BillingCountry, contact__r.Account.BillingCity, contact__r.Account.BillingState,
                                                    contact__r.Account.BillingPostalCode, Contact__r.Email, Contact__r.Phone
                                                    FROM Property_Owners__c 
                                                    WHERE Certificate__c =:CurrentCert[0].id]){
                    String ownerName = '';
                    String ownerStreet = '';
                    String ownerCity = '';
                    String ownerState = '';
                    String ownerPostalCode = '';
                    String ownerCountry = '';
                    String ownerEmail = '';


                    currentPropertyOwners.add(prop);

                    if(!String.isEmpty(prop.property_Owner__c)){
                        ownerName = (String.isEmpty(prop.property_Owner__r.Name) ? '' : prop.property_Owner__r.Name);
                        ownerEmail = (String.isEmpty(prop.property_Owner__r.Primary_Email__c) ? '' : prop.property_Owner__r.Primary_Email__c);
                        if(!String.isEmpty(prop.property_Owner__r.Shippingstreet)){
                            ownerStreet = (String.isEmpty(prop.property_Owner__r.Shippingstreet) ? '' : prop.property_Owner__r.Shippingstreet);
                            ownerCity =  (String.isEmpty(prop.property_Owner__r.ShippingCity) ? '' : prop.property_Owner__r.ShippingCity); 
                            ownerState = (String.isEmpty(prop.property_Owner__r.ShippingState) ? '' : prop.property_Owner__r.ShippingState);
                            ownerPostalCode = (String.isEmpty(prop.property_Owner__r.ShippingPostalCode) ? '' : prop.property_Owner__r.ShippingPostalCode);
                            ownerCountry = (String.isEmpty(prop.property_Owner__r.ShippingCountry) ? '' : prop.property_Owner__r.ShippingCountry);
                        }
                        else if(!String.isEmpty(prop.property_Owner__r.PersonMailingStreet)){
                            ownerStreet = (String.isEmpty(prop.property_Owner__r.PersonMailingStreet) ? '' : prop.property_Owner__r.PersonMailingStreet);
                            ownerCity = (String.isEmpty(prop.property_Owner__r.PersonMailingCity) ? '' : prop.property_Owner__r.PersonMailingCity);
                            ownerState = (String.isEmpty(prop.property_Owner__r.PersonMailingState) ? '' : prop.property_Owner__r.PersonMailingState);
                            ownerPostalCode = (String.isEmpty(prop.property_Owner__r.PersonMailingPostalCode) ? '' : prop.property_Owner__r.PersonMailingPostalCode);
                            ownerCountry = (String.isEmpty(prop.property_Owner__r.PersonMailingCountry) ? '' : prop.property_Owner__r.PersonMailingCountry);
                        }
                        else{
                            ownerStreet = (String.isEmpty(prop.property_Owner__r.BillingStreet) ? '' : prop.property_Owner__r.BillingStreet);
                            ownerCity = (String.isEmpty(prop.property_Owner__r.BillingCity) ? '' : prop.property_Owner__r.BillingCity);
                            ownerState = (String.isEmpty(prop.property_Owner__r.BillingState) ? '' : prop.property_Owner__r.BillingState);
                            ownerPostalCode = (String.isEmpty(prop.property_Owner__r.BillingPostalCode) ? '' : prop.property_Owner__r.BillingPostalCode);
                            ownerCountry = (String.isEmpty(prop.property_Owner__r.BillingCountry) ? '' : prop.property_Owner__r.BillingCountry);
                        }
                    }
                    else{
                        ownerName = (String.isEmpty(prop.contact__r.Account.Name) ? '' : prop.contact__r.Account.Name);
                        ownerEmail = (String.isEmpty(prop.contact__r.Account.Primary_Email__c) ? '' : prop.contact__r.Account.Primary_Email__c);
                        if(!String.isEmpty(prop.contact__r.Account.Shippingstreet)){
                            ownerStreet = (String.isEmpty(prop.contact__r.Account.Shippingstreet) ? '' : prop.contact__r.Account.Shippingstreet);
                            ownerCity = (String.isEmpty(prop.contact__r.Account.ShippingCity) ? '' : prop.contact__r.Account.ShippingCity);
                            ownerState = (String.isEmpty(prop.contact__r.Account.ShippingState) ? '' : prop.contact__r.Account.ShippingState);
                            ownerPostalCode = (String.isEmpty(prop.contact__r.Account.ShippingPostalCode) ? '' : prop.contact__r.Account.ShippingPostalCode);
                            ownerCountry = (String.isEmpty(prop.contact__r.Account.ShippingCountry) ? '' : prop.contact__r.Account.ShippingCountry);
                        }
                        else{
                            ownerStreet = (String.isEmpty(prop.contact__r.Account.BillingStreet) ? '' : prop.contact__r.Account.BillingStreet);
                            ownerCity = (String.isEmpty(prop.contact__r.Account.BillingCity) ? '' : prop.contact__r.Account.BillingCity);
                            ownerState = (String.isEmpty(prop.contact__r.Account.BillingState) ? '' : prop.contact__r.Account.BillingState);
                            ownerPostalCode = (String.isEmpty(prop.contact__r.Account.BillingPostalCode) ? '' : prop.contact__r.Account.BillingPostalCode);
                            ownerCountry = (String.isEmpty(prop.contact__r.Account.BillingCountry) ? '' : prop.contact__r.Account.BillingCountry);
                        }
                    }
                    ownerName = ownerName.replace('null', '');
                    ownerStreet = ownerStreet.replace('null', '');
                    ownerCity = ownerCity.replace('null', '');
                    ownerState = ownerState.replace('null', '');
                    ownerPostalCode = ownerPostalCode.replace('null', '');
                    ownerCountry = ownerCountry.replace('null', '');
                    ownerEmail = ownerEmail.replace('null', '');


                    propOwnerWrapper.add(new PropertyOwnerWrapper(ownerName, ownerStreet, ownerCity, ownerState, ownerPostalCode, ownerCountry, ownerEmail));
                }
                if(!currentPropertyOwners.isEmpty()){
                    currentProperty = [select Lot_Number__c, Plan_Number__c, Property_Address__c, SPI__c, PropertyOwner__c from Property__c where id=:currentPropertyOwners[0].property__c];
                }
            }else{
                currentProperty = [select Lot_Number__c, Plan_Number__c, Property_Address__c, SPI__c, PropertyOwner__c from Property__c where id=:currentCert[0].property__c];
            }
            
            List<Certificate__c> containerCert = [SELECT id,Certificate_Type__c  FROM Certificate__c where Id =: currentCert[0].Parent_COI__c 
                                                    AND  (Certificate_Type__c =: GlobalConstants.CERT_TYPE_OBCONTAINER OR Certificate_Type__c =: GlobalConstants.REC_TYPE_CONTAINER)];

            if(!containerCert.isEmpty()){
                attachments = [SELECT name, ContentType, CreatedDate, CreatedBy.Name FROM Attachment WHERE ParentID=: currentCert[0].Parent_COI__c];
            }else{
                attachments = [SELECT name, ContentType, CreatedDate, CreatedBy.Name FROM Attachment WHERE ParentID=: currentCert[0].id];
            }                                        
            

            foundAttachment = false;
            if(!attachments.isEmpty()){
                foundAttachment = true;
            }
            
           

            System.debug('currentPropertyOwners::>>'+currentPropertyOwners);
            System.debug('attachments::>>'+attachments);
        }

        if (newAccount.size() > 0){
            
            newApplication.Builder_ABN__c = newAccount[0].ABN__c;
            newApplication.Builder_ACN__c = newAccount[0].ACN__c;
            newApplication.Builder_Number__c = newAccount[0].id;
            if(String.isNotBlank(currentCertName[0].Builder__r.LastName)){
                newApplication.Builder_First_Name__c = currentCertName[0].Builder__r.FirstName;
                newApplication.Builders_Last_Name__c = currentCertName[0].Builder__r.LastName;
            }else{
                newApplication.builder_Legal_entity_name__c = currentCertName[0].Builder__r.Name;
            }
            newApplication.vba_number__c = newAccount[0].vba_number__c;
            dbiUser = [Select id, contactId from User where contactId =: newAccount[0].Agent_contact__c Limit 1];
            newApplication.DD_Account__c = newAccount[0].agent__c;

            if (dbiUser.size()>0){
                newApplication.dbi_Distributor__c = dbiUser[0].id;
            }   
        }else{
            
            OBuilderCertificate = true;
        }

        amendPanelTitle = (getItems().size() == 1) ? Label.Notify_COI : Label.Amend_Cancel_COI;
        commentsTitle =  (getItems().size() == 1) ? Label.Notify_COI_Comments : Label.Reason_For_Amend_Cancel;
        propOwnerWrapperListSize = propOwnerWrapper.size();
        currentPropertyListSize = currentProperty.size();
        isEligibleforAmendCancel = (!currentCert.isEmpty() 
                                        && String.isNotBlank(currentCert[0].Status__c)
                                        && (GlobalConstants.STAT_ISSUED_IN_PROGRESS.equalsIgnoreCase(currentCert[0].Status__c)) 
                                        && !currentAmendOrCancelApplications
                                        && getItems().size() > 0) ? true : false; 

    }

    //rollback application & account when file size is more than 25MB
    public void rollBackApplication(){
        try{
            GlobalUtility.deleteApplicaiton(new List<Id>{newApplication.Id});   // invoking utility method which runs under system context as DD cannot delete applicaiton
            newApplication.Id = null;
        }
        catch(Exception exp){
            // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(AmendCOIController.class),
                                GlobalConstants.FN_ROLLBACK_APPLICATION,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }

    public class PropertyOwnerWrapper{
        public String ownerName {get;set;}
        public String street {get;set;}
        public String city {get;set;}
        public String state {get;set;}
        public String postalCode {get;set;}
        public String country {get;set;}
        public String ownerEmail {get;set;}

        public PropertyOwnerWrapper(String newOwnerName, String newStreet, String newCity, String newState, String newPostalCode, String newCountry, String newEmail){
            ownerName = newOwnerName;
            street = newStreet;
            city = newCity;
            state = newState;
            postalCode = newPostalCode;
            country = newCountry;
            ownerEmail = newEmail;
        }
    }

}