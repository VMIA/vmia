/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Helper Class for BatLetterstrigger
  */

/*******************************  History ************************************************
    Date                User                                        Comments


*******************************  History ************************************************/
public with sharing class BatLettersTriggerHelper{

    // variables 
    private Boolean beforeEventFirstRun = true;
    private Boolean afterEventFirstRun = true;

    private Map<String,Id> lettersRecTypeMap ;

    // constructor : initialize necessary variables
    public BatLettersTriggerHelper(){
        lettersRecTypeMap = GlobalUtility.getsObjectRecordTypes(String.valueOf(BAT_Letter__c.sObjectType));
    }

    public Boolean isBeforeEventFirstRun(){
        Boolean retVal = beforeEventFirstRun;
        beforeEventFirstRun = false;
        return retVal;
    }

    public Boolean isAfterEventFirstRun(){
        Boolean retVal = afterEventFirstRun;
        afterEventFirstRun = false;
        return retVal;
    }

    /**
      * @description       This method is invoked from handler for before insert event to populate builder field
      * @param             NA
      * @return            Void
      * @throws            Exception is handled by try-catch block
      */

    public void setBuilderField(){
        Map<Id,BAT_Letter__c> applicationWithBuilderMap = new Map<id,BAT_Letter__c>();
        Map<Id,BAT_Letter__c> certWithBuilderMap = new Map<id,BAT_Letter__c>();

        for(Bat_Letter__c letter: (List<Bat_Letter__c>) Trigger.new){
            if(!String.isBlank(letter.application__c)){
                applicationWithBuilderMap.put(letter.application__c, letter);
            }
            if(!String.isBlank(letter.certificate__c)){
                certWithBuilderMap.put(letter.certificate__c, letter);
            }
        }

        List<Application__c> appList = [Select id, builder_number__c from Application__c where id IN :applicationWithBuilderMap.keyset()];
        List<certificate__c> certList = [Select id, Builder__c from Certificate__c where id IN :certWithBuilderMap.keyset()];

        for(Application__c app : appList){
            applicationWithBuilderMap.get(app.id).builder__c = app.builder_number__c;
        }
        for(certificate__c cert : certList){
            certWithBuilderMap.get(cert.id).builder__c = cert.Builder__c;
        }

    }

}