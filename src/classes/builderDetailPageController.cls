public class builderDetailPageController {
    private Id builderId ;
    public Account builderRecord {get;set;}
    public map<String,Account> ddMap {get;set;}
    public list<Account> ddList {get;set;}
    public list<String> ddListSelection;
    private static final String GUEST = 'Guest';
    private String recordTypeDistributor;

    public String ddSelection {get;set;}

    public String editPhone {get;set;}
    public String editEmail {get;set;}
    public String editTradingStreet {get;set;}
    public String editTradingCity {get;set;}
    public String editTradingState {get;set;}
    public String editTradingPostalCode {get;set;}
    public String editTradingCountry {get;set;}
    public String editPostalStreet {get;set;}
    public String editPostalCity {get;set;}
    public String editPostalState {get;set;}
    public String editPostalPostalCode {get;set;}
    public String editPostalCountry {get;set;}
    public String builderName {get;set;}

    public boolean declarationAccept {get;set;}
    public boolean errorsFound {get;set;}

    public builderDetailPageController (){
        builderId = ApexPages.currentPage().getParameters().get('ID');
        errorsFound = False;
        recordTypeDistributor = GlobalUtility.getsObjectRecordTypes('Account').get(Constants.Distributor);
        ddSelection = '';
        DDMap = new Map<String, Account>();
        ddListSelection = new list<String>();
        builderName = string.valueOf(GlobalUtility.currentBuilder.Name);
        builderRecord = [SELECT Agent_contact__c, Builder_Eligibility_Status_Reason__c, ABN__c, ACN__c, AvailableConstructionLimit__c,
                                        Agent__r.Name, Agent__r.Website, Agent__r.Phone,
                                        Eligibility_Status__c, Construction_Limit__c, SD_Flat_Rate_Premium__c, SP_Flat_Rate_Premium__c,
                                        UsedConstructionLimit__c, Original_Eligibility_Approval_Date__c,Construction_Category_Limit_1__c, 
                                        Construction_Category_Limit_3__c, Construction_Category_Limit_4__c,Construction_Category_Limit_5__c, 
                                        Construction_Category_Limit_6__c,Construction_Category_Limit_7__c,After_Hours_Phone__c,
                                        DBDRU_Ref__c, DBDRU_Date__c, BillingCity,BillingState, BillingStreet, BillingCountry, BillingPostalCode,  
                                        ShippingCity,ShippingState, ShippingStreet, ShippingCountry, ShippingPostalCode,
                                        Legal_Business_Type__c, Phone, Primary_Email__c, Agent__c,Agent__r.Primary_Email__c, Date_Started_Trading__c,
                                        ASIC_Entity__c, ASIC_Status__c, CO1_New_Single_Dwelling__c,C03_Multi_Dwelling_Flat_Rate_Premium__c,CO3_New_Multi_Dwelling__c,
                                        C04_Alterations_Additions__c,C04_Structural_Flat_Rate_Premium__c,C05_Swimming_Pools__c,C06_Renovations_Tradespeople__c,
                                        C07_Other__c,C07_Other_Flat_Rate_Premium__c,C08_All_Cover__c,C08_All_Cover_Flat_Rate_Premium__c,Flat_Rate_Premium__c, 
                                        Builder_Risk_Rating__c,Eligibility_Review_Date__c,Non_Structural_Flat_Rate__c,Eligibility_Conditions__c,Other_Terms__c, Pending_Eligibility_Issued_Date__c
                                        FROM Account WHERE Id =: builderId LIMIT 1];


        ddList = [Select id, name, website                  
                        FROM Account WHERE recordTypeid =:recordTypeDistributor AND id!= :builderRecord.agent__c];

        for (Account acc : ddList){
            DDMap.put(acc.name, acc);
        }

        system.debug('here22: ' + ddList);

        editPhone = builderRecord.phone;
        editEmail = builderRecord.Primary_Email__c;
        editTradingStreet = builderRecord.BillingStreet;
        editTradingCity = builderRecord.BillingCity;
        editTradingState = builderRecord.BillingState;
        editTradingPostalCode = builderRecord.BillingPostalCode;
        editTradingCountry = builderRecord.BillingCountry;
        editPostalStreet = builderRecord.ShippingStreet;
        editPostalCity = builderRecord.ShippingCity;
        editPostalState = builderRecord.ShippingState;
        editPostalPostalCode = builderRecord.ShippingPostalCode;
        editPostalCountry = builderRecord.ShippingCountry;

    }

    public PageReference forwardToCustomAuthPage() {
        PageReference pageRef;
        if(GUEST.equalsIgnoreCase(UserInfo.getUserType())){
            pageRef = Page.Login;
        }
        return pageRef;
    }

    public pageReference confirm(){
        return new PageReference('/builderLandingPage?id=' + builderRecord.id);     
    }

    public pageReference cancel(){
        return new PageReference('/builderLandingPage?id=' + builderRecord.id);     
    }

    public pageReference save(){
        if (!validate()){
            builderRecord.phone = editPhone;
            builderRecord.Primary_Email__c = editEmail;
            builderRecord.BillingStreet = editTradingStreet;
            builderRecord.BillingCity = editTradingCity;
            builderRecord.BillingState = editTradingState ;
            builderRecord.BillingPostalCode = editTradingPostalCode;
            builderRecord.BillingCountry = editTradingCountry;
            builderRecord.ShippingStreet = editPostalStreet;
            builderRecord.ShippingCity = editPostalCity;
            builderRecord.ShippingState = editPostalState;
            builderRecord.ShippingPostalCode = editPostalPostalCode;
            builderRecord.ShippingCountry = editPostalCountry;

            if(!String.isBlank(ddSelection)){
                builderRecord.Agent__c = ddMap.get(ddSelection).id;
                builderRecord.Agent_Contact__c = [Select id from Contact where Accountid =: ddMap.get(ddSelection).id limit 1].id;
                builderRecord.Date_DD_Nominated_by_Builder__c = system.today();
            }

            try{
                update builderRecord;
            }catch(DmlException e) {
                errorsFound = True;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Error updating Builder Record. Please try again Later');
                ApexPages.addMessage(myMsg);
                return null;
            }
            return confirm();
        }

        return null;

        
    }

    public Boolean isValidEmail(String email) {
        Boolean res = true;
            
        
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);

        if (!MyMatcher.matches()) 
            res = false;

        return res; 
    }

     public boolean validate() {
        try{
            errorsFound = False;
            //ABN check
            if(String.isBlank(editEmail) || String.isBlank(editPhone)
                || String.isBlank(editTradingStreet)|| String.isBlank(editTradingCity)
                || String.isBlank(editTradingState)|| String.isBlank(editTradingPostalCode)
                || String.isBlank(editPostalCity)|| String.isBlank(editPostalState)
                || String.isBlank(editPostalPostalCode)|| String.isBlank(editPostalStreet)
                || String.isBlank(editTradingCountry)|| String.isBlank(editPostalCountry)){
                errorsFound = True;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please fill in all madatory fields before saving');
                ApexPages.addMessage(myMsg);
                return true;
            }
            if(!isValidEmail(editEmail)){
                errorsFound = True;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Email address not in correct format');
                ApexPages.addMessage(myMsg);
                return true;
            }

            if(!String.isBlank(ddSelection)){
                if(!declarationAccept){
                    errorsFound = True;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'You must Accept the declaration before changing a Distributor');
                    ApexPages.addMessage(myMsg);
                    return true;
                }
            }


        }catch(DmlException e) {
                errorsFound = True;
                System.debug('HELP2 ERROR: ' + e.getMessage());
                return true;
            }
 
        return false;
        
    }

    public List<SelectOption> getDDListSelection() {
            List<SelectOption> options = new List<SelectOption>();
            for(Account acc : ddMap.values()){
                if(!String.isBlank(acc.website))
                    options.add(new SelectOption(acc.name, acc.name + '   (' + acc.website + ')'));
                else {
                    options.add(new SelectOption(acc.name, acc.name));
                }
            }
            return options;
        }
            
        public String getDDSelection() {
            return ddSelection;
        }
             
        public void setDDSelection(String ddSelection) {
            this.ddSelection = ddSelection;
        }

}