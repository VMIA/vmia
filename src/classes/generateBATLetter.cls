public with sharing class generateBATLetter{
    public String letterID {get;set;}
    public BAT_Letter__c letter {get;set;}
    public DateTime currentDate {get;set;}
    public String currentDateString {get;set;}
    public DateTime issuedDate {get;set;}
    public String issuedDateString {get;set;}
    public String letterDescription{get;set;}
    public List<Contact_Role__c> rbpContact {get;set;}
    public String rbpName {get;set;}

public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return Page.Login;
        }
        return null;
    }
    public generateBATLetter(){
        letterID= ApexPages.CurrentPage().getParameters().get('id');

        List<String> fields = new List<String>(BAT_Letter__c.SObjectType.getDescribe().fields.getMap().keySet());
        fields.add('Builder__r.BillingStreet');
         fields.add('Builder__r.BillingCity');
         fields.add('Builder__r.BillingPostalCode');
         fields.add('Builder__r.BillingState');
         fields.add('Builder__r.BillingCountry');
         fields.add('Builder__r.name');
         fields.add('Builder__r.Account_Number__c');
         fields.add('Builder__r.Agent__r.name');
         fields.add('Builder__r.Agent__r.phone');
         fields.add('Builder__r.Agent__r.BillingStreet');
         fields.add('Builder__r.Agent__r.BillingCity');
         fields.add('Builder__r.Agent__r.BillingPostalCode');
         fields.add('Builder__r.Agent__r.BillingState');
         fields.add('Builder__r.Agent__r.BillingCountry');
         fields.add('Builder__r.Agent__r.Primary_Email__c');
         fields.add('Builder__r.Agent__r.website');
         fields.add('Builder__r.Building_Practitioner_Name_1__c');

        String soql = ''
                + ' select ' + String.join(fields, ',')
                + ' from BAT_Letter__c'
                + ' where id=:' 
                + 'letterID';
                
        System.debug('generateBATLetter.letterID = ' + letterID);
        System.debug('generateBATLetter.soql = ' + soql);
        
        letter = Database.query(soql);
        System.debug('generateBATLetter.letter = ' + letter);

        rbpContact = [select    Contact__c, Contact__r.Name
                                    from    Contact_Role__c 
                                    where   Account__c = :letter.Builder__c 
                                    AND     Role__c = :GlobalConstants.ACCOUNTCONTACTROLE_PRIMARY_DIRECTOR];

        if(rbpContact.isEmpty()){
            rbpName = letter.Builder__r.Name;
        }else {
            rbpName = rbpContact[0].Contact__r.Name;
        }

        currentDate = Date.today();
        letterDescription = letter.Letter_Description__c.replace('\n', '<br/>');
        //letterDescription = letterDescription.replace('<br>', '<br/>');
        currentDateString = currentDate.format('dd/MM/yyyy');

        issuedDate = letter.createdDate;
        issuedDateString = issuedDate.format('dd/MM/yyyy');

    }

}