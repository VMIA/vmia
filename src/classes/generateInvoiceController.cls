public class GenerateInvoiceController {
 
    //Our collection of the class/wrapper objects cContact
    public List<cCertificate> certificateList {get; set;}
    @testVisible String value = ApexPages.currentPage().getParameters().get('id');
    public String currentDateString {get;set;}
    public string invDate {get;set;}
    public DateTime invDateTime {get;set;}
    public DateTime currentDate {get;set;}
    public String ownerText {get;set;}
    public decimal contractPriceTotal {get;set;}
    public String owners {get;set;}
    public list<String> ownerNamesList {get;set;}
    public String builderACN {get;set;}
    public String builderABN {get;set;}
    public String builderNumber {get;set;}
    public String street {get;set;}
    public String city {get;set;}
    public String state {get;set;}
    public String postalcode {get;set;}
    public String country {get;set;}
    public boolean isBuilderAddressAustralia {get;set;}
    public String distributorName {get;set;}
    public String distributorStreet {get;set;}
    public String distributorCity {get;set;}
    public String distributorState {get;set;}
    public String distributorPostalcode {get;set;}
    public String distributorCountry {get;set;}
    public String distributorPhone {get;set;}
    public String distributorWebsite {get;set;}
    public String distributorEmail {get;set;}
    public boolean displayACN {get;set;}
    public boolean displayABN {get;set;}
    public list<Invoice__c> inv{get; set;}
    public string builderAccountNumber{get;set;}

    
    //Get the Invoice record
    public GenerateInvoiceController(ApexPages.StandardController controller){
        ownerText = '';
        owners = '';
        contractPriceTotal = 0;
        displayACN = false;
        displayABN = false;
        street = '';
        city = '';
        state = '';
        postalcode = '';
        country = '';
        isBuilderAddressAustralia = false;

        inv = [select Id
        , Name, Invoice_Date__c, PurchaseDate__c
        , Purchaser__c, PurchaserABN__c, Premium__c
        , PurchaserACN__c, Total__c, UserCreatedBy__c,purchaser__r.Agent__r.name, purchaser__r.Agent__r.Phone, purchaser__r.Agent_Contact__r.name,
        purchaser__r.isPersonAccount, purchaser__r.Agent__r.isPersonAccount, purchaser__r.name, purchaser__r.RecordTypeId, purchaser__r.Agent__r.website, 
        purchaser__r.phone, purchaser__r.BillingStreet, purchaser__r.BillingCity, purchaser__r.website, purchaser__r.Primary_Email__c,
        purchaser__r.BillingPostalCode, purchaser__r.BillingState, purchaser__r.BillingCountry,  purchaser__r.Agent__r.Primary_Email__c,
        purchaser__r.Agent_Contact__r.phone, purchaser__r.Agent__r.BillingStreet, purchaser__r.Agent__r.BillingCity, 
        purchaser__r.Agent__r.BillingPostalCode, purchaser__r.Agent__r.BillingState, purchaser__r.Agent__r.BillingCountry
      from Invoice__c where id = :value];
      
      if(inv.size()<1){
        validateAndRedirect();
      }else{
          currentDate = Date.today();
            currentDateString = currentDate.format('dd/MM/yyyy');
          if(inv[0].Invoice_Date__c == null){
            validateAndRedirect();
          }  else{
            invDateTime = inv[0].Invoice_Date__c;
            invDate = invDateTime.format('dd/MM/yyyy');
          
          }
          getCertificates();
          system.debug('hey3:' + inv[0].purchaser__r.Agent__r.name);

      }

        if(inv[0].purchaser__r.RecordTypeId == GlobalUtility.getsObjectRecordTypes('Account').get('Distributor')){
            distributorName = inv[0].purchaser__r.Name;
            distributorStreet = inv[0].purchaser__r.BillingStreet;
            distributorCity = inv[0].purchaser__r.BillingCity;
            distributorState = inv[0].purchaser__r.BillingState;
            distributorPostalcode = inv[0].purchaser__r.BillingPostalCode;
            distributorCountry = inv[0].purchaser__r.BillingCountry;
            distributorPhone = inv[0].purchaser__r.Phone;
            distributorWebsite = inv[0].purchaser__r.Website;
            distributorEmail = inv[0].purchaser__r.Primary_Email__c;
        }else{
            distributorName = inv[0].purchaser__r.Agent__r.Name;
            distributorStreet = inv[0].purchaser__r.Agent__r.BillingStreet;
            distributorCity = inv[0].purchaser__r.Agent__r.BillingCity;
            distributorState = inv[0].purchaser__r.Agent__r.BillingState;
            distributorPostalcode = inv[0].purchaser__r.Agent__r.BillingPostalCode;
            distributorCountry = inv[0].purchaser__r.Agent__r.BillingCountry;
            distributorPhone = inv[0].purchaser__r.Agent__r.Phone;
            distributorWebsite = inv[0].purchaser__r.Agent__r.Website;
            distributorEmail = inv[0].purchaser__r.Agent__r.Primary_Email__c;
        }
        if(distributorWebsite != null){
            if(distributorWebsite.length()>27){
                String subthird = '';
                String subSecond = '';
                String subfirst = distributorWebsite.substring(0,27);
                if(distributorWebsite.length()>54){
                    subSecond = distributorWebsite.substring(27,54);
                    subthird = distributorWebsite.substring(54, distributorWebsite.length());
                    distributorWebsite = subfirst + '\n' + subSecond + '\n' + subthird; 
                }else{
                    subSecond = distributorWebsite.substring(27,distributorWebsite.length());
                    distributorWebsite = subfirst + '\n' + subSecond; 
                }
                
        }
        }
        
    }

public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return Page.Login;
        }
        return null;
    }
      
    //This method uses a simple SOQL query to return a List of Contacts
    public List<cCertificate> getCertificates() {
        if(certificateList == null) {
            certificateList = new List<cCertificate>();
            List<Id> certIdList = new List<Id>();
            ownerNamesList = new list<String>();
            builderACN = '';
            builderABN ='';
            builderNumber = '';
            for(Certificate__c c: [select Id, Name, Builder__c, Builder__r.name, Builder__r.Account_Number__c, Builder__r.ACN__c, Builder__r.ABN__c, 
                Builder__r.BillingStreet, Builder__r.Billingcity, Builder__r.Billingstate, Builder__r.Billingpostalcode, Builder__r.Billingcountry,
                Builder__r.ShippingStreet, Builder__r.Shippingcity, Builder__r.Shippingstate, Builder__r.Shippingpostalcode, Builder__r.Shippingcountry,
                PropertyAddressHidden__c, Contract_Price__c, Property_Address__c, 
                category_of_building_works__c, Premium__c,Amended_Premium__c, Levies__c, gst__c, stampduty__c, Total__c, InvoiceNo__c,  Policy_Number__c
                from Certificate__c where InvoiceNo__c = :value and (Certificate_Type__c = 'Certificate' or Certificate_Type__c = 'OBCertificate')
                AND Status__c !=: GlobalConstants.STAT_WITHDRAWN]) {
               // As each contact is processed we create a new cContact object and add it to the contactList
                certificateList.add(new cCertificate(c));
                contractPriceTotal += c.Contract_Price__c;
                IF(c.Builder__c == null){
                    ownerText = 'Owner ';
                }else{
                    builderAccountNumber = c.Builder__r.Account_Number__c;
                    owners = c.Builder__r.name; 
                    if(String.isBlank(builderACN)){
                        builderACN = c.Builder__r.ACN__c;
                    }
                    if(String.isBlank(builderABN)){
                        builderABN = c.Builder__r.ABN__c;
                    }
                    if(String.isBlank(builderNumber)){
                        builderNumber = c.Builder__r.Account_Number__c;
                    }
                    if(String.isNotBlank(c.Builder__r.ShippingStreet)){
                        street = c.Builder__r.ShippingStreet;
                        city = c.Builder__r.Shippingcity;
                        state = c.Builder__r.Shippingstate;
                        postalCode = c.Builder__r.Shippingpostalcode;
                        country = c.Builder__r.Shippingcountry;
                        
                    }else{
                        street = c.Builder__r.BillingStreet;
                        city = c.Builder__r.Billingcity;
                        state = c.Builder__r.Billingstate;
                        postalCode = c.Builder__r.Billingpostalcode;
                        country = c.Builder__r.Billingcountry;
                    }
                }
                certIdList.add(c.Id);
                
           } 

           displayACN = String.isNotBlank(builderACN) ? true : false;
           displayABN = !displayACN ? String.isNotBlank(builderABN) ? true : false : false ;
 
           Integer i = 0;
            if(String.isBlank(owners)){
                for(Property_Owners__c pOwner : [select id,Contact__c, Contact__r.Account.Name, Contact__r.Account.ACN__c, Contact__r.Account.ABN__c,
                                                    contact__r.account.recordTypeId, contact__r.account.BillingStreet, contact__r.account.BillingCity, 
                                                    contact__r.account.BillingState, contact__r.account.BillingPostalCode, contact__r.account.BillingCountry, Contact__r.Mailingstreet, 
                                                    Contact__r.Mailingcity, Contact__r.Mailingstate, Contact__r.Mailingpostalcode, Contact__r.Mailingcountry, 
                                                    Property_Owner__c, Property_Owner__r.Name, Property_Owner__r.ACN__c, Property_Owner__r.ABN__c, Property_Owner__r.shippingstreet, 
                                                    Property_Owner__r.shippingcity, Property_Owner__r.shippingstate, Property_Owner__r.shippingpostalcode, 
                                                    Property_Owner__r.shippingcountry,  Property_Owner__r.BillingStreet, Property_Owner__r.BillingCountry,  Property_Owner__r.BillingCity, 
                                                    Property_Owner__r.BillingState, Property_Owner__r.BillingPostalCode  
                                                    from Property_Owners__c where Certificate__c IN: certIdList]){
                    if(i >0){
                        owners += ', ';
                    }
                    if(i == 3){
                        owners += 'et al.';
                        break;
                    }
                    if(!String.isBlank(pOwner.Contact__r.Account.Name)){
                        owners += pOwner.Contact__r.Account.Name;
                        if(String.isBlank(builderACN)){
                            builderACN = pOwner.Contact__r.Account.ACN__c;
                        }
                        if(String.isBlank(builderABN)){
                            builderABN = pOwner.Contact__r.Account.ABN__c;
                        }
                    }
                    if(!String.isBlank(pOwner.Property_Owner__r.Name)){
                        owners += pOwner.Property_Owner__r.Name;
                        if(String.isBlank(builderACN)){
                            builderACN = pOwner.Property_Owner__r.ACN__c;
                        }
                        if(String.isBlank(builderABN)){
                            builderABN = pOwner.Property_Owner__r.ABN__c;
                        }
                    }
                    if(String.isBlank(street)){
                        if(pOwner.contact__r.Mailingstreet!= null && pOwner.contact__r.Mailingcity!=null && pOwner.contact__r.Mailingstate!= null && pOwner.contact__r.Mailingpostalcode!=null && pOwner.contact__r.Mailingcountry!=null ){
                            street = pOwner.contact__r.Mailingstreet;
                            city = pOwner.contact__r.Mailingcity;
                            state = pOwner.contact__r.Mailingstate;
                            postalCode = pOwner.contact__r.Mailingpostalcode;
                            country = pOwner.contact__r.Mailingcountry;
                        }else if(pOwner.contact__r.account.recordTypeId == constants.account_record_type_Person_Account){
                            if(pOwner.contact__r.account.ShippingStreet!= null){
                                street = pOwner.contact__r.account.ShippingStreet;
                                city = pOwner.contact__r.account.ShippingCity;
                                state = pOwner.contact__r.account.ShippingState;
                                postalCode = pOwner.contact__r.account.ShippingPostalCode;
                                country =  pOwner.contact__r.account.ShippingCountry;   
                            }else{
                                street = pOwner.contact__r.account.BillingStreet;
                                city = pOwner.contact__r.account.BillingCity;
                                state = pOwner.contact__r.account.BillingState;
                                postalCode = pOwner.contact__r.account.BillingPostalCode;
                                country =  pOwner.contact__r.account.BillingCountry;   
                            }
                        }else if(pOwner.Property_Owner__r.Shippingstreet!=null && pOwner.Property_Owner__r.shippingcity!=null && pOwner.Property_Owner__r.shippingstate!=null && pOwner.Property_Owner__r.shippingpostalcode!=null && pOwner.Property_Owner__r.shippingcountry!=null ){
                            street = pOwner.Property_Owner__r.Shippingstreet;
                            city = pOwner.Property_Owner__r.shippingcity;
                            state = pOwner.Property_Owner__r.shippingstate;
                            postalCode = pOwner.Property_Owner__r.shippingpostalcode;
                            country =  pOwner.Property_Owner__r.shippingcountry;
                        }else if (pOwner.Property_Owner__r.BillingStreet != null &&  pOwner.Property_Owner__r.BillingCity != null && pOwner.Property_Owner__r.BillingState != null && pOwner.Property_Owner__r.BillingPostalCode != null){
                            street = pOwner.Property_Owner__r.BillingStreet;
                            city = pOwner.Property_Owner__r.BillingCity;
                            state = pOwner.contact__r.account.BillingState;
                            postalCode = pOwner.Property_Owner__r.BillingPostalCode;
                            country =  pOwner.Property_Owner__r.BillingCountry;
                        }
                    }

                    i++;
                   
                }
            }
            if(null != country && country.equalsIgnoreCase('Australia')){
                isBuilderAddressAustralia = True;
            }

        }
        system.debug('hey2:' + certificateList);
        return certificateList;
    }

    public PageReference validateAndRedirect(){
        PageReference retURL = new PageReference('/DistributorLanding');
        retURL.setRedirect(true);
        return retURL;
    }
 
 
    // This is our wrapper/container class. A container class is a class, a data structure, or an abstract data type whose instances are collections of other objects. In this example a wrapper class contains both the standard salesforce object Contact and a Boolean value
    public class cCertificate {
        public Certificate__c con {get; set;}
        public Boolean selected {get; set;}
        public Integer numberOfRow {get; set;} //row counter variable
 
        //This is the contructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
        public cCertificate(Certificate__c c) {
            con = c;
            selected = false;
        }
    }    
 }