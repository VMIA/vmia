public class generateLOEExtension{
    public list <catWrapper> catWithLimits {get;set;}
    public list <catWrapper> categories {get;set;}
    public list<rbpWrapper> rbpWrapperList {get;set;}
    public String accountId {get;set;}
    //public Account standardAccount{get;set;}
    public list <Account> currentAcc {get;set;}
    public list <Eligibility_History__c> currentHistory {get;set;}
    public String accountNameString {get;set;}
    public DateTime dateIssued {get;set;}
    public String dateIssuedString {get;set;}
    public DateTime currentDate {get;set;}
    public String currentDateString {get;set;}
    public list<categories> categoriesList {get;set;}
    public boolean hasFlatRates {get;set;}
    public Date proofSubmissionDate {get;set;}
    public boolean hasMailingAddress {get;set;}
    public String primaryRbpName {get;set;}
    public String primaryRbpNumber {get;set;}
    public string objectName{get;set;}
    public Schema.SObjectType objType {get;set;}
    public id recordId{get;set;}
    public Decimal constructionLimit {get;set;}

    public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return Page.Login;
        }
        return null;
    }

    public generateLOEExtension(){
        recordId= ApexPages.CurrentPage().getParameters().get('id');
        objType = recordId.getSobjectType();
        objectName = string.valueOf(objType);
        system.debug('name@@: ' + objectName);
        if(objectName.equalsIgnoreCase('Eligibility_History__c')){
            List<String> historyFields = new List<String>(Eligibility_History__c.SObjectType.getDescribe().fields.getMap().keySet());
            categoriesList = new list<categories>();
            rbpWrapperList = new List<rbpWrapper>();
            primaryRbpName = '';
            primaryRbpNumber = '';
            List<String> contactRolesAccepted = new List<String>{GlobalConstants.ACCOUNTCONTACTROLE_PRIMARY_DIRECTOR, GlobalConstants.ACCOUNTCONTACTROLE_RBP};
            String historySoql = ''
                    + ' select ' + String.join(historyFields, ',')
                    + ' from Eligibility_History__c'
                    + ' where id=:' 
                    + 'recordId';
            System.debug('** historySoql ==>'+ historySoql);        
            currentHistory = Database.query(historySoql);

            accountId = currentHistory[0].Builder__c;

            List<String> fields = new List<String>(Account.SObjectType.getDescribe().fields.getMap().keySet());
            fields.add('Agent__r.BillingStreet');
            fields.add('Agent__r.BillingCity');
            fields.add('Agent__r.BillingPostalCode');
            fields.add('Agent__r.BillingState');
            fields.add('Agent__r.BillingCountry');
            fields.add('Agent__r.name');
            fields.add('Agent__r.phone');
            fields.add('Agent__r.Website');
            fields.add('Agent__r.Primary_email__c');
            fields.add('Agent_Contact__r.name');
            fields.add('Agent_Contact__r.phone');
            fields.add('Agent__r.Account_Number__c');
            categoriesList = new list<categories>();
            rbpWrapperList = new List<rbpWrapper>();
            primaryRbpName = '';
            primaryRbpNumber = '';
            


            String soql = ''
                    + ' select ' + String.join(fields, ',')
                    + ', (SELECT RBP_VBA_Number__c,Name FROM Contacts ORDER BY CreatedDate ASC)'
                    + ', (SELECT contact__c,contact__r.name, contact__r.RBP_VBA_Number__c, Role__c FROM Contact_Roles__r WHERE (End_Date__c = null OR End_Date__c > today) AND Role__c IN: contactRolesAccepted ORDER BY Role__c ASC)'
                    + ' from Account'
                    + ' where id=:' 
                    + 'accountId';
            System.debug('** soql ==>'+ soql);        
            currentAcc = Database.query(soql);
            System.debug('** currentAcc ==>'+ currentAcc);
            System.debug('** RBP ==>'+ currentAcc[0].Contacts);


            accountNameString = currentHistory[0].Legal_Entity_Name__c;
            dateIssued = (currentHistory[0].Eligibility_Commencement_Date__c != null? currentHistory[0].Eligibility_Commencement_Date__c : currentAcc[0].Original_Eligibility_Approval_Date__c);
            dateIssuedString = dateIssued.format('dd/MM/yyyy');
            constructionLimit = currentHistory[0].Construction_Limit__c;

            if (currentHistory.size()>0){
                categories = new list <catWrapper>();
                catWithLimits = new list <catWrapper>();
                catWrapper newWrapper1 = new catWrapper((currentHistory[0].Single_Dwelling_Category_Limit__c != null?true:false), currentHistory[0].Single_Dwelling_Category_Limit__c, constants.New_Single_Dwelling);
                categories.add(newWrapper1);
                newWrapper1 = new catWrapper((currentHistory[0].Multi_Dwelling_Category_Limit__c != null?true:false), currentHistory[0].Multi_Dwelling_Category_Limit__c, constants.New_Multi_Dwelling);
                categories.add(newWrapper1);
                newWrapper1 = new catWrapper((currentHistory[0].Structural_Alterations_Category_Limit__c != null?true:false), currentHistory[0].Structural_Alterations_Category_Limit__c, constants.Alterations_Additions);
                categories.add(newWrapper1);
                newWrapper1 = new catWrapper((currentHistory[0].Swimming_Pool_Category_Limit__c != null?true:false), currentHistory[0].Swimming_Pool_Category_Limit__c, constants.Swimming_Pools);
                categories.add(newWrapper1);
                newWrapper1 = new catWrapper((currentHistory[0].Non_Structural_Renovations__c != null?true:false), currentHistory[0].Non_Structural_Renovations__c, constants.Renovations_Tradespeople);
                categories.add(newWrapper1);
                newWrapper1 = new catWrapper((currentHistory[0].Other_Limits__c != null?true:false), currentHistory[0].Other_Limits__c, constants.Other);
                categories.add(newWrapper1);

                for(catWrapper i : categories){
                    if(i.category){
                        catWithLimits.add(i);
                    }
                }
                system.debug('@@@@@@@@@@@@@@@@@@@' + catWithLimits.size());
                if (catWithLimits.size()<6){
                    integer size = catWithLimits.size();
                    for (integer c=0; c<=(6-size); c++){
                                    system.debug('hey2: ' + c);

                        catWithLimits.add(new catWrapper(false,0,''));
                    }
                }
            }

            //Account Eligibility
        }else{
            accountId = recordId;

            List<String> fields = new List<String>(Account.SObjectType.getDescribe().fields.getMap().keySet());
            fields.add('Agent__r.BillingStreet');
            fields.add('Agent__r.BillingCity');
            fields.add('Agent__r.BillingPostalCode');
            fields.add('Agent__r.BillingState');
            fields.add('Agent__r.BillingCountry');
            fields.add('Agent__r.name');
            fields.add('Agent__r.phone');
            fields.add('Agent__r.Website');
            fields.add('Agent__r.Primary_email__c');
            fields.add('Agent_Contact__r.name');
            fields.add('Agent_Contact__r.phone');
            fields.add('Agent__r.Account_Number__c');
            categoriesList = new list<categories>();
            rbpWrapperList = new List<rbpWrapper>();
            primaryRbpName = '';
            primaryRbpNumber = '';
            List<String> contactRolesAccepted = new List<String>{GlobalConstants.ACCOUNTCONTACTROLE_PRIMARY_DIRECTOR, GlobalConstants.ACCOUNTCONTACTROLE_RBP};


            String soql = ''
                    + ' select ' + String.join(fields, ',')
                    + ', (SELECT RBP_VBA_Number__c,Name FROM Contacts ORDER BY CreatedDate ASC)'
                    + ', (SELECT contact__c,contact__r.name, contact__r.RBP_VBA_Number__c, Role__c FROM Contact_Roles__r WHERE (End_Date__c = null OR End_Date__c > today) AND Role__c IN: contactRolesAccepted ORDER BY Role__c ASC)'
                    + ' from Account'
                    + ' where id=:' 
                    + 'accountId';
            System.debug('** soql ==>'+ soql);        
            currentAcc = Database.query(soql);
            System.debug('** currentAcc ==>'+ currentAcc);
            System.debug('** RBP ==>'+ currentAcc[0].Contacts);

            if (currentAcc.size()>0){
                categories = new list <catWrapper>();
                catWithLimits = new list <catWrapper>();
                catWrapper newWrapper1 = new catWrapper(currentAcc[0].CO1_New_Single_Dwelling__c, currentAcc[0].Construction_Category_Limit_1__c, constants.New_Single_Dwelling);
                categories.add(newWrapper1);
                newWrapper1 = new catWrapper(currentAcc[0].CO3_New_Multi_Dwelling__c, currentAcc[0].Construction_Category_Limit_3__c, constants.New_Multi_Dwelling);
                categories.add(newWrapper1);
                newWrapper1 = new catWrapper(currentAcc[0].C04_Alterations_Additions__c, currentAcc[0].Construction_Category_Limit_4__c, constants.Alterations_Additions);
                categories.add(newWrapper1);
                newWrapper1 = new catWrapper(currentAcc[0].C05_Swimming_Pools__c, currentAcc[0].Construction_Category_Limit_5__c, constants.Swimming_Pools);
                categories.add(newWrapper1);
                newWrapper1 = new catWrapper(currentAcc[0].C06_Renovations_Tradespeople__c, currentAcc[0].Construction_Category_Limit_6__c, constants.Renovations_Tradespeople);
                categories.add(newWrapper1);
                newWrapper1 = new catWrapper(currentAcc[0].C07_Other__c, currentAcc[0].Construction_Category_Limit_7__c, constants.Other);
                categories.add(newWrapper1);

                for(catWrapper i : categories){
                    if(i.category){
                        catWithLimits.add(i);
                    }
                }
                system.debug('@@@@@@@@@@@@@@@@@@@' + catWithLimits.size());
                if (catWithLimits.size()<6){
                    integer size = catWithLimits.size();
                    for (integer c=0; c<=(6-size); c++){
                                    system.debug('hey2: ' + c);

                        catWithLimits.add(new catWrapper(false,0,''));
                    }
                }
            }

            accountNameString = currentAcc[0].name;
            dateIssued = currentAcc[0].Original_Eligibility_Approval_Date__c;
            dateIssuedString = dateIssued.format('dd/MM/yyyy');
            constructionLimit = currentAcc[0].Construction_Limit__c;
        }
        

        if(currentAcc != null){
            if(currentAcc[0].IsPersonAccount){
                rbpWrapperList.add(new rbpWrapper(currentAcc[0].Name, currentAcc[0].RBP_VBA_Number__pc));
            }
            else{
                //if(currentAcc[0].Contacts != null && !currentAcc[0].Contacts.isEmpty()){
                //  for(Contact con : currentAcc[0].Contacts){
                //      rbpWrapperList.add(new rbpWrapper(con.Name, con.RBP_VBA_Number__c));
                //  }
                    
                //}
                if(currentAcc[0].Contact_Roles__r != null && !currentAcc[0].Contact_Roles__r.isEmpty()){
                    for(Contact_Role__c con : currentAcc[0].Contact_Roles__r){
                        if(con.Role__c == GlobalConstants.ACCOUNTCONTACTROLE_PRIMARY_DIRECTOR){
                            primaryRbpName = con.contact__r.name;
                            primaryRbpNumber = con.contact__r.RBP_VBA_Number__c;
                        }else{
                            rbpWrapperList.add(new rbpWrapper(con.contact__r.name, con.contact__r.RBP_VBA_Number__c));
                        }
                        
                    }
                    
                }
            }
        }

        currentDate = Date.today();
        currentDateString = currentDate.format('dd/MM/yyyy');
        //currentAcc= [Select CO1_New_Single_Dwelling__c,Construction_Category_Limit_1__c, CO3_New_Multi_Dwelling__c, Construction_Category_Limit_3__c, C04_Alterations_Additions__c, Construction_Category_Limit_4__c, C05_Swimming_Pools__c, Construction_Category_Limit_5__c, C06_Renovations_Tradespeople__c, Construction_Category_Limit_6__c, C07_Other__c, Construction_Category_Limit_7__c from Account where id=:accountId ];
        if(currentAcc[0].CO1_New_Single_Dwelling__c){
           categoriesList.add(new categories(constants.New_Single_Dwelling, currentAcc[0].Construction_Category_Limit_1__c, currentAcc[0].SD_Flat_Rate_Premium__c));
        }
        if(currentAcc[0].CO3_New_Multi_Dwelling__c){
            categoriesList.add(new categories(constants.New_Multi_Dwelling, currentAcc[0].Construction_Category_Limit_3__c, currentAcc[0].C03_Multi_Dwelling_Flat_Rate_Premium__c ));
        }
        if(currentAcc[0].C04_Alterations_Additions__c){
            categoriesList.add(new categories(constants.Alterations_Additions, currentAcc[0].Construction_Category_Limit_4__c, currentAcc[0].C04_Structural_Flat_Rate_Premium__c ));
        }
        if(currentAcc[0].C05_Swimming_Pools__c){
            categoriesList.add(new categories(constants.Swimming_Pools, currentAcc[0].Construction_Category_Limit_5__c, currentAcc[0].SP_Flat_Rate_Premium__c ));
        }
        if(currentAcc[0].C06_Renovations_Tradespeople__c){
           categoriesList.add(new categories(constants.Renovations_Tradespeople, currentAcc[0].Construction_Category_Limit_6__c, currentAcc[0].Non_Structural_Flat_Rate__c ));
        }
        if(currentAcc[0].C07_Other__c){
            categoriesList.add(new categories(constants.Other, currentAcc[0].Construction_Category_Limit_7__c, currentAcc[0].C07_Other_Flat_Rate_Premium__c ));
        }
        
        if(String.isNotBlank(currentAcc[0].ShippingStreet) || 
            String.isNotBlank(currentAcc[0].ShippingCity) || 
            String.isNotBlank(currentAcc[0].ShippingState) || 
            String.isNotBlank(currentAcc[0].ShippingPostalCode) || 
            String.isNotBlank(currentAcc[0].ShippingCountry)) {
            hasMailingAddress = true;
        } else {
            hasMailingAddress = false;
        }

        for(Categories c : categoriesList){
            if(c.categoryFlatRate !=0 ){
                hasFlatRates = True;
            }
        }

        
        if(currentAcc[0].Pending_Eligibility_Issued_Date__c != null){
            proofSubmissionDate = currentAcc[0].Pending_Eligibility_Issued_Date__c.addDays(212);
        }else{
            proofSubmissionDate = system.today();
        }
        
    }


    public class catWrapper{
        public boolean category{get;set;}
        public String categoryString{get;set;}
        public decimal catLimit{get;set;}
        public catWrapper(boolean newCat, decimal newCatLimit, String newCatString){
            category = newCat;
            catLimit = newCatLimit;
            categoryString = newCatString;
        }
    }

    public class rbpWrapper{
        public String rbpName{get;set;}
        public String rbpNumber{get;set;}
        public rbpWrapper(String newRbpName, String newRbpNumber){
            rbpName = newRbpName;
            rbpNumber = newRbpNumber;
        }
    }

    public class categories{
        public string categoryName{get;set;}
        public decimal categoryLimit{get;set;}
        public decimal categoryFlatRate{get;set;}

        public categories(String newName, decimal newLimit, decimal newFlatRate){
            categoryName = newName;
            if(newLimit!=null){
                categoryLimit = newLimit; 
            }else{
                categoryLimit = 0;
            }
            if(newFlatRate!=null){
                categoryFlatRate = newFlatRate; 
            }else{
                categoryFlatRate = 0;
            }
        }

    }

}