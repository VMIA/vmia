/**
* Date         :  24-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for generateLOEExtension controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class generateLOEExtension_Test {
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        setupData();
        
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        
        //Set bornBuilder.CO1_New_Single_Dwelling__c = true;
        bornBuilder.Construction_Category_Limit_1__c = 2;
        bornBuilder.SD_Flat_Rate_Premium__c = 2;
        
        //Set bornBuilder.CO3_New_Multi_Dwelling__c = true;
        bornBuilder.Construction_Category_Limit_3__c = 2;
        bornBuilder.C03_Multi_Dwelling_Flat_Rate_Premium__c = 2;
        
        //Set bornBuilder.C04_Alterations_Additions__c = true;
        bornBuilder.Construction_Category_Limit_4__c = 2;
        bornBuilder.C04_Structural_Flat_Rate_Premium__c = 2;
        
        //Set bornBuilder.C05_Swimming_Pools__c = true;
        bornBuilder.Construction_Category_Limit_5__c = 2;
        bornBuilder.SP_Flat_Rate_Premium__c = 2;        
        
        //Set bornBuilder.C06_Renovations_Tradespeople__c = true;
        bornBuilder.Construction_Category_Limit_6__c = 2;
        bornBuilder.Non_Structural_Flat_Rate__c = 2;  
        
        //Set bornBuilder.C07_Other__c = true;
        bornBuilder.Construction_Category_Limit_7__c = 2;
        bornBuilder.C07_Other_Flat_Rate_Premium__c = 2;         
        
        bornBuilder.ShippingCountry = 'VIC';
        bornBuilder.ShippingPostalCode = '3199';
        
        Database.insert(bornBuilder);
        System.debug('** bornBuilder ==>'+ bornBuilder);
        
        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        tonyStark.Builder_Role__c = 'Primary RBP';
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);
        
        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(rexUser); 
        
        Account personAc = TestUtility.createPersonAccount('Person',accRecTypeMap.get('Person Account').getRecordTypeId());
        personAc.Pending_Eligibility_Issued_Date__c = Date.today()-10;
        Database.insert(personAc);
        System.debug('** personAc ==>'+ personAc);
        
    }     
    
    public static testMethod void forwardToCustomAuthPageGuestTest(){
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        User guestUser = [select Id from User where userType = 'Guest' LIMIT 1];
        
        System.runAs(guestUser) {  
            PageReference pageRef = Page.Generate_LOE;
            Test.setCurrentPage(pageRef); 
            System.currentPageReference().getParameters().put('id', bornBuilder.Id); 
            Test.startTest();           
            generateLOEExtension controller = new generateLOEExtension();
            PageReference pg = controller.forwardToCustomAuthPage();
            System.assertEquals(Page.Login.getURL(), pg.getURL());
            Test.stopTest();
        }
    }     
    
    
    public static testMethod void forwardToCustomAuthPageBuilderTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        
        System.runAs(rexUser){
            PageReference pageRef = Page.Generate_LOE;
            Test.setCurrentPage(pageRef); 
            System.currentPageReference().getParameters().put('id', bornBuilder.Id);  
            Test.startTest();
            generateLOEExtension controller = new generateLOEExtension();
            PageReference pg = controller.forwardToCustomAuthPage();
            System.assertEquals(null, pg);
            Test.stopTest();
        }
    }    
    
    public static testMethod void forwardToCustomAuthPageBuilderCatWithLimitTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id, Construction_Category_Limit_7__c, C07_Other_Flat_Rate_Premium__c, Pending_Eligibility_Issued_Date__c FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        
        bornBuilder.Construction_Category_Limit_7__c = 0;
        bornBuilder.C07_Other_Flat_Rate_Premium__c = 0;     //To simulate bornBuilder.C07_Other__c = false;
        bornBuilder.Pending_Eligibility_Issued_Date__c = null;
        Database.update(bornBuilder);
        
        System.runAs(rexUser){
            PageReference pageRef = Page.Generate_LOE;
            Test.setCurrentPage(pageRef); 
            System.currentPageReference().getParameters().put('id', bornBuilder.Id);  
            Test.startTest();
            generateLOEExtension controller = new generateLOEExtension();
            PageReference pg = controller.forwardToCustomAuthPage();
            System.assertEquals(null, pg);
            Test.stopTest();
        }
    } 
    
    public static testMethod void forwardToCustomAuthPageBuilderPersonAccountTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account personAc = [SELECT Id FROM Account WHERE lastName = 'Person' LIMIT 1];
        
        System.runAs(rexUser){
            PageReference pageRef = Page.Generate_LOE;
            Test.setCurrentPage(pageRef); 
            System.currentPageReference().getParameters().put('id', personAc.Id);  
            Test.startTest();
            generateLOEExtension controller = new generateLOEExtension();
            PageReference pg = controller.forwardToCustomAuthPage();
            System.assertEquals(null, pg);
            Test.stopTest();
        }
    }     
}