public with sharing class generateMultipleCertificate {

	public String builderId;
	public Account builderRecord {get;set;} 
	public string selectedSearchType{get;set;}
	public list<selectOption> searchType {get;set;}
	public Boolean noFilter{get;set;}

	public string spi {get;set;}
	public integer unitNumber {get;set;}
	public integer streetNumber {get;set;}
	public string street {get;set;}
	public string suburb {get;set;}
	public integer postcode {get;set;}
	public string state{get;set;}
	public Boolean addressTypeSearch{get;set;}

	public Boolean spiTypeSearch{get;set;}
	public Boolean InCompleteFilter{get;set;}


	public List<Property__c> propertyList {get;set;}
public List<Property__c> propertyListClone {get;set;}

public Boolean proprtyNotFound {get;set;}
public Boolean displayProperty {get;set;}

	public generateMultipleCertificate() {

		builderId= ApexPages.CurrentPage().getParameters().get('Id');
		builderRecord = new Account();
	    noFilter = false;
		spiTypeSearch = false;
		addressTypeSearch = false;
		InCompleteFilter = false;
		proprtyNotFound = false;
		displayProperty = false;

		if(builderId!=null && builderId!=''){
            builderRecord = [Select Id, Name,Builder_Risk_Rating__c,SP_Flat_Rate_Premium__c,SD_Flat_Rate_Premium__c,Flat_Rate_Premium__c,Agent__c,Agent__r.name,AvailableConstructionLimit__c,CO1_New_Single_Dwelling__c,CO3_New_Multi_Dwelling__c,C04_Alterations_Additions__c,C05_Swimming_Pools__c,C06_Renovations_Tradespeople__c,C07_Other__c,C08_All_Cover__c from Account where Id=: builderId];
            
        }

        searchType =  new List<SelectOption>();
        searchType.add(new SelectOption('--None--','--None--'));
        searchType.add(new SelectOption('SPI','SPI'));
        searchType.add(new SelectOption('Address','Address'));


	}	


	public pagereference applyFilter(){ 
        spiTypeSearch = false;
        addressTypeSearch = false;  
        noFilter = false;
        InCompleteFilter = false ;
        System.debug('selectedSearchType--->'+selectedSearchType);
        if(selectedSearchType.equalsIgnoreCase('--None--')){            
            spiTypeSearch = false;
            addressTypeSearch = false;
        }else if (selectedSearchType.equalsIgnoreCase('SPI')){
            spiTypeSearch = true;
        }else if (selectedSearchType.equalsIgnoreCase('Address')){
            addressTypeSearch = true;
        }
        return null;
    }


    public pagereference searchProperty(){
        proprtyNotFound = false;
        string SoqlStr = 'Select Id, Name,IsValidated__c,Street_Number__c,Street__c,Suburb__c,Unit_Number__c,Postcode__c from Property__c where ';
        string filterStr ;
        noFilter = false;
        InCompleteFilter = false ;
        System.debug('Inside the Search Method');
        
        if(selectedSearchType.equalsIgnoreCase('--None--')){
            noFilter = true;
        }else if (selectedSearchType.equalsIgnoreCase('SPI') && string.isBlank(spi)){
            InCompleteFilter = true;
        }else if (selectedSearchType.equalsIgnoreCase('SPI') && !string.isBlank(spi)){
            System.debug('Inside the spi call');
            filterStr = 'SPI__c = ' + '\'' +  spi + '\'';
        }else if (selectedSearchType.equalsIgnoreCase('Address') && (unitNumber == null  || streetNumber == null || string.isBlank(street) || string.isBlank(suburb) || postcode == null || string.isBlank(state))){
            InCompleteFilter = true ;
        }else{
            System.debug('Inside Else');           
            filterStr = 'Unit_Number__c = '  + unitNumber + ' AND ' + 'Street_Number__c = '  + streetNumber + ' AND ' +'Street__c = ' + '\'' + street + '\''  +  ' AND ' + 'Postcode__c = '  + postcode + ' AND ' + 'State__c = ' + '\'' + state + '\'';
            System.debug('filterStr::::'+filterStr);
            
        }
        
         propertyList = new List<Property__c>();
        propertyListClone = new List<Property__c>();
        System.debug('InCompleteFilter---->'+InCompleteFilter);
        System.debug('noFilter---->'+noFilter);
        
        if(!noFilter && !InCompleteFilter){
            string finalStr = SoqlStr + filterStr ;
            System.debug('finalStr--->'+finalStr);
           
            propertyList = Database.query(finalStr);
            System.debug('propertyList:::'+propertyList);
            
            if(!propertyList.isEmpty()){
                propertyListClone.add(propertyList[0])  ;
               
                displayProperty = true;
            } else{
                proprtyNotFound = true ;
            }
       }
        return null;
    }

    //Remove a property from the table.
public void removeProperty(){
    Integer indexVal = Integer.valueof(system.currentpagereference().getparameters().get('index'));
    
    //Remove the contact from the table    
    propertyListClone.remove(indexVal - 1);            
}
}