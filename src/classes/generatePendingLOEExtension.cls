/**
  * Date         :  06-Apr-2017
  * Author       :  SMS Management & Technology
  * Description  :  Custom Controller for "Generate Eligibility Pending Letter"
  */ 
 
/*******************************  History ************************************************
    Date                User                                        Comments

    
*******************************  History ************************************************/
public with sharing class GeneratePendingLOEExtension{
    
    // Page Variables
    public String builderACN {get;set;}
    public String builderABN {get;set;}
    public Date proofSubmissionDate {get;set;}  
    public Boolean hasMailingAddress {get;set;}
    public boolean hasFlatRates {get;set;}
    public boolean builderHasACN {get;set;}
    public boolean builderHasABN {get;set;}
    public DateTime currentDate {get;set;} 
    public String currentDateString {get;set;}
    public List<BuilderCategory> categoriesList {get;set;}
    public List<Contact_Role__c> pendingRBP {get;set;}
    public Integer conNumber {get;set;}
    
    // variables
    @testVisible private Application__c application;
    
    /*
        constructor to fetch details for pending eligibility letter
    */
    public GeneratePendingLOEExtension(ApexPages.StandardController controller){
        List<String> additionalfieldsToFetch = new List<String>();
        List<Schema.FieldSetMember> fields = Application__c.sobjectType.getDescribe().fieldSets.getMap().get(GlobalConstants.PENDING_LOE_CATEGORY_DETAILS).getFields();
        for(Schema.FieldSetMember field : fields){
            additionalfieldsToFetch.add(field.getFieldPath());
        }

        if (!Test.isRunningTest()) {
            controller.addFields(additionalfieldsToFetch);
        }
        application = (Application__c) controller.getRecord();
        System.debug('** application ==>'+ application);
        categoriesList = new List<BuilderCategory>();
        
        builderHasACN = String.isNotBlank(application.Builder_ACN__c) ? true : false;
        builderHasABN = String.isNotBlank(application.Builder_ABN__c) ? true : false;
        
        builderACN = String.isNotBlank(application.Builder_ACN__c) ? application.Builder_ACN__c : GlobalConstants.EMPTY_SPACE;
        builderABN = String.isNotBlank(application.Builder_ABN__c) ? application.Builder_ABN__c : GlobalConstants.EMPTY_SPACE;
        //System.debug('** builderACNABN ==>'+ builderACNABN);
        if(application.Single_Dwelling__c){
            categoriesList.add(new BuilderCategory(Constants.New_Single_Dwelling, application.Single_Dwelling_Limit__c, application.Flat_Rate_Premium_C01__c));
        }
        if(application.Multi_Dwelling__c){
            categoriesList.add(new BuilderCategory(Constants.New_Multi_Dwelling, application.Multi_Dwelling_Limit__c, application.Flat_rate_Premium_C03__c ));
        }
        if(application.Alterations__c){
            categoriesList.add(new BuilderCategory(Constants.Alterations_Additions, application.Alteration_Limit__c, application.Flat_Rate_Premium_C04__c ));
        }
        if(application.Swimming_Pool__c){
            categoriesList.add(new BuilderCategory(Constants.Swimming_Pools, application.Swimming_Pool_Limit__c, application.Flat_Rate_Premium_C05__c ));
        }
        if(application.Renovation_Non_Structural__c){
            categoriesList.add(new BuilderCategory(Constants.Renovations_Tradespeople, application.Renovation_Non_Structural_Limit__c, application.Flat_Rate_Premium_C06__c ));
        }
        if(application.C07_Other__c){
            categoriesList.add(new BuilderCategory(Constants.Other, application.Category_Limit_Other__c, application.Flat_Rate_Premium_C07__c ));
        }
        System.debug('** categoriesList ==>'+ categoriesList);
        hasFlatRates = (application.Flat_Rate_Premium_C01__c > 0) || (application.Flat_rate_Premium_C03__c > 0)
                        || (application.Flat_Rate_Premium_C04__c > 0) || (application.Flat_Rate_Premium_C05__c > 0)
                        || (application.Flat_Rate_Premium_C06__c > 0) || (application.Flat_Rate_Premium_C07__c > 0);
        System.debug('** hasFlatRates ==>'+ hasFlatRates);
        hasMailingAddress = String.isNotBlank(application.Builder_Number__r.ShippingStreet) 
                                || String.isNotBlank(application.Builder_Number__r.ShippingCity) 
                                || String.isNotBlank(application.Builder_Number__r.ShippingState) 
                                || String.isNotBlank(application.Builder_Number__r.ShippingPostalCode);
        
        System.debug('** hasMailingAddress ==>'+ hasMailingAddress);
        proofSubmissionDate = (application.Builder_Number__r.Pending_Eligibility_Issued_Date__c != null) ?
                                    application.Builder_Number__r.Pending_Eligibility_Issued_Date__c.addDays(Integer.valueOf(Label.Pending_LOE_Submission_Date)) : System.today();
        System.debug('** proofSubmissionDate ==>'+ proofSubmissionDate);
        currentDate = Date.today();
        currentDateString = currentDate.format('dd/MM/yyyy');
        conNumber = 0;
        pendingRBP = new List<Contact_Role__c>();
        pendingRBP = [SELECT Id,Contact__r.Name FROM Contact_Role__c WHERE Account__c = :application.Builder_Number__c AND Role__c = :Label.RBP_Pending];
    }
    
    /*
        Wrapper class to store builder category details
    */
    public class BuilderCategory{
        public String categoryName{get;set;}
        public Decimal categoryLimit{get;set;}
        public Decimal categoryFlatRate{get;set;}

        public BuilderCategory(String newName, Decimal newLimit, Decimal newFlatRate){
            categoryName = newName;
            categoryLimit = (newLimit != null) ? newLimit : 0; 
            categoryFlatRate = (newFlatRate != null) ? newFlatRate : 0; 
        }
    }
}