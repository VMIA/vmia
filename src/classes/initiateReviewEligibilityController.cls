public with sharing class initiateReviewEligibilityController{
    public String reviewReasons{get;set;}
    public String reviewSummary{get;set;}
    public string builderID {get;set;}
    public List<Account> newAccount {get;set;}
    public Application__c app {get;set;}
    public String selectedAlertType {get;set;}
    public List<SelectOption> options {get;set;}
    
    public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() != 'Standard'){
            return Page.Login;
        }
        else{
            return null;
        }
    }


    public PageReference confirmation(){

        return null;
    }

    public PageReference save(){
        List<RecordType> recType = [Select Id,SobjectType,Name From RecordType WHERE Name ='Review Eligibility' and SobjectType ='Application__c'  limit 1];
        User usr = [Select id from User where ContactID =: newAccount[0].Agent_contact__c LIMIT 1];
        if(String.isNotBlank(newAccount[0].FirstName)){
            app = new Application__c(RecordTypeId = recType[0].id, DD_Account__c =newAccount[0].agent__c, Adverse_Alert_Type__c=selectedAlertType, application_status__c = 'New', ownerid = userinfo.getUserId(), application_source__c='Adverse Alert', review_reason__c = reviewReasons, builder_number__c = newAccount[0].id, Builder_ABN__c = newAccount[0].ABN__c, Builder_ACN__c = newAccount[0].ACN__c, Builder_First_Name__c = newAccount[0].FirstName, Builders_Last_Name__c = newAccount[0].LastName, vba_number__c = newAccount[0].vba_number__c, DBI_Distributor__c =usr.id );
        }else{
            app = new Application__c(RecordTypeId = recType[0].id, DD_Account__c =newAccount[0].agent__c, Adverse_Alert_Type__c=selectedAlertType, application_status__c = 'New', ownerid = userinfo.getUserId(), application_source__c='Adverse Alert', review_reason__c = reviewReasons, builder_number__c = newAccount[0].id, Builder_ABN__c = newAccount[0].ABN__c, Builder_ACN__c = newAccount[0].ACN__c, builder_Legal_entity_name__c = newAccount[0].name, vba_number__c = newAccount[0].vba_number__c, DBI_Distributor__c =usr.id );
        }
        try{
               insert app;
           } catch(DmlException e) {
               System.debug('An unexpected error has occurred: ' + e.getMessage());
           }
           
           /*
           Application__share applicationShare = new Application__share();
            applicationShare.AccessLevel = 'Edit';
            applicationShare.ParentID = app.id; 
            applicationShare.RowCause ='Manual' ;
            applicationShare.UserOrGroupId = usr.id;
            if(applicationShare != null){
                insert applicationShare ;
            } 
            applicationShare = new Application__share();
            applicationShare.AccessLevel = 'Edit';
            applicationShare.ParentID = app.id; 
            applicationShare.RowCause ='Manual' ;
            applicationShare.UserOrGroupId = '00GN0000001undRMAQ';
            if(applicationShare != null){
                insert applicationShare ;
            }
            */


        //if(app != null ){
        //    System.debug('Sahring Rule Code started');
  //        Application__share applicationShare = new Application__share();
    //      applicationShare.AccessLevel = 'Edit';
  //            applicationShare.ParentID = app.id; 
  //            applicationShare.RowCause ='Manual' ;
    //      applicationShare.UserOrGroupId = usr.id;
  //        System.debug('applicationShare:::::'+applicationShare);
  //        if(applicationShare != null){
  //            insert applicationShare ;
  //        } 
        //}

        Task tsk = new Task(whatID = app.ID, Ownerid = usr.id, Builder__c = newAccount[0].Id, ActivityDate=system.today()+180, Subject = reviewSummary, description = reviewReasons); 
        try{
               insert tsk;
           } catch(DmlException e) {
               System.debug('An unexpected error has occurred: ' + e.getMessage());
           } 
        return null;
    }

    public initiateReviewEligibilityController(){
        builderID = ApexPages.currentPage().getParameters().get('id');
        newAccount = [Select id, name, FirstName, LastName, Agent_contact__c, ABN__c, vba_number__c, ACN__c, AvailableConstructionLimit__c, Builder_Risk_Rating__c, 
                   Eligibility_Status__c, Construction_Limit__c, Flat_Rate_Premium__c, SD_Flat_Rate_Premium__c, SP_Flat_Rate_Premium__c,
                   UsedConstructionLimit__c, Original_Eligibility_Approval_Date__c, CO1_New_Single_Dwelling__c, CO3_New_Multi_Dwelling__c,
                   C04_Alterations_Additions__c, C05_Swimming_Pools__c, C06_Renovations_Tradespeople__c, C07_Other__c, C08_All_Cover__c,
                   Construction_Category_Limit_1__c, Construction_Category_Limit_3__c, Construction_Category_Limit_4__c,
                   Construction_Category_Limit_5__c, Construction_Category_Limit_6__c, Other_Construction_Category_Limit__c, Agent__c
                   from Account where id=:builderID  limit 1];
                   app = new Application__c(application_source__c='Adverse Alert');

    }
    public List<SelectOption> getCountries()
    {
      List<SelectOption> options = new List<SelectOption>();
            
       Schema.DescribeFieldResult fieldResult =
     Application__c.Adverse_Alert_type__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
       for( Schema.PicklistEntry f : ple)
       {
          options.add(new SelectOption(f.getLabel(), f.getValue()));
       }       
       return options;
    }
    
}