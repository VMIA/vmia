public with sharing class invoiceDetailController {
	public String invoiceId;
	public list<Invoice__c> invoiceList{get;set;}
    public string certificateId{get;set;}
    public invoiceDetailController(ApexPages.StandardController controller) {

    	invoiceId= ApexPages.CurrentPage().getParameters().get('id');
        certificateId = ApexPages.CurrentPage().getParameters().get('cerId');
    	invoiceList = new list<Invoice__c>();
    	if(invoiceId != null && invoiceId != ''){

    		invoiceList =[select id ,PurchaseDate__c,Purchaser__r.name,Total__c,Status__c,Invoice_Date__c,Purchaser__r.abn__c,Purchaser__r.acn__c from Invoice__c where id=: invoiceId] ;
    	}
    }	

     public pagereference returnHome(){
        
            string urlStrr = '/apex/DistributorLanding';
             PageReference pageRefback= new PageReference(urlStrr);
           pageRefback.setRedirect(false);        
            return pageRefback;  
        }

     public pagereference backPage(){
            string urlStrr ; 

            System.debug('certificateId>>>'+certificateId);
         
            if(certificateId.equalsIgnoreCase('multipleCertificate')){
                    urlStrr = '/apex/paymentSuccessPage?id=multipleCertificate' ;
            }else{
                    urlStrr = '/apex/paymentSuccessPage?id='+ certificateId ;
            }
            
          
             PageReference pageRefback= new PageReference(urlStrr);
           pageRefback.setRedirect(true);        
            return pageRefback;  
        }   

        public pagereference generateCertificate(){
        string urlStrr = '/apex/GenerateInvoice?id='+invoiceId;
        PageReference pageRefback= new PageReference(urlStrr);
           pageRefback.setRedirect(false);        
            return pageRefback;  

    }
}