/**
* Date         :  24-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for SuccessinvoiceDetailController controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class parseXml_Test {

	//variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        
        setupData();
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> invRecTypeMap = Schema.Sobjecttype.Invoice__c.getRecordTypeInfosByName();
        
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);

        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(rexUser);

     }
     
     public static testMethod void parseXmlWestpacErrorTest(){
     	User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
     	System.runAs(rexUser){
     		String responseStr = '<?xml version="1.0" ?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><ns2:processRequestResponse xmlns:ns2="urn:com.qvalent.quickgateway"><responseParameter><name>response.summaryCode</name><value>1</value></responseParameter><responseParameter><name>response.responseCode</name><value>61</value></responseParameter><responseParameter><name>response.text</name><value>Amount 6050000 is greater than merchant maximum of 5000000</value></responseParameter><responseParameter><name>response.referenceNo</name><value>731793562</value></responseParameter><responseParameter><name>response.orderNumber</name><value>INV-0344</value></responseParameter><responseParameter><name>response.cardSchemeName</name><value>VISA</value></responseParameter><responseParameter><name>response.creditGroup</name><value>VI/BC/MC</value></responseParameter><responseParameter><name>response.previousTxn</name><value>0</value></responseParameter></ns2:processRequestResponse></S:Body></S:Envelope>';

     		XMLParser.parseXml(responseStr,'westpacRefund');
     	}
     }  

     public static testMethod void parseXmlWestpacSuccessTest(){
     	User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
     	System.runAs(rexUser){
     		String responseStr = '<?xml version="1.0" ?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><ns2:processRequestResponse xmlns:ns2="urn:com.qvalent.quickgateway"><responseParameter><name>response.summaryCode</name><value>0</value></responseParameter><responseParameter><name>response.responseCode</name><value>08</value></responseParameter><responseParameter><name>response.text</name><value>Honour with identification</value></responseParameter><responseParameter><name>response.referenceNo</name><value>731793496</value></responseParameter><responseParameter><name>response.orderNumber</name><value>INV-00000753</value></responseParameter><responseParameter><name>response.RRN</name><value>731793496   </value></responseParameter><responseParameter><name>response.settlementDate</name><value>20170530</value></responseParameter><responseParameter><name>response.transactionDate</name><value>30-MAY-2017 13:41:10</value></responseParameter><responseParameter><name>response.cardSchemeName</name><value>VISA</value></responseParameter><responseParameter><name>response.creditGroup</name><value>VI/BC/MC</value></responseParameter><responseParameter><name>response.previousTxn</name><value>0</value></responseParameter><responseParameter><name>response.traceCode</name><value>967751</value></responseParameter></ns2:processRequestResponse></S:Body></S:Envelope>';

     		XMLParser.parseXml(responseStr,'westpacRefund');
     	}
     }  

     public static testMethod void parseXmlTechOneSuccessTest(){
     	User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
     	System.runAs(rexUser){
     		String responseStr = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><DocumentFile_DoInsertResponse xmlns="http://TechnologyOneCorp.com/T1.F1.Public/Services"><DocumentFile_DoInsertResult><Errors IsError="false" IsWarning="false"><Items xmlns="WebService" /></Errors></DocumentFile_DoInsertResult></DocumentFile_DoInsertResponse></soap:Body></soap:Envelope>';

     		XMLParser.parseXml(responseStr,'Tech1Refund');
     	}
     }   
}