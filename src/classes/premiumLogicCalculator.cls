/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Apex class is used for Premium Rate Calculation for Builders
  */

public class premiumLogicCalculator{
    public list<PremiumRatesTable__c> FlatepremiumRecord ;
    public list<PremiumRatesTable__c> premiumRecord ;
    public   boolean premiumFromAccount = false;
    public   decimal premiumValue ;

/**
      * @description       This method is used for calculating the premium for Owner Builder
      * @return            Boolean      
  */


public  list<PremiumRatesTable__c> calculatePremiumforOwnerBuilder(date occupancyPermitDate , decimal contractValue){

    date todayDate = System.today();    
    id recordId = Schema.Sobjecttype.PremiumRatesTable__c.getRecordTypeInfosByName().get('OBRate').getRecordTypeId();
    string soqlStr ;
    string filterStr;    
    String finalSoqlQuery ;
    string coverYear ;
    premiumRecord = new list<PremiumRatesTable__c>();
    System.debug('Inside OWner Builder calculation');

    //integer differenceInYear = (todayDate.monthsBetween(occupancyPermitDate))/12 ;
    integer differenceInYear = (occupancyPermitDate.monthsBetween(todayDate))/12 ;

    System.debug('differenceInYear<><>::'+differenceInYear);

/*
    
    if(todayDate.monthsBetween(occupancyPermitDate) < 12){
        coverYear = constants.ObFirstCover ;
    }else if(todayDate.monthsBetween(occupancyPermitDate) > 12 && todayDate.monthsBetween(occupancyPermitDate) < 36 ){
        coverYear = constants.ObSecondCover ;
    }else if(todayDate.monthsBetween(occupancyPermitDate) > 36 && todayDate.monthsBetween(occupancyPermitDate) < 60){
        coverYear = constants.ObThirdCover;
    }else if(todayDate.monthsBetween(occupancyPermitDate) > 60 ){
        coverYear = constants.ObFourthCover;
    }
    */

    if((6-differenceInYear) > 0 && (6-differenceInYear) <= 1){
        coverYear = constants.ObFirstCover ;
    }else if((6-differenceInYear) > 1 && (6-differenceInYear) <= 3 ){
        coverYear = constants.ObSecondCover ;
    }else if((6-differenceInYear) > 3 && (6-differenceInYear) <= 5){
        coverYear = constants.ObThirdCover;
    }else if((6-differenceInYear) > 5){
        coverYear = constants.ObFourthCover;
    }
    System.debug('coverYear<>'+coverYear);
    System.debug('contractValue<>'+contractValue);
    if(null != coverYear && null != contractValue){
       soqlStr = 'Select id,Premium__c,GST__c,Range_From__c,Range_To__c,Stamp_Duty__c,Total__c,CategoryType__c from PremiumRatesTable__c ';
        
        filterStr = ' where Range_From__c <= ' +contractValue+ ' AND Range_To__c >= ' +contractValue + ' and Years_of_Cover_Remaining__c =' + '\'' + coverYear + '\'' + ' and recordTypeId =' + '\'' + recordId + '\'' +' limit 1';
        
        finalSoqlQuery = soqlStr + filterStr ;

        System.debug('finalSoqlQuery::>>'+finalSoqlQuery);
           
        premiumRecord = database.query(finalSoqlQuery);
        System.debug('premiumRecord<>'+premiumRecord);

    }

    
   
return premiumRecord ;
}

/**
      * @description       This method is used for calculating the Flat Rate premium for  Builder
      * @return            Boolean      
  */

public  list<PremiumRatesTable__c> calculatePremiumforFlateRate(Account builderRecord,string selectedCategory){

    string soqlStr ;
    string filterStr;    
    String finalSoqlQuery ;
    decimal flatAmount ;
    FlatepremiumRecord = new list<PremiumRatesTable__c>();
    id recordId = Schema.Sobjecttype.PremiumRatesTable__c.getRecordTypeInfosByName().get('FlatRate').getRecordTypeId();

    System.debug('Inside the calculatePremiumforFlateRate');
    
premiumFromAccount = true ;

    if(null != selectedCategory && selectedCategory != '' && selectedCategory == constants.New_Single_Dwelling){
        flatAmount = builderRecord.SD_Flat_Rate_Premium__c ;

    }else if(null != selectedCategory && selectedCategory != '' && selectedCategory == constants.New_Multi_Dwelling){
        flatAmount = builderRecord.C03_Multi_Dwelling_Flat_Rate_Premium__c ;
    }else if(null != selectedCategory && selectedCategory != '' && selectedCategory == constants.Alterations_Additions){
        flatAmount = builderRecord.C04_Structural_Flat_Rate_Premium__c ;
    }else if(null != selectedCategory && selectedCategory != '' && selectedCategory == constants.Swimming_Pools){
        flatAmount = builderRecord.SP_Flat_Rate_Premium__c ;
    }else if(null != selectedCategory && selectedCategory != '' && selectedCategory == constants.Renovations_Tradespeople){
        flatAmount = builderRecord.Non_Structural_Flat_Rate__c ;
    }else if(null != selectedCategory && selectedCategory != '' && selectedCategory == constants.Other){
        flatAmount = builderRecord.C07_Other_Flat_Rate_Premium__c ;
    }

System.debug('flatAmount<<<<'+flatAmount);
    soqlStr = 'Select id,Premium__c,GST__c,Stamp_Duty__c,Total__c from PremiumRatesTable__c ';
     filterStr = ' where Premium__c ='   + flatAmount + ' and recordTypeId =' + '\'' + recordId + '\'' + ' limit 1';

      finalSoqlQuery  = soqlStr + filterStr ;

             System.debug('finalSoqlQuery::>>'+finalSoqlQuery);
           
                FlatepremiumRecord = database.query(finalSoqlQuery);
               // premiumValue = (decimal)premiumRecord[0].get('Premium__c') ;
                system.debug('premiumRecord::--'+premiumRecord); 
                return  FlatepremiumRecord ;


  }

  /**
      * @description      This method is used for calculating the Normal Rate premium for  Builder
      * @return            Boolean      
  */

public  list<PremiumRatesTable__c> calculatePremium(Certificate__c certRecord , Account builderRecord,Property__c propertyRecord, decimal contractValue,string selectedCategory,integer selectedPropertytoinsuread){


string soqlStr ;
string filterStr;
string customSettingName ;
String finalSoqlQuery ;
//referralFlg = false ;
premiumRecord = new list<PremiumRatesTable__c>();
id recordId = Schema.Sobjecttype.PremiumRatesTable__c.getRecordTypeInfosByName().get('NormalRate').getRecordTypeId();

System.debug('Inside Calculate Premium');

    
        System.debug('Inside if Part');
        premiumFromAccount = false;
        

            string CategoryCode;
            string categoryType;

        System.debug('builderRecord.Builder_Risk_Rating__c::>>'+builderRecord.Builder_Risk_Rating__c); 
        System.debug('selectedPropertytoinsuread::>>'+selectedPropertytoinsuread);  
        System.debug('selectedCategory::>>'+selectedCategory);     

        if(null != certRecord && certRecord.Taken_Over__c== false) {

                if((selectedCategory == constants.New_Single_Dwelling ) && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread <=2){                
                    
                    CategoryCode = constants.code_NSD_S ;
                    categoryType = constants.categoryA ;

                }else if (selectedCategory == constants.New_Single_Dwelling && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread <=2){  
                    CategoryCode = constants.code_NSD_S ;
                    categoryType = constants.categoryB ;              
                    
                }else if (selectedCategory == constants.New_Single_Dwelling && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread <=2){                
                    CategoryCode = constants.code_NSD_S ;
                    categoryType = constants.categoryC ; 
                }else if (selectedCategory == constants.New_Multi_Dwelling && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread ==3){                
                    CategoryCode = constants.code_MBW_S ;
                    categoryType = constants.categoryA ; 
                }else if (selectedCategory == constants.New_Multi_Dwelling && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread ==3){                
                    CategoryCode = constants.code_MBW_S ;
                    categoryType = constants.categoryB ; 
                }else if (selectedCategory == constants.New_Multi_Dwelling && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread ==3){                
                    CategoryCode = constants.code_MBW_S ;
                    categoryType = constants.categoryC ; 
                }else if (selectedCategory == constants.New_Multi_Dwelling && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread >=4){                
                    CategoryCode = constants.code_MBW_S ;
                    categoryType = constants.categoryA ; 
                    referralAndValidationCheck.referralFlag = true;
                }else if (selectedCategory == constants.New_Multi_Dwelling && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread >=4){                
                    CategoryCode = constants.code_MBW_S ;
                    categoryType = constants.categoryB ; 
                    referralAndValidationCheck.referralFlag = true;
                }else if (selectedCategory == constants.New_Multi_Dwelling && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread >=4){                
                    CategoryCode = constants.code_MBW_S ;
                    categoryType = constants.categoryC ; 
                    referralAndValidationCheck.referralFlag = true;
                }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread <=2){                
                    CategoryCode = constants.code_NSD_S ;
                    categoryType = constants.categoryA ; 
                    
                }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread <=2){                
                    CategoryCode = constants.code_NSD_S ;
                    categoryType = constants.categoryB ; 
                    
                }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread <=2){                
                    CategoryCode = constants.code_NSD_S ;
                    categoryType = constants.categoryC ; 
                    
                }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread >=3){                
                    CategoryCode = constants.code_MBW_S ;
                    categoryType = constants.categoryA ; 
                    referralAndValidationCheck.referralFlag = true;
                }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread >=3){                
                    CategoryCode = constants.code_MBW_S ;
                    categoryType = constants.categoryB ; 
                    referralAndValidationCheck.referralFlag = true;
                }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread >=3){                
                    CategoryCode = constants.code_MBW_S ;
                    categoryType = constants.categoryC ; 
                    referralAndValidationCheck.referralFlag = true;
                }else if (selectedCategory == constants.Swimming_Pools && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread ==1){                
                    CategoryCode = constants.code_SP ;
                    categoryType = constants.categoryA ; 
                    
                }else if (selectedCategory == constants.Swimming_Pools && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread ==1){                
                    CategoryCode = constants.code_SP ;
                    categoryType = constants.categoryB ; 
                    
                }else if (selectedCategory == constants.Swimming_Pools && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread ==1){                
                    CategoryCode = constants.code_SP ;
                    categoryType = constants.categoryC ; 
                    
                }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread <=2){                
                    CategoryCode = constants.code_NSD_NS ;
                    categoryType = constants.categoryA ; 
                    
                }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread <=2){                
                    CategoryCode = constants.code_NSD_NS ;
                    categoryType = constants.categoryB ; 
                    
                }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread <=2){                
                    CategoryCode = constants.code_NSD_NS ;
                    categoryType = constants.categoryC ; 
                    
                }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread >=3){                
                    CategoryCode = constants.code_MBW_NS ;
                    categoryType = constants.categoryA ; 
                    referralAndValidationCheck.referralFlag = true;
                    
                }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread >=3){                
                    CategoryCode = constants.code_MBW_NS ;
                    categoryType = constants.categoryB ; 
                    referralAndValidationCheck.referralFlag = true;
                    
                }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread >=3){                
                    CategoryCode = constants.code_MBW_NS ;
                    categoryType = constants.categoryC ; 
                    referralAndValidationCheck.referralFlag = true;
                    
                }else if (selectedCategory == constants.Other && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread ==1){                
                    CategoryCode = constants.code_NSD_S ;
                    categoryType = constants.categoryA ; 
                    
                }else if (selectedCategory == constants.Other && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread ==1){                
                    CategoryCode = constants.code_NSD_S ;
                    categoryType = constants.categoryB ; 
                    
                }else if (selectedCategory == constants.Other && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread ==1){                
                    CategoryCode = constants.code_NSD_S ;
                    categoryType = constants.categoryC ; 
                    
                }

         }else if(null != certRecord && certRecord.Taken_Over__c== true ) {// This I  have to Keep
            System.debug('propertyRecord<><>'+propertyRecord);
            list<Certificate__c> cerRecord = new list<Certificate__c>();
            Boolean CertificateFound = false;
            if(null != propertyRecord && null != propertyRecord.Id){
                cerRecord = [select id ,Category_of_Building_Works__c from Certificate__c where Property__c =: propertyRecord.Id and Status__c =: constants.certificate_status_Issued_Works_In_Progress and (Category_of_Building_Works__c =: constants.New_Single_Dwelling or Category_of_Building_Works__c =: constants.New_Multi_Dwelling or Category_of_Building_Works__c =: constants.Swimming_Pools) limit 1] ;
                System.debug('cerRecord<><>'+cerRecord);
                if(null != cerRecord && !cerRecord.isEmpty()){
                        CertificateFound = true;
                    if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread <=2 && cerRecord[0].Category_of_Building_Works__c == constants.New_Single_Dwelling){                
                        CategoryCode = constants.code_NSD_S ;
                        categoryType = constants.categoryA ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread <=2 && cerRecord[0].Category_of_Building_Works__c == constants.New_Single_Dwelling){              
                        CategoryCode = constants.code_NSD_S ;
                        categoryType = constants.categoryB ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread <=2 && cerRecord[0].Category_of_Building_Works__c == constants.New_Single_Dwelling){                
                        CategoryCode = constants.code_NSD_S ;
                        categoryType = constants.categoryC ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread ==3 && cerRecord[0].Category_of_Building_Works__c == constants.New_Multi_Dwelling){                
                        CategoryCode = constants.code_MBW_S ;
                        categoryType = constants.categoryA ;  
                        referralAndValidationCheck.referralFlag = false;                    
                     }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread ==3 && cerRecord[0].Category_of_Building_Works__c == constants.New_Multi_Dwelling){                
                        CategoryCode = constants.code_MBW_S ;
                        categoryType = constants.categoryB ;  
                        referralAndValidationCheck.referralFlag = false;                    
                     }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread ==3 && cerRecord[0].Category_of_Building_Works__c == constants.New_Multi_Dwelling){                
                        CategoryCode = constants.code_MBW_S ;
                        categoryType = constants.categoryC ;  
                        referralAndValidationCheck.referralFlag = false;                    
                     }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread >=4 && cerRecord[0].Category_of_Building_Works__c == constants.New_Multi_Dwelling){                
                        CategoryCode = constants.code_MBW_S ;
                        categoryType = constants.categoryA ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread >=4 && cerRecord[0].Category_of_Building_Works__c == constants.New_Multi_Dwelling){                
                        CategoryCode = constants.code_MBW_S ;
                        categoryType = constants.categoryB ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread >=4 && cerRecord[0].Category_of_Building_Works__c == constants.New_Multi_Dwelling){                
                        CategoryCode = constants.code_MBW_S ;
                        categoryType = constants.categoryC ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread <=2 && cerRecord[0].Category_of_Building_Works__c == constants.New_Single_Dwelling){                
                        CategoryCode = constants.code_NSD_NS ;
                        categoryType = constants.categoryA ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread <=2 && cerRecord[0].Category_of_Building_Works__c == constants.New_Single_Dwelling){                
                        CategoryCode = constants.code_NSD_NS ;
                        categoryType = constants.categoryB ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread <=2 && cerRecord[0].Category_of_Building_Works__c == constants.New_Single_Dwelling){                
                        CategoryCode = constants.code_NSD_NS ;
                        categoryType = constants.categoryC ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread ==3 && cerRecord[0].Category_of_Building_Works__c == constants.New_Multi_Dwelling){                
                        CategoryCode = constants.code_MBW_NS ;
                        categoryType = constants.categoryA ;  
                        referralAndValidationCheck.referralFlag = false;                    
                     }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread ==3 && cerRecord[0].Category_of_Building_Works__c == constants.New_Multi_Dwelling){                
                        CategoryCode = constants.code_MBW_NS ;
                        categoryType = constants.categoryB ;  
                        referralAndValidationCheck.referralFlag = false;                    
                     }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread ==3 && cerRecord[0].Category_of_Building_Works__c == constants.New_Multi_Dwelling){                
                        CategoryCode = constants.code_MBW_NS ;
                        categoryType = constants.categoryC ;  
                        referralAndValidationCheck.referralFlag = false;                    
                     }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread >=4 && cerRecord[0].Category_of_Building_Works__c == constants.New_Multi_Dwelling){                
                        CategoryCode = constants.code_MBW_NS ;
                        categoryType = constants.categoryA ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread >=4 && cerRecord[0].Category_of_Building_Works__c == constants.New_Multi_Dwelling){                
                        CategoryCode = constants.code_MBW_NS ;
                        categoryType = constants.categoryB ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread >=4 && cerRecord[0].Category_of_Building_Works__c == constants.New_Multi_Dwelling){                
                        CategoryCode = constants.code_MBW_NS ;
                        categoryType = constants.categoryC ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Swimming_Pools && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread ==1 && cerRecord[0].Category_of_Building_Works__c == constants.Swimming_Pools){                
                        CategoryCode = constants.code_MBW_NS ;
                        categoryType = constants.categoryC ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Swimming_Pools && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread ==1 && cerRecord[0].Category_of_Building_Works__c == constants.Swimming_Pools){                
                        CategoryCode = constants.code_MBW_NS ;
                        categoryType = constants.categoryC ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }else if (selectedCategory == constants.Swimming_Pools && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread ==1 && cerRecord[0].Category_of_Building_Works__c == constants.Swimming_Pools){                
                        CategoryCode = constants.code_MBW_NS ;
                        categoryType = constants.categoryC ;  
                        referralAndValidationCheck.referralFlag = true;                    
                     }
                 }
                        
                }
                
                System.debug('CertificateFound<>>><<'+CertificateFound);
                if(!CertificateFound){            
                        System.debug('Coming here');                          

                        if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread <=2){                
                            CategoryCode = constants.code_NSD_S ;
                            categoryType = constants.categoryA ; 
                            
                        }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread <=2){                
                            CategoryCode = constants.code_NSD_S ;
                            categoryType = constants.categoryB ; 
                            
                        }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread <=2){                
                            CategoryCode = constants.code_NSD_S ;
                            categoryType = constants.categoryC ; 
                            
                        }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread >=3){                
                            CategoryCode = constants.code_MBW_S ;
                            categoryType = constants.categoryA ; 
                            referralAndValidationCheck.referralFlag = true;
                        }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread >=3){                
                            CategoryCode = constants.code_MBW_S ;
                            categoryType = constants.categoryB ; 
                            referralAndValidationCheck.referralFlag = true;
                        }else if (selectedCategory == constants.Alterations_Additions && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread >=3){                
                            CategoryCode = constants.code_MBW_S ;
                            categoryType = constants.categoryC ; 
                            referralAndValidationCheck.referralFlag = true;
                        }else if (selectedCategory == constants.Swimming_Pools && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread ==1){                
                            CategoryCode = constants.code_SP ;
                            categoryType = constants.categoryA ; 
                            
                        }else if (selectedCategory == constants.Swimming_Pools && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread ==1){                
                            CategoryCode = constants.code_SP ;
                            categoryType = constants.categoryB ; 
                            
                        }else if (selectedCategory == constants.Swimming_Pools && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread ==1){                
                            CategoryCode = constants.code_SP ;
                            categoryType = constants.categoryC ; 
                            
                        }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread <=2){                
                            CategoryCode = constants.code_NSD_NS ;
                            categoryType = constants.categoryA ; 
                            
                        }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread <=2){                
                            CategoryCode = constants.code_NSD_NS ;
                            categoryType = constants.categoryB ; 
                            
                        }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread <=2){                
                            CategoryCode = constants.code_NSD_NS ;
                            categoryType = constants.categoryC ; 
                            
                        }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread >=3){                
                            CategoryCode = constants.code_MBW_NS ;
                            categoryType = constants.categoryA ; 
                            referralAndValidationCheck.referralFlag = true;
                            
                        }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread >=3){                
                            CategoryCode = constants.code_MBW_NS ;
                            categoryType = constants.categoryB ; 
                            referralAndValidationCheck.referralFlag = true;
                            
                        }else if (selectedCategory == constants.Renovations_Tradespeople && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread >=3){                
                            CategoryCode = constants.code_MBW_NS ;
                            categoryType = constants.categoryC ; 
                            referralAndValidationCheck.referralFlag = true;
                            
                        }else if (selectedCategory == constants.Other && builderRecord.Builder_Risk_Rating__c == 'A' && selectedPropertytoinsuread ==1){                
                            CategoryCode = constants.code_NSD_S ;
                            categoryType = constants.categoryA ; 
                            
                        }else if (selectedCategory == constants.Other && builderRecord.Builder_Risk_Rating__c == 'B' && selectedPropertytoinsuread ==1){                
                            CategoryCode = constants.code_NSD_S ;
                            categoryType = constants.categoryB ; 
                            
                        }else if (selectedCategory == constants.Other && builderRecord.Builder_Risk_Rating__c == 'C' && selectedPropertytoinsuread ==1){                
                            CategoryCode = constants.code_NSD_S ;
                            categoryType = constants.categoryC ; 
                            
                        }
                    
                    }  
            }
          

         System.debug('CategoryCode<>'+CategoryCode);
          System.debug('categoryType<>'+categoryType);
         decimal maxAmountTocheck ;
         if(CategoryCode == constants.code_NSD_S){
            maxAmountTocheck = constants.code_NSD_S_MaxLimit ;
        }else if (CategoryCode == constants.code_NSD_NS){
            maxAmountTocheck = constants.code_NSD_NS_MaxLimit ;
        }else if (CategoryCode == constants.code_SP){
            maxAmountTocheck = constants.code_SP_MaxLimit ;
        }else if (CategoryCode == constants.code_MBW_S){
            maxAmountTocheck = constants.code_MBW_S_MaxLimit ;
        }else if (CategoryCode == constants.code_MBW_NS){
            maxAmountTocheck = constants.code_MBW_NS_MaxLimit ;
        }   

System.debug('contractValue:::'+contractValue);
system.debug('maxAmountTocheck:::'+maxAmountTocheck);

if(null != contractValue && null != maxAmountTocheck && null != categoryType && null != CategoryCode && null != recordId){
        if(contractValue >= maxAmountTocheck){
                soqlStr = 'Select id,Premium__c,GST__c,Range_From__c,Range_To__c,Stamp_Duty__c,Total__c,CategoryType__c from PremiumRatesTable__c ';
             filterStr = ' where Range_From__c = ' +maxAmountTocheck+  ' and CategoryType__c =' + '\'' + categoryType + '\''+' and TypeOfWork__c = ' + '\'' + CategoryCode +  '\''  + ' and recordTypeId =' + '\'' + recordId + '\'' +' limit 1';
            }else{

                soqlStr = 'Select id,Premium__c,GST__c,Range_From__c,Range_To__c,Stamp_Duty__c,Total__c,CategoryType__c from PremiumRatesTable__c ';
             filterStr = ' where Range_From__c <= ' +contractValue+ ' AND Range_To__c >= ' +contractValue + ' and CategoryType__c =' + '\'' + categoryType + '\''+' and TypeOfWork__c = ' + '\'' + CategoryCode +  '\'' + ' and recordTypeId =' + '\'' + recordId + '\'' +' limit 1';
            }
             if(null != filterStr && filterStr != ''){

             finalSoqlQuery  = soqlStr + filterStr ;

             System.debug('finalSoqlQuery::>>'+finalSoqlQuery);
           
                premiumRecord = database.query(finalSoqlQuery);
               // premiumValue = (decimal)premiumRecord[0].get('Premium__c') ;
               }

}               
                system.debug('premiumRecord::--'+premiumRecord); 
    return  premiumRecord ;
}



}