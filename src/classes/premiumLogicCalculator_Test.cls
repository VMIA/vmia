/**
* Date         :  19-May-2017
* Author       :  SMS Management & Technology
* Description  :  Test Class for GenerateCertificatesController controller
*/ 

/*******************************  History ************************************************
Date                User                                        Comments


*******************************  History ************************************************/   
@isTest
private class premiumLogicCalculator_Test {
    
    //variables
    private static Map<String,Id> profileMap = new Map<String,Id>();
    
    @testSetup
    private static void testDataSetup(){
        for(Profile prof : [SELECT Name FROM Profile]){
            profileMap.put(prof.Name,prof.Id);
        }
        

        setupData();
        
    }
    
    @future
    private static void setupData(){
        TestUtility.createCustomSettings();
        
        Map<String,Schema.RecordTypeInfo> accRecTypeMap = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> conRecTypeMap = Schema.Sobjecttype.Contact.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Map<String,Schema.RecordTypeInfo> appRecTypeMap = Schema.Sobjecttype.Application__c.getRecordTypeInfosByName();
        
        Account rexDistributor = TestUtility.createBusinessAccount('Rex Home Distributor',accRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexDistributor);
        System.debug('** rexDistributor ==>'+ rexDistributor);
        
        Contact rexSmith = TestUtility.createContact('Rex','Smith','rex.smith@rexHomeDistributorTest.com',rexDistributor.Id,conRecTypeMap.get('Distributor').getRecordTypeId());
        Database.insert(rexSmith);
        System.debug('** rexSmith ==>'+ rexSmith);
        
        Account bornBuilder = TestUtility.createBusinessAccount('Born Builders',accRecTypeMap.get('Company').getRecordTypeId());
        bornBuilder.Agent__c = rexDistributor.Id;
        bornBuilder.Agent_Contact__c = rexSmith.Id;
        
        Database.insert(bornBuilder);

        Contact tonyStark = TestUtility.createContact('Tony','Stark','tony.stark@bornBuildersTest.com',bornBuilder.Id,conRecTypeMap.get('Builder').getRecordTypeId());
        Database.insert(tonyStark);
        System.debug('** tonyStark ==>'+ tonyStark);
        
        Certificate__c  cert = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
        Database.insert(cert);
        System.debug('** Certificate ==>'+ cert.id);

        User rexUser = TestUtility.createPortalUser('RexSmith',profileMap.get('Distributor'),rexSmith.Id);
        Database.insert(rexUser);   
        
    }

    private static testMethod void calculatePremiumforFlateTest(){
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
      
    System.runAs(rexUser){
       premiumLogicCalculator pLogic = new premiumLogicCalculator();
       pLogic.calculatePremiumforOwnerBuilder(System.today()-1*365 , 20000);
       pLogic.calculatePremiumforOwnerBuilder(System.today()-2*365 , 20000);
       pLogic.calculatePremiumforOwnerBuilder(System.today()-3*365 , 20000);
       pLogic.calculatePremiumforOwnerBuilder(System.today()-4*365 , 20000);
       pLogic.calculatePremiumforOwnerBuilder(System.today()-5*365 , 20000);
       pLogic.calculatePremiumforOwnerBuilder(System.today()-6*365 , 20000);
    }   
        
    }   

     private static testMethod void calculatePremiumforFlateRateTest(){
        
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        
        System.runAs(rexUser){


             premiumLogicCalculator pLogic = new premiumLogicCalculator();
             bornBuilder.SD_Flat_Rate_Premium__c = 20000;
             pLogic.calculatePremiumforFlateRate(bornBuilder,'C01: New Single Dwelling Construction');
             pLogic.calculatePremiumforFlateRate(bornBuilder,'C03: New Multi-Dwelling Construction');
             pLogic.calculatePremiumforFlateRate(bornBuilder,'C04: Alterations/Additions/Renovations - Structural');
             pLogic.calculatePremiumforFlateRate(bornBuilder,'C05: Swimming Pools');
             pLogic.calculatePremiumforFlateRate(bornBuilder,'C06: Refurbishment - Non Structural');
             pLogic.calculatePremiumforFlateRate(bornBuilder,'C07: Other');
             pLogic.calculatePremiumforFlateRate(bornBuilder,'C08: All Cover');
        }

     } 

     private static testMethod void calculatePremiumTest(){
        
        User rexUser = [SELECT Id,Name FROM User WHERE Name = 'RexSmith' LIMIT 1];
        Account bornBuilder = [SELECT Id FROM Account WHERE Name = 'Born Builders' LIMIT 1];
        Account rexDistributor = [SELECT Id FROM Account WHERE Name = 'Rex Home Distributor' LIMIT 1];
        Map<String,Schema.RecordTypeInfo> certRecTypeMap = Schema.Sobjecttype.Certificate__c.getRecordTypeInfosByName();
        Certificate__c cert = [Select Id from Certificate__c limit 1];

         // Data for Property Record Creation
            Property__c property = TestUtility.createProperty('TestStreet', 'TestSuburb','1234', 'VIC');
            database.insert(property);

        System.runAs(rexUser){
            premiumLogicCalculator pLogic = new premiumLogicCalculator();
            cert.Taken_Over__c = false;
            cert.Speculative__c = false;
            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C01: New Single Dwelling Construction',1);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C01: New Single Dwelling Construction',1);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C01: New Single Dwelling Construction',1);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',3);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',3);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',3);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',4);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',4);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',4);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C04: Alterations/Additions/Renovations - Structural',2);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C04: Alterations/Additions/Renovations - Structural',2);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C04: Alterations/Additions/Renovations - Structural',2);

             bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C04: Alterations/Additions/Renovations - Structural',4);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C04: Alterations/Additions/Renovations - Structural',4);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C04: Alterations/Additions/Renovations - Structural',4);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C05: Swimming Pools',1);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C05: Swimming Pools',1);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C05: Swimming Pools',1);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C06: Refurbishment - Non Structural',1);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C06: Refurbishment - Non Structural',1);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C06: Refurbishment - Non Structural',1);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C06: Refurbishment - Non Structural',4);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C06: Refurbishment - Non Structural',4);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C06: Refurbishment - Non Structural',4);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C07: Other',1);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C07: Other',1);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C07: Other',1);


            cert.Taken_Over__c = true;
            cert.Speculative__c = true;

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C01: New Single Dwelling Construction',1);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',3);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',3);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',3);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',4);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',4);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',4);

            cert.Taken_Over__c = false;
            cert.Speculative__c = true;

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C01: New Single Dwelling Construction',1);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C01: New Single Dwelling Construction',1);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C01: New Single Dwelling Construction',1);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',3);

             bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',3);

             bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',3);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',4);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',4);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C03: New Multi-Dwelling Construction',4);

            cert.Taken_Over__c = true;
            cert.Speculative__c = false;

            Certificate__c  certOne = TestUtility.createCertificate(bornBuilder.Id, rexDistributor.Id, certRecTypeMap.get('Certificate').getRecordTypeId());
            certOne.Property__c = property.Id;
            certOne.Status__c = 'Issued Works in Progress' ;
            certOne.Category_of_Building_Works__c = 'C01: New Single Dwelling Construction' ;
            Database.insert(certOne);


            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(certOne,bornBuilder,property,20000,'C04: Alterations/Additions/Renovations - Structural',1);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(certOne,bornBuilder,property,20000,'C04: Alterations/Additions/Renovations - Structural',1);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(certOne,bornBuilder,property,20000,'C04: Alterations/Additions/Renovations - Structural',1);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(certOne,bornBuilder,property,20000,'C06: Refurbishment - Non Structural',1);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(certOne,bornBuilder,property,20000,'C06: Refurbishment - Non Structural',1);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(certOne,bornBuilder,property,20000,'C06: Refurbishment - Non Structural',1);

            certOne.Category_of_Building_Works__c = 'C03: New Multi-Dwelling Construction' ;
            database.update(certOne);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C04: Alterations/Additions/Renovations - Structural',3);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C04: Alterations/Additions/Renovations - Structural',3);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C04: Alterations/Additions/Renovations - Structural',3);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C06: Refurbishment - Non Structural',3);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C06: Refurbishment - Non Structural',3);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C06: Refurbishment - Non Structural',3);

            certOne.Category_of_Building_Works__c = 'C05: Swimming Pools' ;
            database.update(certOne);

            bornBuilder.Builder_Risk_Rating__c = 'A';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C05: Swimming Pools',1);

            bornBuilder.Builder_Risk_Rating__c = 'B';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C05: Swimming Pools',1);

            bornBuilder.Builder_Risk_Rating__c = 'C';
            pLogic.calculatePremium(cert,bornBuilder,property,20000,'C05: Swimming Pools',1);

        }
    }        


  }