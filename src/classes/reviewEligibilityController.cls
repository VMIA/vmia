public without sharing class reviewEligibilityController{
    public List<Account> newAccount {get;set;}
    public List<Account> newAccountName{get;set;}
    public string builderID {get;set;}
    public Application__c newApplication {get;set;}
    public List<User> dbiUser {get;set;}
    public List<Contact_Role__c> contactList {get;set;}
    public string appId{get;set;}
    public String appIdParam{get;set;}
    public Boolean isAppAlreadyCreated{get;set;}
    public list <categoryWrapper> categories {get;set;}
    public list<String> selectedCheckboxFieldApiNames{get;set;}
    public list<String> selectedEligibility{get;set;}
    public map<Id, String> builderIdNameMap {get;set;}
    public String builderName {get;set;}
    public boolean reviewInProgress {get;set;}

    public String otherRequests {get;set;}
    public boolean hasErrors{get;set;}
    public boolean hasDocuments{get;set;}


    public String rbpName{get;set;}
    public String rbpEmail{get;set;}
    public String rbpPhone{get;set;}
    public String eligibilityConditions{get;set;}

    public boolean limitChanges{get;set;}
    public boolean categoryChanges{get;set;}
    public boolean rbpChanges{get;set;}

    public boolean singleDwellingDisabled{get;set;}
    public boolean showDownloadButton{get;set;}

    private static final String CATEGORY_1 = constants.New_Single_Dwelling;
    private static final String CATEGORY_3 = constants.New_Multi_Dwelling;
    private static final String CATEGORY_4 = constants.Alterations_Additions;
    private static final String CATEGORY_5 = constants.Swimming_Pools;
    private static final String CATEGORY_6 = constants.Renovations_Tradespeople;
    private static final String CATEGORY_7 = constants.Other;
    private static final String CATEGORY_8 = constants.All_Cover;

// Code we will invoke on page load.
    public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return Page.Login;
        }
        else{
            return null;
        }
    }

    public PageReference save(){
        //get record type of Review Eligibility
        if(!isAppAlreadyCreated){
                newApplication.RecordTypeId = constants.application_RecordTypeId_Review_Eligibility;
        }
        hasErrors = False;
        if(!rbpChanges && !limitChanges && !categoryChanges)
        {
            hasErrors = True;
            /*message updated by KATE LUU - SMS BRisbane - 12June2017*/
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please select at least one change you would like to make to the current eligibility' );
            ApexPages.addMessage(myMsg);
            return null;
        }

        if(String.isBlank(otherRequests)){
            hasErrors = True;
            /*message updated by KATE LUU - SMS BRisbane - 12June2017*/
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please complete a detailed description of the request' );
            ApexPages.addMessage(myMsg);
            return null;
        }
        newApplication.External_Comments__c = otherRequests;
        newApplication.rbp_Changes__c = rbpChanges;
        newApplication.limit_changes__c = limitChanges;
        newApplication.category_changes__c = categoryChanges;
        newApplication.type__c = constants.reviewEligibility;

        Id ownerId = GlobalUtility.getDistributorQueue() ;

        try{
            if (isAppAlreadyCreated){
                update newApplication;
            }else{
                insert newApplication;
            }
            appId = newApplication.id;
        }catch(DmlException e){
           System.debug('An unexpected error has occurred: ' + e.getMessage());
        } 

        
        newAccount[0].Builder_Eligibility_Status_Reason__c = constants.account_Builder_Eligibility_Status_Reason_Under_Review;

        try{
            update newAccount;
        }catch(DmlException e){
           System.debug('An unexpected error has occurred: ' + e.getMessage());
        }

        return null;
    }


    public PageReference confirmation(){

        return new PageReference('/apex/EligibilityApplicationConfirmation?id=' + newApplication.id);
    }

    public PageReference cancel(){
        return new PageReference('/apex/DistributorLanding');

    }

    public reviewEligibilityController() {
        newApplication = new Application__c();
        selectedCheckboxFieldApiNames = new list<String>();
        selectedEligibility = new list<String>();
        builderID = ApexPages.currentPage().getParameters().get('id');
        appIdParam = ApexPages.currentPage().getParameters().get('appID');
        isAppAlreadyCreated = String.isBlank(appIdParam) ? False : True;
        contactList = new List<Contact_Role__c>();
        builderIdNameMap = new Map<Id, String>();
        newAccountName = new List<Account>();
        reviewInProgress = False;
        Id reviewEligibilityId = GlobalUtility.getsObjectRecordTypes('Application__c').get(GlobalConstants.APP_REC_TYPE_REVIEW_ELIGIBILITY);
        Set<String> validApplicationStatuses = new Set<String>{GlobalConstants.APPLICATION_STATUS_BUILDER_CHANGED, GlobalConstants.APPLICATION_STATUS_BUILDER_UNCHANGED, GlobalConstants.APPLICATION_STATUS_BUILDER_WITHDRAWN};


        List<Application__c> inProgressApplication = [SELECT id FROM Application__c WHERE Builder_Number__c =: builderId AND RecordTypeId =: reviewEligibilityId 
                                                    AND Application_Status__c NOT IN: validApplicationStatuses];


        if(!inProgressApplication.isEmpty()){
            reviewInProgress = True;
        }
        if(!reviewInProgress){
            newAccount = [Select id, Eligibility_Conditions__c,  Other_Terms__c, Agent_contact__c, agent__c, ABN__c, vba_number__c, ACN__c, AvailableConstructionLimit__c, Builder_Risk_Rating__c, 
                   Eligibility_Status__c, Construction_Limit__c, Flat_Rate_Premium__c, SD_Flat_Rate_Premium__c, SP_Flat_Rate_Premium__c,
                    Original_Eligibility_Approval_Date__c, CO1_New_Single_Dwelling__c, CO3_New_Multi_Dwelling__c, C03_Multi_Dwelling_Flat_Rate_Premium__c,
                   C04_Alterations_Additions__c, C04_Structural_Flat_Rate_Premium__c, C05_Swimming_Pools__c, C06_Renovations_Tradespeople__c, C07_Other__c, C08_All_Cover__c,
                   Construction_Category_Limit_1__c, Construction_Category_Limit_3__c, Construction_Category_Limit_4__c, Non_Structural_Flat_Rate__c,
                   Construction_Category_Limit_5__c, Construction_Category_Limit_6__c, Other_Construction_Category_Limit__c, Construction_Category_Limit_7__c,
                   C08_All_Cover_Flat_Rate_Premium__c, C07_Other_Flat_Rate_Premium__c, Name,IsPersonAccount,
                    (SELECT role__c, contact__r.name, id FROM Contact_Roles__r)
                   from Account where id=:builderID  limit 1]; 

           newAccountName = [SELECT FirstName, LastName, Name FROM Account where id=:builderID  limit 1];

            
            builderName = '';


            if (newAccount.size() > 0){
                builderIdNameMap.put(newAccount[0].id, newAccountName[0].Name);
                builderName = newAccountName[0].Name;
                selectedEligibility = String.isNotBlank(newAccount[0].Eligibility_Conditions__c) ? 
                                            newAccount[0].Eligibility_Conditions__c.split(';') : selectedEligibility;
                if(!String.isBlank(newAccount[0].Other_Terms__c)){
                    selectedEligibility.add(newAccount[0].Other_Terms__c);
                }
                
                listCategories(newAccount[0]);

                if(newAccount[0].Eligibility_Status__c == constants.account_Eligibility_Status_Active_Restricted || newAccount[0].Eligibility_Status__c == constants.account_Eligibility_Status_Active){
                    showDownloadButton = True;
                }
                Id ownerId ;
                if (isAppAlreadyCreated){
                    newApplication = [Select id from Application__c where id=:appIdParam Limit 1];
                    newApplication.application_status__c = constants.application_Status_New_Information_Received;
                } else{
                    User u = new User();
                    try{
                        u = [SELECT Id,ContactId, accountId,IsActive, Profile.name 
                            FROM User
                            WHERE Id =: UserInfo.getUserID()
                            AND IsActive =: true];
                    } catch(DmlException e){
                        System.debug('An unexpected error has occurred: ' + e.getMessage());
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, string.valueOf(e.getMessage()), ''));
                        hasErrors = True;
                    }
                    if(u.profile.name == constants.Distributor){
                        newApplication.Application_Status__c = constants.application_Status_new;
                    }else{
                      ownerId = GlobalUtility.getDistributorQueue() ;  
                       newApplication.Application_Status__c = constants.application_Status_DD_Review; 
                    }
                }  

                newApplication.application_source__c = constants.application_source_DD_Initiated;
                

                System.debug('ownerId>>>>>>'+ownerId);
                newApplication.ownerid = ownerId != null ? ownerId : GlobalUtility.getQueues(new list<String>{GlobalConstants.BAT_QUEUE}).get(GlobalConstants.BAT_QUEUE);
                //newApplication.ownerid = GlobalUtility.getQueues(new list<String>{GlobalConstants.BAT_QUEUE}).get(GlobalConstants.BAT_QUEUE); 

                if(!newAccount[0].Contact_Roles__r.isEmpty()){
                    for(Contact_Role__c acr : newAccount[0].Contact_Roles__r){
                        contactList.add(acr);
                    }
                }
                
                newApplication.Builder_ABN__c = newAccount[0].ABN__c;
                newApplication.Builder_ACN__c = newAccount[0].ACN__c;
                newApplication.Builder_Number__c = newAccount[0].id;
                newApplication.Builder_Result_Status__c = newAccount[0].Eligibility_Status__c;
                /*newApplication.Builder_Legal_Entity_Name__c = newAccount[0].Name;
                if(String.isNotBlank(newAccountName[0].LastName)){
                    newApplication.Builder_First_Name__c = newAccountName[0].FirstName;
                    newApplication.Builders_Last_Name__c = newAccountName[0].LastName;
                }else{
                    newApplication.builder_Legal_entity_name__c = builderIdNameMap.get(newAccount[0].id);
                }*/
                
                if(newAccount[0].IsPersonAccount ) {
                	newApplication.Builder_First_Name__c = newAccountName[0].FirstName;
                    newApplication.Builders_Last_Name__c = newAccountName[0].LastName;
                }else {
                	newApplication.Builder_Legal_Entity_Name__c = newAccount[0].Name;
                }
                
                newApplication.vba_number__c = newAccount[0].vba_number__c;
                dbiUser = [Select id, contactId from User where AccountId =: newAccount[0].Agent__c AND IsActive=TRUE Limit 1];
                newApplication.DD_Account__c = newAccount[0].agent__c;

                if (dbiUser.size()>0){
                    newApplication.dbi_Distributor__c = dbiUser[0].id;
                }



            }
        }
        
    }

    //rollback application & account when file size is more than 25MB
    public void rollBackApplication(){
        try{
            Database.delete(newApplication);
            newApplication.Id = null;
        }
        catch(Exception exp){
            // log all the exceptions to application log
            Logger.logMessage(String.valueOf(LoggingLevel.Error),String.valueOf(reviewEligibilityController.class),
                                GlobalConstants.FN_ROLLBACK_APPLICATION,null,exp.getTypeName(),exp.getMessage(),null,exp,null);
        }
    }

    public void listCategories(Account objAcc) {
        categories = new list <categoryWrapper>();
        
        if(objAcc.CO1_New_Single_Dwelling__c)
            categories.add(new categoryWrapper(CATEGORY_1,objAcc.Construction_Category_Limit_1__c, objAcc.SD_Flat_Rate_Premium__c));
            
        if(objAcc.CO3_New_Multi_Dwelling__c)
            categories.add(new categoryWrapper(CATEGORY_3,objAcc.Construction_Category_Limit_3__c, objAcc.C03_Multi_Dwelling_Flat_Rate_Premium__c));
            
        if(objAcc.C04_Alterations_Additions__c)
            categories.add(new categoryWrapper(CATEGORY_4,objAcc.Construction_Category_Limit_4__c, objAcc.C04_Structural_Flat_Rate_Premium__c));
            
        if(objAcc.C05_Swimming_Pools__c)
            categories.add(new categoryWrapper(CATEGORY_5,objAcc.Construction_Category_Limit_5__c, objAcc.SP_Flat_Rate_Premium__c));
            
        if(objAcc.C06_Renovations_Tradespeople__c)
            categories.add(new categoryWrapper(CATEGORY_6,objAcc.Construction_Category_Limit_6__c, objAcc.Non_Structural_Flat_Rate__c));
            
        if(objAcc.C07_Other__c)
            categories.add(new categoryWrapper(CATEGORY_7,objAcc.Construction_Category_Limit_7__c, objAcc.C07_Other_Flat_Rate_Premium__c));
            
    }
    
    public class categoryWrapper{
        public String catName{get;set;}
        public Decimal catLimit{get;set;}
        public Decimal catPremium{get;set;}
        public categoryWrapper(String newCatName, Decimal newCatLimit, Decimal newCatPremium){
            catName = newCatName;
            catLimit = newCatLimit;
            catPremium = newCatPremium;
        }
    }


     public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Limit','Request Eligibility Limit Changes'));
        options.add(new SelectOption('Category','Request Eligibility Category Changes'));
        options.add(new SelectOption('RBP','Request Director/Registered Building Practitioner Changes'));
        return options;
    }



}