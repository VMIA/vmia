public class sendEmail {
    public String body { get; set; }

    @TestVisible private final Account account;

    // Create a constructor that populates the Account object
    public sendEmail() {
        account = [SELECT Name, 
                  (SELECT Contact.Name, Contact.Email FROM Account.Contacts) 
                   FROM Account 
                   WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    }

    public Account getAccount() {
        return account;
    }   

    @future(callout=true)
    public static void createTask(String appID, String ddID){
        List<Application__c> newApp = [Select id, name, Further_Information_Summary__c, Task_Summary_Selection__c, Further_Information_Description__c,  Builder_Number__c from Application__c where id=: appId limit 1];
        if (newApp.size()>0){
            if((newApp[0].Further_Information_Description__c !='' && newApp[0].Further_Information_Description__c !=null) && (newApp[0].Further_Information_Summary__c !='' && newApp[0].Further_Information_Summary__c !=null)){
                Task tsk = new Task(IsVisibleInSelfService= true,whatID = appID, Ownerid = ddID, Builder__c = newApp[0].Builder_Number__c, activityDate =system.today()+180, subject = newApp[0].Further_Information_Summary__c, description = newApp[0].Further_Information_Description__c); 
                insert tsk; 
            }else if(newApp[0].Task_Summary_Selection__c == constants.application_Task_Summary_Selection_Pending_Declinature || newApp[0].Task_Summary_Selection__c ==constants.application_Task_Summary_Selection_Builder_Suspended){
                Task tsk = new Task(IsVisibleInSelfService= true,whatID = appID, Ownerid = ddID, Builder__c = newApp[0].Builder_Number__c, activityDate =system.today()+28, subject = newApp[0].Task_Summary_Selection__c, description = newApp[0].Further_Information_Description__c); 
                insert tsk;
            }else{
                Task tsk = new Task(IsVisibleInSelfService= true,whatID = appID, Ownerid = ddID, Builder__c = newApp[0].Builder_Number__c, activityDate =system.today()+180, subject = newApp[0].Task_Summary_Selection__c, description = newApp[0].Further_Information_Description__c); 
                insert tsk;  
            }
 
        } 
            
    }
    @future(callout=true)
    public static void createCertTask(String certID, String ddID, String subjectField, String descField){
        List<Certificate__c> newCert = [Select id, name from Certificate__c where id=: certId limit 1];
        if(newCert.size()>0){
            Task tsk = new Task(IsVisibleInSelfService= true,whatID = certID, Ownerid = ddID, activityDate = (system.today()+180), subject =subjectField, description = descField); 
            insert tsk;
        }
    }


    @future(callout=true)
    public static void createNewTask(String appID, String ddID, ID builderID, String builderLegalName, String requestedSubject, String requestedDescription, Integer daysToExpiry){
        Task tsk = new Task(IsVisibleInSelfService= true,whatID = appID, Ownerid = ddID, Builder__c = builderID, activityDate =system.today()+ daysToExpiry, subject = requestedSubject, description = requestedDescription); 
        insert tsk; 
    } 
            

}