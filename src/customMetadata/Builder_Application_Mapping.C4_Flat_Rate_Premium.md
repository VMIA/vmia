<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>C4 Flat Rate Premium</label>
    <protected>false</protected>
    <values>
        <field>Application_Field_API_Name__c</field>
        <value xsi:type="xsd:string">Flat_Rate_Premium_C04__c</value>
    </values>
    <values>
        <field>Builder_Field_API_Name__c</field>
        <value xsi:type="xsd:string">C04_Structural_Flat_Rate_Premium__c</value>
    </values>
</CustomMetadata>
