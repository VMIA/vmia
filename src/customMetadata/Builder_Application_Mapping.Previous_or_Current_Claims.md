<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Previous or Current Claims?</label>
    <protected>false</protected>
    <values>
        <field>Application_Field_API_Name__c</field>
        <value xsi:type="xsd:string">Previous_or_Current_Claims__c</value>
    </values>
    <values>
        <field>Builder_Field_API_Name__c</field>
        <value xsi:type="xsd:string">Previous_or_Current_Claims__c</value>
    </values>
</CustomMetadata>
