<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Approved</label>
    <protected>false</protected>
    <values>
        <field>Days_for_due_date__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>Task_Description__c</field>
        <value xsi:type="xsd:string">COI has been approved. Please contact the builder.</value>
    </values>
    <values>
        <field>Task_Subject__c</field>
        <value xsi:type="xsd:string">COI Approved - Notify Builder</value>
    </values>
</CustomMetadata>
