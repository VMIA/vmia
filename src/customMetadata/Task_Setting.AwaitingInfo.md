<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AwaitingInfo</label>
    <protected>false</protected>
    <values>
        <field>Days_for_due_date__c</field>
        <value xsi:type="xsd:double">180.0</value>
    </values>
    <values>
        <field>Task_Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Task_Subject__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
