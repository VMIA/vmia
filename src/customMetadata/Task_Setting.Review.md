<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Review</label>
    <protected>false</protected>
    <values>
        <field>Days_for_due_date__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Task_Description__c</field>
        <value xsi:type="xsd:string">Task requiring DD to review completeness of application and that required documents are uploaded.</value>
    </values>
    <values>
        <field>Task_Subject__c</field>
        <value xsi:type="xsd:string">Review Referred COI Application</value>
    </values>
</CustomMetadata>
