<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Stores debug, info and error messages for support purposes</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Age__c</fullName>
        <description>The age in days of the record.  Enables batch jobs to delete old records easily.</description>
        <externalId>false</externalId>
        <formula>TODAY() - DATEVALUE( CreatedDate )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The age in days of the record</inlineHelpText>
        <label>Age</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Debug_Level__c</fullName>
        <description>The debug level of the record.</description>
        <externalId>false</externalId>
        <inlineHelpText>The type of application log</inlineHelpText>
        <label>Debug Level</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Debug</fullName>
                    <default>false</default>
                    <label>Debug</label>
                </value>
                <value>
                    <fullName>Info</fullName>
                    <default>false</default>
                    <label>Info</label>
                </value>
                <value>
                    <fullName>Error</fullName>
                    <default>false</default>
                    <label>Error</label>
                </value>
                <value>
                    <fullName>Warning</fullName>
                    <default>false</default>
                    <label>Warning</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Integration_Payload__c</fullName>
        <description>The payload for integration messages</description>
        <externalId>false</externalId>
        <inlineHelpText>The payload for integration messages</inlineHelpText>
        <label>Integration Payload</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>20</visibleLines>
    </fields>
    <fields>
        <fullName>Log_Code__c</fullName>
        <description>The related code for the record (either error code or Service NSW code)</description>
        <externalId>false</externalId>
        <inlineHelpText>The related code for the record (either error code or Service NSW code)</inlineHelpText>
        <label>Log Code</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Message__c</fullName>
        <description>The log message (either an error message or info message)</description>
        <externalId>false</externalId>
        <label>Message</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Reference_Id__c</fullName>
        <description>The record id of the related object that initiated the log record</description>
        <externalId>false</externalId>
        <inlineHelpText>The record id of the related object that initiated the log record</inlineHelpText>
        <label>Reference Id</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reference_Information__c</fullName>
        <description>The salesforce sObject for the reference id (e.g ref id 0000e9kkks is a Contact)</description>
        <externalId>false</externalId>
        <inlineHelpText>The salesforce sObject for the reference id (e.g ref id 0000e9kkks is a Contact)</inlineHelpText>
        <label>Reference Information</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Source_Function__c</fullName>
        <description>The source function name of the error (e.g. getContactInformation)</description>
        <externalId>false</externalId>
        <inlineHelpText>The source function name of the error (e.g. getContactInformation)</inlineHelpText>
        <label>Source Function</label>
        <length>200</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Source__c</fullName>
        <description>The name of the utility class or trigger that the message originated from</description>
        <externalId>false</externalId>
        <inlineHelpText>The name of the utility class or trigger that the message originated from</inlineHelpText>
        <label>Source</label>
        <length>150</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Stack_Trace__c</fullName>
        <description>The original stack trace for exceptions</description>
        <externalId>false</externalId>
        <inlineHelpText>The original stack trace for exceptions</inlineHelpText>
        <label>Stack Trace</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Timer__c</fullName>
        <description>The time in milliseconds taken as part of the processing (used to record time taken for callouts and batch jobs)</description>
        <externalId>false</externalId>
        <inlineHelpText>The time in milliseconds taken as part of the processing (used to record time taken for callouts and batch jobs)</inlineHelpText>
        <label>Timer</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Application Log</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Source__c</columns>
        <columns>Source_Function__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>ALOG-{0000000}</displayFormat>
        <label>Log #</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Application Logs</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Source__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Source_Function__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <customTabListAdditionalFields>Debug_Level__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Log_Code__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Age__c</customTabListAdditionalFields>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>Source__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Source_Function__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Debug_Level__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Age__c</lookupDialogsAdditionalFields>
        <searchFilterFields>Source__c</searchFilterFields>
        <searchFilterFields>Source_Function__c</searchFilterFields>
        <searchFilterFields>CREATEDBY_USER</searchFilterFields>
        <searchFilterFields>Debug_Level__c</searchFilterFields>
        <searchFilterFields>Age__c</searchFilterFields>
        <searchFilterFields>Log_Code__c</searchFilterFields>
        <searchResultsAdditionalFields>Source__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Source_Function__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATED_DATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATEDBY_USER</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Debug_Level__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Log_Code__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Age__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
    <visibility>Public</visibility>
</CustomObject>
