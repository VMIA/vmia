<apex:page Controller="EditEligibilityApplication" tabStyle="Account" showHeader="false"  sidebar="false" id="mypage" action="{!forwardToCustomAuthPage}" applyHtmlTag="HTML-5" standardStylesheets="false">
    <head>
        <c:IncludeAssets />
    </head>

    <body>
        <div class="container">
            <header>
                <c:IncludeHeader />
            </header>
            
            <c:IncludeNavBar />
            
            <apex:form id="myform">
                
                <div class="row container-header">
                    <div class="row fsi-row-md-level fsi-row-lg-level">
                        <div class="col-md-9">
                           <h2>APPLICATION DETAILS</h2>
                        </div>
                        <div class="col-md-4">
                            <br/>
                                <apex:commandButton styleClass="btn btn-primary pagebtn" action="{!withdrawApplication}" value="{!IF(cancelCOI || amendCOI, $Label.Withdraw_Certificate,$Label.Withdraw_Application)}" rendered="{!showWithdrawBtn}" rerender="myform" style="padding-bottom: 50px;"/>
                        </div>
                    </div>
                </div>      
                <br/>
                <div class="row fsi-row-md-level fsi-row-lg-level"> 
                    <div class="col-md-6 container-sections">
                        <dl>
                            <dt>Application No.</dt>
                            <dd>{!application.Name}</dd>
                            <dt>Legal Entity Name</dt>
                            <dd>
                                <apex:outputLink value="{!URLFOR($Page.BuilderLandingPage,null,[id = application.Builder_Number__c])}" target="_blank" styleclass="outputlink" rendered="{!!isOwnerBuilder}">{!builderName}</apex:outputLink>
                                <apex:outputText value="{!builderName}" rendered="{!isOwnerBuilder}"/>
                            </dd>
                        </dl>
                    </div>
                    <div class="col-md-6 container-sections">
                        <dl>
                            <dt>Date Created</dt>
                            <dd><apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!application.createdDate}" /></apex:outputText></dd>
                            <dt>Status</dt>
                            <dd>{!application.Application_Status__c}</dd>
                            <apex:outputPanel rendered="{!!cancelCOI && !amendCOI}">
                                <dt>Source</dt>
                                <dd>{!application.Application_Source__c}</dd>
                            </apex:outputPanel>
                            <apex:outputPanel rendered="{!(application.Certificate__c != null)}">
                                <dt>Related Certificate</dt>
                                <dd>
                                    <apex:outputPanel rendered="{!if(application.Certificate__r.Status__c != 'Awaiting Payment' && application.Certificate__r.Status__c != 'Referred', true, false)}">
                                        <apex:outputLink value="{!URLFOR($Page.RequestAmendCOI,null,[id = application.Certificate__c])}" styleclass="outputlink"> {!application.Certificate__r.Policy_Number__c} </apex:outputLink>
                                    </apex:outputPanel>
                                    
                                    <apex:outputPanel rendered="{!if(application.Certificate__r.Status__c == 'Awaiting Payment' || application.Certificate__r.Status__c == 'Referred', true, false)}">
                                        <apex:outputLink value="{!URLFOR($Page.RequestAmendCOI,null,[id = application.Certificate__c])}" styleclass="outputlink"> {!application.Certificate__r.Name} </apex:outputLink>
                                    </apex:outputPanel>

                                </dd>
                                <dt>Related Certificate Address</dt>
                                <dd>
                                    {!application.Certificate__r.Property_Address__c}
                                </dd>
                            </apex:outputPanel>
                        </dl>
                    </div>
                </div>
                <apex:outputPanel rendered="{!if(application.Application_Source__c == application_source_DD_Initiated && !cancelCOI && !amendCOI, true, false)}">

                    <div class="row container-sections" >
                        <div class="row col-md-12">
                        <apex:outputLabel value="Builder Has Requested To Change:"  />
                            <dl>
                                <apex:outputPanel rendered="{!application.Category_Changes__c}" >
                                    <dt>Construction Categories</dt>
                                    <dd>Yes</dd>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!application.Limit_Changes__c}" >
                                    <dt>Limits</dt>
                                    <dd>Yes</dd>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!application.RBP_Changes__c}" >
                                    <dt>People</dt>
                                    <dd>Yes</dd>
                                </apex:outputPanel>
                            </dl>
                        </div>
                    </div>
                </apex:outputPanel>
                
                
                <!-- Display amendment reason -->
                <apex:outputPanel rendered="{!amendCOI}">
                    <div class="row container-sections" >
                        <div class="row col-md-12">
                        <apex:outputLabel value="Amendment Details:"  />
                            <dl>
                                <dt>Reason</dt>
                                <dd>
                                    <apex:outputPanel rendered="{!application.Project_Contract_Change__c}">
                                        {!$Label.Request_Project_Contract_Details_Change}
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!application.Property_Address_Change__c}">
                                        {!$Label.Request_Property_Address_Details_Change}
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!application.Owner_Details_Change__c}">
                                        {!$Label.Request_Owner_Details_Change}
                                    </apex:outputPanel>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </apex:outputPanel>
                
                <!-- Display cancellation reason -->
                <apex:outputPanel rendered="{!cancelCOI}">
                    <div class="row container-sections" >
                        <div class="row col-md-12">
                        <apex:outputLabel value="Cancellation Details:"  />
                            <dl>
                                <dt>Reason</dt>
                                <dd>
                                    <apex:outputPanel rendered="{!application.Contract_Terminated__c}">
                                        {!$Label.Contract_Terminated}
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!application.Construction_Did_Not_Proceed__c}">
                                        {!$Label.Construction_Did_not_Proceed}
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!application.Other_Reason__c}">
                                        {!$Label.Other}
                                    </apex:outputPanel>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </apex:outputPanel>
                
                <apex:outputPanel rendered="{!application.Application_Source__c == application_source_Adverse_Alert}">
                <html>
                    <div class="row container-sections" >
                        <div class="row col-md-12">
                        <apex:outputLabel value="Reason for Application:" />
                            <dl>
                                <dt>{!application.Adverse_Alert_Type__c}</dt>
                                <dt>{!application.Review_Reason__c}</dt>
                            </dl>
                        </div>
                    </div>
                    </html>
                </apex:outputPanel>

                <div class="row container-sections" >
                    <div class="row col-md-12">
                        <dl>
                            <dt>Current Comments:</dt>
                            <dd><apex:outputText escape="false" value="{!currentCommentsOutput}" /></dd>
                        </dl>
                    </div>
                </div>      

                <apex:outputPanel rendered="{!application.Final_Assessment_Summary__c != ''}">
                    <apex:outputLabel value="Assessment Summary:"  /> 
                    <apex:outputField style="margin-top:20px; margin-bottom:20px; font-size:20px; font-weight:800;" value="{!application.Final_Assessment_Summary__c}" label=""/> 
                    <br></br>
                </apex:outputPanel>
                
                <apex:outputPanel rendered="{!application.Application_Source__c != application_source_new}" >
                    <h2>Eligibility details as at:&nbsp;<apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!application.createdDate}" /></apex:outputText></h2>
                    <div class="row fsi-row-md-level fsi-row-lg-level">
                        <div class="col-md-6 container-sections">
                            <dl>
                                <dt>Builder Status</dt>
                                <dd>{!application.Builder_Status_at_Start_of_Application__c}</dd>
                                <dt>Requested Limit</dt>
                                <dd>{!application.Requested_Costruction_Limit__c}</dd>
                                <dt>Next Review Date</dt>
                                <dd><apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!application.Snapshot_Next_Review_Date__c}" /></apex:outputText></dd>
                            </dl>
                        </div>
                        <div class="col-md-6 container-sections">
                            <dl>
                                <dt>Construction Limit</dt>
                                <dd><apex:outputText value="{0, number, $ ###,###,###,###,###,###}"><apex:param value="{!application.Snapshot_Construction_Limit__c}" /></apex:outputText></dd>
                                <dt>Risk Score</dt>
                                <dd>{!application.Snapshot_Risk_Rating__c}</dd>
                                <dt>Eligibility Conditions</dt>
                                <dd>{!application.Snapshot_Eligibility_Conditions__c}</dd>
                            </dl>
                        </div>
                    </div>
                    <div class="row fsi-row-md-level fsi-row-lg-level container-sections">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                      <th>Category</th>
                                      <th>Limit</th>
                                      <th style="display:{!if(hasFlatRates, 'block', 'none')}">Flat Rate</th>
                                    </tr>
                                    <apex:repeat value="{!categoriesList}" var="c">
                                        <tr>
                                          <td>{!c.categoryName}</td>
                                          <td><apex:outputText value="{0, number, $ ###,###,###,###,###,###}"><apex:param value="{!c.categoryLimit}" /></apex:outputText></td>
                                          <td style="display:{!if(hasFlatRates, 'block', 'none')}"><apex:outputText value="{0, number, $ ###,###,###,###,###,###}"><apex:param value="{!c.categoryFlatRate}" /></apex:outputText></td>
                                        </tr>
                                    </apex:repeat>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                    <apex:outputPanel rendered="{!!cancelCOI && !amendCOI}">
                        <br/>
                        <h2>VBA Details as at:&nbsp;<apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!application.createdDate}" /></apex:outputText></h2>
                        <div class="row fsi-row-md-level fsi-row-lg-level">
                            <div class="col-md-6 container-sections">
                                <dl>
                                    <dt>VBA Registration Number</dt>
                                    <dd>{!application.Snapshot_VBA_Number__c}</dd>
                                    <dt>VBA Type</dt>
                                    <dd>{!application.Snapshot_VBA_Type__c}</dd>
                                    <dt>VBA Issued Date</dt>
                                    <dd><apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!application.Snapshot_VBA_Issue_Date__c}" /></apex:outputText></dd>
                                </dl>
                            </div>
                            <div class="col-md-6 container-sections">
                                <dl>
                                    <dt>VBA Anniversary Date</dt>
                                    <dd><apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!application.Snapshot_VBA_Anniversary_Date__c}" /></apex:outputText></dd>
                                    <dt>VBA Expiry Date</dt>
                                    <dd><apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!application.Snapshot_VBA_Expiry_Date__c}" /></apex:outputText></dd>
                                </dl>
                            </div>
                        </div>
                    </apex:outputPanel>
                    
                    <br/>

                    <h2>Entity Details as at:&nbsp;<apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!application.createdDate}" /></apex:outputText></h2>
                    <div class="row fsi-row-md-level fsi-row-lg-level">
                        <div class="col-md-6 container-sections">
                            <dl>
                                <dt>Builder Account Number</dt>
                                <dd>{!application.Snapshot_Builder_Account_Number__c}</dd>
                                <dt>Legal Entity Name</dt>
                                <dd>{!application.Snapshot_Legal_Entity_Name__c}</dd>
                                <dt>Legal Entity Type</dt>
                                <dd>{!application.Snapshot_Legal_Entity_Type__c}</dd>
                                <dt>Date Commenced</dt>
                                <dd>{!application.Snapshot_Date_Commenced_Trading__c}</dd>
                                <dt>ASIC Entity</dt>
                                <dd>{!application.Snapshot_ASIC_Entity__c}</dd>
                                <dt>ASIC Status</dt>
                          <dd>{!application.Snapshot_ASIC_Status__c}</dd>
                                <dt>Parent Entity</dt>
                                <dd>{!application.Snapshot_Parent_Entity__c}</dd>
                            </dl>
                        </div>
                        <div class="col-md-6 container-sections">
                            <dl>
                                <dt>ABN</dt>
                                <dd>{!application.Snapshot_ABN__c}</dd>
                                <dt>ACN</dt>
                                <dd>{!application.Snapshot_ACN__c}</dd>
                                <dt>DBDRV Ref Number</dt>
                                <dd>{!application.Snapshot_DBDRU_Ref__c}</dd>
                                <dt>DBDRV Complied ?</dt>
                                <dd>{!application.Snapshot_DBDRU_Complied__c}</dd>
                                <dt>DBDRV Date</dt>
                                <dd><apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!application.Snapshot_DBDRU_Date__c}" /></apex:outputText></dd>
                            </dl>
                        </div> 
                    </div>
                    
                    <br/>

                    <h2>Communication as at:&nbsp;<apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!application.createdDate}" /></apex:outputText></h2>
                    <div class="row fsi-row-md-level fsi-row-lg-level">
                        <div class="col-md-6 container-sections">
                            <dl>
                                <dt>Builder's Billing Address</dt>
                                <dd>{!billingAddress}</dd>
                                <dt>Business Email</dt>
                                <dd>{!application.Snapshot_Business_Email__c}</dd>
                            </dl>
                        </div>
                        <div class="col-md-6 container-sections">
                            <dl>
                                <dt>Business Phone</dt>
                                <dd>{!application.Snapshot_Business_Phone__c}</dd>
                                <dt>Business After Hours Phone</dt>
                                <dd>{!application.Snapshot_After_Hours_Phone__c}</dd>
                            </dl>
                        </div>
                    </div>
                </apex:outputPanel>

                <apex:outputPanel rendered="{!application.Application_Source__c == application_source_new}" >
                    <h2>Eligibility Details in Application</h2>
                    <div class="row fsi-row-md-level fsi-row-lg-level">
                        <div class="col-md-6 container-sections">
                            <dl>
                                <dt>Resulting Builder Status</dt>
                                <dd>{!application.Builder_Result_Status__c}</dd>
                                <dt>Requested Limit</dt>
                                <dd><apex:outputText value="{0, number, $ ###,###,###,###,###,###}"><apex:param value="{!application.Requested_Costruction_Limit__c}" /></apex:outputText></dd>
                                <dt>Builder Legal Entity Type</dt>
                                <dd>{!application.Builder_Legal_Entity_Type__c}</dd>
                                <dt>Date of Builder's Application</dt>
                                <dd><apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!application.Date_of_Builders_Application__c}" /></apex:outputText></dd>
                            </dl>
                        </div>
                        <div class="col-md-6 container-sections">
                            <dl>
                                <dt>Construction Limit</dt>
                                <dd><apex:outputText value="{0, number, $ ###,###,###,###,###,###}"><apex:param value="{!application.Resulting_Construction_Limit__c}" /></apex:outputText></dd>
                                <dt>Risk Score</dt>
                                <dd>{!application.Builder_Risk_Rating__c}</dd>
                                <dt>Eligibility Conditions</dt>
                                <dd>{!application.Eligibility_Conditions__c}</dd>
                                <dt>Other Eligibility Conditions</dt>
                                <dd>{!application.Other__c}</dd>
                            </dl>
                        </div>
                    </div>
                    <div class="row fsi-row-md-level fsi-row-lg-level container-sections">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                      <th>Category</th>
                                      <th>Limit</th>
                                      <th style="display:{!if(hasFlatRates, 'block', 'none')}">Flat Rate</th>
                                    </tr>
                                    <apex:repeat value="{!categoriesListNonSnapshot}" var="c">
                                        <tr>
                                          <td>{!c.categoryName}</td>
                                          <td><apex:outputText value="{0, number, $ ###,###,###,###,###,###}"><apex:param value="{!c.categoryLimit}" /></apex:outputText></td>
                                          <td style="display:{!if(hasFlatRates, 'block', 'none')}"><apex:outputText value="{0, number, $ ###,###,###,###,###,###}"><apex:param value="{!c.categoryFlatRate}" /></apex:outputText></td>
                                        </tr>
                                    </apex:repeat>
                                </table>
                            </div>
                        </div>
                    </div>
                    <br/>

                    <h2>Entity Details</h2>
                    <div class="row fsi-row-md-level fsi-row-lg-level">
                        <div class="col-md-6 container-sections">
                            <dl>
                                <dt>Builder Account Number</dt>
                                <dd>{!application.Builder_Number__r.Account_Number__c}</dd>
                                <dt>Legal Entity Name</dt>
                                <dd>{!application.Hidden_Builder_Name__c}</dd>
                            </dl>
                        </div>
                        <div class="col-md-6 container-sections">
                            <dl>
                                <dt>ABN</dt>
                                <dd>{!application.Builder_ABN__c}</dd>
                                <dt>ACN</dt>
                                <dd>{!application.Builder_ACN__c}</dd>
                            </dl>
                        </div> 
                    </div>
                    
                    <br/>

                </apex:outputPanel>
                
                <br/>
                <apex:outputPanel rendered="{!relatedContacts.size > 0}" > 
                    <h2>Related Contact Details</h2>
                    <div class="row fsi-row-md-level fsi-row-lg-level container-sections">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Date of Birth</th>
                                    <th>RBP VBA Number</th>
                                    <th>Position</th>
                                    <th>Date Commenced</th>
                                </tr>
                                <!-- 
                                <apex:repeat value="{!rbpContactWithRoles}" var="rbp">
                                    <tr>
                                        <td>{!rbp.con.firstName}</td>
                                        <td>{!rbp.con.lastName}</td>
                                        <td>{!rbp.con.mailingStreet + rbp.con.mailingCity + '' + rbp.con.mailingState + '' + rbp.con.mailingPostalCode + '' + rbp.con.mailingCountry}</td>
                                        <td>{!rbp.con.email}</td>
                                        <td>{!rbp.con.phone}</td>
                                        <td>{!rbp.con.Birthdate}</td>
                                        <td>{!rbp.con.RBP_VBA_Number__c}</td>
                                        <td>{!rbp.conRole}</td>
                                        <td>{!rbp.con.Date_Commenced_with_Company__c}</td>
                                    </tr>
                                </apex:repeat>
                                -->
                                <apex:repeat value="{!relatedContacts}" var="rbp">
                                    <tr>
                                        <td>{!rbp.Contact__r.firstName}</td>
                                        <td>{!rbp.Contact__r.lastName}</td>
                                        <td>{!rbp.Contact__r.mailingStreet + ' ' + rbp.Contact__r.mailingCity + ' ' + rbp.Contact__r.mailingState + ' ' + rbp.Contact__r.mailingPostalCode + ' ' + rbp.Contact__r.mailingCountry}</td>
                                        <td>{!rbp.Contact__r.email}</td>
                                        <td>{!rbp.Contact__r.phone}</td>
                                        <td><apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!rbp.Contact__r.Birthdate}" /></apex:outputText></td>
                                        <td>{!rbp.Contact__r.RBP_VBA_Number__c}</td>
                                        <td>{!rbp.Role__c}</td>
                                        <td><apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!rbp.Contact__r.Date_Commenced_with_Company__c}" /></apex:outputText></td>
                                    </tr>
                                </apex:repeat>
                            </table>
                        </div>
                    </div>
                </apex:outputPanel>
                <br/>
                <h2>Supporting Documents</h2>
                <div class="row container-sections">

                    
                    <div class="row col-md-12">
                        <label>Attachments</label>
                         <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>File</th>
                                    <th>Type</th>
                                    <th>Size</th>
                                    <th>Uploaded On</th>
                                    <th>Uploaded By</th>
                                </tr>
                                <apex:repeat value="{!attachments}" var="att">
                                    <tr>    
                                        <td><apex:outputLink value="{!URLFOR($Action.Attachment.Download, att.id)}" target="_blank">{!att.name}</apex:outputLink></td>
                                        <td>{!att.ContentType}</td>
                                        <td>
                                        	<!-- Show file size in KB -->
                                        	<apex:outputPanel rendered="{!if(att.BodyLength / (1024*1024) < 1, true, false)}">
                                        		<apex:outputText value="{0, number,##.## KB}">
                                        			<apex:param value="{!att.BodyLength / 1024}"></apex:param>
                                        		</apex:outputText>
                                        	</apex:outputPanel>
                                        	
                                        	<!-- Show file size in MB -->
                                        	<apex:outputPanel rendered="{!if(att.BodyLength / (1024*1024) >= 1, true, false)}">
                                        		<apex:outputText value="{0, number,##.## MB}">
                                        			<apex:param value="{!att.BodyLength / (1024 * 1024)}"></apex:param>
                                        		</apex:outputText>
                                        	</apex:outputPanel>
                                        </td>
                                        <td><apex:outputText value="{0,date,dd'/'MM'/'yyyy}"><apex:param value="{!att.CreatedDate}" /></apex:outputText></td>
                                        <td>{!att.CreatedBy.Name}</td> 
                                    </tr>
                                </apex:repeat>
                            </table>
                         </div>
                    </div>
                </div>
                <apex:outputPanel rendered="{!IF((application.Application_Current_Activity__c == application_Current_Activity_Final_Declinature)||(application.Application_Status__c == application_Status_Builder_changed) || (application.Application_Status__c == application_Status_Builder_Unchanged) || (application.Application_Status__c == application_Status_Withdrawn), False, True)}" >
                    <br/>
                    <h2>Additional Information</h2>
                    <div class="row container-sections" >
                        <div class="form-group">
                            <label>Comments</label>                  
                            <apex:inputTextarea value="{!comments}" id="commentsSection1" richText="false" styleClass="form-control"  />
                        </div>
                        <br/>
                        <div class="row col-md-12">
                        
                            <label>Select files to attach</label>&nbsp;<span class="glyphicon glyphicon-file"></span>
                            <c:FileUpload parentId="{!application.id}" /> 
                            <br/>
                        </div>
                    </div>
                </apex:outputPanel>




                <br/>
                <apex:inputHidden value="{!hasNoDocuments}" id="hdnField"/>
                <div class="row container-sections container-buttons" align="center">

                    <div class="row errorMessage">
                                <apex:outputPanel id="pMessage">
                                <center>
                                    <apex:pageMessages id="errorMessages" />
                                </center>
                                </apex:outputPanel>
                            </div>

                    <apex:commandButton styleClass="btn btn-primary pagebtn" action="{!cancel}" value=" Back " immediate="true" html-formnovalidate="formnovalidate"/>
                    <apex:commandButton styleClass="btn btn-primary pagebtn" id="uploadButton" onclick="beginSave();"  reRender="commentsSection1" value=" Save Updates " rendered="{!IF((application.Application_Current_Activity__c == 'Final Declinature') || (application.Application_Status__c == 'Builder Change') || (application.Application_Status__c == 'Builder Unchanged') || (application.Application_Status__c == application_Status_Withdrawn), False, True)}"/>
                       
                    <apex:actionFunction action="{!confirmation}" name="confirm"/>
                    <apex:actionFunction action="{!save}" name="save" oncomplete="if({!!errorsFound}){beginUploadProcess('{!application.id}');}"  reRender="pMessage" /> 

                    <script>
                        function beginSave(){
                            var x = document.getElementById("fileTable").rows.length;
                            if(x<2){
                                document.getElementById("{!$Component.hdnField}").value = "True";
                            }
                            else{
                                document.getElementById("{!$Component.hdnField}").value = "False";
                            }
                            save();
                        };
                       function beginUploadProcess(appId){
                            //$('div#divLoading').addClass('show');
                            var isSuccess = uploadFiles(appId);
                            if(isSuccess != false){
                                confirm();
                            }
                       };
                    </script>

                </div>
            </apex:form>
        </div>
        <footer class="footer">
            <c:IncludeFooter />
        </footer>
    </body>

</apex:page>