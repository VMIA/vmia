<!-- 
        Date        : 07-Mar-2017
        Author      : SMS Management & Technology
        Description : List of Tasks with a paginated view
 -->
<apex:page controller="SearchTaskController" doctype="html-5.0" showheader="false" sidebar="false" docType="html-5.0" title="VMIA | Accounts" applyHtmlTag="false" applyBodyTag="false" id="page" standardStylesheets="false"> 
    <head>
        <!-- Include style sheets and scripts -->
        <c:IncludeAssets id="assetComponent"/>
        <apex:stylesheet value="{!URLFOR($Resource.TaskSearchStyle)}"/>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
    </head>
    
    <body>
        <div class="container">
            <!-- Branding Header  -->
            <header>
                <c:IncludeHeader id="headerComponent"/>
            </header>
            
            <!-- Navigation Bar -->
            
            <c:IncludeNavBar />
            <!-- Navigation Bar Ends -->
            
            <!-- Search Panel -->
            <apex:form id="form">
                <div class="row search-criteria">
                    <div class="row search-header">
                        <div class="pull-left">
                            <h2>{!$Label.Task_Search}</h2>
                        </div>
                        <div class="pull-right">
                            <apex:image url="{!URLfor($Resource.VMIAResources,'VMIAResources/images/Info_Icon.png')}" alt="Info" id="vmiaRes" title="Search will return results on full and partial matches"/>
                        </div>
                    </div>
                    <!-- Task Search Fields Search -->
                    <div class="row">
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="appStatus">{!$ObjectType.Task.Fields.Builder__c.Label}</label>
                                <apex:inputText value="{!builderName}" id="tskBuilder" styleClass="form-control"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="appStatus">{!$ObjectType.Task.Fields.ActivityDate.Label} from</label>
                                <apex:inputText value="{!activityDateStart}" id="tskActivityDateStart" styleClass="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>{!$ObjectType.Task.Fields.ActivityDate.Label} to </label>
                                <apex:inputText value="{!activityDateEnd}" id="tskActivityDateEnd" styleClass="form-control"/>
                            </div>
                            
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="appStatus">{!$ObjectType.Task.Fields.CreatedDate.Label} from</label>
                                <apex:inputText value="{!createdDateStart}" id="tskCreatedDateStart" styleClass="form-control" />
                            </div>
                            <div class="form-group">
                                <label>{!$ObjectType.Task.Fields.CreatedDate.Label} to </label>
                                <apex:inputText value="{!createdDateEnd}" id="tskCreatedDateEnd" styleClass="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="appStatus">{!$ObjectType.Task.Fields.Subject.Label}</label>
                                <apex:selectList value="{!tskSubject}" size="5" id="tskSubj" styleClass="selectpicker form-control" multiselect="true">
                                    <apex:selectOptions value="{!TaskSubject}" id="subjOptions"/>
                                </apex:selectList>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="appStatus">{!$ObjectType.Task.Fields.Status.Label}</label>
                                <apex:selectList value="{!tskStatus}" size="5" id="tskStatus" styleClass="selectpicker form-control" multiselect="true">
                                    <apex:selectOptions value="{!TaskStatus}" id="statOptions"/>
                                </apex:selectList>
                            </div>
                        </div>
                    </div>
                    <!-- Task Search Fields Ends -->
                    <div class="row search-criteria">
                        <div class="pull-right">
                            <apex:commandButton value="{!$Label.Btn_Clear}" styleClass="btn btn-primary pagebtn" id="clearBtn" rerender="thetable"/> 
                            <apex:commandButton action="{!search}" value="{!$Label.Btn_Search}" styleClass="btn btn-primary pagebtn" rerender="thetable" id="searchBtn" status="loadStatus"/>
                        </div>
                    </div>
                    <apex:actionStatus id="loadStatus">
                        <apex:facet name="start">
                            <apex:outputPanel id="loadPanel">
                                <img src="/img/loading32.gif" width="25" height="25" id="loadImg"/>
                                <apex:outputLabel value="{!$Label.Status_Loading}" id="loadLabel"/>
                            </apex:outputPanel>            
                        </apex:facet>
                    </apex:actionStatus>
                </div>
                <br/>
                
                <!-- Search Results Panel -->
                <apex:actionRegion >
                <apex:outputPanel id="thetable">
                    <apex:pageMessages id="pageMessages"/>
                    <apex:pageMessage id="noTaskMsg" severity="Info" title="No Tasks found." strength="2" rendered="{!taskList.size == 0}"/>
                    <apex:outputPanel id="subPanel" rendered="{!taskList.size > 0}">
                        <div class="tableNames">
                            <h2 >List of Tasks</h2>
                            <div class="search-pagenumbers">
                                {!$Label.Header_Results_Page} {!currentPage} |   {!$Label.Header_Total_Pages} {!pageNumber}  | &nbsp;&nbsp;
                                <apex:outputPanel id="exportPanel" style="float:right;">
                                    <apex:commandLink action="{!export}" value=" Export" id="tskExport" rendered="{!taskList.size > 0}" rerender="thetable"/>&nbsp;  | &nbsp;
                                    <apex:commandLink action="{!exportAll}" value="Export All" id="tskExportAll" rendered="{!taskList.size > 0}" rerender="thetable"/>
                                </apex:outputPanel>
                            </div>  
                        </div>
                       
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <apex:repeat value="{!$ObjectType.Task.FieldSets.Task_Search_Table}" var="field" id="displayFieldSet">
                                        <th>
                                            <!--{!field.Label}-->
                                            <apex:commandLink action="{!sortValues}"  value="{!field.Label} {!IF(sortExpression == field.Label,IF(sortDirection='ASC',' ▼',' ▲'),'')}"   rerender="thetable">
                                                        <apex:param name="{!field.Label}" value="{!field.fieldPath}" assignTo="{!sortExpression}" ></apex:param>
                                            </apex:commandLink>
                                        </th>
                                    </apex:repeat>
                                </tr>
                                <apex:repeat var="tsk" value="{!taskList}" id="tskListRepeat">
                                    <tr>
                                        <apex:repeat value="{!$ObjectType.Task.FieldSets.Task_Search_Table}" var="field" id="tskFieldSet">
                                            <td> 
                                                <apex:outputPanel id="fldValPanel">
                                                    <apex:outputLink value="CustomTaskView?id={!tsk.id}" id="tskNo" styleclass="outputlink" rendered="{!(field == 'Activity_Number__c')}"> {!tsk.Activity_Number__c} </apex:outputLink>
                                                    <apex:outputPanel rendered="{!tsk.Builder__c != null && distId != tsk.Builder__c}">
                                                        <apex:outputLink value="BuilderLandingPage?id={!tsk.Builder__c}" id="tskBuilder" styleclass="outputlink" rendered="{!(field == 'Builder__c')}"> {!tsk.Builder__r.Name} </apex:outputLink>
                                                    </apex:outputPanel>
                                                    <apex:outputPanel rendered="{!tsk.Builder__c == null || distId == tsk.Builder__c}">
                                                        <apex:outputText rendered="{!(field == 'Builder__c')}">    Owner Builder </apex:outputText>
                                                    </apex:outputPanel>
                                                    <apex:outputText value="{!tsk[field]}" id="tskFldVal" rendered="{!(field.Type != 'datetime' && field.Type != 'date' && field != 'Activity_Number__c' && field != 'Builder__c')}"/>
                                                    <apex:outputText value="{0,date,dd'/'MM'/'yyyy}" rendered="{!field.Type == 'datetime' || field.Type == 'date'}" id="tskDateFormat">
                                                        <apex:param value="{!tsk[field]}" id="tskParam"/>
                                                    </apex:outputText>
                                                </apex:outputPanel>
                                            </td>
                                        </apex:repeat>
                                    </tr>
                                </apex:repeat>
                            </table>
                        </div>                       
                        
                        <apex:outputPanel id="paginationPanel">
                            <apex:outputText style="float:left;">{!$Label.Header_Results_Page} {!currentPage} |   {!$Label.Header_Total_Pages} {!pageNumber}  | &nbsp;&nbsp;</apex:outputText>
                            <apex:outputPanel id="paginationBtnPanel">
                                <center>
                                    <apex:panelGrid columns="4" id="linkGrid">
                                        <apex:commandLink rerender="thetable" action="{!first}" id="firstLink" rendered="{!!hasPrevious}">First</apex:commandLink>
                                        <apex:commandLink rerender="thetable" action="{!previous}" id="previousLink" rendered="{!!hasPrevious}">Previous</apex:commandLink>
                                        <apex:commandLink rerender="thetable" action="{!next}" id="nextLink" rendered="{!hasNext}">Next</apex:commandLink>
                                        <apex:commandLink rerender="thetable" action="{!last}" id="lastLink" rendered="{!hasNext}">Last</apex:commandLink>
                                    </apex:panelGrid>
                                </center>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        
                    </apex:outputPanel>
                </apex:outputPanel>
                </apex:actionRegion>
                <!-- Search Results Panel Ends -->
            </apex:form>
            <!-- Search Panel Ends -->
            <br/>
            
            <!-- Footer -->
            <footer>
                <c:IncludeFooter id="footerComponent"/>
            </footer>
        </div>
    </body>
    
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            // clear the input values on "Clear" button click
            $('input[id$=clearBtn]').on('click',function(){
                $("[id$='tskSubj']").val('');
                $("[id$='tskBuilder']").val('');
                $("[id$='tskStatus']").val('');
                $("[id$='tskCreatedDateStart']").val('');
                $("[id$='tskCreatedDateEnd']").val('');
                $("[id$='tskActivityDateStart']").val('');
                $("[id$='tskActivityDateEnd']").val('');
            });
            
            // set date picker for created date & activity date fields
            $('input[id$=tskActivityDateStart]').datepicker({dateFormat: "dd/mm/yy"});
            $('input[id$=tskActivityDateEnd]').datepicker({dateFormat: "dd/mm/yy"});
            $('input[id$=tskCreatedDateStart]').datepicker({dateFormat: "dd/mm/yy"});
            $('input[id$=tskCreatedDateEnd]').datepicker({dateFormat: "dd/mm/yy"});
            
        });
    </script>
</apex:page>