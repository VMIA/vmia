trigger AccountTrigger on Account (before insert,before update,before delete,
										after insert,after update,after delete,after undelete) {

    if(!TriggerHelper.isTriggerDisabled(String.valueOf(Account.sObjectType))){     // verify if triggers are disabled
        AccountTriggerHandler.execute();  // Account trigger handler dispatches appropriate event
    }
}