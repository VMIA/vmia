trigger BatLetterTrigger on BAT_Letter__c (before insert,before update,before delete,
                                                after insert,after update,after delete,after undelete) {
	
	if(!TriggerHelper.isTriggerDisabled(String.valueOf(BAT_Letter__c.sObjectType))){     // verify if triggers are disabled
        BatLettersTriggerHandler.execute();  // BATLetter trigger handler dispatches appropriate event
    }
}