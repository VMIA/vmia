/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Trigger on Certificate Object to call the Handler class to perform necessary action
  */  
trigger CertificateTrigger on Certificate__c(before insert,before update,before delete,
                                                after insert,after update,after delete,after undelete){    
    if(!TriggerHelper.isTriggerDisabled(String.valueOf(Certificate__c.sObjectType))){     // verify if triggers are disabled
        CertificateTriggerHandler.execute();  // Certificate trigger handler dispatches appropriate event
    }
}