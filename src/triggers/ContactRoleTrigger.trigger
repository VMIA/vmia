/**
  * Date         :  23-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Trigger on Contact RoleObject to call the Handler class to perform necessary action
  */  
trigger ContactRoleTrigger on Contact_Role__c(before insert,before update,before delete,
                                                after insert,after update,after delete,after undelete){    
    if(!TriggerHelper.isTriggerDisabled(String.valueOf(Contact_Role__c.sObjectType))){     // verify if triggers are disabled
        ContactRoleTriggerHandler.execute();  // Certificate trigger handler dispatches appropriate event
    }
}