/**
  * Date         :  07-Jun-2017
  * Author       :  SMS Management & Technology
  * Description  :  Trigger on Property Object to call the Handler class to perform necessary action
  */  
trigger PropertyOwnerTrigger on Property_Owners__c (after insert){    
    if(!TriggerHelper.isTriggerDisabled(String.valueOf(Property_Owners__c.sObjectType))){     // verify if triggers are disabled
        PropertyOwnerTriggerHandler.execute();  // PropertyOwner trigger handler dispatches appropriate event
    }
}