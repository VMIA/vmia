/**
  * Date         :  30-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Trigger on User Object to call the Handler class to perform necessary action
  */  
trigger UserTrigger on User(before insert,before update,before delete,
                                after insert,after update,after delete,after undelete){    
    if(!TriggerHelper.isTriggerDisabled(String.valueOf(User.sObjectType))){     // verify if triggers are disabled
       UserTriggerHandler.execute();  // User trigger handler dispatches appropriate event
    }
}