/**
  * Date         :  20-Mar-2017
  * Author       :  SMS Management & Technology
  * Description  :  Trigger on Task Object to call the Handler class to perform necessary action
  */  
trigger TaskTrigger on Task(before insert,before update,before delete,
                                after insert,after update,after delete,after undelete){    
    if(!TriggerHelper.isTriggerDisabled(String.valueOf(Task.sObjectType))){     // verify if triggers are disabled
        TaskTriggerHandler.execute();  // Task trigger handler dispatches appropriate event
    }
}