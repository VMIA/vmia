<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Review_is_Due</fullName>
        <field>Scheduled_Review_Due__c</field>
        <literalValue>1</literalValue>
        <name>Review is Due</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto Review Eligibility</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Eligibility_Status__c</field>
            <operation>equals</operation>
            <value>Active,Active - Restricted</value>
        </criteriaItems>
        <description>Business has currently abandoned the business rule for auto reviewing eligibility. Hence, deactivated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Review_is_Due</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Eligibility_Review_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
