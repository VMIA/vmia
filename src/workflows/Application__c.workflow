<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Assign_to_BAT_Manager</fullName>
        <field>OwnerId</field>
        <lookupValue>BAT_Manager_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to BAT Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Awaiting_Approval</fullName>
        <field>BAT_Manager_Approval_Status__c</field>
        <literalValue>Awaiting Approval</literalValue>
        <name>Awaiting Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Hidden_Declinature_Overdue_Checkbox</fullName>
        <field>Hidden_Declinature_Overdue__c</field>
        <literalValue>1</literalValue>
        <name>Set Hidden Declinature Overdue Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Hidden_Declinature_True</fullName>
        <field>Hidden_Declinature_Overdue__c</field>
        <literalValue>1</literalValue>
        <name>Set Hidden Declinature to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Hidden_Declinature_to_True</fullName>
        <field>Hidden_Declinature_Overdue__c</field>
        <literalValue>1</literalValue>
        <name>Set Hidden Declinature to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Application_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>BAT_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Application Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Pending Declinature Timer</fullName>
        <active>true</active>
        <description>A time based workflow that checks if the 28 days has expired to set the builder to Declined or Cancelled</description>
        <formula>ISPICKVAL(Application_Current_Activity__c, &apos;Pending Declinature&apos;) || ISPICKVAL(Application_Current_Activity__c, &apos;Suspended&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Hidden_Declinature_True</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Application__c.Final_Declinature_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Application_Requires_VBA_Registration_Number</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Application Requires VBA Registration Number</subject>
    </tasks>
</Workflow>
