<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BuildVic_Account_Activation_Notification</fullName>
        <description>BuildVic Account Activation Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>dbi.noreply@buildvic.vic.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/User_Account_Activated</template>
    </alerts>
    <rules>
        <fullName>User Account Activated</fullName>
        <actions>
            <name>BuildVic_Account_Activation_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Terms_and_Condition__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Terms_and_Condition_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Rule to send email to builder on accepting terms &amp; conditions</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
